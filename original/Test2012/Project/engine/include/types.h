#ifndef _TYPES_H_
#define _TYPES_H_

#include "BuildSettings.h"
#include "platform.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <stack>
#include <string>
#include <algorithm>
#include <stdexcept>
#include <cstdio>
#include <cmath>
#include <limits>

extern "C" {
	#include <png.h>

	#include <jpeglib.h>
	#include <jerror.h>

	#include <lua.h>
	#include <lauxlib.h>
	#include <lualib.h>
}

#undef min
#undef max

#include <boost/smart_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/cast.hpp>

// на MacOS отключаем системный макрос check, иначе будет конфликт с luabind
#if defined(ENGINE_TARGET_MACOS)
#if defined(check)
#undef check
#endif // check
#endif

#if defined(ENGINE_TARGET_IPHONE) || defined(ENGINE_TARGET_MACOS)
#undef nil
#define nil _nil
#endif

#include <luabind/luabind.hpp>
#include <luabind/operator.hpp>
#include <luabind/adopt_policy.hpp>

#if defined(ENGINE_TARGET_IPHONE) || defined(ENGINE_TARGET_MACOS)
#undef nil
#define nil __null
#endif

#include "../tinyxml/tinyxml.h"
#include "../rapidxml/rapidxml.hpp"
#include "../rapidxml/rapidxml_utils.hpp"
#include "../rapidxml/rapidxml_print.hpp"

#include "EngineAssert.h"
#include "Utils/utils.hpp"
#include "Utils/IPoint.h"
#include "Utils/IRect.h"
#include "Utils/FRect.h"
#include "Utils/FPoint.h"
#include "Utils/Color.h"
#include "Utils/Vector3.h"
#ifndef ENGINE_TARGET_LINUX
#include "Utils/half.hpp"
#endif
#include "Core/Log.h"

typedef void (*Callback)(void *);

// на MacOSX инклудим файл с виртуальными кодами клавиш
#ifdef ENGINE_TARGET_MACOS
#include <signal.h>
#include <sys/stat.h>
#include <mach/mach_time.h>
#include <unistd.h>
#include <glob.h>
#include <OpenGL/OpenGL.h>
#include <OpenGL/glu.h>
#include <AGL/agl.h>
#include "Core/KeyCodes_mac.h"
#endif //ENGINE_TARGET_MACOS

#ifdef ENGINE_TARGET_LINUX
#   include <sys/stat.h>
#   include <sys/types.h>
#   include <errno.h>
#endif

#ifdef ENGINE_TARGET_ANDROID
#include <jni.h>
#include <android/log.h>
#include <GLES/gl.h>
#include <GLES/glext.h>
#endif // ENGINE_TARGET_ANDROID

#if defined(ENGINE_TARGET_IPHONE)
#include <sys/stat.h>
#endif // ENGINE_TARGET_IPHONE

#if defined(ENGINE_TARGET_IPHONE) || defined(ENGINE_TARGET_MACOS)
#	include <OpenAL/al.h>
#	include <OpenAL/alc.h>
#else
#	include <al.h>
#	include <alc.h>
#endif

#ifdef ENGINE_TARGET_IPHONE
#define EngineFloat half_float::half
#else
#define EngineFloat float
#endif

// Disable some annoying warnings
//
#if defined(_MSC_VER)
// warning C4244: conversion from 'float' to 'int', possible loss of data
// warning C4800: forcing value to bool 'true' or 'false' (performance warning)
// warning C4305: truncation from 'double' to 'float'
#pragma warning(disable: 4244 4305 4800)
#elif defined(__clang__) || defined(__llvm__)
#pragma clang diagnostic ignored "-Wdouble-promotion"
#pragma clang diagnostic ignored "-Wfloat-conversion"
#pragma clang diagnostic ignored "-Wunused"
#pragma clang diagnostic ignored "-Wshadow"
#pragma clang diagnostic ignored "-Wparentheses"
#else
#pragma GCC diagnostic ignored "-Wdouble-promotion"
//#pragma GCC diagnostic ignored "-Wfloat-conversion"
#pragma GCC diagnostic ignored "-Wunused"
#pragma GCC diagnostic ignored "-Wshadow"
#pragma GCC diagnostic ignored "-Wparentheses"
#endif

#endif //_TYPES_H_
