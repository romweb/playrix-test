#ifndef __RENDERTARGET_H__
#define __RENDERTARGET_H__

#pragma once

#if defined(ENGINE_TARGET_WIN32_DX9) && !defined(USING_GLES2)
#include <d3d9.h>
#elif defined(USING_GLES2)
#include "GLES2/gl2.h"
#endif
#include "Render/Texture.h"
#include "Render/RenderTypes.h"
#include "Utils/IRect.h"

namespace Render {

	enum CompareOp
	{
		CMP_EQ,
		CMP_NE,
		CMP_LT,
		CMP_LE,
		CMP_GT,
		CMP_GE,
		CMP_FALSE,
		CMP_TRUE
	};
//
// ������, ������� ���� ������� ��������,
// � ������� ����� ���-�� ��������.
//
// � public �������� ������ �� ������, ������� ����� ������� ������������
// ��� ���������, � ��� ����� � �����������, � private.
// ����� �������, ��������� ������� � ����� ������ ������ � ��� ����� ������
// ��������� ������ Render::RenderDeviceImpl.
//
// �����������:
// 1) ��� ��� ������ ������ ������� Render::RenderDeviceImpl::CreateRenderTarget,
//    ������ ����� ������� ��� ������� Render::RenderDeviceImpl::DeleteRenderTarget;
// 2) ��� ���� ��������� � Render::Target ����� ���������, ������� �� ��
//    �������� IsValid(); ������ ������ ���������� ����� ��������� ����������;
// 3) ��������� � �������� ����� ������ � Update, �� �� � Draw.
//
// ���, ��������� RenderTarget ������ ��������� �������� ���:
// 
// RenderTarget* _renderTarget;
// ...
// if (_renderTarget != NULL && !_renderTarget->IsValid()) {
//    Render::device.DeleteRenderTarget(_renderTarget);
//	  _renderTarget = NULL;
// }
// if (_renderTarget == NULL) {
//    _renderTarget = Render::device.CreateRenderTarget(_texture->Width(), _texture->Height());
//    Render::device.BeginRenderTo(_renderTarget);
//    ... // ����� ������ ���-������
//    Render::device.EndRenderTo();
// }
//
// �� ���� �� _�������_ ������� ������ ����� ����, ��� �� ��� �� �����.
// �������� ��������, ��� ���� ��� ������ ���� � Update, �� �� � Draw.
//
class Target
{
public:
	virtual ~Target();

	bool needTranslate() const;
	
	void TranslateUV(FPoint& uv) const;
	void TranslateUV(FRect& rect, FRect& uv) const;
	void TranslateUV(math::Vector3& v0, math::Vector3& v1, math::Vector3& v2, math::Vector3& v3, FRect &uv);

	/// ���������� ������ �������
	int Width() const { return static_cast<int>(_width); }
	/// ���������� ������ �������
	int Height() const { return static_cast<int>(_height); }

	/// ���������� �������� ����� �������
	IRect getBitmapRect() const { return IRect(IPoint(0, 0), _width, _height); }

	//
	// ��������� ������� ���������
	//
	void Bind(int cannel = 0, int stageOp = 0);

	//
	// ��������� ������� ��������� ��� �����-���������
	//
	void BindAlpha();

	//
	// ��������� ��������
	//
	void Draw(const FPoint& position);

	//
	// ������� �� ������
	//
	bool IsValid();

	//
	// ���������� �� ������ ��������������
	//
	bool IsMultisampled();

	/// ������ �� ������.
	/// ������ ������ ���� �������� ������, �� ��� ���������� ���� ������� � ���������� ������ ����������
	bool Empty();

	//
	// ��������� � ������� BMP
	// �����������: �������������� �������� �� ���������
	//
	void SaveAsBmp(const std::string & path);

	//
	// ��������� � ������� PNG
	// �����������: �������������� �������� �� ���������
	//
	void SaveAsPng(const std::string & path);
	
	//
	// ��������� � ������� JPEG
	//
	void SaveAsJpeg(const std::string & path, int quality = 80);

	//
	// �������� ���������� ������� � ������������ �� ������������� (� ���� ������ ���������� false)
	// ������� ��������� ����� ������ ��������� ��� �������� (��. �.2 � ������������ ����)
	// 
	static bool EnsureValidity(Target** t);

	//
	// ��������� ���������� �������
	//
	void setFilterType(Texture::FilteringType filter);

	//
	// ��������� ��������� ���������� ��������� �������
	//
	void setAddressType(Texture::AddressType address);

#if (defined(ENGINE_TARGET_WIN32_DX9) || defined(ENGINE_TARGET_MACOS)) && !defined(USING_GLES2)
	//
	// ��������� ���������� � �����
	//
	bool CopyToBuffer(unsigned char* buffer);
#endif

private:
	friend class RenderDeviceImpl;
	friend class RenderDeviceGLES1;
	friend class RenderDeviceGLES2;
	friend class RenderDeviceGL;
		// ����� ������ ��� ����� ���� ��������� ������� ������ ����

	DEVTEXTURE _texture;	
		// ��������

#if defined(ENGINE_TARGET_WIN32_DX9) && !defined(USING_GLES2)
	IDirect3DSurface9* _renderTargetSurface;
		// �����������
	
	IDirect3DSurface9* _depthStencilSuface;
		// Z-�����
#else // GLES
    DEVTEXTURE _depthBuffer;
    DEVTEXTURE _msRB;
    DEVTEXTURE _msDB;
    DEVTEXTURE _msFB;
#endif
	
	bool _valid;
		// ������� �� ���� ���������� (���������� ����������, ���� ���������� ����������������������)
	/// ����� �� ���� ����������
	bool _empty;

	bool _isBinded;
		// ��������� �� ���� ����������

	int _width;
		// ������ ��������

	int _height;
		// ������ ��������
	
	int _widthPow2;
		// ������ ��������, ���������� �� ��������� ������� ������� ������
	
	int _heightPow2;
		// ������ ��������, ���������� �� ��������� ������� ������� ������

	bool _alpha;
		// ����� �� �����-����� ����?

	bool _depth;
		// ������������ �� depth buffer

	Texture::FilteringType _filter;
		// ��� ����������

	Texture::AddressType _address;
		// ��� ��������� ���������� ���������

	bool _filterDirty;

	bool _addressDirty;
	
	bool _multisampled;
		// ������������ �� �����������

	MultisampleType _msType;
		// ��� ���������������

	//
	// ����������� ����� ��� ���� ����� RenderDeviceImpl
	//
	Target(int width, int height);

	//
	// �������� �������, �������� ������� ������
	//
	void ReleaseResources();

	//
	// �������� ��� ������� � ������ ���� ����������.
	//
	void SetInvalid();
};

} // namespace Render

#endif // __RENDERTARGET_H__
