#ifndef _RENDERDEVICE_GL_H_
#define _RENDERDEVICE_GL_H_

#include <stack>
#include <vector>
#include <set>
#include "EngineAssert.h"
#include "Utils/FPoint.h"
#include "Render/Texture.h"
#include "ClippingMode.h"
#include "Render/RenderDeviceInterface.h"
#include "Render/VertexBuffer.h"
#include "Render/Target.h"
#include "SceneGraph/DeviceBuffer.h"
#include "SceneGraph/Material.h"
#include "SceneGraph/Light.h"

#if defined(ENGINE_TARGET_MACOS)
#include <OpenGL/OpenGL.h>
#endif

namespace Core
{
	class Window;
}

class VertexBuffer;
class TextureTextPrinter;

namespace Render
{

class Texture;
class ShaderProgram;
class ShaderProgramGL;
class VolumeTexture;

class RenderDeviceGL
	: public RenderDeviceInterface
{

protected:

	bool _windowBinded;
	DeviceMode _currentDeviceMode;

	MatrixMode _currentMatrixMode;
	DEVCOLOR _currentColor;
	BlendMode _currentBlendMode;

	Texture* bindedTexture; ///< Последняя забинденная текстура

	int curTexChannel; ///< Выбранный текстурный канал
	
	bool useFog;
	float fogStart, fogEnd;
	Color fogColor;

	bool tex_matrix_set; ///< Отличается ли текстурная матрица от единичной.

	bool areUniformsUpToDate;

	ShaderProgramGL* _specShader; ///< Шейдер, выбранный вручную.
	ShaderProgramGL* _currentShader; ///< Текущий забинденный шейдер.

	Target* _currentRenderTarget; // Активный таргет

	enum DrawMode {
		MODE_STANDARD,
		MODE_ADD_COLOR,
		MODE_ONLY_ALPHA,
		MODE_NO_ALPHA,
		MODE_NO_TEXTURE,
		MODE_A_REPLACE,
		MODE_A_MULTIPLY
	};

	///< Используется для выбора шейдера.
	DrawMode _drawMode; 

	/// текущие размеры окна
	int _current_width, _current_height;

	/// размеры приложения
	int _appWidth, _appHeight;

	typedef std::set<Texture*> DynamicTexturesSet;

	/// список динамических текстур
	DynamicTexturesSet _dynamicTextures;

	/// Количество текущих плоскостей отсечения
	int _nClipPlanes;
	std::stack<int> _clipPlanesStack;
	/// Проверка плоскости отсечения
	void CheckPlane(float plane[4], float x, float y, float z);

	//int MaxModelStack, MaxProjectionStack, MaxTextureStack;
	int MaxTextureUnits; //~ Кол-во каналов текстур, поддерживаемых opengl

	GLuint FBO;
	bool offscreenRendering;

	std::vector< std::stack<math::Matrix4> > _matrixStack;
		// стэки сохраненных матриц для каждого типа матрицы
	
	std::vector< math::Matrix4 > _currentMatrix;
		// текущие матрицы для каждого типа матрицы

	std::stack<IRect> _scissorsStack;
		// стек обрезаний для BeginClippoing и EndClipping

	MultisampleType _preferredMultisampleType;

	// тип мультисэплинга
	GLsizei _currentMultisamples;
	// текущее количество сэмплов в мультисэмплинге

public:
	RenderDeviceGL();
	~RenderDeviceGL();

	virtual void SetAppPresentParams(int appWidth, int appHeight);
	virtual void SetPresentParams(DeviceMode mode, int width = 800, int height = 600);

	virtual void SetupGeometry(unsigned char* addr);
	virtual void TrueDraw(const math::Vector3& v1, const math::Vector3& v2, const math::Vector3& v3, const math::Vector3& v4, DEVCOLOR cv1, DEVCOLOR cv2, DEVCOLOR cv3, DEVCOLOR cv4, FRect uv=FRect(0.f, 1.f, 0.f, 1.f));
	virtual void TrueDraw(const math::Vector3& v1, const math::Vector3& v2, const math::Vector3& v3, const math::Vector3& v4, DEVCOLOR cv1, DEVCOLOR cv2, DEVCOLOR cv3, DEVCOLOR cv4, FPoint uv1, FPoint uv2, FPoint uv3, FPoint uv4);
	virtual void TrueDrawLine(const math::Vector3 &p1, const math::Vector3 &p2);
	
	virtual void SetCamera(const math::Vector3& lookFrom, const math::Vector3& lookTo);
	virtual void BindWindowGL(void* layer, int contextWidth=0, int contextHeight=0);
	virtual void ReleaseDepthBuffer();
	virtual void CreateDepthBuffer();
	virtual void ClearDepthBuffer();
	virtual void Clear(const Color& color);
	virtual void Present();
	virtual void SetDepthTest(bool bEnable);
	virtual void Draw(VertexBuffer* vb, int verticesCount = -1);
	virtual void DrawPrimitives(QuadVert *buf, int verticesCount);
	virtual void DrawIndexedPrimitives(QuadVert *buf, int verticesCount, unsigned short* indices);
	virtual void DrawStrip(VertexBuffer* vb);
	virtual void DrawFan(VertexBuffer* vb);
	virtual void DrawIndexed(VertexBufferIndexed* buffer);
	virtual void InitVertexBuffer(VertexBuffer* buffer, int numOfVertices);
	virtual void InitIndexBuffer(VertexBufferIndexed* buffer, int numIndices);
	virtual void Upload(VertexBuffer* buffer, int verticesCount = -1);
	virtual int Lock(Texture *tex, unsigned int *&frame);
	virtual void Unlock(Texture *tex);
	virtual void UploadIndexBuffer(VertexBufferIndexed* buffer);
	virtual void Release(VertexBuffer* buffer);
	virtual void ReleaseIndexBuffer(VertexBufferIndexed* buffer);
	virtual void Upload(Texture* tex);
    virtual void UploadRegion(Texture* tex, int x, int y, int width, int height, const char* data);
	virtual void Upload(const Image& image, DEVTEXTURE& _tex, bool bilinear = true, bool clamp = true, const std::string& texId = "");
	virtual void Reload(Texture *tex);
	virtual void Bind(Texture* tex, int channel=0, unsigned int stageOperation = STAGE_DEFAULT);
	virtual void BindAlpha(Texture* tex);
	virtual void Release(Texture* tex);
	virtual void SetTexParams(CubeTexture *tex);
	virtual Target* CreateRenderTarget(int width, int height, bool alpha = true, bool multisampled = true, bool depth = true, MultisampleType msType = MULTISAMPLE_NONE);
	virtual void DeleteRenderTarget(Target*& tar);
	virtual void Bind(Target* tar, int channel=0, unsigned int stageOperation = STAGE_DEFAULT);
	virtual void BindAlpha(Target* tar);
	virtual void Release(Target* tar);
	virtual void BeginRenderTo(Target* tar);
	using RenderDeviceInterface::BeginRenderTo;
	virtual void EndRenderTo();
	virtual void Upload(VolumeTexture* texture);
	virtual void Bind(VolumeTexture* texture, int channel = 0, unsigned int stageOperation = STAGE_DEFAULT);
	virtual void Release(VolumeTexture*);
	virtual SceneGraph::DeviceBuffer::HardPtr CreateVertexBuffer(MeshVertexFormat vertexFormat, std::size_t stride, std::size_t count);
	virtual SceneGraph::DeviceBuffer::HardPtr CreateIndexBuffer(IndexFormat indexFormat, std::size_t count);
	virtual void DrawIndexedPrimitive(PrimitiveType type, SceneGraph::DeviceBuffer::HardPtr vertices, SceneGraph::DeviceBuffer::HardPtr indices);
	
	virtual void SetMaterial(SceneGraph::Material::HardPtr pMaterial);
	
	virtual void SetWorldMatrices(const std::vector<math::Matrix4>& /* matrices */) {
		// заглушка
		assert(false);
	}
	
	virtual void EnableIndexedVertexBlend(bool /* enable */) {
		// заглушка
		assert(false);
	}
	
	virtual void SetViewFrustum(float left, float right, float bottom, float top, float zNear, float zFar);
	virtual void SetCurrentMatrix(MatrixMode mode);
	virtual void ResetMatrix();
    virtual MatrixMode GetCurrentMatrix();
    
    virtual void MatrixMultiply(const math::Matrix4& matrix);
	virtual void MatrixTransform(const math::Matrix4& matrix);
	virtual void MatrixTranslate(const math::Vector3& v);
	virtual void MatrixRotate(const math::Vector3& axis, float angle);
	virtual void MatrixScale(float scale);
	virtual void MatrixScale(float scaleX, float scaleY, float scaleZ);
	virtual void PushMatrix();
	virtual void PopMatrix();

	virtual void GetDeviceMatrix();
	virtual IPoint Project(const math::Vector3& point);
	virtual void SetTexturing(bool bEnable);
	virtual void SetCulling(bool cullEnable);
	virtual void SetBlendMode(BlendMode blendMode);
	virtual void SetFog(bool isEnable);
	virtual bool GetFog();
	virtual void SetFogSettings(float fogStart, float fogEnd, float density, FogMode fogMode, FogRenderState fogRenderState, Color color);
	virtual void SetBlend(bool isEnable);
	virtual void SetLighting(bool isEnable);
	virtual void SetAlpha(int alpha);
    virtual int  GetAlphaReference();
	virtual void SetAlphaReference(int value);
	virtual void SetColorWriteMask(bool red, bool green, bool blue, bool alpha);
	virtual void SetCurrentColor(const Color& color = Color(0xff, 0xff, 0xff, 0xff));
	virtual Color GetCurrentColor();
	virtual void SetColor(DEVCOLOR col);
	virtual DEVCOLOR GetColor();
	virtual void DirectDrawQuad(const QuadVert* quad);
	virtual void DirectDrawQuad(const QuadVertT2* quad);
	virtual void DirectDrawQuad(const QuadVertT3* quad);
	virtual void Begin2DMode();
	virtual void SetOrthoProjection(float width, float height, float minZ, float maxZ);
	virtual void Begin2DModeInViewport(int x, int y, int width, int height);
	virtual void ResetViewport();
    virtual void SetViewport(int x, int y, int width, int height);
    virtual IRect GetViewport();
    virtual void SetFullscreenViewport();
	virtual void End2DMode();
	virtual bool isLost();
	virtual void BeginScene();
	virtual void EndScene();
	virtual void Release();
	virtual void Reset();
	virtual void Reset(DeviceMode mode, int, int);
	virtual void WriteVendorInfo();
	virtual int Width() const;
	virtual int Height() const;
	virtual FRect ScreenPos();
	virtual FRect ScreenPosPhysical();
	virtual int PhysicalWidth() const;
	virtual int PhysicalHeight() const;
	virtual bool LetterBoxScreen();
	virtual bool PillarBoxScreen();
	virtual void SetDefaultRenderState();
	virtual bool IsPower2Required();
	virtual void StoreTextureToFile(Texture *tex, const std::string& file);
	virtual void RegisterDynamicTexture(Texture *tex);
	virtual void UnregisterDynamicTexture(Texture *tex);
	virtual long GetVideoMemUsage();
	virtual void TranslateUV(FRect& rect, FRect& uv);
	virtual Texture* GetBindedTexture();
	virtual void SetBindedTexture(Texture * tex);
	virtual void BeginClipping(IRect window, ClippingMode clippingMode);
	virtual void EndClipping();
	virtual void CreateGLContext();
	virtual void DestroyGLContext();
	virtual void ClearBindedObject(); // DEPRECATED
	virtual bool CanUseGPUSkinning(int bonesCount);
	virtual math::Matrix4 GetViewProjectionMatrix();
	virtual math::Matrix4 GetModelViewProjectionMatrix();

	virtual void BeginPickPass() {assert(false);}
	virtual int EndPickPass(const IPoint &/*mouse_pos*/) {assert(false); return 0;}
	virtual bool IsPickPass() {return false;}
	virtual void SetColorForMesh();
	virtual void AddLight(SceneGraph::Light::HardPtr /*light*/) {assert(false);}
	virtual bool GetLighting() {assert(false); return false;};
	virtual void DrawTriangle(FPoint p1, FPoint p2, FPoint p3);
	virtual void DrawTriangle(float x1, float y1, float x2, float y2, float x3, float y3,
							 float u1, float v1, float u2, float v2, float u3, float v3);
	virtual BlendMode GetBlendMode() const;
	virtual void GLFinish() const;
	
	virtual void Upload(CubeTexture* tex);
	virtual void Release(CubeTexture* tex);
	virtual void Bind(CubeTexture* tex, int channel=0, unsigned int stageOperation = STAGE_DEFAULT);

	virtual ShaderProgram *GetBindedShader();
	virtual void Bind(ShaderProgram* shader);
	virtual void Release(ShaderProgram* shader);
	virtual void ResetShader();
    virtual int GetMaxTextureUnits();
    virtual void SetPreferredMultiSampleType(MultisampleType type);
    
	virtual bool IsVertexShaderSupported(int majorVersion, int minorVersion);
	virtual bool IsPixelShaderSupported(int majorVersion, int minorVersion);

	virtual math::Matrix4 GetModelMatrix();
    virtual void SetVSyncState(int intervals){};

protected:

	void UseShaderProgram(ShaderProgramGL * program);

	/// Установить юниформные (неизменяемые в течение одного рисования)
	/// переменные шейдера (матрица преобразования и номер текстурного канала)
	void SetUniforms();

	void SetupShader(int uv_units = 1);

	void SetStageOperation(int channel, unsigned int stageOperation);
    
    GLint GetMultisamples(MultisampleType msType);

    void SetTexParams(Target *tex);
    void SetTexParams(Texture *tex);
};

extern RenderDeviceInterface& device;

ShaderProgramGL* loadShaderGL(const char * vertexFileName, const char * fragmentFileName);

} // namespace Render

#endif //_RENDERDEVICE_GL_H_
