#ifndef __TEXTURE_H__
#define __TEXTURE_H__

#pragma once

#include "Platform/TargetPlatforms.h"
#include "Render/Image.h"
#include "Utils/FPoint.h"
#include "Utils/Math.hpp"
#include "Utils/FRect.h"
#include "Utils/OpacityMask.h"
#include "Core/Resource.h"

#include <boost/smart_ptr.hpp>

namespace Core
{
	class ResourceManager;
}

#if defined(ENGINE_TARGET_WIN32_DX9) && !defined(USING_GLES2)
	struct IDirect3DTexture9;
#	define DEVTEXTURE IDirect3DTexture9*
#endif

#if defined(USING_GLES2)
#	include <GLES2/gl2.h>
#	include <GLES2/gl2ext.h>
#	define DEVTEXTURE GLuint
#endif

#if defined(ENGINE_TARGET_IPHONE)
#	include <OpenGLES/ES1/gl.h>
#	define DEVTEXTURE GLuint
#endif

#if defined(ENGINE_TARGET_ANDROID)
#	include <GLES/gl.h>
#	include <GLES/glext.h>
#	define DEVTEXTURE GLuint
#endif

#ifdef ENGINE_TARGET_MACOS
#	include <OpenGL/OpenGL.h>
#	define DEVTEXTURE GLuint
#endif

class TextureLoader;

namespace Render {

	class RenderDeviceImpl;
	class RenderDeviceGLES1;
	class RenderDeviceGLES2;
	class RenderDeviceGL;
	class PartialTexture;

	/// Класс текстуры
	class Texture : public Resource
	{
		friend class Render::RenderDeviceImpl;
		friend class Render::RenderDeviceGLES1;
		friend class Render::RenderDeviceGLES2;
		friend class Render::RenderDeviceGL;
		friend class Render::PartialTexture;
		friend class ::TextureLoader;

	public:
		enum FilteringType {
			NEAREST = 0, BILINEAR, TRILINEAR
		};

		enum AddressType {
			REPEAT = 0, CLAMP
		};

		enum ColorOperation {
			ADD = 0, MODULATE
		};

		enum TextureType {
			SIMPLE_TEXTURE = 0, COMBINED_TEXTURE, PARTIAL_TEXTURE
		};
		
		Texture();
		Texture(const std::string& filename, int alphaLimit = 0);
		Texture(const Image &image, int alphaLimit = 0);
		Texture(int width, int height, unsigned char fill);

		virtual ~Texture();

		const std::string& GetName() const { return textureID; }
		size_t GetMemoryInUse() const;

		/// Возвращает ширину текстуры
		int Width() const { return static_cast<int>(_bitmap_width); }
		/// Возвращает высоту текстуры
		int Height() const { return static_cast<int>(_bitmap_height); }

		/// Возвращает размер изображения в текстуре
		IRect getBitmapRect() const { return IRect(0, 0, _bitmap_width, _bitmap_height); }

		/// Текстура не загружена или принудительно выгружена.
		/// Принудительно выгружаются динамические текстуры при переинициализации RenderDeviceImpl. См. Texture::setDynamic
		bool Empty() const;
		
		/// Динамическая текстура может быть принудительно выгружена при переинициализации RenderDeviceImpl.
		/// См. Texture::Empty, RenderDeviceImpl::RegisterDynamicTexture
		void setDynamic(bool dynamic);
		
#ifdef ENGINE_TARGET_WIN32
		bool isDynamic() const { return _dynamicTexture; }
#endif

		void setFilteringType(FilteringType filter);
		FilteringType getFilteringType() const { return (FilteringType)_filter; }
		
		void setAddressType(AddressType address);
		AddressType getAddressType() const { return (AddressType)_address; }

		void setAlphaLimit(uint8_t limit) { _alphaLimit = limit; }
		void setUseDithering(bool dither) { _useDithering = dither; }

		// Установить формат пикселя
		void setPixelType(EnginePixelType pixelType) { _pixelType = pixelType; }
        EnginePixelType getPixelType() const { return (EnginePixelType)_pixelType; }

		void setColorOp(ColorOperation colorOp) { _colorOp = colorOp; }
		ColorOperation getColorOp() const { return (ColorOperation)_colorOp; }

		void setPurgeAfterLoading(bool value) { _purgeAfterLoading = value; }
		bool getPurgeAfterLoading() const { return _purgeAfterLoading; }

		virtual void GetPropertiesFromFile(const std::string& filename);

		/// Указывает, нужно ли трансформировать текстурные координаты. Если да, то необходимо вызвать TranslateUV перед отрисовкой.
		virtual bool needTranslate() const;
		/// Трансформация текстурных координат. Вызывается в RenderDeviceImpl если needTranslate возвращает true.
		virtual void TranslateUV(FRect &rect, FRect &uv) const;
		virtual void TranslateUV(math::Vector3& v0, math::Vector3& v1, math::Vector3& v2, math::Vector3& v3, FRect &uv) const;
		
		/// Не используйте этот метод! Он не работает (точнее, работает только в OpenGLES1 и только не для атласов)!
		/// Вместо него используйте метод TranslateUV(rect, uv) или TranslateUV(v0, v1, v2, v3, uv).
		/// Если вы не понимаете, почему так, обратитесь за разъяснением к специалистам.
		virtual void TranslateUV(FPoint &uv) const;

		// функции для записи непосредственно в текстуру
		int Lock(unsigned int *&frame);
		void Unlock();

		/// забиндить текстуру в заданный канал
		virtual void Bind(int channel = 0, int stageOp = 0);
		/// забиндить альфа-канал текстуры
		virtual void BindAlpha();

		/// Отрисовка текстуры в заданной позиции. Если текстура ещё не забиндена, она биндится.
		/// Для uv-координат применяется TranslateUV.		
		virtual void Draw(const FPoint& position, const RectOrient orient);		
		virtual void Draw(const FRect& rect, const FRect& uv);		
		virtual void DrawBinded(const FPoint& position, const RectOrient orient);		
		virtual void DrawBinded(const FRect& rect, const FRect& uv);
		
		void Draw(const IPoint& p = IPoint()) {
			Draw(FRect(float(p.x), float(p.x) + _bitmap_width, float(p.y), float(p.y) + _bitmap_height), FRect(0.f, 1.f, 0.f, 1.f));
		}
		
		void Draw(float x, float y) {
			Draw(FRect(x, x + _bitmap_width, y, y + _bitmap_height), FRect(0.f, 1.f, 0.f, 1.f));
		}
		
		void Draw(int x, int y) {
			Draw(FRect(float(x), float(x) + _bitmap_width, float(y), float(y) + _bitmap_height), FRect(0.f, 1.f, 0.f, 1.f));
		}
		
		void Draw(const FPoint& p) {
			Draw(FRect(p.x, p.x + _bitmap_width, p.y, p.y + _bitmap_height), FRect(0.f, 1.f, 0.f, 1.f));
		}
		
		void Draw(const IRect& rect, float xStart, float xEnd, float yStart, float yEnd) {
			Draw(FRect(rect), FRect(xStart, xEnd, yStart, yEnd));
		}
		
		void Draw(const IRect& rect, const FRect& uv) {
			Draw(FRect(rect), uv);
		}
		
		void Draw(float x, float y, float width, float height, const FRect& uv) {
			Draw(FRect(x, x + width, y, y + height), uv);
		}
		
		void DrawCut(const IPoint& p, float cut_factor_x, float cut_factor_y = 1.0f) {
			Draw(FRect(float(p.x), float(p.x) + _bitmap_width * cut_factor_x, float(p.y), float(p.y) + _bitmap_height * cut_factor_y),
				FRect(0.f, cut_factor_x, 0.f, cut_factor_y));
		}
		
		void DrawCut(const FPoint& p, float cut_factor_x, float cut_factor_y = 1.0f) {
			Draw(FRect(p.x, p.x + _bitmap_width * cut_factor_x, p.y, p.y + _bitmap_height * cut_factor_y),
				FRect(0.f, cut_factor_x, 0.f, cut_factor_y));
		}
		
		void Draw(const IPoint& position, const RectOrient orient) {
			Draw(FPoint(position), orient);
		}

		virtual bool isPixelTransparent(int x, int y) const;

		bool isPixelOpaque(int x, int y) const { return !isPixelTransparent(x, y); }
		bool isPixelOpaque(const IPoint& p) const { return !isPixelTransparent(p.x, p.y); }
		
		virtual int GetTextureType() const { return SIMPLE_TEXTURE; }
		virtual DEVTEXTURE GetNativeHandle() const { return _tex; }

		static Texture* CreateFromXml(rapidxml::xml_node<>* elem);
		
	protected:
		void LoadFromFileWithoutMask(const std::string& filename);
		virtual void LoadFromFile(const std::string& filename);
		virtual void LoadFromImage(const Image& image);

		/// Удаляет данные изображения
		virtual void Purge();

		/// Выгрузжает текстуру
		virtual void Release();

		// Работа с alphaMask
		//
		void CreateAlphaMask(int alphaLimit);
		void CreateAlphaMask(const Render::Image &image, int alphaLimit);
		void LoadAlphaMask(const std::string& filename);
		void ReleaseAlphaMask();

	public:
		std::string textureID;
		std::string path;

		Image image;

		/// Премасштабирование текстуры при загрузке
		float _prescale_x, _prescale_y;

		/// Размер текстуры
		int16_t _rect_width, _rect_height;
		/// Размер изображения в текстуре
		int16_t _bitmap_width, _bitmap_height;

#if defined(ENGINE_TARGET_IPHONE) || defined(ENGINE_TARGET_ANDROID)
		static bool allowSubscaleFromRetina; ///< Разрешить программно уменьшать @2x графику, когда нет уменьшенной копии (только PIXEL_TYPE_8888 и PIXEL_TYPE_888)
		static bool checkForHD; ///< проверять наличие HD вариантов текстур (для universal app, предназначенных и для ipad, и для iphone).
			///< По умолчанию checkForHD = false;  Universal игры должны при запуске устанавливать Texture::checkForHD = true;
#endif

	protected:
		/// была ли текстура загружена
		bool wasUploaded;
		/// текстура сейчас забиндена
		bool binded;

		uint8_t _filter; // FilteringType
		uint8_t _address; // AddressType
		uint8_t _colorOp; // ColorOperation

		bool _filterDirty;  // Необходимо ли обновить фильтрацию при следующем бинде
		bool _addressDirty;  // Необходимо ли обновить адресацию при следующем бинде

#ifdef ENGINE_TARGET_WIN32
		bool _dynamicTexture;
#endif

		DEVTEXTURE _tex; ///< внутренний ID текстуры в видеопамяти.

		uint8_t _pixelType; ///< формат хранения цвета пикселя (EnginePixelType)
		bool _useDithering;

		uint8_t _alphaLimit; ///< Пороговое значение для альфа-маски.

		bool _purgeAfterLoading; ///< Удалять ли данные изображения после загрузки текстуры

		boost::scoped_ptr<utils::OpacityMask> mask;

	private:
		// Texture is not copyable
		Texture(const Texture&);
		Texture& operator=(const Texture&);
	};

	typedef boost::intrusive_ptr<Texture> TexturePtr;

#if defined(ENGINE_TARGET_IPHONE) || defined(ENGINE_TARGET_LINUX)
	// Модифицирует имя файла текстуры для устройства iPhone/iPad при наличии соответствующих файлов
	// *HD.* - iPad
	// *@2x.* - Retina или iPad
	// При отсутствии файлов и во всех остальных случаях используется файл для iPhone
	void setFileByIPhoneDevice(std::string& filename, float& prescale_x, float& prescale_y, bool& needSubscaleFromRetina);
#endif

} // namespace Render

#endif // __TEXTURE_H__
