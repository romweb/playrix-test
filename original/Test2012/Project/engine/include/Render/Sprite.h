#ifndef __SPRITE_H__
#define __SPRITE_H__

#pragma once

#include "Render/Texture.h"
#include "Render/Animation.h"
#include "Render/VertexBuffer.h"
#include "Utils/IPoint.h"

namespace Render {
	
	/// Высокоуровневый спрайт.
	/// Класс спрайта, кеширует информацию о положении текстуры,
	/// а так же умеет мультитекстурирование
	class Sprite
	{
	public:
		Sprite();
		Sprite(Texture *tex, const FRect& pos = FRect(0, 0, 0, 0), const FRect& uv = FRect(0.0f, 1.0f, 0.0f, 1.0f));
		virtual ~Sprite();
		
		IRect GetRectangle() const;
		void SetRectangle(const FRect& r);
		
		virtual void AppendLayer(Texture* tex, const FRect& uv = FRect(0.0f, 1.0f, 0.0f, 1.0f));
		virtual void AppendLayer(Animation* anim, const FRect& uv = FRect(0.0f, 1.0f, 0.0f, 1.0f));
		virtual void Clear();

		virtual void MoveTo(const FPoint& pos);
		virtual void Draw();
	
	private:
		struct Layer {
			Texture* tex;
			Animation* anim;
			
			Layer() : tex(NULL), anim(NULL)
			{
			}
		};
		
		typedef std::vector<Layer> Layers;
		Layers _layers;
		
		std::vector<FRect> _uvs;		
		FRect _position;		
		VertexBuffer _buffer;
	
	private:
		void UpdateBuffer();
	};

}

#endif // __SPRITE_H__
