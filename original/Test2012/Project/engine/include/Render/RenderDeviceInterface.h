﻿#ifndef _RENDER_RENDERDEVICE_INTERFACE_H_
#define _RENDER_RENDERDEVICE_INTERFACE_H_

#pragma once

#include "SceneGraph.h"
#include "ClippingMode.h"
#include "Render/Target.h"
#include "Render/RenderTypes.h"
#include "platform.h"

namespace Core
{
	class Window;
	class Application;
}

class VertexBuffer;
class VertexBufferIndexed;

namespace Render {

class Texture;
class Target;
class VolumeTexture;
class CubeTexture;
class ShaderProgram;

typedef std::set<Texture*> TextureSet;

//
// Общий для всех платформ интерфейс рендердевайса
// При добавлении нового метода есть 2 варианта:
//  1) объявить его здесь как чисто виртуальный (= 0);
//  2) объявить его здесь и определить тривиальную версию в RenderDeviceInterface.cpp
//     (с обязательным использованием Assert(false)).
//
class RenderDeviceInterface {

public:

	// Конструктор по умолчанию
	RenderDeviceInterface();

	virtual ~RenderDeviceInterface();

	/// Задать viewport
	/// Координаты логические, внутри произойдет пересчет
	/// (dx only)
	virtual void SetViewport(int x, int y, int width, int height);

	/// (dx only)
	virtual IRect GetViewport();

	/// сделать вывод на всю область экрана, в т.ч. и на полосы по бокам
	/// (dx only)
	virtual void SetFullscreenViewport();

	/// установить ортогональную матрицу проецирования
	/// ширина и высота логические, т.е. определяются игровыми координатами
	virtual void SetOrthoProjection(float logicalWidth, float logicalHeight, float minZ, float maxZ);

	/// Рисовалка
	virtual void TrueDraw(const math::Vector3& v1, const math::Vector3& v2, const math::Vector3& v3, const math::Vector3& v4, Color cv1, Color cv2, Color cv3, Color cv4, FRect uv=FRect(0.f, 1.f, 0.f, 1.f)) = 0;
	
	/// Самая брутальная рисовалка
	virtual void TrueDraw(const math::Vector3& v1, const math::Vector3& v2, const math::Vector3& v3, const math::Vector3& v4, Color cv1, Color cv2, Color cv3, Color cv4, FPoint uv1, FPoint uv2, FPoint uv3, FPoint uv4) = 0;

	/// Рисование линии
	virtual void TrueDrawLine(const math::Vector3& p1, const math::Vector3& p2) = 0;

	/// (dx only)
	virtual void SetCamera(const math::Vector3& lookFrom, const math::Vector3& lookTo) = 0;

	/// (dx only)
	virtual void BindWindowDX(Core::Window* wnd, int contextWidth, int contextHeight);

	/// layer must be a CAEAGLLayer
	/// (gl only)
	virtual void BindWindowGL(void* layer, int contextWidth, int contextHeight);
	
	virtual void BindWindowGLmac(Core::Window* wnd, int contextWidth, int contextHeight);
	
	// Освободить буфер глубины. Если вызвать до BindWindow, то буфер создан не будет.
	virtual void ReleaseDepthBuffer();
	// Создать буфер глубины. Можно вызывать после ReleaseDepthBuffer().
	virtual void CreateDepthBuffer();
	
	/// (dx only)
	virtual void AddLight(SceneGraph::Light::HardPtr light) = 0;
	
	/// Настраивает параметры отрисовки вершин (и шейдер) для последующих множественных вызовов Draw
	virtual void SetupGeometry(unsigned char* addr = NULL) = 0;

	virtual void Draw(VertexBuffer* vb, int verticesCount = -1) = 0;

	virtual void DrawIndexed(VertexBufferIndexed *vb) = 0;

	virtual void DrawStrip(VertexBuffer* vb) = 0;

	virtual void DrawFan(VertexBuffer* vb) = 0;

	/// (dx only)
	virtual void DrawTriangle(float x1, float y1, float x2, float y2, float x3, float y3,
					  float u1, float v1, float u2, float v2, float u3, float v3);

	/// (dx only)
	virtual void DrawTriangle(float x1, float y1, float x2, float y2, float x3, float y3,
					  float u1, float v1, float u2, float v2, float u3, float v3,
					  float u21, float v21, float u22, float v22, float u23, float v23);

	/// (dx only)
	virtual void DrawTriangleWithColor(float x1, float y1, float x2, float y2, float x3, float y3,
							   float u1, float v1, float u2, float v2, float u3, float v3, 
							   Color c1, Color c2, Color c3);

	/// (gl only)
	virtual void DrawTriangle(FPoint p1, FPoint p2, FPoint p3);
	
	/// Пока в gl-версии ничего не делает, так как использование
	/// VBO под айфоно не дало никакого профита
	virtual void InitVertexBuffer(VertexBuffer* buffer, int numOfVertices) = 0;

	/// Пока в gl-версии ничего не делает, так как использование
	/// VBO под айфоно не дало никакого профита
	virtual void InitIndexBuffer(VertexBufferIndexed *buffer, int numOfIndices) = 0;

	virtual void Upload(VertexBuffer* buffer, int verticesCount = -1) = 0;

	virtual void UploadIndexBuffer(VertexBufferIndexed *buffer) = 0;

	virtual void Release(VertexBuffer* buffer) = 0;

	virtual void ReleaseIndexBuffer(VertexBufferIndexed *buffer) = 0;

	virtual void Upload(Texture* tex) = 0;

	virtual void Upload(Texture* tex, Image& image) = 0;
	
	virtual void Upload(const Image& image, DEVTEXTURE& _tex, bool bilinear = true, bool clamp = true, const std::string& texId = "");
	
	virtual void UploadRegion(Texture* tex, int x, int y, int width, int height, const char* data);

	virtual void Reload(Texture *tex) = 0;

	/// (dx only)
	/// ToDo: если никому не нужен, удалить
	virtual int Lock(Texture *tex, unsigned int *&frame) = 0;

	/// (dx only)
	/// ToDo: если никому не нужен, удалить
	virtual void Unlock(Texture *tex) = 0;

	virtual void Bind(Texture* tex, int channel = 0, unsigned int stageOperation = STAGE_DEFAULT) = 0;

	virtual void Bind(Target* rt, int channel = 0, unsigned int stageOperation = STAGE_DEFAULT) = 0;

	virtual void BindAlpha(Texture* tex) = 0;

	virtual void BindAlpha(Target* rt) = 0;

	virtual void Release(Texture* tex) = 0;

	// (dx only)
	virtual void Upload(VolumeTexture* tex) = 0;

	// (dx only)
	virtual void Release(VolumeTexture* tex) = 0;

	// (dx only)
	virtual void Bind(VolumeTexture* tex, int channel = 0, unsigned int stageOperation = STAGE_DEFAULT) = 0;

	// (dx only)
	virtual void Upload(CubeTexture* tex) = 0;

	// (dx only)
	virtual void Release(CubeTexture* tex) = 0;

	// (dx only)
	virtual void Bind(CubeTexture* tex, int channel = 0, unsigned int stageOperation = STAGE_DEFAULT) = 0;

	// (dx only)
	virtual void Upload(ShaderProgram* shader);

	// (dx/gles2 only)
	virtual void Bind(ShaderProgram* shader);

	// (dx/gles2 only)
	virtual void Release(ShaderProgram* shader);

    // android/gles2 - used for reloading shaders, if GLContext was destroyed
    virtual void Add(ShaderProgram* shader);

	// (dx/gles2 only)
	virtual ShaderProgram *GetBindedShader() = 0;

	// (dx only)
	virtual void SetPSParam(int baseReg, const float* fv, int size);

	// (dx only)
	virtual void SetVSParam(int baseReg, const float* fv, int size);
	
	// методы для GPU Skinning
	// (почему-то везде возвращает false!)
	// (dx only)
	virtual bool CanUseGPUSkinning(int bonesCount) = 0;
	
	virtual void SetViewFrustum(float left, float right, float bottom, float top, float zNear, float zFar) = 0;

	virtual void SetCurrentMatrix(MatrixMode mode) = 0;

	// (dx only)
	virtual MatrixMode GetCurrentMatrix();

	virtual void ResetMatrix() = 0;

	/// (dx only)
	virtual void MatrixMultiply(const math::Matrix4& matrix);

	virtual void MatrixTransform(const math::Matrix4& matrix) = 0;

	virtual void MatrixTranslate(const math::Vector3& v) = 0;

	virtual void MatrixRotate(const math::Vector3& axis, float angle) = 0;

	virtual void MatrixScale(float scale) = 0;

	virtual void MatrixScale(float scaleX, float scaleY, float scaleZ) = 0;

	/// (dx only)
	virtual void SetWorldMatrices(const std::vector<math::Matrix4>& matrices) = 0;

	virtual void PushMatrix() = 0;

	virtual void PopMatrix() = 0;

	/// Служебная функция для проверки глубины стека матриц
	/// (dx only)
	virtual int GetStackSize(MatrixMode matrix);

	/// (dx only)
	virtual void GetDeviceMatrix() = 0;

	virtual math::Matrix4 GetModelMatrix();

	virtual math::Matrix4 GetViewProjectionMatrix() = 0;

	/// (dx only)
	virtual math::Matrix4 GetModelViewProjectionMatrix();
	virtual math::Matrix4 GetViewMatrix();
	virtual math::Matrix4 GetProjectionMatrix();

	virtual IPoint Project(const math::Vector3& point) = 0;

	/// Включает или выключает текстурирование. При выключении обнуляются все использованные каналы текстур. При включении восстанавливается нулевой канал из последней текстуры.
	virtual void SetTexturing(bool bEnable);

	virtual bool GetTexturing() const;

	virtual void SetCulling(bool cullEnable) = 0;

	virtual void SetBlendMode(BlendMode blendMode) = 0;

	virtual BlendMode GetBlendMode() const = 0;

	virtual void SetFog(bool isEnable) = 0;

	virtual bool GetFog();

	virtual void SetBlend(bool isEnable) = 0;

	/// Включает/отключает запись цвета и/или альфы в фреймбуффер
	/// (может быть удобно например при рисовании в рендер-таргеты, если хочется рисовать
	/// с альфа-блендингом, но так, чтобы это не испортило альфу хранящуюся в самом таргете)
	virtual void SetColorWriteMask(bool red, bool green, bool blue, bool alpha) = 0;

	/// (dx only)
	virtual void SetLighting(bool isEnable) = 0;

	/// (dx only)
	virtual bool GetLighting() = 0;

	/// Вообще-то, лучше не использовать
	virtual void SetAlpha(int alpha) = 0;

	/// (dx only)
	/// на iPad нужно реализовывать через шейдеры
	virtual void SetAlphaReference(int value) = 0;

	/// (dx only)
	/// на iPad нужно реализовывать через шейдеры
	virtual int GetAlphaReference();

	/// Прямое изменение текущего цвета.
	/// Рекомендуется использовать вместо этого метода метод Render::BeginColor.
	virtual void SetCurrentColor(const Color& color = Color(0xff, 0xff, 0xff, 0xff)) = 0;

	virtual Color GetCurrentColor() = 0;

	/// Прямое изменение текущего цвета.
	/// Рекомендуется использовать вместо этого метода метод Render::BeginColor.
	virtual void SetColor(Color col) = 0;

	virtual Color GetColor() = 0;

	/// Использовать не рекомендуется (но если очень хочется, то можно)
	virtual void DirectDrawQuad(const QuadVert* quad) = 0;

	virtual void DirectDrawQuad(const QuadVertT2* quad) = 0;

	virtual void DirectDrawQuad(const QuadVertT3* quad) = 0;
	
	/// Рисует TRIANGLE_LIST
	virtual void DrawPrimitives(QuadVert *buf, int verticesCount) = 0;

	virtual void DrawIndexedPrimitives(QuadVert *buf, int verticesCount, unsigned short* indices) = 0;

	virtual void Begin2DMode() = 0;

	virtual void Begin2DModeInViewport(int x, int y, int width, int height) = 0;

	virtual void ResetViewport() = 0;

	virtual void End2DMode() = 0;

	virtual bool isLost() = 0;

	// Внимание! При переопределении следует вызвать метод предка.
	virtual void BeginScene() = 0;

	// включает/выключает z-тест
	virtual void SetDepthTest(bool bEnable) = 0;

	// включает/выключает запись в буфер глубины
	virtual void SetDepthWrite(bool bEnable) = 0;

	/// (dx only)
	virtual void ClearDepthBuffer();

	// Внимание! При переопределении следует вызвать метод предка.
	virtual void EndScene() = 0;

	/// Очищает задний буфер
	virtual void Clear(const Color& color = Color::BLACK) = 0;

	/// (dx only)
	virtual void Present();

	virtual void Release() = 0;
	
	virtual DeviceMode GetCurrentDeviceMode() const { return DevFullscreen; }

	/// В GL 2 последних параметра игнорируются
	virtual void Reset(DeviceMode mode, int contextWidth, int contextHeight) = 0;
	
	/// Вызываются все _d3dDevice->SetRenderState,
	/// одинаковые для BindWindow, Reset
	virtual void SetDefaultRenderState() = 0;

	/// Правда ли, что устройство вывода поддерживает текстуры только со
	/// сторонами, являющимися степенями двойки.
	/// (в gl всегда возвращает true)
	virtual bool IsPower2Required() = 0;

	/// Сохранить текстуру в файл
	/// (dx only)
	virtual void StoreTextureToFile(Texture *tex, const std::string& file) = 0;

	/// Зарегистрировать динамическую текстуру. 
	/// Динамические текстуры уничтожаются при переинициализации RenderDeviceImpl. См. Texture::SetDynamic, Texture::Empty
	virtual void RegisterDynamicTexture(Texture *tex) = 0;

	/// Отрегистрировать динамическую текстуру.
	/// Динамические текстуры уничтожаются при переинициализации RenderDeviceImpl. См. Texture::SetDynamic, Texture::Empty
	virtual void UnregisterDynamicTexture(Texture *tex) = 0;

	/// Выгружает загруженные в данный момент GPU ресурсы. Ресурсы при этом продолжают считаться загруженными.
	/// Вызов необходим на мобильных платформах при паузе приложения.
	virtual void UnloadGPUResources();

	/// Загружает выгруженные предыдущим методом GPU ресурсы.
	/// Вызов необходим на мобильных платформах при активизации приложения после паузы.
	virtual void UploadGPUResources();

	/// Возвращает использование видео памяти в Мб
	virtual long GetVideoMemUsage() = 0;

	/// Пересчитывает UV координаты для текущей текстуры
	/// Вот этот TranslateUV самый правильный :)
	virtual void TranslateUV(FRect& rect, FRect& uv) = 0;

	virtual void WriteVendorInfo() = 0;

	/// true если включена компенсация для полноэкранного режима на "высоком" экране
	virtual bool LetterBoxScreen();

	/// true если включена компенсация для полноэкранного режима на широком экране
	virtual bool PillarBoxScreen();

	/// размеры и положение изображения на экране (логические)
	virtual FRect ScreenPos();

	/// размеры и положение изображения на экране (физические)
	virtual FRect ScreenPosPhysical();

	virtual int Width() const = 0;

	virtual int Height() const = 0;

	virtual int PhysicalWidth() const;

	virtual int PhysicalHeight() const;

	// масштабный коэффициент для таргетов, вьюпортов и т.д.
	// 1 - обычный режим
	// 2 - ретина
	virtual int ContentScaleFactor() {
		return _contentScaleFactor;
	}
	
	/// Возвращает расстояние до плоскости отсечения.
	virtual float getFrustumZNear() { return _frustumZNear; } ///< Ближняя плоскость (обычно -1000.f)
	virtual float getFrustumZFar() { return _frustumZFar; } ///< Дальняя плоскость (обычно +1000.f)

	/// получить оптимальные размеры для полноэкранного режима, основываясь на переданных данных
	/// (dx only)
	virtual IRect GetBestFSModeSizes(int origWidth, int origHeight);

	/// Возвращает указатель на текущую текстуру
	virtual Texture* GetBindedTexture() = 0;
	virtual void SetBindedTexture(Texture * tex) = 0;

	
	//
	// Начать режим отсечения прямоугольным окном window
	// Координаты окна в текущей системе координат,
	// которая может быть перемещена и повёрнута.
	// Перед отсечением разрешается вращение ТОЛЬКО вокруг оси Z
	//
	// Режим отсечения говорит, отсекать ли по каждой из четырёх сторон.
	// По умолчанию - отсекается всё. На количестве плоскостей отсечения
	// имеет смысл экономить, поскольку их может быть всего 6, т.е.
	// даже два прямоугольника отсечь проблематично.
	//
	virtual void BeginClipping(IRect window, ClippingMode clippingMode = ClippingMode::ALL) = 0;

	//
	// Завершение участка, открытого последним BeginClipping
	//
	virtual void EndClipping() = 0;

	/**
	* Создает линию отсечения в текущей координатной системе.
	* Координаты линии задаются текущей системе координат,
	* которая может быть перемещена и повёрнута.
	* Перед отсечением разрешается вращение ТОЛЬКО вокруг оси Z.
	* Отсекает все, что находится справа от вектора (x1,y1)(x2,y2)
	* Предполагается, что вызовы данной функции не перемежаются с вызовами
	* BeginClipping / EndClipping, т.е. если последней вызывалась BeginClipping,
	* то первой должна вызваться EndClipping, и наоборот, для BeginClipPlane
	* должна вызываться EndClipPlane.
	*/
	virtual void BeginClipPlane(float x1, float y1, float x2, float y2) = 0;
	/**
	* Выключает последнюю включенную линию отсечения
	*/
	virtual void EndClipPlane() = 0;

	//
	// Создать цель рендеринга с нужными размерами.
	// Тот, кто создаёт таргет с помощью CreateRenderTarget _обязан_ 
	// вызвать DeleteRenderTarget, как только текстура становится не нужна.
	// При выходе из игры количество висящих в памяти объектов проверяется
	// на равернство с нулём. Таргеты можно удалять в деструкторах виджетов,
	// в этот момент Render::device еще живой.
	// Параметр alpha включает или отключает альфа-канал у цели
	// Если не требуется использовать альфу отрендеренного в неё изображения, то alpha стоит передать как false
	// Параметр multisampled отвечает за сглаживание при рендере в текстуру с таким же качеством, что и рендер в экранный буфер.
	// Таким образом, если сглаживание отключено при создании девайса, то и в целях сглаживания не будет.
	// Но если включено, то в цели будут multisampled, что делает невозможным сохранение их в image_pool, т.е.
	// их можно использовать только для рендера, а не для сохранения на диск
	// Параметр depth указывает на использование буфера глубины целью (если его не надо использовать - в большинстве случаев не надо, только если в цель не
	// будут выводиться 3д модели, то можно немного скурить видеопамять)
	// Параметр msType позволяет задать качество мультисэмплинга таргета вручную. Если он равен MULTISAMPLE_NONE, при multisampled == true,
	// то цель будет использовать качество рендера экранного буфера.
	virtual Target* CreateRenderTarget(int width, int height, bool alpha = true, bool multisampled = true, bool depth = true, MultisampleType msType = MULTISAMPLE_NONE) = 0;

	//
	// Удалить рендертаргет и освободить память
	//
	virtual void DeleteRenderTarget(Target*& renderTarget) = 0;

	//
	// Начать рендерить в рендер-таргет
	// При этом начало координат будет в левом нижнем углу получающеся текстуры.
	// Цвет фона будет (0, 0, 0, 0), то есть прозрачный.
	// Если нужен непрозрачный фон, то можно отрендерить в таргет прямоугольник
	// без текстуры обычным образом.
	// Внимание! При переопределении следует вызвать метод предка.
	//
	virtual void BeginRenderTo(Target* render) = 0;

	void BeginRenderTo(Target* render, const Color& color) {
		BeginRenderTo(render);
		Clear(color);
	}

	//
	// Завершить рендерить в рендер-таргет
	// Внимание! При переопределении следует вызвать метод предка.
	//
	virtual void EndRenderTo() = 0;

	// Начать и закончить отрисовку буфера выбора объектов
	// В буфер должны рисоваться объекты с определённым значением цвета, чтобы в 
	// дальнейшем узнать, какой именно объект попал под курсор
	// EndPickPass возвращает id цвета, который был под курсором
	// По-умолчанию, буфер очищается белым цветом, поэтому если под курсором не оказалось
	// объекта, то функция EndPickPass вернёт значение 0x00FFFFFF
	// (dx only)
	virtual void BeginPickPass() = 0;

	// (dx only)
	virtual int EndPickPass(const IPoint &mouse_pos) = 0;

	// (dx only)
	virtual bool IsPickPass() = 0;

	// (dx only)
	virtual Target* GetPickTarget() const;
	
	// (dx only)
	virtual void SetColorForMesh() = 0;	// Устанавливает текущий цвет для рендера меша, если у него отсутствует
							// цветовой канал

	// Установить параметры тумана
	// (dx|GLES2 only)
	virtual void SetFogSettings(float fogStart, float fogEnd, float density, FogMode fogMode, FogRenderState fogRenderState, Color color);

	/// Создаёт буфер вершин
	// (dx only)
	virtual SceneGraph::DeviceBuffer::HardPtr CreateVertexBuffer(MeshVertexFormat vertexFormat, std::size_t stride, std::size_t count) = 0;
	
	/// Создаёт буфер индексов
	// (dx only)
	virtual SceneGraph::DeviceBuffer::HardPtr CreateIndexBuffer(IndexFormat indexFormat, std::size_t count) = 0;
	
	/// Рисует примитивы с помощью буфера вершин
	// (dx only)
	virtual void DrawPrimitive(PrimitiveType type, SceneGraph::DeviceBuffer::HardPtr vertices);
	
	/// Рисует примитивы с помощью буфера вершин и буфера индексов
	// (dx only)
	virtual void DrawIndexedPrimitive(PrimitiveType type, SceneGraph::DeviceBuffer::HardPtr vertices, SceneGraph::DeviceBuffer::HardPtr indices) = 0;
	
	/// Устанавливает текущий материал для взаимодействия с освещением
	// (dx only)
	virtual void SetMaterial(SceneGraph::Material::HardPtr material) = 0;
	
	/// Устанавливает или снимает индексированное смешивание преобразований вершин
	// (dx only)
	virtual void EnableIndexedVertexBlend(bool enable) = 0;

	// (dx only)
	virtual bool CopyTexture(Render::Target* from, Render::Texture* to);

	//
	// Рекомендуется вызывать в конструкторе Application или его наследников.
	// Установить _предпочтитаемый_ тип мультисэмплирования.
	// Это не означает, что будет использоваться именно он;
	// если он не поддерживается видеокартой, возьмётся ближайший меньший.
	//
	// Мультисэмплирование задаёт режим полноэкранного антиалиасинга.
	// при этом (в directx) сглаживаются только пиксели на границах полигонов.
	//
	// (dx only)
	virtual void SetPreferredMultiSampleType(MultisampleType type);

	// 
	// Включить/выключить вертикальную синхронизацию.
	// Влияет только на полноэкранный режим, при включении немного страдает производительность.
	// Имеет смысл вызывать в конструкторе Application (или  производного класса),
	// поскольку параметр применяется только при вызове _d3dDevice->Reset().
	//
	// (dx only)
	virtual void SetVSyncState(int intervals);
	virtual int GetVSyncState() const { return 0; }

	// Определение поддержки определённого вида шейдеров
	// (dx only)
	virtual bool IsVertexShaderSupported(int majorVersion, int minorVersion);

	// (dx only)
	virtual bool IsPixelShaderSupported(int majorVersion, int minorVersion);

	// Определение поддержки кубических текстур
	virtual bool IsCubeTextureSupported();

	// Определение поддержки объёмных текстур
	virtual bool IsVolumeTextureSupported();

	// Максимальное количество текстур, поддерживаемых при мультитекстурировании.
	// В общем случае верно для фиксированного конвейера, т.к. количество текстур
	// поддерживаемых в шейдерах зависит от версии шейдеров и может отличаться.
	virtual int GetMaxTextureUnits();

	/// Создать контекст OpenGL
	/// Должна вызываться в начале каждого потока (и главного тоже)
	/// Также _обязательно_ нужно вызывать парный метод DestroyContext 
	/// (под directx не делает ничего)
	virtual void CreateGLContext() = 0;
	
	//
	// Удалить контекст OpenGL
	// Нужно вызывать парно CreateGLContext в конце каждого потока
	// (под directx не делает ничего)
	//
	virtual void DestroyGLContext() = 0;
	
	/// (gl only)
	virtual void Release(Target* tar);
	
	/// (gl only)
	virtual void ClearBindedObject();
	
	// Завершить все команды OpenGL (актуально для iOS, где
	// используется отложенное выполнение команд)
	virtual void GLFinish() const;
	
	virtual void SetSwapInterval(int value);

	virtual void SetAppPresentParams(int appWidth, int appHeight);

	virtual void SetPresentParams(DeviceMode mode, int w=800, int h=600);
	
	// задать размеры рендер-буфера
	// это костыль для андроида, т.к. там возможно изменение размеров области экрана, в которой рисуется приложение
	//
	// android only
	//
	virtual void SetRenderBufferSize(int width, int height);

	virtual void* GetNativeHandle() const { return NULL; }

	virtual void SetSeparateBlendMode(bool enable) {}

	// Функции для работы с буфером трафарета
	virtual void SetStencilTest(bool bEnable);
	virtual void SetStencilFunc(StencilFunc::Type func, unsigned char value, unsigned char mask = 0xFF);
	virtual void SetStencilOp(StencilOp::Type fail, StencilOp::Type zFail, StencilOp::Type pass);
	
/* НЕВИРТУАЛЬНЫЕ МЕТОДЫ */
	
	// Происходит ли рендер в таргет
	bool IsRenderingToTarget() const;
	
	bool IsRenderingToScreen() const;



	TextureSet GetDynamicTextures() const { return _dynamicTextures; };

private:

	bool _isRenderingToTarget;
		// рендерим ли в цель (находимся между BeginRenderTo и EndRenderTo)

	bool _isRenderingToScreen;
		// рендерим ли на экран (находимся между BeginScene и EndScene)

	bool _isTexturingEnabled;
		// Включено ли текстурирование

protected:
	/// Расстояние до ближней и дальней плоскостей отсечения по умолчанию.
	/// Используется, например, в Begin2DMode().
	const float _defaultZNear;
	const float _defaultZFar;

	/// Расстояние до плоскостей отсечения на данный момент.
	/// Может отличаться от _defaultZNear/Far после вызова
	/// SetOrthoProjection(..) или SetViewFrustum(..).
	float _frustumZNear;
	float _frustumZFar;

	int _contentScaleFactor;

	// list of uploaded textures, for dx - dynamic only, for opengl - all
    TextureSet _dynamicTextures;

	typedef std::set<Target*> TargetSet;
	// list of uploaded render targets
	TargetSet _renderTargets;

	typedef std::set<ShaderProgram*> ShaderSet;
	// list of loaded shaders
	ShaderSet _shaderPrograms;
};

extern RenderDeviceInterface& device;
	
} // namespace Render

#endif // _RENDER_RENDERDEVICE_INTERFACE_H_
