#ifndef _RENDERDEVICE_IPHONE_H_
#define _RENDERDEVICE_IPHONE_H_

#include <stack>
#include <vector>
#include <set>
#include "EngineAssert.h"
#include "Utils/FPoint.h"
#include "Render/Texture.h"
#include "Render/RenderDeviceInterface.h"
#include "ShaderProgramGL.h"
//#import <QuartzCore/QuartzCore.h>

extern bool isFullscreen;
extern IRect _FullscreenViewportRect, _WndViewportRect;

namespace Core
{
	class Window;
}

class VertexBuffer;
class TextureTextPrinter;

namespace Render
{
class Texture;
	
class RenderDeviceImpl 
	: public RenderDeviceInterface
{
	long memUsage;
	DeviceMode _currentDeviceMode;
	WindowRef _bindedWindow;
	CFDictionaryRef _originalDisplayMode;

	bool _windowBinded;
	int _physWidth, _physHeight;
	int _nClipPlanes;
	static const int MAX_CLIP_PLANES = 6;
	std::stack<int> _clipPlanesStack;
	typedef std::list<Target*> Targets;
	Targets _targets;
	Target* _pickBuffer;
	bool _pickPass;
	unsigned int MaxActiveLights, ActiveLights;
	BlendMode _currentBlendMode;
	int _currentAlphaRef;
	AGLContext _windowContext, _fullscreenContext, _secondaryContext;
	
	void SetPresentParams(DeviceMode mode, int w=800, int h=600);
	void SetTexParams(Target *tex);
	void SetTexParams(Texture *tex);

	MatrixMode _currentMatrix;
	DEVCOLOR _currentColor;
	DEVTEXTURE _currentTexture;

	/// Стек цвета. Все Draw... методы (кроме Direct... методов) пересчитывают цвет/альфу точек, относительно заданных ранее. То есть, если был задан цвет ARGB=0.5,0.3,1,1, то рисование точки с цветом 1,0.5,0.5,1 приведёт к реальному значению 0.5,0.15,0.5,1. При восстановлении из стека предыдущего значения, пересчёт прекращается. Смотри SetColor и ResetColor.
	std::stack<Color> _colorStack;
	/// стек чисел из интервала [0; 1]
	/// на вершине - <текущая альфа> / 255.0f.
	/// Стек используется функциями BeginAlphaMul и EndAlphaMul
	//std::stack<float> _alphaStack;

	/// текущая забинденая текстура
	Texture* bindedTexture;
	Target* bindedTarget;
	/// количество использованных каналов текстур, используется при выключении текстур
	int usedChannels;

	/// Правда ли, что устройство вывода поддерживает текстуры только со
	/// сторонами, являющимися степенями двойки.
	bool _isPower2Required;
	
	/// текущие размеры окна
	int _current_width, _current_height;

	typedef std::set<Texture*> DynamicTexturesSet;
	/// список динамических текстур
	DynamicTexturesSet _dynamicTextures;
	
	GLuint FBO;
	
	bool _isRenderingToTarget;
public:

	/// Рисовалка
	void TrueDraw(const math::Vector3& v1, const math::Vector3& v2, const math::Vector3& v3, const math::Vector3& v4, DEVCOLOR cv1, DEVCOLOR cv2, DEVCOLOR cv3, DEVCOLOR cv4, FRect uv=FRect(0.f, 1.f, 0.f, 1.f));
	/// Ещё рисовалка
	void TrueDraw(const math::Vector3& v1, const math::Vector3& v2, const math::Vector3& v3, const math::Vector3& v4, DEVCOLOR cv1, DEVCOLOR cv2, DEVCOLOR cv3, DEVCOLOR cv4, FPoint uv1, FPoint uv2, FPoint uv3, FPoint uv4);

	void TrueDrawLine(const math::Vector3& p1, const math::Vector3& p2);
	
	RenderDeviceImpl();
	~RenderDeviceImpl();
	
	void SetCamera(const math::Vector3& lookFrom, const math::Vector3& lookTo);

	/// context must be a EAGLContext, layer must be a CAEAGLLayer
	void BindWindowGL(void* context, void* layer);

	void AddLight(SceneGraph::Light::HardPtr light);

	void SetupGeometry(unsigned char* addr);
	
	void Draw(VertexBuffer* vb, int verticesCount = -1);
	void DrawIndexed(VertexBufferIndexed *vb);	
	void Draw(VertexBuffer* vb);
	void DrawStrip(VertexBuffer* vb);
	void DrawFan(VertexBuffer* vb);
    void InitVertexBuffer(VertexBuffer* buffer, int numOfVertices);
	void Upload(VertexBuffer* buffer);
	void InitIndexBuffer(VertexBufferIndexed *buffer, int numOfIndices);
	void Upload(Texture* tex);
	void Upload(VertexBuffer* buffer, int verticesCount = -1);
	void UploadIndexBuffer(VertexBufferIndexed *buffer);
	void Release(VertexBuffer* buffer);
	void ReleaseIndexBuffer(VertexBufferIndexed *buffer);
	void Reload(Texture *tex);
	void Bind(Texture* tex, int channel=0);
	void Bind(Texture* tex, int channel=0, unsigned int stageOperation = STAGE_DEFAULT);
	void Bind(Target* rt, int channel=0, unsigned int stageOperation = STAGE_DEFAULT);
	void BindAlpha(Texture* tex);
	void BindAlpha(Target* rt);
	void Release(Texture* tex);
	void Release(VolumeTexture* tex){}
	void Upload(VolumeTexture* tex){};
	void Bind(VolumeTexture* tex, int channel=0, unsigned int stageOperation = STAGE_DEFAULT){}
	int Lock(Texture *tex, DWORD *&frame);
	void Unlock(Texture *tex);
	bool CanUseGPUSkinning(int bonesCount){return false;}
	void SetViewFrustum(float left, float right, float bottom, float top, float zNear, float zFar);
	void SetCurrentMatrix(MatrixMode mode);

	void ResetMatrix();
	void MatrixTransform(const math::Matrix4& matrix);
	void MatrixTranslate(const math::Vector3& v);
	void MatrixRotate(const math::Vector3& axis, float angle);
	void MatrixTranslate(math::Vector3 v);
	void MatrixTranslate(const math::Vector3* v);
	void MatrixRotate(math::Vector3 axis, float angle);
	void MatrixRotate(const math::Vector3* axis, float angle);
	void MatrixScale(float scale);
	void MatrixScale(float scaleX, float scaleY, float scaleZ);
	void PushMatrix();
	void PopMatrix();
	void SetWorldMatrices(const std::vector<math::Matrix4>& matrices){};
	math::Matrix4 GetViewProjectionMatrix();
	
	void GetDeviceMatrix(){};
	IPoint Project(const math::Vector3& point);
	/// Включает или выключает текстурирование. При выключении обнуляются все использованные каналы текстур. При включении восстанавливается нулевой канал из последней текстуры.
	void SetTexturing(bool bEnable);
	void SetCulling(bool cullEnable);
	BlendMode GetBlendMode() const;
	void SetBlendMode(BlendMode blendMode);
	void SetFog(bool isEnable);
	void SetBlend(bool isEnable);
	void SetLighting(bool isEnable);
	void SetAlpha(int alpha);
	void SetAlphaReference(int value);
	void SetColorWriteMask(bool red, bool green, bool blue, bool alpha);
	bool GetLighting();
	/// Прямое изменение текущего цвета.
	/// Рекомендуется использовать вместо этого метода метод SetColor.
	void SetCurrentColor(const Color& color = Color(0xff, 0xff, 0xff, 0xff));
	Color GetCurrentColor();

	void SetColor(DEVCOLOR col);
	DEVCOLOR GetColor();

	/// В этом методе _не_ пересчитывается альфа и цвет, так же _не_ пересчитываются координаты текстуры.
	/// Использовать не рекомендуется (но если очень хочется, то можно)
	void DirectDrawQuad(const QuadVert* quad);
	void DirectDrawQuad(const QuadVertT2* quad);
	void DirectDrawQuad(const QuadVertT3* quad);
	void Begin2DMode();
	void SetOrthoProjection(float width, float height);
	void Begin2DModeInViewport(int x, int y, int width, int height);
	void ResetViewport();
	void End2DMode();
	void DrawTriangle(FPoint p1, FPoint p2, FPoint p3);
	bool isLost();

	void BeginScene();
	void SetDepthTest(bool bEnable);
	void EndScene();

	void Release();
	//void Reset();
	void Reset(DeviceMode mode, int contextWidth, int contextHeight);
	void Reset(DeviceMode mode);
	void Reset(DeviceMode mode, WindowRef hWnd, int contextWidth, int contextHeight);

	void WriteVendorInfo();
	Texture* GetBindedTexture();
	void SetBindedTexture(Texture * tex);
	void BeginClipping(IRect window, ClippingMode clippingMode = ClippingMode::ALL);
	void EndClipping();
	Target* CreateRenderTarget(int width, int height, bool alpha = true, bool multisampled = true, MultisampleType msType = MULTISAMPLE_NONE);	
	void DeleteRenderTarget(Target*& renderTarget);
	void BeginRenderTo(Target* render);
	using RenderDeviceInterface::BeginRenderTo;
	void EndRenderTo();
	void BeginPickPass(){}
	int EndPickPass(const IPoint &mouse_pos){return 0;}
	bool IsPickPass(){return false;}
	void SetColorForMesh(){};
	void DrawIndexedPrimitive(PrimitiveType type, SceneGraph::DeviceBuffer::HardPtr vertices, SceneGraph::DeviceBuffer::HardPtr indices){};
	
	void SetMaterial(SceneGraph::Material::HardPtr material){};
	void EnableIndexedVertexBlend(bool enable){};
	
	//unsigned int Width();
	//unsigned int Height();
	int Width();
	int Height();
	
	/// Вызываются все _d3dDevice->SetRenderState,
	/// одинаковые для BindWindowGL, Reset() и Reset(mode)
	void SetDefaultRenderState();
	/// Правда ли, что устройство вывода поддерживает текстуры только со
	/// сторонами, являющимися степенями двойки.
	bool IsPower2Required();
	bool IsPower2(int n);
	/// Сохранить текстуру в файл
	void StoreTextureToFile(Texture *tex, const std::string& file);
	/// Зарегистрировать динамическую текстуру. 
	/// Динамические текстуры уничтожаются при переинициализации RenderDeviceImpl. См. Texture::SetDynamic, Texture::Empty
	void RegisterDynamicTexture(Texture *tex);
	/// Отрегиестрировать динамическую текстуру.
	/// Динамические текстуры уничтожаются при переинициализации RenderDeviceImpl. См. Texture::SetDynamic, Texture::Empty
	void UnregisterDynamicTexture(Texture *tex);
	/// Возвращает использование видео памяти в Мб
	long GetVideoMemUsage();
	/// Пересчитывает UV координаты для текущей текстуры
	void TranslateUV(FRect& rect, FRect& uv);
	void TranslateUV(FRect& rect);
	void TranslateUV(FPoint& uv1, FPoint& uv2);
	int PhysicalWidth();
	int PhysicalHeight();
	void CreateGLContext();
	void DestroyGLContext();
	Target* GetPickTarget() const{};
	void MatrixMultiply(const math::Matrix4& matrix);
	void ClearDepthBuffer();
	void Clear(const Color& color);
	void SetPSParam(int baseReg, const float* value, int count);
	void SetPreferredMultiSampleType(MultisampleType type);
	void ClearBindedObject();
	void DrawTriangle(float x1, float y1, float x2, float y2, float x3, float y3,
										float u1, float v1, float u2, float v2, float u3, float v3);
	ShaderProgram *GetBindedShader(){return NULL;}
	SceneGraph::DeviceBuffer::HardPtr CreateVertexBuffer(MeshVertexFormat vertexFormat, std::size_t stride, std::size_t count); //!!!
	SceneGraph::DeviceBuffer::HardPtr CreateIndexBuffer(IndexFormat indexFormat, std::size_t count); //!!!
		
	void DrawTriangleWithColor(float x1, float y1, float x2, float y2, float x3, float y3,
							   float u1, float v1, float u2, float v2, float u3, float v3, 
							   DEVCOLOR c1, DEVCOLOR c2, DEVCOLOR c3);
	bool PillarBoxScreen();
	bool LetterBoxScreen();
	IRect GetBestFSModeSizes(int origWidth, int origHeight);
	math::Matrix4 GetModelMatrix();
	bool IsPixelShaderSupported(int manorVersion, int minorVersion);
	bool IsVertexShaderSupported(int manorVersion, int minorVersion);
	int GetMaxTextureUnits();
	void Bind(ShaderProgram* shader);
	void Release(ShaderProgram* shader);
	void Present();
	void GLFinish() const{abort();}
	//void BindWindowGL(void* layer){}
	void BindWindowGLmac(Core::Window* wnd, int contextWidth, int contextHeight);
	int GetAlphaReference();
	void SetViewport(int x, int y, int width, int height);
	int GetStackSize(MatrixMode matrix);
	FRect ScreenPos();
	void SetGlobalCompress(bool allow);
	FRect ScreenPosPhysical();
	bool CopyTexture(Render::Target* from, Render::Texture* to);
	MatrixMode GetCurrentMatrix();
	void SetVSyncState(bool enable);
    void SetVSyncState(int intervals);
	bool GetFog(){return false; abort();}
	void Release(Target* tar);
	void SetOrthoProjection(float logicalWidth, float logicalHeight, float minZ, float maxZ);
	math::Matrix4 GetModelViewProjectionMatrix();
	void DrawTriangle(float x1, float y1, float x2, float y2, float x3, float y3,
										float u1, float v1, float u2, float v2, float u3, float v3,
										float u21, float v21, float u22, float v22, float u23, float v23);
	void CreateContexts();
	void RenderInit();
	void SetStageOperation(int channel, int stageOperation);
	void UnbindAll();
	void SetFullscreenViewport();
	void SetSwapInterval(int value);
	void DrawPrimitives(Render::QuadVert *buf, int verticesCount);
	
	virtual void Upload(CubeTexture* tex){abort();}
	virtual void Release(CubeTexture* tex){abort();}
	virtual void Bind(CubeTexture* tex, int channel=0, unsigned int stageOperation = STAGE_DEFAULT){abort();}
};
	
	extern RenderDeviceInterface& device;
	
}

#endif //_RENDERDEVICE_IPHONE_H_