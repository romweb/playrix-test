#ifndef _REDNER_BITMAPFONT_H_
#define _REDNER_BITMAPFONT_H_

#include "PixelType.h"

namespace Render
{

class Texture;
struct BitmapFontImpl;

//
// Растровый шрифт
// См. MSDN: Windows GDI/Fonts and Text/About Text Output/Formatting Text/
// В статье "Character Widths" освещены aWidth, bWidth, cWidth
// http://msdn2.microsoft.com/en-us/library/ms534206.aspx
// В статье "String Widths and Heights" освещены ascent и descent
// http://msdn2.microsoft.com/en-us/library/ms534015.aspx
//
class BitmapFont
{
public:

	std::string _fontName;
		// имя шрифта

	Texture *_texture;
		// текстура, на которой нарисованы буквы

	int _ascent;
		// расстояние от базовой линии до верхней границы строки
	
	int _descent;
		// расстояние от базовой линии до нижней границы строки

	int _fontHeight;
		// высота строки текста/расстояние между последовательными строками

	int _charTrack;
		// дополнительное горизонтальное расстояние между символами

	int _spaceWidth;
		// ширина пробела

	int _yOffset;
		// Дополнительный отступ по Y сверху и снизу буквы для нанесения эффектов.
		// Эффекты-то должны наноситься, а вот расстояние между строками
		// зависеть от _yOffset не должно.

	int _width, _height;
		// размеры текстуры шрифта

	// Коэффициент, на который умножается размер пробела при выводе его в качестве разделителя групп в больших числах
	float _spaceCoeff;

	float _scale;

	// Этот флаг используется для iphone Retina и масштабирует шрифт в 2 раза меньше
	bool _downscale;
	
	bool _useKerning;

	//
	// Информация об одном символе
	//
	struct CharInfo
	{
		int code;
			// код символа, >= 0

		int aWidth;
			// горизонтальный отступ от конца предыдущей буквы до начала текущей

		int bWidth;
			// ширина рисуемой части текущей буквы

		int cWidth;
			// горизонтальный отступ от конца рисуемой части буквы до конца буквы

		int x;
			// расстояние от левого края текстуры до места буквы, куда прибавляется B

		int y;
			// расстояние от нижнего края текстуры до буквы с отступом yOffset
        
        int charHeight;
			// высота прямоугольника символа
        
        int charYOffset;
			// смещение от нижней стороны прямоугольника символа до рисуемой части
	};

	typedef std::vector<CharInfo> Chars;

	Chars charInfo;
		// массив с информацией о символах
	
	struct KerningInfo : public std::pair<int, int> {
		int amount;
		
		KerningInfo(const std::pair<int, int>& pair)
			: std::pair<int, int>(pair)
		{
		}
		
		KerningInfo(int first, int second, int amount)
			: std::pair<int, int>(first, second)
			, amount(amount)
		{
		}
		
		bool operator<(const KerningInfo& rhs) {
			if (first < rhs.first) {
				return true;
			} else if (first > rhs.first) {
				return false;
			}
			// first == rhs.first
			return second < rhs.second;
		}
	};
	
	typedef std::vector<KerningInfo> Kernings;
	
	Kernings _kernings;
	
	const Kernings& GetKernings() const { return _kernings; }

	//
	// Получение свойств одной буквы по её номеру в списке
	//
	void GetCharProperties(size_t index, int &code, int &aWidth, int &bWidth, int &cWidth, int &x, int &y, int &charHeight, int &charYOffset);

public:

	//
	// Загрузка шрифта из файлов
	//
	BitmapFont(const std::string& fontName);
	
	void Load(const std::string& path, bool autoUpload);
	void LoadFromCocosFormat(const std::string& path, bool autoUpload);
	void LoadFromXml(const std::string& path, bool autoUpload);
	
	void SaveToXml(const std::string& filename);

	const std::string& GetName() const { return _fontName; }

	//
	// Заполнение BitmapFontImpl
	//
	void LoadTo(BitmapFontImpl& bf);

	// Возвращает коэффициент, на который умножается размер пробела при отображении его в качестве разделителя разрядов
	float GetSpaceCoeff() const { return _spaceCoeff; }
};

}

#endif
