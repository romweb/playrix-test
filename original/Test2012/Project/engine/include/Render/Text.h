#ifndef __TEXT_H__
#define __TEXT_H__

#ifdef _MSC_VER
#pragma once
#endif

#include "Core/Resource.h"
#include "Render/RenderTypes.h"

namespace Render {

class Text;
typedef boost::intrusive_ptr<Text> TextPtr;    

class Text : public Resource {
public:
	virtual float getWidth() const = 0;
	
	virtual float getHeight() const = 0;
	
	virtual float getBaseLine() const = 0;

	virtual TextAlign getHAlign() const = 0;

	virtual TextAlign getVAlign() const = 0;

	virtual float getScale() const = 0;

	virtual const std::string& getFontName() const = 0;

	virtual void getLettersCoords(std::vector<IPoint>& coords) = 0;

	virtual IPoint GetSize() const = 0;

	virtual void Draw(const FPoint& p = FPoint()) = 0;

	virtual void SetVariable(const std::string& name, const std::string& value) = 0;

	virtual void UpdateVariables() = 0;

	virtual const std::string& GetSource() const = 0;

	virtual void SetSource(const std::string& source) = 0;

	virtual std::string ToString(const std::string& lineSeparatorChar = "\n") = 0;
    
    virtual Render::TextPtr Clone() const = 0;

public:
	/// ������ �� ����������� ����� �������� � ������
	static bool IsLocaleNumberFormatNeeded();

	/// ���������� ����������� ����� �������� � ������
	static const std::string& GetThousandsSeparator();

	/// ���������� ���������� �����
	static const std::string& GetDecimalPoint();

	/// ������������� ����������� �������� �������� � ������. ����������� �������� � ���������� ����������
	static void SetNumberFormatParameters(const std::string &thousandsSeparator);

	/// ���������� ����������������� ����� � ������ ������������ ����� �������� � ������
	/// lookingInText - ���� � �������? ���� ��, �� ������������ ����� � �����, �������� {color=255;255;255;255}...{}
	static std::string FormatNumbers(const std::string &str, bool lookingInText = false);

	/// ���������� ������ ������� ����� � ������ ����� � ������
	/// str - ������, � ������� ������ �����
	/// lookingInText - ���� � �������? ���� ��, �� ������������ ����� � �����, �������� {color=255;255;255;255}...{}
	static std::deque< std::pair<size_t, size_t> > GetNumbersPositions(const std::string &str, bool lookingInText = false);

	/// ���������� ���, ���� ���������� ������ �������� ���������� �������� ��� ������ �����
	static bool IsValidNumberChar(int code);

	/// ����� �� ����������� �������
	static bool IsThousandsSeparatorSpace();
	
	/// �������� � ������ ������ ���������� �������
	static std::string TrimString(const std::string& s);

private:
	/// ��������� ����������� � ����� (�� ����� ������ ���� �������� �����)
	static std::string FormatNumbersHelper(const std::string &str);

private:
	/// ����������� ����� �������� � ������
	static std::string _thousandsSeparator;
    
	/// ���������� ����� (������, ����������� ����� � ������� ����� � ���������� ������)
	static std::string _decimalPoint;

	/// ��������� �� �������������� ����� � ������ ������������ ��������? (�����, ���� ����������� ����� �������� �� ������ ������)
	static bool _isLocaleNumberFormatNeeded;
};

} // namespace Render

#endif // __TEXT_H__
