#ifndef __MESH_H__
#define __MESH_H__

#ifdef _MSC_VER
#pragma once
#endif // _MSC_VER

#include "SceneGraph/SceneNode.h"
#include "SceneGraph/DeviceBuffer.h"
#include "SceneGraph/Material.h"
#include "Utils/Vector3.h"
#include "RefCounter.h"

#include <boost/intrusive_ptr.hpp>

namespace SceneGraph {

class MeshData;

typedef boost::intrusive_ptr<MeshData> MeshDataHardPtr;
	// Здесь определяется дублирующий тип умного указателя для класса MeshData,
	// т.к. мы не можем включить здесь весь заголовочный файл во имя предотвращения
	// циклической зависимости.

///
/// Меш.
///
class Mesh : public RefCounter {
public:
	typedef boost::intrusive_ptr<Mesh> HardPtr;
	
	///
	/// Формат вершины.
	///
	struct Vertex {
		math::Vector3 position;
		math::Vector3 normal;
		float tu, tv;
		
		static const unsigned long FVF;
	};
	
	virtual ~Mesh();
	
	virtual Mesh::HardPtr Clone();
		/// Клонирует экземпляр объекта

	void SetFilename(const std::string& filename) { _filename = filename; }

	void SetDataOffset(size_t offset) { _offset = offset; }
	
	virtual size_t GetMemoryInUse() const;
	
	DeviceBuffer::HardPtr GetVertices() const;
		/// Возвращает буфер вершин
	
	DeviceBuffer::HardPtr GetIndices() const;
		/// Возвращает буфер индексов
	
	Material::HardPtr GetMaterial() const;
		/// Возвращает материал меша
	
	void SetMaterial(Material::HardPtr material);
		/// Устанавливает материал меша
	
	const MeshDataHardPtr GetData() const;
		/// Возвращает данные меша
	
	void SetData(MeshDataHardPtr data);
		/// Устанавливает данные меша

	void SetRenderTarget(Render::Target *target);
	
	virtual void LoadData();
		/// Загружает данные меша в аппаратные буферы
	
	virtual void UnloadData();
		/// Выгружает данные и освобождает аппаратные буферы
	
	virtual void Draw() const;
		/// Отрисовывает себя

	virtual bool Hit(const math::Vector3 &base, const math::Vector3 &dir) const;
		/// проверка попадания в меш
	
	static Mesh::HardPtr Create();
		/// Создаёт объект меша, но не заполняет его данными

protected:
	Mesh(Render::MeshVertexFormat FVF);
	
	Mesh(const Mesh& rhs);
	
	Mesh& operator = (const Mesh& rhs);

protected:
	std::string _filename;
		/// Имя двоичного файла с данными

	size_t _offset;
		/// Смещение в файле

	DeviceBuffer::HardPtr _vertices;
		/// Буфер вершин
	
	DeviceBuffer::HardPtr _indices;
		/// Буфер индексов
	
	Material::HardPtr _material;
		/// Материал меша
	
	MeshDataHardPtr _data;
		/// Данные меша
};

} // namespace SceneGraph

#endif // __MESH_H__
