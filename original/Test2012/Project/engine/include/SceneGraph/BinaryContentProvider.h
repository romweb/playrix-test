﻿#ifndef __BINARY_CONTENT_PROVIDER_H__
#define __BINARY_CONTENT_PROVIDER_H__

#ifdef _MSC_VER
#pragma once
#endif


#include "SceneGraph/ContentProvider.h"
#include "SceneGraph/ModelNode.h"
#include "SceneGraph/Mesh.h"
#include "SceneGraph/MeshData.h"
#include "SceneGraph/SkinnedMesh.h"
#include "SceneGraph/KeyFramedAnimation.h"
#include "SceneGraph/Material.h"

namespace SceneGraph {

/* Структура файла
	Версия
	Количество материалов, материалы
	Количество мешей, меши
	Количество узлов
	Узел
		Количество узлов
		Узел
		...
		Узел
		...
	Узел
	...
	Количество анимаций, анимации
*/

class BinaryContentProvider : public ContentProvider
{
public:
	ModelNode::HardPtr LoadModel(const std::string& filename);

	// Сохраняет модель в бинарник
	void Binarize(const std::string& filename, const std::string& binaryFilename);

	static void LoadMeshData(const std::string& filename, size_t offset, MeshData* data);
	static void LoadChannelsToAnimation(const std::string& filename, size_t offset, KeyFramedAnimation* animation);

private:
	// Вспомогательные методы загрузки
	//
	void ReadMaterials();
	void ReadMeshes();
	Mesh::HardPtr ReadMesh();
	void ReadNodes();
	SceneNode::HardPtr ReadNode(SceneNode::HardPtr parent);
	void ReadAnimations();
	std::string ReadString();
	math::Matrix4 ReadMatrix();
	void FindMeshNodes(ModelNode::HardPtr pModel, SceneNode::HardPtr pNode);

	static void ReadMeshDataFrom(IO::InputStream* stream, MeshData* pMeshData, int verticesCount, int facesCount);

	static void ReadStringFrom(IO::InputStream* stream, std::string& str);
	static void ReadMatrixFrom(IO::InputStream* stream, math::Matrix4& matrix);

	static void ReadChannelFrom(IO::InputStream* stream, KeyFramedAnimationChannel* channel = NULL);
	static void ReadChannelsFrom(IO::InputStream* stream, KeyFramedAnimation* animation = NULL);

	struct Context {
		typedef std::multimap<std::string, BoneContent::HardPtr> BonesMap;
		BonesMap bones;
	} _context;

	ModelNode::HardPtr _model;
	std::map<std::string, Material::HardPtr> _materials;	// Материалы модели
	std::map<std::string, Mesh::HardPtr> _meshes;			// Меши модели
	IO::InputStreamPtr _stream;
	std::string _srcName;
	bool _lazyLoad;

	// Вспомогательные методы сохранения
	//
	void WriteMaterials();
	void WriteMeshes();
	void WriteMesh(Xml::TiXmlElement *meshXml);
	void WriteNodes();
	void WriteNode(Xml::TiXmlElement *nodeXml);
	void WriteAnimations();
	void WriteAnimation(Xml::TiXmlElement *animationXml);
	void WriteChannel(Xml::TiXmlElement *channelXml);
	void WriteString(const char *str);
	void WriteMatrix(Xml::TiXmlElement *matrixXml);

	FILE *_dest;
	Xml::TiXmlElement *_src;
};

} // namespace SceneGraph



#endif // __BINARY_CONTENT_PROVIDER_H__