#ifndef __SOFTWARESKINNEDMESH_H__
#define __SOFTWARESKINNEDMESH_H__

#ifdef _MSC_VER
#pragma once
#endif // _MSC_VER

#include "SkinnedMesh.h"

#include <boost/intrusive_ptr.hpp>

namespace SceneGraph {

///
/// Скелетный меш с программным скиннингом.
///
class SoftwareSkinnedMesh : public SkinnedMesh {
public:
	typedef boost::intrusive_ptr<SoftwareSkinnedMesh> HardPtr;
	
	using Mesh::Vertex;
		/// Используется формат вершины статического меша
	
	Mesh::HardPtr Clone();
		/// Клонирует экземпляр объекта
	
	size_t GetMemoryInUse() const;
	
	void LoadData();
		/// Загружает данные меша в аппаратные буферы
	
	void UnloadData();
		/// Выгружает данные и освобождает аппаратные буферы
	
	void Draw() const;
		/// Рисует себя

	virtual bool Hit(const math::Vector3 &base, const math::Vector3 &dir) const;
		/// проверка попадания в модель с учетом текущей анимации модели
	
	static SoftwareSkinnedMesh::HardPtr Create();
		/// Создаёт объект меша, но не заполняет его данными

protected:
	SoftwareSkinnedMesh(Render::MeshVertexFormat FVF);
	
	SoftwareSkinnedMesh(const SoftwareSkinnedMesh& rhs);
	
	SoftwareSkinnedMesh& operator = (const SoftwareSkinnedMesh& rhs);
	
	void UpdateSkin() const;
		/// Обновляет данные в буфере вершин для нового положения костей

private:
	unsigned int _bonesCount;
		/// Количество костей
};

} // namespace SceneGraph

#endif // __SOFTWARESKINNEDMESH_H__
