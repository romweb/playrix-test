#ifndef __VERTEXBUFFERD3D9_H__
#define __VERTEXBUFFERD3D9_H__

#ifdef _MSC_VER
#pragma once
#endif // _MSC_VER

#include "SceneGraph/DeviceBuffer.h"
#include "Render/RenderTypes.h"

#include <d3d9.h>

namespace SceneGraph {

///
/// ����� ������.
///
class VertexBufferD3D9 : public DeviceBuffer {
public:
	typedef boost::intrusive_ptr<VertexBufferD3D9> HardPtr;
	
	VertexBufferD3D9(LPDIRECT3DVERTEXBUFFER9 vertices, Render::MeshVertexFormat FVF, std::size_t count, std::size_t stride);
	
	virtual ~VertexBufferD3D9();
	
	void* GetDeviceObject() const;
		/// ���������� ��������� �� �������� �����
	
    Render::MeshVertexFormat GetFVF() const;
		/// ���������� ������ ������, ���������� � ������
	
	void* Lock();
		/// ��������� ���� ����� ��� ������� � ��� ������
	
	void Unlock();
		/// ������������ �����.
		/// ������ ���� ������, ���� �������������� ����� Lock ��� �������� �������.

protected:
	LPDIRECT3DVERTEXBUFFER9 _vertices;
		/// ����� ������
	
    Render::MeshVertexFormat _FVF;
		/// ������ �������
};

} // namespace SceneGraph

#endif // __VERTEXBUFFERD3D9_H__
