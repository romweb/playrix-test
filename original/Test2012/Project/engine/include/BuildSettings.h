#ifndef __BUILDSETTINGS_H__
#define __BUILDSETTINGS_H__

#pragma once

#include "platform.h"

/// \file тут собраны конфигурационные флаги, влияющие на компиляцию движка

/// если выключить будет trial версия
#define FULL_VERSION

/// Установить, чтобы использовать нормальную координатную систему (верхний левый угол (0, 0))
//#define SCREEN_COORD_SYSTEM
/// Установить, чтобы эмулировать retina на windows (физическое разрешение будет в два раза больше логического)
#undef WINDOWS_RETINA

#if defined(ENGINE_TARGET_WIN32_DX9)
#define DXVERSION_ERROR_CAPTION		"DirectX Version"
#define DXVERSION_ERROR_MESSAGE		"Required DirectX 9.0 or later"
#endif

#define TRIAL_TIME					120

#define TIXML_USE_STL

#if !defined(USING_GLES2)
#define THREADED
#endif

#endif // __BUILDSETTINGS_H__
