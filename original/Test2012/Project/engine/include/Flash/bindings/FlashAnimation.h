#pragma once

#include "RefCounter.h"
#include "Render/RenderDevice.h"

class IFlashDisplayObject;
class IFlashMovieClip;

/**
* Класс для работы с Flash-анимацией в обход виджетов.
* Для использования совместно с fQuery не подходит, рекомендуется только если вы
* используете flash объекты как анимацию и не планируете делать их интерактивными.
*/
class FlashAnimation : public RefCounter {
public:
	FlashAnimation(const std::string &libId, const std::string &animationId, unsigned int fps = 0, bool dynamic = true);
	~FlashAnimation();
	
	///Устанавливает центр объекта (точку, относительно которой будет рисоваться объект)
	void SetHotSpot(float x, float y);
	IPoint GetHotSpot() const { return IPoint((int)_xSpot, (int)_ySpot); }

	void Draw(float x, float y, bool mirror = false);
	void Update(float dt);

	bool IsLastFrame();
	void Reset();

	/**Устанавливает воспроизведение в обратном порядке.
	* Данная функция не рекомендуется к использованию. Если вам действительно нужно 
	* воспроизводить анимации наоборот, обратитесь ко мне и я добавлю эту фичу более
	* хорошим способом. @Денис Степанов
	*/
	void SetReversed(bool rev);
	bool IsReversed() const { return _reversed; }

	int GetCurrentFrame();
	int GetLastFrame();

	//deprecated
    Render::Texture *GetTexture();
	//deprecated
    VertexBufferIndexed &GetVB();
	IFlashMovieClip* GetMovieClip() { return _movieClip; }

	IFlashDisplayObject* getDisplayObject();
private:
	float _xSpot;
	float _ySpot;
	IFlashMovieClip* _movieClip;
	Render::Texture *_texture;
	unsigned int _fps;       
	float _timeCounter;        
	bool _reversed;
};

typedef boost::intrusive_ptr<FlashAnimation> FlashAnimationPtr;
