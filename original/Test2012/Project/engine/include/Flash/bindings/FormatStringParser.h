#pragma once

#include "Flash/bindings/TextFormat.h"

freetype::charcode utf8Decoder(const std::string& str, size_t& index);
ITextElement* ParseFormatString(const std::string& source);