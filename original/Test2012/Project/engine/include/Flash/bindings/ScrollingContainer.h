#pragma once

#include "Flash/bindings/Scroller.h"
#include "Flash/core/FlashClipContainer.h"
#include "Flash/events/TouchEvents.h"
#include "Flash/events/EventManager.h"
#include "RefCounter.h"

class ScrollingContainer: public FlashDisplayObject, public IFlashSprite{
public:
	ScrollingContainer(float left, float top, float right, float bottom, bool horizontal, bool vertical);
	~ScrollingContainer();

	enum SpatialIndexType{
		IndexQTree,
		IndexHorizontal,
		IndexVertical,
		IndexNone
	};
	/**
	* ������������� ��� ����������������� ������� ��� ����������.
	* ���� ��������� ����� �������������� ��� ������������ �������, ���� �������� ��� ������� 
	* IndexHorizontal � IndexVertical ��������������
	* ���� ��������� ����� ����������, ���� �������� ��� IndexQTree
	* ���� ����� �������� � ���������� ����� ��������� (<100), ���� ������� �� ����� (>25%)
	* ������ ����� �����, ����� �������� ��� IndexNone
	*/
	void setIndexType(SpatialIndexType indexType);

	/**
	* ���������, ��� ������� ��� ��������� ������-���� �������� ���������� ����������.
	*/
	void contentChanged();

	/**
	* ������������� ������ ����������� ���������� (����� ������� ����� �������� ��� ���������)
	*/
	void setContentRect(float left, float top, float right, float bottom);
	void getContentRect(float& left, float& top, float& right, float& bottom);

	/**
	* �� ������� �������� ��������� �������� �� ���� ����������.
	* ������ � ���������� ContentRect �������� �������� ������� ���������.
	* ��������� - ���� overscroll �� ����� ��������� � ������������ ���������. �� ������������ ���
	* ������� �������������� �������� ��� autoContentRect.
	*/
	void setOverscroll(float left, float top, float right, float bottom);
	void getOverscroll(float& left, float& top, float& right, float& bottom);

	/**
	* ���� ��������, ������ ������� ��� ��������� ����� ������������� ����������� �� ������� �����������.
	*/
	void setAutoContentRect(bool autoSize);
	bool getAutoContentRect();

	/**
	* �������� � ��������� ������������ ��������� ������� �� ������������� �����������
	*/
	void setHorizontalKineticScroll(bool flag);
	void setVerticalKineticScroll(bool flag);
	KineticScroller* getHorizontalScroller();
	KineticScroller* getVerticalScroller();

	/**
	* ������ ������� ������� ���������
	*/
	void setClipRect(float left, float top, float right, float bottom);
	void getClipRect(float& left, float& top, float& right, float& bottom);

	void setHorizontalScroll(float scroll);
	void setVerticalScroll(float scroll);
	float getHorizontalScroll();
	float getVerticalScroll();

	/**
	* ������ ��������� ��������, ����� ������� ������ ������� ��������� (�� ��������� ����� ������� ����) 
	* (0.f, 0.f) - ����� ������� ����, (0.5f, 0.5f) �����, (1.f, 1.f) - ������ ������ ���� � �.�.
	*/
	void setContentAlign(float hAlign, float vAlign);

	IFlashDisplayObject* DisplayObject();
	IFlashSprite* Sprite();

	void setPosition(float x, float y);
	void getPosition(float& x, float& y);

	void setMatrix(const float matrix[6]);

	void render(FlashRender& render);
	bool hitTest(float x, float y, IHitTestDelegate* delegate);
	bool getBounds(float& left, float& top, float& right, float& bottom, IFlashDisplayObject* targetCoordinateSystem);
	
	void advance(FlashUpdateListener* updateListener, float dt);
	void update(float dt);
	bool hasUpdate() { return true; }

	IFlashDisplayObject* addChild(IFlashDisplayObject* displayObject);
	IFlashDisplayObject* removeChild(IFlashDisplayObject* displayObject);
	IFlashDisplayObject* addChildAt(IFlashDisplayObject* displayObject, int index);
	IFlashDisplayObject* removeChildAt(int index);
	IFlashDisplayObject* getChildAt(int index);
	IFlashDisplayObject* getChildByName(const std::string& string);
	void swapChildren(int index1, int index2);
	int getChildrenCount();

	GC_BLACKEN_DECL(ScrollingContainer)
private:
	void validateVisibleChildren();
	void validateIndex();
	void updateContentSizeFromContent();
	void updateScrollParams();

	void onTouchBegin(TouchEvent& event);
	void onTouchMove(TouchEvent& event);
	void onTouchEnd(TouchEvent& event);
	void onTouchCancel(TouchEvent& event);
	
	void startScroll();
	void stopScroll();
	void removeListeners();

	struct ChildContainer {
		float left, top, right, bottom;
		IFlashDisplayObject* object;
	};
	
	class SpatialIndex{
	public:
		SpatialIndex(ScrollingContainer* parent);
		void setIndexType(SpatialIndexType indexType);
		void update(float left, float top, float right, float bottom, std::vector<ChildContainer*>& content);
		void query(float left, float top, float right, float bottom, std::vector<ChildContainer*>& output);
	
		ScrollingContainer* parent;
		SpatialIndexType indexType;
	};
	friend class SpatialIndex;

	SpatialIndex spatialIndex;
	std::vector<ChildContainer*> children;
	std::vector<ChildContainer*> visibleChildren;

	float left, top, right, bottom;

	float contentLeft, contentTop, contentRight, contentBottom;
	float overscrollLeft, overscrollTop, overscrollRight, overscrollBottom;
	bool autoSize, validContentSize;
	float hAlign, vAlign;

	bool horizontalKineticScroll, verticalKineticScroll;
	KineticScroller horizontalScroller, verticalScroller;

	bool visibleChildrenValid;
	bool indexValid;

	bool horizontal;
	bool vertical;
	
	float xScroll, yScroll;

	bool selfCancelDispatch;
	bool scrollStarted;

	float grabX, grabY, lastX, lastY;
	float scrollGrabX, scrollGrabY;
	IFlashDisplayObject* root;
	HandlerID handlers[3];
	
};