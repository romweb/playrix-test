#pragma once

#include "Flash/core/FlashStage.h"
#include "Flash/core/FlashUpdateListener.h"
#include "GUI/Widget.h"

class FlashStageWidget: public GUI::Widget, public FlashStage{
public:
	static const std::string INITIALIZE;
	static const std::string DEINITIALIZE;

	FlashStageWidget(const std::string& name, rapidxml::xml_node<>* elem);
	~FlashStageWidget();

	virtual bool MouseDown(const IPoint& mouse_pos);
	virtual void MouseDoubleClick(const IPoint& mouse_pos);
	virtual void MouseUp(const IPoint& mouse_pos);
	virtual void MouseMove(const IPoint& mouse_pos);
	virtual void MouseWheel(int delta);
	virtual void Draw();
	virtual void Update(float);

	virtual void AcceptMessage(const Message& message);
protected:
	void initialize();
	void deinitialize();

	FlashUpdateListener listener;
	bool initialized;

	float mouseX, mouseY;
	bool mouseInput, mouseDown;

	bool interceptAllTouches;
};