#pragma once

#include "Flash/freetype/FreeTypeText.h"
#include "Flash/core/FlashTextStyle.h"

class TextFormat{
public:
	TextFormat(const FlashTextStyle& textStyle, freetype::FontInstance*, freetype::byte* = 0);

	freetype::FontInstance* getFontInstance();
	freetype::byte* getColor();
	float getLetterSpacing();

	const FlashTextStyle& getTextStyle();
private:
	freetype::FontInstance* font;
	freetype::byte color[4];
	FlashTextStyle textStyle;
};

freetype::Word* wordFromString(freetype::FontInstance* font, const std::string& string, float letterSpacing);
int charCodesFromString(const std::string& string, freetype::charcode** chars, int* length);

class IDictionary;

class ITextElement{
public:
	virtual ~ITextElement(){};
	virtual void print(freetype::Text* target, IDictionary* dictionary, TextFormat* baseTextFormat) = 0;
	virtual std::string getPlainText(IDictionary* dictionary) = 0;
};

class IDictionary{
public:
	virtual ~IDictionary(){};
	virtual ITextElement* get(const std::string& id) = 0;
};

class SubstitutionTextElement: public ITextElement, public IDictionary{
public:
	SubstitutionTextElement(const std::string& id);
	~SubstitutionTextElement();
	void print(freetype::Text* target, IDictionary* dictionary, TextFormat* baseTextFormat);
	std::string getPlainText(IDictionary* dictionary);
	void addParameter(const std::string& id, ITextElement* value);
	ITextElement* get(const std::string& id);
	
private:
	std::string id;
	std::map<std::string, ITextElement*> dictionary;
	IDictionary* baseDictionary;
};

class ParagraphTextElement: public ITextElement{
public:
	ParagraphTextElement();
	~ParagraphTextElement();
	void setWordWrap(ITextElement* textElement);
    void setWordBreak(ITextElement* textElement);
	void setAlign(ITextElement* textElement);
	void setLineInterval(ITextElement* textElement);
	void print(freetype::Text* target, IDictionary* dictionary, TextFormat* baseTextFormat);
	std::string getPlainText(IDictionary* dictionary);
private:
	ITextElement *align;
	ITextElement *wordWrap;
    ITextElement *wordBreak;
	ITextElement *lineInterval;
};

class StyleTextElement: public ITextElement{
public:
	StyleTextElement();
	~StyleTextElement();
	void setStyle(ITextElement* textElement);
	void setContent(ITextElement* textElement);
	void print(freetype::Text* target, IDictionary* dictionary, TextFormat* baseTextFormat);
	std::string getPlainText(IDictionary* dictionary);
private:
	ITextElement *style;
	ITextElement *content;
};


class FontTextElement: public ITextElement{
public:
	FontTextElement();
	~FontTextElement();
	void setFontName(ITextElement* textElement);
	void setFontSize(ITextElement* textElement);
	void setSpaceWidth(ITextElement* textElement);
	void setFontColor(ITextElement* textElement);
	void setFontEffects(ITextElement* textElement);
	void setLetterSpacing(ITextElement* textElement);
	void setContent(ITextElement *content);
	void print(freetype::Text* target, IDictionary* dictionary, TextFormat* baseTextFormat);
	std::string getPlainText(IDictionary* dictionary);
private:
	ITextElement *fontName;
	ITextElement *fontSize;
	ITextElement *fontColor;
	ITextElement *fontEffects;
	ITextElement *fontLetterSpacing;
	ITextElement *content;
	ITextElement *spaceWidth;
};

// Картинка
class SymbolTextElement: public ITextElement{
public:
	SymbolTextElement(ITextElement* id);
	void print(freetype::Text* target, IDictionary* dictionary, TextFormat* baseTextFormat);
	std::string getPlainText(IDictionary* dictionary);
private:
	ITextElement* id;
};

// Ссылка на переменную в IDictionary
class VariableTextElement: public ITextElement{
public:
	VariableTextElement(const std::string& id);
	void print(freetype::Text* target, IDictionary* dictionary, TextFormat* baseTextFormat);
	std::string getPlainText(IDictionary* dictionary);
private:
	std::string id;
};

class PlainTextElement: public ITextElement{
public:
	PlainTextElement(const std::string& text);
	void print(freetype::Text* target, IDictionary* dictionary, TextFormat* baseTextFormat);
	std::string getPlainText(IDictionary* dictionary);
private:
	std::string text;
};

class SequenceTextElement: public ITextElement{
public:
	SequenceTextElement();
	~SequenceTextElement();
	void print(freetype::Text* target, IDictionary* dictionary, TextFormat* baseTextFormat);
	void add(ITextElement* element);
	std::string getPlainText(IDictionary* dictionary);
private:
	std::vector<ITextElement*> elements;
};


template<class T>
struct MutationEq{
	T cached;
	bool initValue;

	MutationEq(){
		initValue = false;
	}

	bool operator()(const T* ref){
		if ( !initValue ){
			cached = *ref;
			return true;
		}
		if ( *ref == cached )
			return false;
		cached = *ref;
		return true;
	}
};

template<class T>
struct FormatLexCast{
	void operator()(const T* ref, freetype::charcode** chars, int* length){
		*length = charCodesFromString(utils::lexical_cast(*ref), chars, length);
	}
};

template<class T>
freetype::Word* createVariableWord(T* ref, freetype::FontInstance* fontInstance, const freetype::byte* color){
	return new freetype::DynamicWord<T, FormatLexCast<T>, MutationEq<T> >(ref, fontInstance, color);			
}

typedef size_t ptr_t;

// Ссылка на реальную переменную
template<class T, typename Format = FormatLexCast<T>, typename Mutation = MutationEq<T> >
class ReferenceTextElement: public ITextElement{
public:
	ReferenceTextElement(const T* _reference):reference(_reference){
	}

	void print(freetype::Text* target, IDictionary* dictionary, TextFormat* baseTextFormat){
		freetype::Word* word = new freetype::DynamicWord<T, Format, Mutation>(baseTextFormat->getFontInstance(), baseTextFormat->getColor(), reference);
		target->addWord(word);
	}

	std::string getPlainText(IDictionary* dictionary){
		Log::Warn("Getting plain text from dynamic word - may be unreliable");
		return "";
	}
private:
	const T* reference;
};

template<class T, typename Format, typename Mutation>
ReferenceTextElement<T, Format, Mutation>* createReferenceElement(ptr_t reference){
	return new ReferenceTextElement<T, Format, Mutation>((const T*)reference);
}
