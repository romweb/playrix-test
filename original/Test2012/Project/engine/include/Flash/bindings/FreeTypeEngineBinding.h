#pragma once

#include "Flash/freetype/FreeType.h"
#include "Flash/freetype/FreeTypeText.h"
#include "Flash/core/FlashTextStyle.h"

#if defined(ENGINE_TARGET_WIN32)
#	define FT_NEED_HALFPIXEL_OFFSET
#	define FT_COLOR_RGBA
#else
#	undef FT_NEED_HALFPIXEL_OFFSET
#	define FT_COLOR_BGRA
#endif

#define TEXT_CACHE_LIMIT 256
#define TEXT_CACHE_LIFETIME 32
#define FONTS_PRESCALE 1.0f

class IDictionary;

namespace Render {
	class Texture;
}

namespace FreeType{

	void initialize();

	freetype::ITexture* createTexture(Render::Texture* texture);
	freetype::IVertexStream* createVertexStream();
	freetype::IRender* createRenderer();

	freetype::InlinedObject* createInlinedTexture(Render::Texture* texture, float x, float y, float width, float height);
			
	/**
	* Устанавливает переменную в текстах.
	* Сбрасывает кэш текстов, поэтому менять часто не рекомендуется
	*/
	void setGlobalConstant(const std::string& id, const std::string& value);
	/**
	* Устанавливает переменную в текстах.
	* Работает только для простых текстов, внутри value строки не обрабатываются
	* специальные символы и подстановки. Зато данная конструкция не сбрасывает кэш
	* строк и эффективно работает для часто меняющихся элементов
	*/
	void setGlobalVariable(const std::string& id, const std::string& value);
	/**
	* Возвращает глобальный словарь подстановочных символов, используемый текстами
	*/
	IDictionary* getGlobalDictionary();
		
	template<class T>
	char convT(T&);

	///Возвращает строковую ссылку на переменную.
	///С помощью этой функции можно подставить в текст непосредственно переменную, например:
	/// textField->setText("Score: " + ref(score));
	///И при каждом изменении score текст на экране будет обновлен.
	template<class T>
	std::string ref(T& val){
		char textual[1024];
		sprintf(textual, "{ref %c %x}", convT<T>(val), &val);
		return textual;
	}

	freetype::FontInstance* createFont(FlashTextStyle* style);
	freetype::Text* getText(const std::string& source, FlashTextStyle* textStyle);
	void render(freetype::Text* text, unsigned char* color);
	void getLettersCoords(freetype::Text* text, std::vector<IPoint>& coords);
        
    void applyFontEffectTemplate( const std::string &type, freetype::FontEffectTemplate *creator );
}
