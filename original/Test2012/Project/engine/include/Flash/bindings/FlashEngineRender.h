#pragma once

#include "Flash/core/FlashRender.h"
#include "Flash/bindings/FreeTypeEngineBinding.h"
#include "Render/RenderDevice.h"
#include "Render/VertexBuffer.h"

class FlashEngineQuadBuffer: public IDeviceQuadBuffer{
public:
	FlashEngineQuadBuffer(){
		const int maxQuads = 8096;
		vbi.Init(maxQuads * 4, maxQuads * 6);
		for ( int i = 0; i < maxQuads; i++ ){
			vbi._ibuffer[i * 6 + 0] = i * 4 + 0;
			vbi._ibuffer[i * 6 + 1] = i * 4 + 1;
			vbi._ibuffer[i * 6 + 2] = i * 4 + 3;

			vbi._ibuffer[i * 6 + 3] = i * 4 + 1;
			vbi._ibuffer[i * 6 + 4] = i * 4 + 2;
			vbi._ibuffer[i * 6 + 5] = i * 4 + 3;
		}
		vbi.UploadIndex();
	}

	void upload(char* data, int quads){
		vbi.SetRawData(0, (Render::QuadVert*)data, quads * 4);
	}

	void draw(int quads){
		vbi.numVertices = quads * 4;
		vbi.numIndices = quads * 6;
		vbi.UploadVertex();
		Render::device.DrawIndexed(&vbi);		
	}

	VertexBufferIndexed vbi;
};

class FlashFreetypeTextRenderEngine: public IFlashTextRenderingEngine{
public:
	void renderText(FlashRender* render, TextObjectOutputParams& params){
		render->flush();
		render->invalidateConstant(FlashTextureCh0);

		TextObjectOutputParams* text = &params; 
		const float epsilon = 1e-5f;
		bool hasRotation = (text->matrix[3] * text->matrix[3] + text->matrix[1] * text->matrix[1]) > epsilon;
		float scale = 1.0f;

		if ( !hasRotation ){
			scale = ::sqrtf(text->matrix[0] * text->matrix[0] + text->matrix[4] * text->matrix[4]);
		}

		float matrix[16] =
		{
			text->matrix[0], text->matrix[3], 0.0f, 0.0f,
			text->matrix[1], text->matrix[4], 0.0f, 0.0f,
			0.0f,            0.0f,            1.0f, 0.0f,
			text->matrix[2], text->matrix[5], 0.0f, 1.0f
		};
		Render::device.PushMatrix();

		float diagA = ::sqrt(text->matrix[0] * text->matrix[0] + text->matrix[4] * text->matrix[4]);
		float diagB = ::sqrt(text->matrix[1] * text->matrix[1] + text->matrix[3] * text->matrix[3]);

		if ( ::fabs(diagB) < epsilon ){
			matrix[1] = 0.0f;
			matrix[4] = 0.0f;
			matrix[0] = matrix[0] / FONTS_PRESCALE;
			matrix[5] = matrix[5] / FONTS_PRESCALE;

			if ( text->snapToPixels ){
#						if defined(FT_NEED_HALFPIXEL_OFFSET)
				matrix[12] = ((int)((matrix[12]+ 0.5f) / matrix[0])) * matrix[0];
				matrix[13] = ((int)((matrix[13]+ 0.5f) / matrix[5])) * matrix[5];
#						else
				matrix[12] = ((int)(matrix[12] / matrix[0])) * matrix[0];
				matrix[13] = ((int)(matrix[13] / matrix[5])) * matrix[5];
#						endif
			}
		}

	
		Render::device.MatrixMultiply(math::Matrix4(matrix));
					
		unsigned char *color = (unsigned char*)&text->color;

#					if defined(FT_COLOR_RGBA)
			unsigned char color4[4] = {color[0], color[1], color[2], color[3]};
#					elif defined(FT_COLOR_BGRA)
			unsigned char color4[4] = {color[2], color[1], color[0], color[3]};
#					endif
	
		Render::device.SetBlendMode(Render::ALPHA);
				
		freetype::Text* ftText = FreeType::getText(std::string(text->text, text->textLength), text->textStyle);
		ftText->setLeft(0);
		ftText->setRight(text->width * FONTS_PRESCALE);
		ftText->setTop(0);
		ftText->setBottom(text->height * FONTS_PRESCALE);

		FreeType::render(ftText, color4);
			
					
		Render::device.PopMatrix();
	}
};

class FlashEngineRenderDevice: public IDeviceRender{
public:
	FlashEngineRenderDevice(){
		nextTexture = 1;
	}
	
	IDeviceQuadBuffer* getQuadBuffer(unsigned int quads){
		return &quadBuffer;
	}

	FlashTexture registerTexture(Render::Texture* texture){
		FlashTexture textureId = nextTexture++;
		textures.insert(std::make_pair(textureId, texture));
		return textureId;
	}

	void setShader(FlashRender* render, FlashConstantId id, FlashShader shader){
		throw std::logic_error("not supported");
	}

	void setConstantEnum(FlashRender* render, FlashConstantId id, FlashEnum x){
		switch ( id ){
		case FlashBlendMode:
			render->flush();
			switch ( x ){
			case FlashBlendNormal:
				Render::device.SetBlendMode(Render::ALPHA);
			break;
			case FlashBlendAdd:
				Render::device.SetBlendMode(Render::ADD);
			break;
			case FlashBlendMultiply:
				Render::device.SetBlendMode(Render::MULTIPLY);
			break;
			default:
				throw std::logic_error("invalid blend mode");
			break;
			}
		break;
		default:
			throw std::logic_error("invalid enum constant");
		}
	}

	void setConstantFloat(FlashRender* render, FlashConstantId id, float x){
		throw std::logic_error("not supported");
	}

	void setConstantVector(FlashRender* render, FlashConstantId id, float* v){
		throw std::logic_error("not supported");
	}

	void setConstantMatrix3x2(FlashRender* render, FlashConstantId id, float* m){
		throw std::logic_error("not supported");
	}

	void setConstantMatrix4x4(FlashRender* render, FlashConstantId id, float* m){
		throw std::logic_error("not supported");
	}

	void setTexture(FlashRender* render, FlashConstantId id, FlashTexture texture){
		render->flush();

		switch ( id ){
		case FlashTextureCh0:
			if ( texture ){
				Render::device.Bind(textures.at(texture));
			}
		break;
		default:
			throw std::logic_error("invalid texture bind");
		}
	}

	void getVertexBufferDescriptor(FlashRender* render, FlashVertexBuffer* vertexBuffer, void* vboBase){
		vertexBuffer->coordsBuffer = (FlashVertexCoords*)(((char*)vboBase) + (((unsigned int)&(((Render::QuadVert*)0)->x))));
		vertexBuffer->texCoordsBuffer = (FlashTexCoords*)(((char*)vboBase) + (((unsigned int)&(((Render::QuadVert*)0)->u))));
		vertexBuffer->colorBuffer = (FlashColor*)(((char*)vboBase) + (((unsigned int)&(((Render::QuadVert*)0)->color))));
		vertexBuffer->colorOffset = sizeof(Render::QuadVert) / 4;
		vertexBuffer->coordsOffset = sizeof(Render::QuadVert) / 4;
		vertexBuffer->texCoordsOffset = sizeof(Render::QuadVert) / 4;
	}

	struct ClipPlane{
		float x1, y1, x2, y2;

		ClipPlane(float x1, float y1, float x2, float y2){
			this->x1 = x1;
			this->y1 = y1;
			this->x2 = x2;
			this->y2 = y2;
		}
	};

	std::vector<ClipPlane> clipPlanes;
	std::vector<ClipPlane> usedClipPlanes;

	void beginClipPlane(float x1, float y1, float x2, float y2){
		clipPlanes.push_back(ClipPlane(x1, y1, x2, y2));
		updateClipPlanes();
	}

	void endClipPlane(){
		clipPlanes.pop_back();
		updateClipPlanes();
	}

	void updateClipPlanes(){
		while ( !usedClipPlanes.empty() ){
			usedClipPlanes.pop_back();
			Render::device.EndClipPlane();
		}
		
		//TODO:: ����� ����� �������� �����-������ �������� ���������� ������ plane'��
		for ( int i = 0; i < (int)clipPlanes.size(); i++ ){
			usedClipPlanes.push_back(clipPlanes[i]);
		}

		for ( int i = 0; i < (int)usedClipPlanes.size(); i++ ){
			ClipPlane plane = usedClipPlanes[i];
			float x1 = plane.x1, y1 = plane.y1, x2 = plane.x2, y2 = plane.y2;
			Render::device.BeginClipPlane(x2, y2, x1, y1);
		}
	}
	
	FlashTexture nextTexture;
	std::map<FlashTexture, Render::Texture*> textures;
	FlashEngineQuadBuffer quadBuffer;
};