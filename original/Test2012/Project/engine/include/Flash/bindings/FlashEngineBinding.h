/*
* В этом файле собраны функции, которые обеспечивают работу ядра libflash с
* нашим движком.
*/

#pragma once

#include "RefCounter.h"
#include "Flash/freetype/FreeTypeText.h"
#include "Flash/bindings/ICustomTransform.h"
#include "Flash/fx/FlashFX.h"

class DynamicLibraryChunkReader;

namespace Render {
	class Texture;
}

#define BUFFER_SIZE 8000

struct lua_State;

namespace Flash {

	struct FlashLibraryRecord;

	/// Инициализирует внутренние структуры. Можно вызывать несколько раз.
	void initialize();

	//Выполните следующие функции в своем проекте, чтобы использовать
	//libFlash из lua.

	///Выполняет привязку в lua новых функций
	void bindLua(lua_State* luaState);


	enum BlendMode {
		BlendNormal,
		BlendAdd
	};

	/**
	* Запрос на загрузку swl библиотеки
	* Данная структура редко нужна напрямую
	*/
	struct SwlRequest {
		std::string swlName;
		std::string textureName;
		std::string sheetName;

		//Использовать ли динамическую загрузку символов библиотеки
		bool isDynamic;

		SwlRequest(const std::string& swlName, const std::string& textureName, const std::string& sheetName, bool ownTexture = true, bool isDynamic = true) {
			this->swlName = swlName;
			this->textureName = textureName;
			this->sheetName = sheetName;
			this->isDynamic = isDynamic;
		}
	};

	/**
	* Информация о загрузке swl библиотеки
	* Данный класс редко нужен напрямую
	*/
	class SwlLoaderInfo : public RefCounter {
	public:
		~SwlLoaderInfo();

		IFlashLibrary* getLibrary();
		Render::Texture* getTexture();
		FlashTextureInfo getTextureInfo();
		int getDataSize();

	protected:
		SwlLoaderInfo();

	private:
		Render::Texture* texture;
		FlashTextureInfo textureInfo;
		IFlashLibraryPtr flashLibrary;
		
		MutableData loaderBytes;

		int dataSize;

		DynamicLibraryChunkReaderPtr chunkReader;
		bool isDynamic;

		friend SwlLoaderInfo* loadSwl(const SwlRequest& request);
		friend struct Flash::FlashLibraryRecord;
	};

	typedef boost::intrusive_ptr<SwlLoaderInfo> SwlLoaderInfoPtr;

	///Устанавливает режим смешивания
	void setBlendMode(BlendMode mode);

	void beginClipArea(float x, float y, float width, float height);
	void endClipArea();
	///Выполняет немедленную отрисовку объекта в текущий Render::device.
	void drawFlashDisplayObjectNow(IFlashDisplayObject* displayObject, bool keepContext = false);
	

	void getTextFieldContentMetrics(IFlashText* fText, float& left, float& top, float& right, float& bottom);
	
	///Устанавливает значение выравнивания текста по пикселям.
	///Если хоть кто-то в стеке установил false - текст перестает быть выровненым.
	void pushTextSnapping(bool value);
	void popTextSnapping();
	
	void setTexture(FlashTextureId id, Render::Texture* texture);
	Render::Texture* getTexture(FlashTextureId id);

	SwlLoaderInfo* loadSwl(const SwlRequest& request);
	void findSwl(const std::string& baseName, std::string* resultName);

	freetype::InlinedObject* createFlashInline(IFlashDisplayObject* displayObject, float x, float y, float width, float height);
}
