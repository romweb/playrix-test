#pragma once

#include "Flash/fquery/fquery.h"
#include "Flash/core/IPlaybackOperation.h"
#include "Flash/core/IFlashMovieClip.h"
#include "Flash/core/FlashDisplayObject.h"
#include "Flash/events/EventManager.h"
#include "Flash/events/FlashEvents.h"
#include "Flash/bindings/ScrollingContainer.h"
#include "Flash/bindings/FlashResourceManager.h"

namespace fquery {

	class FQueryEventsPlaybackOperation: public IPlaybackOperation {
	public:
		void onFrameConstructed(IFlashMovieClip* movieClip, int frame){
			PlaybackEvent event(PlaybackFrame, frame);
			EventManager::Get().dispatchTarget(movieClip->DisplayObject(), event);
		}

		bool onAnimationCompleted(IFlashMovieClip* movieClip){
			PlaybackEvent event(PlaybackEnd, movieClip->getCurrentFrame());
			EventManager::Get().dispatchTarget(movieClip->DisplayObject(), event);
			return !(event.isDefaultPrevented());
		}

		void onAnimationRestarted(IFlashMovieClip* movieClip){
			PlaybackEvent event(PlaybackRestarted, movieClip->getCurrentFrame());
			EventManager::Get().dispatchTarget(movieClip->DisplayObject(), event);
		}

		void onMovieDisposed(IFlashMovieClip* movieClip){
		}
	};

	class LocalStorage: public IGCRef{
	public:
		LocalStorage(){}
		~LocalStorage(){}

		template<class T>
		T* get(const std::string& id){
			return dynamic_cast<T*>(data.at(id));
		}

		template<class T>
		void set(const std::string& id, T* t){
			data.insert(std::make_pair(id, t));
		}

		bool has(const std::string& id){
			return data.count(id) > 0;
		}

		IGCRef* get_raw(const std::string& id){
			return data.at(id);
		}

		void set_raw(const std::string& id, IGCRef* value){
			data.insert(std::make_pair(id, value));
		}

		GC_BLACKEN_INLINE(LocalStorage){
			for ( auto it = data.begin(); it != data.end(); it++ ){
				GC_SHOULD_MARK(it->second);
			}
		}		
	private:
		std::map<std::string, IGCRef*> data;
	};

	class LocalStorageDictionary: public IGCRef{
	public:
		LocalStorageDictionary(){
			GC_SHOULD_TRACK(this);
			GC_ADD_REF(this); //root
		}

		~LocalStorageDictionary(){
			GC_REMOVE_REF(this);
		}

		void removeStorage(IFlashDisplayObject* target){
			if ( storages.count(target) != 0 ){
				delete storages.at(target);
				storages.erase(target);
			}
		}

		LocalStorage* getStorage(IFlashDisplayObject* target){
			if ( storages.count(target) == 0 ){
				LocalStorage* storage = new LocalStorage();
				storages.insert(std::make_pair(target, storage));
			}
			return storages.at(target);
		}

		GC_BLACKEN_INLINE(LocalStorageDictionary){
			for ( auto it = storages.begin(); it != storages.end(); it++ ){
				GC_SHOULD_MARK(it->second);
			}
		}
		
	private:
		std::map<IFlashDisplayObject*, LocalStorage*> storages;
	};

	struct Vec2{
		float x, y;
		Vec2(){
			x = y = 0;
		}
		Vec2(float x, float y){
			this->x = x;
			this->y = y;
		}
	};

	static inline int gcd(int a, int b){
		if ( b == 0 )
			return a;
		return gcd(b, a % b);
	}

	static inline int lcd(int a, int b){
		return a * b / gcd(a, b);
	}

	class FQuerySelector;
	IFlashDisplayObject* DisplayObjectCast(IFlashDisplayObject* t);
	IFlashDisplayObject* DisplayObjectCast(FQuerySelector& t);
	
	inline IFlashDisplayObject* DisplayObjectCast(IFlashDisplayObject* t){
		return t;
	}
	
	inline IFlashDisplayObject* DisplayObjectCast(IFlashConcreteDisplayObject* t){
		return t->DisplayObject();
	}
	
	inline IFlashDisplayObject* DisplayObjectCast(FlashDisplayObject* t){
		return t;
	}

	inline IFlashDisplayObject* DisplayObjectCast(ScrollingContainer* t){
		return t->DisplayObject();
	}

	inline IFlashDisplayObject* DisplayObjectCast(FlashSprite* t){
		return t->DisplayObject();
	}
	
	class FQuerySelector{
		static const int InplaceTargets = 4;

	public:
		//TODO:: Thread local
		static IFlashDisplayObject* context;
	public:

		FQuerySelector(){
			numTargets = 0;
			targets = inplace;
			allocated = InplaceTargets;
		}

		~FQuerySelector(){
			for ( int i = 0; i < numTargets; i++ ){
				GC_REMOVE_REF(targets[i]);
			}
			if ( allocated > InplaceTargets ){
				delete[] targets;
			}
		}

		FQuerySelector(const FQuerySelector& other){
			numTargets = 0;
			targets = inplace;
			allocated = InplaceTargets;

			if ( other.numTargets > InplaceTargets ){
				reallocate(other.numTargets);
			}

			for ( int i = 0; i < other.numTargets; i++ ){
				targets[i] = other.targets[i];
				GC_ADD_REF(targets[i]);
			}
			numTargets = other.numTargets;
		}

		FQuerySelector& operator=(const FQuerySelector& other){
			for ( int i = 0; i < other.numTargets; i++ ){
				GC_ADD_REF(other.targets[i]);
			}
			for ( int i = 0; i < numTargets; i++ ){
				GC_REMOVE_REF(targets[i]);
			}
			numTargets = other.numTargets;
			if ( allocated < other.numTargets ){
				reallocate(other.numTargets);
			}
			for ( int i = 0; i < other.numTargets; i++ ){
				targets[i] = other.targets[i];
			}
			return *this;
		}

		/*
		* Добавляет объект t в селектор
		*/
		template<class T>
		FQuerySelector& add(T t){
			IFlashDisplayObject* target = DisplayObjectCast(t);
			if ( target == 0 ){
				return *this;
			}

			if ( numTargets >= allocated ){
				reallocate(allocated * 2 + 1);
			}
			targets[numTargets++] = target;
			GC_ADD_REF(target);
			return *this;
		}

		/*
		* Добавляет объект t в контейнер. Если T - строка, создает и добавляет во все выбранные контейнеры
		* символ с заданным идентификатором.
		*/
		template<class T>
		FQuerySelector append(T t){
			FQuerySelector newSelector;
			IFlashDisplayObject* target = DisplayObjectCast(t);

			if ( numTargets > 1 ){
				throw std::runtime_error("can't append object to multiple targets");
			}else if ( numTargets == 1 ){
				IFlashSprite* sprite = targets[0]->Sprite();
				if ( !sprite ){
					sprite = FQuerySelector().add(targets[0]).wrap().get()->Sprite();
				}
				sprite->addChild(target);
				newSelector.add(target);
				return newSelector;
			}else{
				return newSelector;
			}
			
		}

		
		FQuerySelector append(const std::string& name){
			FQuerySelector selector;
			FQuerySelector newSelector;
			for ( int i = 0; i < numTargets; i++ ){
				IFlashDisplayObject* newDisplayObject = Flash::FlashResourceManager::instance->createItem(name);
				if ( newDisplayObject ){
					selector = selector.select(targets[i]).append(newDisplayObject);
					if ( selector.numTargets > 0 ){						
						newSelector.add(selector.targets[0]);						
					}
				}
			}
			return newSelector;
		}

		FQuerySelector append(const char* s){
			return append(std::string(s));
		}

		template<class T>
		FQuerySelector& replace(T t){
			IFlashDisplayObject* target = DisplayObjectCast(t);

			if ( numTargets > 1 ){
				throw std::runtime_error("can't append object to multiple targets");
			}else if ( numTargets == 1 ){
				IFlashSprite* sprite = targets[0]->getParent()->Sprite();
				if ( !sprite ){
					return clear();
				}
				GC_ADD_REF(target);
				GC_ADD_REF(sprite->DisplayObject());
				target->applyTransform(targets[0]);
				target->setName(targets[0]->getName());
				sprite->addChildAt(target, sprite->getChildIndex(targets[0]));
				sprite->removeChild(targets[0]);
				GC_REMOVE_REF(targets[0]);
				GC_REMOVE_REF(sprite->DisplayObject());
				targets[0] = target;
				return *this;
			}else{
				return clear();
			}
		}


		FQuerySelector& replace(const std::string& name){
			int j = 0;
			for ( int i = 0; i < numTargets; i++ ){
				IFlashSprite* parent = targets[i]->getParent()->Sprite();
				if ( !parent ){
					GC_REMOVE_REF(targets[i]);
					continue;
				}
				GC_ADD_REF(parent->DisplayObject());
				IFlashDisplayObject* newDisplayObject = Flash::FlashResourceManager::instance->createItem(name);
				if ( newDisplayObject ){
					parent->addChildAt(newDisplayObject, parent->getChildIndex(targets[i]));
					newDisplayObject->applyTransform(targets[i]);
					newDisplayObject->setName(targets[i]->getName());
					parent->removeChild(targets[i]);
					GC_REMOVE_REF(targets[i]);
					GC_ADD_REF(newDisplayObject);				
					targets[j] = newDisplayObject;
					j++;
				}else{
					GC_REMOVE_REF(targets[i]);
				}
				GC_REMOVE_REF(parent->DisplayObject());
			}
			numTargets = j;
			return *this;
		}

		FQuerySelector& replace(const char* name){
			return replace(std::string(name));
		}

		FQuerySelector& empty(){
			for ( int i = 0; i < numTargets; i++ ){
				IFlashSprite* parent = targets[i]->Sprite();
				if ( parent ){
					while ( parent->getChildrenCount() > 0 ){
						parent->removeChildAt(parent->getChildrenCount() - 1);
					}
				}
			}
			return *this;
		}

		FQuerySelector& detach(){
			for ( int i = 0; i < numTargets; i++ ){
				IFlashDisplayObject* parent = targets[i]->getParent();
				if ( parent ){
					IFlashSprite* sprite = parent->Sprite();
					if ( sprite ){
						sprite->removeChild(targets[i]);
					}
				}
			}
			return *this;
		}

		/*
		* Возвращает элемент из селектора по индексу
		*/
		IFlashDisplayObject* get(int index = 0){
			if ( index < 0 || index >= numTargets ){
				throw std::runtime_error("index out of bounds");
			}
			return targets[index];
		}

		IFlashDisplayObject* maybeGet(int index = 0){
			if ( index < 0 || index >= numTargets ){
				return 0;
			}
			return targets[index];
		}

		/**
		* Возвращает число элементов селектора
		*/
		int size(){
			return numTargets;
		}
		
		/*
		* Оборачивает объекты селектора в контейнеры и возвращает новый селектор, содержащий эти
		* контейнеры
		*/
		FQuerySelector wrap(){
			FQuerySelector newSelector;
			for ( int i = 0; i < numTargets; i++ ){
				IFlashSprite* sprite = createFlashSprite();
				IFlashDisplayObject* parent = targets[i]->getParent();
				IFlashSprite* parentSprite = parent->Sprite();
				if ( !parentSprite ){
					throw std::runtime_error("parent is not a sprite - can't wrap");
				}
				int index = parentSprite->getChildIndex(targets[i]);
				parentSprite->removeChildAt(index);
				parentSprite->addChildAt(targets[i], index);
				sprite->DisplayObject()->applyTransform(targets[i]);
				float identity[] = {
					1, 0, 0,
					0, 1, 0
				};
				targets[i]->setMatrix(identity);
				targets[i]->setColor(0xffffff);
				targets[i]->setAlpha(1.0f);
				newSelector.add(sprite->DisplayObject());
			}
			return newSelector;
		}

		/*
		* Выбирает объект t в селектор, сбрасывая текущий выбор
		*/
		template<class T>
		FQuerySelector& select(T t){
			IFlashDisplayObject* target = DisplayObjectCast(t);

			for ( int i = 0; i < numTargets; i++ ){
				GC_REMOVE_REF(targets[i]);
			}
			targets[0] = target;
			GC_ADD_REF(target);
			numTargets = 1;
			return *this;
		}

		/*
		* Выбирает дочерние элементы объектов селектора
		*/
		FQuerySelector children(){
			FQuerySelector newSelector;
			for ( int i = 0; i < numTargets; i++ ){
				IFlashSprite* sprite = targets[i]->Sprite();
				if ( sprite ){
					for ( int j = 0; j < sprite->getChildrenCount(); j++ ){
						newSelector.add(sprite->getChildAt(j));
					}
				}
			}
			return newSelector;
		}

		/**
		* Возвращает родительские элементы объектов селектора
		*/
		FQuerySelector parent(){
			FQuerySelector newSelector;
			for ( int i = 0; i < numTargets; i++ ){
				IFlashDisplayObject* newTarget = targets[i]->getParent();
				if ( newTarget ){
					newSelector.add(newTarget);
				}
			}
			return newSelector;
		}

		/**
		* Убирает из селектора повторяющиеся элементы
		*/
		FQuerySelector& unique(){
			int k = 0;
			for ( int i = 0; i < numTargets; i++ ){
				GC_REMOVE_REF(targets[i]);
				int j;
				for ( j = 0; j < k; j++ ){
					if ( targets[j] == targets[i] ){
						break;
					}
				}
				if ( j >= k ){
					GC_ADD_REF(targets[i]);
					targets[k] = targets[i];
					k++;
				}
			}
			numTargets = k;
			return *this;
		}

		/**
		* Выбирает дочерние элементы по имени
		*/
		FQuerySelector children(const std::string& name){
			FQuerySelector newSelector;

			for ( int i = 0; i < numTargets; i++ ){
				IFlashDisplayObject* newTarget = targets[i]->queryChild(name);
				if ( newTarget ){
					newSelector.add(newTarget);
				}
			}

			return newSelector;
		}

		FQuerySelector children(const std::string& name1, const std::string& name2){
			FQuerySelector newSelector;
			for ( int i = 0; i < numTargets; i++ ){
				IFlashDisplayObject* newTarget;
				
				newTarget = targets[i]->queryChild(name1);
				if ( newTarget ){
					newSelector.add(newTarget);
				}

				newTarget = targets[i]->queryChild(name2);
				if ( newTarget ){
					newSelector.add(newTarget);
				}
			}
			return newSelector;
		}

		FQuerySelector children(const std::string& name1, const std::string& name2, const std::string& name3){
			FQuerySelector newSelector;
			for ( int i = 0; i < numTargets; i++ ){
				IFlashDisplayObject* newTarget;
				
				newTarget = targets[i]->queryChild(name1);
				if ( newTarget ){
					newSelector.add(newTarget);
				}

				newTarget = targets[i]->queryChild(name2);
				if ( newTarget ){
					newSelector.add(newTarget);
				}

				newTarget = targets[i]->queryChild(name3);
				if ( newTarget ){
					newSelector.add(newTarget);
				}
			}
			return newSelector;
		}

		FQuerySelector children(const std::string& name1, const std::string& name2, const std::string& name3, const std::string& name4){
			FQuerySelector newSelector;
			for ( int i = 0; i < numTargets; i++ ){
				IFlashDisplayObject* newTarget;
				
				newTarget = targets[i]->queryChild(name1);
				if ( newTarget ){
					newSelector.add(newTarget);
				}

				newTarget = targets[i]->queryChild(name2);
				if ( newTarget ){
					newSelector.add(newTarget);
				}

				newTarget = targets[i]->queryChild(name3);
				if ( newTarget ){
					newSelector.add(newTarget);
				}

				newTarget = targets[i]->queryChild(name4);
				if ( newTarget ){
					newSelector.add(newTarget);
				}
			}
			return newSelector;
		}

		template<class It>
		FQuerySelector children(It begin, It end){
			FQuerySelector newSelector;
			for ( int i = 0; i < numTargets; i++ ){
				IFlashDisplayObject* newTarget;				
				It it = begin;
				for ( It it = begin; it != end; it++ ){
					newTarget = targets[i]->queryChild(*it);
					if ( newTarget ){
						newSelector.add(newTarget);
					}
				}
			}
			return newSelector;
		}

		/**
		* Применяет функцию ко всем объектам селектора
		*/
		template<class F>
		FQuerySelector& apply(F f){
			IFlashDisplayObject* savedContext = context;
			for ( int i = 0; i < numTargets; i++ ){
				context = targets[i];
				f(targets[i]);				
			}
			context = savedContext;
			return *this;
		}

		template<class F>
		FQuerySelector& with(F f){
			FQuerySelector copy(*this);
			f(copy);
			return *this;
		}

		/**
		* Применяет фильтр к объектам селектора
		*/
		template<class F>
		FQuerySelector& filter(F f){
			int j = 0;
			for ( int i = 0; i < numTargets; i++ ){
				if ( !(bool)f(targets[i]) ){
					GC_REMOVE_REF(targets[i]);
				}else{
					targets[j] = targets[i];
					j++;
				}				
			}
			numTargets = j;
			return *this;
		}

		/**
		* Находит ближайший объект в иерархии, удовлетворяющий условию
		*/
		template<class F>
		FQuerySelector closest(F f){
			FQuerySelector newSelector;
			for ( int i = 0; i < numTargets; i++ ){
				IFlashDisplayObject* child = targets[i];
				while ( child ){
					if ( (bool)f(child) ){
						newSelector.add(child);
						break;
					}
					child = child->getParent();
				}
			}
			return newSelector;
		}

		/**
		* Очищает селектор
		*/
		FQuerySelector& clear(){
			for ( int i = 0; i < numTargets; i++ ){
				GC_REMOVE_REF(targets[i]);		
			}
			numTargets = 0;
			return *this;
		}

		/**
		* Устанавливает обработчик события
		*/
		FQuerySelector& bind(const std::string& event, EventHandler handler, bool capture = false){
			for ( int i = 0; i < numTargets; i++ ){
				EventManager::Get().addEventListener(targets[i], event, handler, capture);
			}
			return *this;
		}

		/**
		* Снимает обработчик события
		*/
		FQuerySelector& unbind(const std::string& event, EventHandler handler){
			for ( int i = 0; i < numTargets; i++ ){
				EventManager::Get().removeEventListener(targets[i], event, handler);
			}
			return *this;
		}

		/**
		* Снимает все обработчики событий
		*/
		FQuerySelector& unbindAll(){
			for ( int i = 0; i < numTargets; i++ ){
				EventManager::Get().removeEventListeners(targets[i]);
			}
			return *this;
		}

		
		/**
		* Вписывает объект в заданный прямоугольник
		*/
		FQuerySelector& fit(float left, float top, float right, float bottom){
			for ( int i = 0; i < numTargets; i++ ){
				float l, t, r, b;
				targets[i]->getBounds(l, t, r, b, targets[i]->getParent());
				float w = r - l;
				float h = b - t;
				float scaleX = (right - left) / w;
				float scaleY = (bottom - top) / h;
				float scale = std::min(scaleX, scaleY);
				targets[i]->getScale(scaleX, scaleY);
				targets[i]->setScale(scaleX * scale, scaleY * scale);
				targets[i]->getBounds(l, t, r, b, targets[i]->getParent());
				float x, y;
				targets[i]->getPosition(x, y);
				float dw = ((right - left) - (r - l)) * 0.5f;
				float dh = ((bottom - top) - (b - t)) * 0.5f;
				targets[i]->setPosition(x + (left - l) + dw, y + (top - t) + dh);
			}
			return *this;
		}
		
		/**
		* Отправляет событие объектам
		*/
		FQuerySelector& send(FlashEvent& event){
			for ( int i = 0; i < numTargets; i++ ){
				EventManager::Get().dispatch(targets[i], event);
			}
			return *this;
		}

		/**
		* Устанавливает текст для текстового поля
		*/
		FQuerySelector& text(const std::string& text){
			for ( int i = 0; i < numTargets; i++ ){
				if ( targets[i]->Text() ){
					targets[i]->Text()->setText(text);
				}
			}
			return *this;
		}

		/**
		* Возвращает текст объекта
		*/
		const std::string& text(int index = 0){
			IFlashDisplayObject* object = get(index);
			if ( !object->Text() ){
				throw std::runtime_error("not a text field");
			}
			return object->Text()->getText();
		}

		/**
		* Устанавливает имя объекта
		*/
		FQuerySelector& name(const std::string& name){
			for ( int i = 0; i < numTargets; i++ ){
				targets[i]->setName(name);
			}
			return *this;
		}

		/**
		* Возвращает имя объекта
		*/
		const std::string& name(int index = 0){
			IFlashDisplayObject* object = get(index);
			return object->getName();
		}

		/**
		* Получает фактические границы текста в текстовом поле
		*/
		FQuerySelector& textBounds(float &left, float &top, float &right, float &bottom){
			IFlashText* text = get(0)->Text();
			if ( text ){
				Flash::getTextFieldContentMetrics(text, left, top, right, bottom);
			}else{
				bounds(left, top, right, bottom);
			}
			return *this;
		}

		/**
		* Получает фактические размеры текста в текстовом поле
		*/
		FQuerySelector& textSize(float &width, float &height){
			float left, top, right, bottom;
			textBounds(left, top, right, bottom);
			width = right - left;
			height = bottom - top;
			return *this;
		}

		/**
		* Получает границы объекта
		*/
		FQuerySelector& bounds(IFlashDisplayObject* target, float &left, float &top, float &right, float &bottom){
			get(0)->getBounds(left, top, right, bottom, target);
			return *this;
		}

		/**
		* Получает границы объекта
		*/
		FQuerySelector& bounds(float &left, float &top, float &right, float &bottom){
			get(0)->getBounds(left, top, right, bottom, 0);
			return *this;
		}

		Vec2 position(int index = 0){
			float x, y;
			get(index)->getPosition(x, y);
			return Vec2(x, y);
		}

		FQuerySelector& position(Vec2 vec){
			return position(vec.x, vec.y);
		}

		FQuerySelector& position(float x, float y){
			for ( int i = 0; i < numTargets; i++ ){
				targets[i]->setPosition(x, y);
			}
			return *this;
		}

		Vec2 scale(int index = 0){
			float x, y;
			get(index)->getScale(x, y);
			return Vec2(x, y);
		}

		FQuerySelector& scale(Vec2 vec){
			return scale(vec.x, vec.y);
		}

		FQuerySelector& scale(float x, float y){
			for ( int i = 0; i < numTargets; i++ ){
				targets[i]->setScale(x, y);
			}
			return *this;
		}

		float rotation(int index = 0){
			return get(index)->getRotation();
		}

		FQuerySelector& rotation(float x){
			for ( int i = 0; i < numTargets; i++ ){
				targets[i]->setRotation(x);
			}
			return *this;
		}

		float skew(int index = 0){
			return get(index)->getShear();
		}

		FQuerySelector& skew(float x){
			for ( int i = 0; i < numTargets; i++ ){
				targets[i]->setShear(x);
			}
			return *this;
		}

		float alpha(int index = 0){
			return get(index)->getAlpha();
		}

		FQuerySelector& alpha(float x){
			for ( int i = 0; i < numTargets; i++ ){
				targets[i]->setAlpha(x);
			}
			return *this;
		}

		FQuerySelector& show(bool flag = true){
			for ( int i = 0; i < numTargets; i++ ){
				targets[i]->setVisible(flag);
			}
			return *this;
		}

		FQuerySelector& hide(bool flag = true){
			for ( int i = 0; i < numTargets; i++ ){
				targets[i]->setVisible(!flag);
			}
			return *this;
		}

		float rate(){
			return get()->getUpdateRate();
		}

		FQuerySelector& rate(float x){
			for ( int i = 0; i < numTargets; i++ ){
				targets[i]->setUpdateRate(x);
			}
			return *this;
		}

		LocalStorage* data(int index = 0){
			if ( !storage ){
				storage = new LocalStorageDictionary();
			}
			return storage->getStorage(get(index));
		}

		static void clearStorageFor(IFlashDisplayObject* target){
			if ( storage ){
				storage->removeStorage(target);
			}
		}

		/*
		movie clips
		*/
		int frames(){
			int frames = 1;
			for ( int i = 0; i < numTargets; i++ ){
				if ( targets[i]->MovieClip() ){
					frames = lcd(targets[i]->MovieClip()->countFrames(), frames);
				}
			}
			return frames;
		}

		FQuerySelector& play(bool state = true){
			for ( int i = 0; i < numTargets; i++ ){
				if ( targets[i]->MovieClip() ){
					targets[i]->MovieClip()->setPlayback(state);
				}
			}
			return *this;
		}
		
		FQuerySelector& stop(){
			return play(false);
		}

		int frame(){
			IFlashMovieClip* movieClip = get(0)->MovieClip();
			if ( movieClip ){
				return movieClip->getCurrentFrame();
			}
			return 0;
		}

		FQuerySelector& frame(int frame){
			for ( int i = 0; i < numTargets; i++ ){
				if ( targets[i]->MovieClip() ){
					targets[i]->MovieClip()->gotoFrame(frame);
				}
			}
			return *this;
		}

		FQuerySelector& frame(const std::string& label){
			for ( int i = 0; i < numTargets; i++ ){
				if ( targets[i]->MovieClip() ){
					targets[i]->MovieClip()->gotoLabel(label);
				}
			}
			return *this;
		}

		/**
		* Активирует события воспроизведения для клипа (PlaybackFrame, PlaybackEnd, PlaybackRestarted)
		*/
		FQuerySelector& listenPlayback(){
			if ( !fQueryEventsPlaybackOperation ){
				fQueryEventsPlaybackOperation = new FQueryEventsPlaybackOperation();
			}

			for ( int i = 0; i < numTargets; i++ ){
				if ( targets[i]->MovieClip() ){
					targets[i]->MovieClip()->setPlaybackOperation(fQueryEventsPlaybackOperation);
				}
			}
			return *this;
		}

		FQuerySelector& disable(){
			FlashEvent event("disable");
			this->send(event);
			return *this;
		}

		FQuerySelector& enable(){
			FlashEvent event("enable");
			this->send(event);
			return *this;
		}

		FQuerySelector& check(){
			FlashEvent event("check");
			this->send(event);
			return *this;
		}

		FQuerySelector& uncheck(){
			FlashEvent event("uncheck");
			this->send(event);
			return *this;
		}

		FQuerySelector& value(bool value){
			ValueEvent<bool> event("setBoolValue", value);
			this->send(event);
			return *this;
		}

		FQuerySelector& value(double value){
			ValueEvent<double> event("setNumberValue", value);
			this->send(event);
			return *this;
		}

		bool queryBool(){
			QueryEvent<bool> event("queryBoolValue");
			this->send(event);
			return event.get();
		}

		double queryNumber(){
			QueryEvent<double> event("queryNumberValue");
			this->send(event);
			return event.get();
		}

		bool queryBool(bool def){
			QueryEvent<bool> event("queryBoolValue");
			this->send(event);
			return event.get(def);
		}

		double queryNumber(double n){
			QueryEvent<double> event("queryNumberValue");
			this->send(event);
			return event.get(n);
		}
		
		class iterator: public std::iterator<std::bidirectional_iterator_tag, IFlashDisplayObject*>{
		public:
			typedef IFlashDisplayObject* value_type;

			iterator(FQuerySelector* selector, int index):selector(selector), index(index){
			}

			bool operator==(iterator const& rhs) const 
			{
				return (selector == rhs.selector) && (index == rhs.index || invalid_index(index) && invalid_index(rhs.index));
			}

			bool operator!=(iterator const& rhs) const 
			{
				return !(*this==rhs);
			}

			iterator& operator++() 
			{
				index++;
				return *this;
			}   

			iterator operator++(int) 
			{
				iterator tmp (*this);
				++(*this);
				return tmp;
			}

			iterator& operator--() 
			{
				if ( index >= 0 ){
					index--;
				}
				return *this;
			}

			iterator operator--(int) 
			{
				iterator tmp (*this);
				--(*this);
				return tmp;
			}

			value_type operator* () const 
			{
				return selector->get(index);
			}

		private:
			bool invalid_index(int index) const{
				return index < 0 || index >= selector->size();
			}

			FQuerySelector* selector;
			int index;
		};

		iterator begin(){
			return iterator(this, 0);
		}

		iterator end(){
			return iterator(this, -1);
		}

		static IFlashDisplayObject* pushContext(IFlashDisplayObject* context){
			IFlashDisplayObject* old = FQuerySelector::context;
			FQuerySelector::context = context;
			return old;
		}

		static void restoreContext(IFlashDisplayObject* context){
			FQuerySelector::context = context;
		}
	private:
		static LocalStorageDictionary* storage;
		static FQueryEventsPlaybackOperation* fQueryEventsPlaybackOperation;

		void reallocate(int size){
			IFlashDisplayObject** newTargets = new IFlashDisplayObject*[size];
			memcpy(newTargets, targets, numTargets * sizeof(IFlashDisplayObject*));
			if ( allocated > InplaceTargets ){
				delete[] targets;
			}
			allocated = size;
			targets = newTargets;
		}

		int numTargets, allocated;
		IFlashDisplayObject** targets;
		IFlashDisplayObject* inplace[InplaceTargets];
	};

	template<class T>
	FQuerySelector fQuery(T t){
		return FQuerySelector().add(t);
	}

	inline IFlashDisplayObject* DisplayObjectCast(FQuerySelector& t){
		return t.get(0);
	}

};