#pragma once

#include "RefCounter.h"

namespace freetype {
	typedef unsigned long charcode;
	typedef unsigned long glyphindex;

	const glyphindex USERGLYPHS = 0xffffff00;
	const glyphindex NEWLINE = 0xffffff30;
	const glyphindex SPACE = 0xffffff10;
	const glyphindex TAB = 0xffffff20;

	typedef unsigned char byte;

	//представление глифа на текстуре
	struct GPUGlyph {
		float u1, v1, u2, v2;
		float width, height;
		float offsetX, offsetY;
		float advanceX, advanceY;
	};

	//Текстура должна реализовывать этот интерфейс
	class ITexture : public RefCounter {
	public:
		virtual void upload(int x, int y, int width, int height, byte* data) = 0;
	};

	typedef boost::intrusive_ptr<ITexture> ITexturePtr;

	struct Vertex {
		float x, y, z;
		byte color[4];
		float u, v;		
	};

	//Буфер вершин должен реализовывать этот интерфейс
	class IVertexStream : public RefCounter {
	public:
		virtual void lock(Vertex** data, int verticiesRequired) = 0;
		virtual void unlock(int verticiesWritten) = 0;
	};

	typedef boost::intrusive_ptr<IVertexStream> IVertexStreamPtr;

	//Рендер должен реализовывать этот интерфейс
	class IRender : public RefCounter {
	public:
		virtual void drawTriangleList(ITexture* texture, IVertexStream* stream, int triangleCount) = 0;
	};

	typedef boost::intrusive_ptr<IRender> IRenderPtr;

	struct FontReference;
	class FontTexture;
	class FontInstance;
	class FontEffect;
    class FontEffectTemplate;

	void InitFreetype();

	FontReference* createFontReference(byte* data/*ownership transfered to FontReference*/, int dataLength, void(*deleter)(void*) = ::free);
	FontTexture* createFontTexture(ITexture* texture, int width, int height);
	FontInstance* createFontInstance(FontReference* reference, int size);
	
	void destroyFontReference(FontReference*);
	void destroyFontTexture(FontTexture*);
	void destroyFontInstance(FontInstance*);

	void applyFontEffect(FontInstance* instance, FontEffect* effect);
	void setFontColor(FontInstance* instance, unsigned int color);
	void setFontSpaceWidth(FontInstance* instance, float multiplier);

	struct DropShadow{
		int blurX;
		int blurY;
		int offset;
		float angle;
		float power;
		byte color[4];
		bool inner;
	};
	FontEffect* createDropShadowEffect(DropShadow* descriptor);
	
	struct GradientFill{
		unsigned int colors[256];
		float ratios[256];
		int anchors;
	};
	FontEffect* createGradientFillEffect(GradientFill* descriptor);
	FontEffect* createSkewEffect(float angle);

	void destroyFontEffect(FontEffect* effect);
	
	FontReference* getReference(FontInstance* fontInstance);

	void resolveCharCodes(FontReference* reference, const charcode* charcodes, int count, glyphindex* glyphs);

	float calculateStringWidth(FontInstance* instance, glyphindex* glyphs, int length, float letterSpacing = 0);
	float getStringHeight(FontInstance* instance);
	float getStringBearing(FontInstance* instance);
	float getStringAscender(FontInstance* instance);
	float getStringDescender(FontInstance* instance);
	float getLineSpacing(FontInstance* instance);

	void renderString(IRender* render, IVertexStream* vertexStream, FontTexture* texture, FontInstance* fontInstance, glyphindex* glyphs, int length, const byte* color, float& x, float& y);
	
	void startRender(IRender* render, IVertexStream* vertexStream, FontTexture* texture);
	void bufferString(FontInstance* fontInstance, glyphindex* glyphs, int length, const byte* color, float& x, float& y, float letterSpacing = 0);
	void endRender();

    void discardGlyphs(FontTexture* texture);
	GPUGlyph* getGlyph(FontInstance* fontInstance, FontTexture* texture, glyphindex glyph);
	float getKerning(FontInstance* fontInstance, glyphindex glyph1, glyphindex glyph2);
	float getSpaceWidth(FontInstance* fontInstance);
	
} // namespace freetype
