#pragma once

#include "RefCounter.h"

struct AtlasSlot{
	float x, y, width, height;
};

struct SpaceNode;

class Atlas : public RefCounter {
public:
	Atlas(int width, int height, int padding);
	~Atlas();
	AtlasSlot pack(int width, int height);
private:
	bool rec_pack(SpaceNode* currentNode, int width, int height, AtlasSlot* slot);
	SpaceNode* rootNode;
	int padding;
};

typedef boost::intrusive_ptr<Atlas> AtlasPtr;
