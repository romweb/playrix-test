#pragma once

#include "Flash/core/IFlashLibraryItem.h"
#include "Flash/core/FlashCommon.h"
#include "Flash/core/FlashLibraryItem.h"
#include "Flash/core/FlashRasterSprite.h"
#include "Flash/core/FlashTextureInfo.h"
#include "../rapidxml/rapidxml.hpp"

/**
* Шаблон для создания FlashRasterSprite
* @see FlashRasterSprite
*/
class FlashLibRasterSprite: public IFlashLibRasterSprite, public FlashLibraryItem{
public:
	FlashLibRasterSprite(FlashLibrary* library, FlashTextureId textureId, const float *coords, const float *uv, const float *slice9Grid, Data mask, int maskWidth, int maskHeight, bool ownMask);
	FlashLibRasterSprite(FlashLibrary* library, FlashTextureId textureId, const float *coords, const float *uv, const float *slice9Grid, IFlashTextureHitTestDelegate* hitTestDelegate);
	~FlashLibRasterSprite();

	IFlashLibRasterSprite* RasterSprite();
	IFlashRasterSprite* newInstance();
	FlashRasterSprite* createInstance();

	void getUV(float* u1, float* v1, float* u2, float* v2);
	void getPosition(float* x, float* y, float* width, float* height);
	bool getSlice9Grid(float* x1, float* y1, float* x2, float* y2);
	FlashTextureId getTextureId();

	void saveBinary(const std::string& id, std::vector<unsigned char>& buffer);
	void saveXML(const std::string& id, rapidxml::xml_node<>* rootNode);

	static FlashLibRasterSprite* fromBinary(FlashLibrary* library, const std::string& id, Data data, FlashTextureInfo textureInfo);
	static FlashLibRasterSprite* fromXML(FlashLibrary* library, const std::string& id, rapidxml::xml_node<>* xml, FlashTextureInfo textureInfo);
private:
	union{
		struct{
			Data transparentData;
			int maskWidth, maskHeight;
			bool ownMask;
		};
		IFlashTextureHitTestDelegate* hitTestDelegate;
	};
	FlashTextureId textureId;
	float uv[4];
	float coords[4];

	struct{
		float uv[4];
		float coords[4];
	} base;

	bool hitTestMask;
	bool slice9Grid;
	float slice9GridCoords[4];

	friend class FlashRasterSprite;
};

typedef boost::intrusive_ptr<FlashLibRasterSprite> FlashLibRasterSpritePtr;
