#pragma once

#include "Flash/core/IFlashDisplayObject.h"
#include "Flash/core/IFlashMovieClip.h"
#include "Flash/core/IFlashRasterSprite.h"

class FlashMovieClip;

/**
* Базовый объект во flash-иерархии, от которого можно наследоваться
* Сам по себе не определяет какое-либо поведение и рисование, но реализует большинство
* операций интерфейса.
* Чтобы сделать полноценного наследника этого класса, нужно доопределить методы
* @see IFlashDisplayObject::render
* @see IFlashDisplayObject::hitTest
* @see IFlashDisplayObject::getBounds
*/
class FlashDisplayObject: public IFlashDisplayObject{
public:
	FlashDisplayObject();
	virtual ~FlashDisplayObject();

	void setPosition(float x, float y);
	void getPosition(float& x, float& y);
	void setRotation(float radians);
	float getRotation();
	void setShear(float k);
	float getShear();
	void setScale(float scaleX, float scaleY);
	void getScale(float& x, float& y);
	void setMatrix(const float *matrix);
	void getMatrix(float *matrix);
	void setVisible(bool value);
	bool getVisible();
	bool getAutoPlay();
	void setCustomDrawOperation(IFlashCustomDrawOperation*);

	void changeUpdateListeners(int value);

	void setHitTestTransparent(bool value);
	void setHitTestDispatcher(bool value);
	bool getHitTestTransparent();
	bool getHitTestDispatcher();

	virtual IFlashMovieClip* MovieClip();
	virtual IFlashText* Text();
	virtual IFlashRasterSprite* RasterSprite();
	virtual IFlashSprite* Sprite();

	void localToParent(float& x, float& y);
	void parentToLocal(float& x, float& y);

	void localToGlobal(float &x, float &y);
	void globalToLocal(float &x, float &y);

	void advance(FlashUpdateListener* updateListener, float dt);
	void update(float dt){}

	void setUpdateRate(float rate);
	float getUpdateRate();

	float getAlpha();
	unsigned int getColor();

	void getColorVector(float color[4]);
	void setColorVector(const float color[4]);

	void setAlpha(float value);
	void setColor(unsigned int color);

	const std::string& getName();
	void setName(const std::string& name);

	IFlashDisplayObject* getParent();

	void unsafeSetParent(IFlashDisplayObject* parent);

	void localToTarget(float &x, float& y, IFlashDisplayObject* target);
	
	IFlashDisplayObject* getRoot();

	void visitDFS(IFlashVisitor* visitor, bool preorder);
	void visitBFS(IFlashVisitor* visitor, bool preorder);

	GC_BLACKEN_DECL(FlashDisplayObject)
public:
	IFlashDisplayObject* parent;
			
	float ownMatrix[9];	
	float ownColor[4];

	float updateRate;

	MatrixDecomposition decomposition;
	
	void __decompose();

	const char* name;
	std::string* nameStr;

	IFlashCustomDrawOperation* operation;

	bool mouseTransparent, skipMouseEvents;
	bool ownVisible, autoPlay;

	bool decompositionValid;

	int numUpdateListeners;
};