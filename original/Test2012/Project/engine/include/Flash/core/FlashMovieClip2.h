#pragma once

#include "Flash/core/FlashDisplayObject.h"
#include "Flash/core/IFlashMovieClip.h"
#include "Flash/core/IFlashSprite.h"

class FlashLibraryItem;
class FlashRasterSprite;
class FlashTimeline2;
class FlashLibMovieClip2;

/**
* Экземпляр анимации, который является DisplayObject'ом
*/
class FlashMovieClip2: public FlashDisplayObject, public IFlashMovieClip, public IFlashSprite{
public:
	FlashMovieClip2(FlashLibMovieClip2* movieClip);
	virtual ~FlashMovieClip2();

	void visitKeyFrame();
	void visitAttach(ID id, unsigned char* color, float alpha, float* matrix);

	void visitDetach(ID id, int index);
	void visitAttachAt(ID id, int index, unsigned char* color, float alpha, float* matrix);
	void visitBakeFrame();
	void visitSwap(int index1, int index2);
	void visitColor(int index, unsigned char* color);
	void visitAlpha(int index, unsigned char alpha);

	float* getRawMatrix(ID uniqueId);
	void commitRawMatrix();

	int countFrames();
	int nextFrame();
	int prevFrame();

	int gotoFrame(int frameId);
	int gotoLabel(const std::string& label);
	int getCurrentFrame();

	void setLooping(bool isLooping);
	void setLoop(int startFrame, int endFrame);

	int resolveLabelFrame(const std::string& label);

	FlashMovieClip2* MovieClip();
	FlashMovieClip2* Sprite();

	void render(FlashRender& render);

	bool hitTest(float x, float y, IHitTestDelegate* hitTestDelegate);
	
	FlashMovieClip2* DisplayObject();

	void advance(FlashUpdateListener* updateListener, float dt);
	
	void setPlayback(bool value);
	bool playing() const { return isPlaying; }

	void freezeToSprite(IFlashSprite* sprite);

	bool getBounds(float& left, float& top, float& right, float& bottom, IFlashDisplayObject* targetCoordinateSystem);

	FlashDisplayObject* forceLibraryItem(int index);

	IFlashDisplayObject* addChildAt(IFlashDisplayObject* displayObject, int index);
	IFlashDisplayObject* removeChildAt(int index);
	IFlashDisplayObject* getChildAt(int index);
	void swapChildren(int index1, int index2);
	int getChildrenCount();

	void setPlaybackOperation(IPlaybackOperation* playbackOperation);

	void visitDFS(IFlashVisitor* visitor, bool preorder);
	void visitBFS(IFlashVisitor* visitor, bool preorder);

	GC_BLACKEN_DECL(FlashMovieClip2)
private:
	FlashTimeline2* timeline;
	FlashLibMovieClip2* movieClip;

	unsigned int currentFrame;

	std::vector<IFlashDisplayObject*> children;
	std::vector<FlashDisplayObject*> library;

	void destroyChildren();
	void destroyChild(FlashDisplayObject* displayObject);

	IPlaybackOperation* playbackOperation;

	bool isPlaying;
	bool advanceFrame;
	bool isLooping;
	int loopStart, loopEnd;
	
	float fps, framePiece;

	bool needAdvanceFrame();
	void updateAdvanceFrameStatus();
		

	friend IFlashDisplayObject* recursiveFreeze(IFlashDisplayObject* displayObject);
	friend class FlashMovieClip;
};