#pragma once

#include "Flash/core/FlashDisplayObject.h"
#include "Flash/core/FlashSprite.h"
#include "Flash/events/UIEventManager.h"

enum FlashStageScaleMode{
	//никогда не изменять масштаб
	ScaleModeNoScale,
	//изменять масштаб с нарушением пропорций
	ScaleModeExactFit,
	//изменять масштаб до тех пор, пока рамка окна не пропадет
	ScaleModeNoBorder,
	//изменять масштаб до тех пор, пока не упремся в край экрана
	ScaleModeShowAll
};

class FlashStage: public FlashSprite, public UIEventManager{
public:
	FlashStage(float width, float height);

	void setScaleMode(FlashStageScaleMode scaleMode);
	void setScaleBounds(float minScale, float maxScale);

	FlashStageScaleMode getScaleMode() const;

	void setHorizontalAlign(float p);
	void setVerticalAlign(float p);

	IFlashDisplayObject* addChildAt(IFlashDisplayObject* displayObject, int index);
	IFlashDisplayObject* removeChildAt(int index);

	void setRotation(float x);
	void setScale(float x, float y);
	void setShear(float x);
	void setMatrix(float* m);
	void setPosition(float x, float y);
	void setViewport(float x, float y, float width, float height);

	/// Точка начала координат в экранной системе координат
	void getOrigin(float& x, float& y);
	/// Размер сцены в экранной системе координат
	void getStageSize(float& width, float& height) const;
	/// Размер игрового экрана, в котором спроектирована игра
	void getDesignSize(float& width, float& height) const;
	void getViewport(float& x, float& y, float& width, float& height) const;

	void drawToRenderDevice();

protected:
	void resized();

	float originX, originY;
	float viewX, viewY, viewWidth, viewHeight;

	float stageWidth, stageHeight;

	float designWidth, designHeight;

	float horAlign, vertAlign;
	float minScale, maxScale;

	FlashStageScaleMode scaleMode;
};