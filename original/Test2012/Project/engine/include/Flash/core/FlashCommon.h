#pragma once

#include <string>
#include "Flash/core/FlashErrors.h"

//используется сборщик мусора
#define USE_MARKSWEEP_GC
//используется GC_FINALIZER
#define GC_CUSTOM_FINALIZER
//ведется лог
#define WRITE_LOG

#if defined(USE_MARKSWEEP_GC)

#include "Flash/core/FlashMarkSweep.h"

#else
//скорее всего без сборщика мусора корректной работы не будет
#error USE_MARKSWEEP_GC required!

#define GC_CLASS(clss) //определяет, что класс будет подвержен сборке мусора
#define GC_CLASS_DATA(clss) //вводит данные в класс, который подлежит сборке мусора
#define GC_CLASS_INTERFACE(clss) //вводит методы в класс, который подлежит сборке мусора
#define GC_CLASS_NOINHERITANCE(clss)  //вводит наследование в класс, подлежащий сборке мусора
#define GC_CLASS_INHERITANCE(clss, ...) : __VA_ARGS__ //вводит наследование в класс, подлежащий сборке мусора

#define GC_SHOULD_TRACK(object) //вызывается, когда объект должен отслеживаться сборщиком мусора
#define GC_SHOULD_SWEEP(object) delete object//вызывается, когда объект гарантированно обречен
#define GC_SHOULD_MARK(object) //вызывается, когда объект должен быть помечен
#define GC_BLACKEN(сlss) template<class DoNotCompile> static void clss##_gc_method()//определяет метод 'затенения' для объекта класса
#define GC_BLACKEN_DECL(clss)
#define GC_ADD_REF(object)
#define GC_REMOVE_REF(object)
#define GC_PERFORM()
#define GC_UNMANAGED(operation) operation
#define GC_STACKREF(object)

#endif

typedef unsigned int ID;

typedef const char* Data;
typedef char* MutableData;

typedef unsigned int FlashTextureId;

#include <Core/Log.h>

#if defined(WRITE_LOG)
#	define FLASH_INFO(msg) Log::Info(msg)
#	define FLASH_WARNING(msg) Log::Warn(msg)
#	define FLASH_ERROR(msg) Log::Error(msg)
#else
#	define FLASH_INFO(msg) 
#	define FLASH_WARNING(msg) 
#	define FLASH_ERROR(msg) 
#endif

struct MatrixDecomposition{
	float shear;
	float scaleX, scaleY;
	float rotation;
};

extern float zeroMatrix[9];

void multMatrix3x3(float dest[6], const float src1[6], const float src2[6]);

bool matrixIdentity(const float m[6]);

 MatrixDecomposition decompose(const float* matrix);

void recompose(float* matrix, const MatrixDecomposition& decomp);
