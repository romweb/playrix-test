#pragma once

/**
* Объект, позволяющий узнать, является ли пиксель в точке текстуры прозрачным
*/
class IFlashTextureHitTestDelegate{
public:
	virtual ~IFlashTextureHitTestDelegate(){}

	virtual bool hitTest(float u, float v) = 0;
};

/**
* Объект, преобразующий информацию о текстуре в разные данные
*/
class IFlashTextureTranslator: public IFlashTextureHitTestDelegate{
public:
	//Какую информацию о прозрачности имеет объект
	enum TransparencyInfo{
		//никакой
		NoTransparencyInfo = 0,
		//может сгенерировать битовую карту
		MaskTransparencyInfo = 1,
		//может предоставить IFlashTextureHitTestDelegate 
		HitTestTransparencyInfo = 2,
		//совмещает HitTestTransparencyInfo и MaskTransparencyInfo
		FullTransparencyInfo = 3
	};

	virtual ~IFlashTextureTranslator(){}
	/*
	(uvs[0], uvs[1]) --------------+
		|                          |
		+----------------(uvs[2], uvs[3])
	*/
	virtual void translateUV(const std::string& id, float uvs[4], float coords[4]) = 0;
	virtual TransparencyInfo getTransparencyInfoType() = 0;
	virtual void getTransparencyInfo(const std::string& id, float translatedUVS[4], float translatedCoords[4], int maskWidth, int maskHeight, char* maskData) = 0;
};

struct FlashTextureInfo{
	FlashTextureId textureId;
	float textureWidth, textureHeight;
	float top, left;
	float right, bottom;

	/**
	* Выполняет преобразование координат 
	*/
	IFlashTextureTranslator* translator;
};