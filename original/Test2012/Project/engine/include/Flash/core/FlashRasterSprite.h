#pragma once

#include "Flash/core/FlashDisplayObject.h"
#include "Flash/core/IFlashRasterSprite.h"

class FlashLibRasterSprite;

/**
* Растровый спрайт (текстура).
* Поддерживает разбивку (setTesselation), 9-patch (setSlive9Grid) и обрезку (setCustomBox)
*/
class FlashRasterSprite: public FlashDisplayObject, public IFlashRasterSprite{
public:
	FlashRasterSprite(FlashLibRasterSprite* libRasterSprite);
	virtual ~FlashRasterSprite();

	void render(FlashRender& render);

	FlashTextureId getTextureId();

	bool hitTest(float x, float y, IHitTestDelegate* hitTestDelegate);

	FlashRasterSprite* DisplayObject();
	FlashRasterSprite* RasterSprite();

	void setCustomBox(float beginX, float beginY, float endX, float endY);
	void getCustomBox(float& beginX, float& beginY, float& endX, float& endY);

	void setSlice9Grid(float beginX, float beginY, float endX, float endY);

	bool getBounds(float& left, float& top, float& right, float& bottom, IFlashDisplayObject* targetCoordinateSystem);
	void setTesselation(int horizontal, int vertical);

	virtual void getUVRect(float& beginX, float& beginY, float& endX, float& endY);
	virtual void getTexRect(float& beginX, float& beginY, float& endX, float& endY);
private:
	FlashLibRasterSprite* libRasterSprite;
	FlashTextureId textureId;
	float x, y, width, height;
	float u1, u2, v1, v2;

	bool tesselated;
	int vCount, hCount;

	struct tagCustomBox{
		float x, y, width, height;
		float u1, u2, v1, v2;
		float beginX, beginY, endX, endY;
	} *customBox;

	struct tagScale9Grid{
		float xBegin, xEnd, yBegin, yEnd;
	} *scale9Grid;
};