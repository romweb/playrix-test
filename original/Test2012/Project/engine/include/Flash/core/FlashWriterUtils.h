#pragma once

#include "../rapidxml/rapidxml.hpp"

/*
* Утилиты для записи XML и бинарного формата
*/

class FlashXML {
public:
	FlashXML(rapidxml::xml_node<>* node):node(node), doc(node->document()){
	}

	void setAttribute(const char* name, const char* value){
		rapidxml::xml_attribute<>* attr = node->first_attribute(name);
		if ( attr ){
			attr->value(doc->allocate_string(value));
		}else{
			attr = doc->allocate_attribute(name, doc->allocate_string(value));
			node->append_attribute(attr);
		}
	}

	void setAttribute(const char* name, const std::string& value){
		setAttribute(name, value.c_str());
	}

	void setAttribute(const char* name, int value){
		char buffer[80];
		sprintf(buffer, "%d", value);
		setAttribute(name, buffer);
	}

	void setAttribute(const char* name, float value){
		char buffer[256];
		sprintf(buffer, "%f", value);
		setAttribute(name, buffer);
	}

	void setColorAttribute(const char* name, unsigned int value){
		char buffer[256];
		sprintf(buffer, "#%06x", value);
		setAttribute(name, buffer);
	}

	void setColorAttribute(const char* name, unsigned char value[3]){
		char buffer[256];
		sprintf(buffer, "#%02x%02x%02x", (unsigned int)value[0], (unsigned int)value[1], (unsigned int)value[2]);
		setAttribute(name, buffer);
	}

	void setText(const char* text){
		node->value(doc->allocate_string(text));
	}

	FlashXML addElement(const char* name){
		rapidxml::xml_node<>* childNode = doc->allocate_node(rapidxml::node_element, name);
		node->append_node(childNode);
		return childNode;
	}

	FlashXML getElement(const char* name){
		rapidxml::xml_node<>* childNode = node->first_node("name");
		if ( childNode ){
			return childNode;
		}
		childNode = doc->allocate_node(rapidxml::node_element);
		node->append_node(childNode);
		return childNode;
	}

	rapidxml::xml_node<>* getNode(){
		return node;
	}
private:
	rapidxml::xml_node<>* node;
	rapidxml::xml_document<>* doc;
};

class FlashBinaryOutStream{
public:
	FlashBinaryOutStream(std::vector<unsigned char>& data){
		this->data = &data;
	}

	size_t reserve(int size){
		size_t pos = data->size();
		while ( size-- ){
			data->push_back(0xcd);
		}
		return pos;
	}

	void writeBytes(const void* buffer, size_t size){
		const char* s = (const char*)buffer;
		while ( size-- ){
			data->push_back(*s);
			s++;
		}
	}

	void insertBytes(size_t where, const void* buffer, size_t size){
		const char* s = (const char*)buffer;

		while ( data->size() < where + size ){
			data->push_back(0xcd);
		}

		while ( size-- ){
			data->at(where) = *s;
			where++;
			s++;
		}
	}

	template<class T>
	void write(const T& value){
		writeBytes(&value, sizeof(T));
	}

	template<class T>
	void insert(size_t where, const T& value){
		insertBytes(where, &value, sizeof(T));
	}

	void writeCString(const char* s){
		writeBytes(s, strlen(s) + 1);
	}

	void writeString(const char* s){
		writeBytes(s, strlen(s));
	}

	template<int N>
	void align(unsigned char ch = 0x00){
		while ( data->size() % N ){
			data->push_back(ch);
		}
	}

	std::vector<unsigned char>* data;
};