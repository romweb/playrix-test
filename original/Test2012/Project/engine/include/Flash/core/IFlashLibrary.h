#pragma once

#include "RefCounter.h"
#include "Flash/core/FlashCommon.h"

class IFlashSprite;
class IFlashLibraryItem;

/**
* Интерфейс библиотеки flash-символов
*/
class IFlashLibrary : public RefCounter {
public:
	/**
	* Получить символ из библиотеки по идентификатору.
	* Если символа нет, будет возвращен 0.
	*/
	virtual IFlashLibraryItem* getLibraryItem(const std::string& id) = 0;
	/**
	* Создает и возвращает пустой спрайт
	*/
	virtual IFlashSprite* createEmptySprite() = 0;
	/**
	* Возвращает число объектов библиотеки, которые еще используются
	*/
	virtual int getUsageCount() = 0;

	/**
	* Возвращает имена всех объектов библиотеки
	* @param privates указывает, нужно ли возвращать внутренние объекты
	*/
	virtual void getNames(std::vector<std::string>& target, bool privates) = 0;

	/**
	* Выгружает неиспользуемые части библиотеки
	*/
	virtual void unloadUnused() = 0;
};

typedef boost::intrusive_ptr<IFlashLibrary> IFlashLibraryPtr;
