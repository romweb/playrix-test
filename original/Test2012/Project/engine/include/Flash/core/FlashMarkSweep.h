#pragma once

#include "Flash/core/FlashCommon.h"

class IGCRef{
public:
	IGCRef();
	virtual ~IGCRef();
	virtual void gcBlacken();

	short gcExternalRefs;
	short mark;
};

extern int gc_managed;

void gcAddRoot(IGCRef* gcRef);
void gcRemoveRoot(IGCRef* gcRef);
void gcTrack(IGCRef* gcRef);
void gcGreyObject(IGCRef* gcRef);
void gcSweep();
void gcRemoveTrack(IGCRef* gcRef);
void gcPushStackRef(IGCRef* gcRef);
void gcPopGCRef();

void intrusive_ptr_add_ref( const IGCRef* ref );
void intrusive_ptr_release( const IGCRef* ref );

#define GC_CLASS(clss) //определяет, что класс будет подвержен сборке мусора
#define GC_CLASS_DATA(clss) //вводит данные в класс, который подлежит сборке мусора
#define GC_CLASS_INTERFACE(clss) //вводит методы в класс, который подлежит сборке мусора
#define GC_CLASS_INHERITANCE(clss, ...) :public IGCRef, __VA_ARGS__ //вводит наследование в класс, подлежащий сборке мусора
#define GC_CLASS_NOINHERITANCE(clss) :public IGCRef

#define GC_SHOULD_TRACK(object) gcTrack(object); //вызывается, когда объект должен отслеживаться сборщиком мусора
#define GC_SHOULD_SWEEP(object) //вызывается, когда объект гарантированно обречен
#define GC_SHOULD_MARK(object) gcGreyObject(object);//вызывается, когда объект должен быть помечен
#define GC_BLACKEN(A) void A :: gcBlacken()//определяет метод 'затенения' для объекта класса
#define GC_BLACKEN_DECL(clss) virtual void gcBlacken();
#define GC_BLACKEN_INLINE(clss) virtual void gcBlacken()
#define GC_ADD_REF(object) gcAddRoot(object);
#define GC_REMOVE_REF(object) gcRemoveRoot(object);
#define GC_PERFORM() gcSweep();
#define GC_NOTRACK(object) (gcRemoveTrack(object), object);

struct GCStackRef{
	GCStackRef(IGCRef* ref){
		gcPushStackRef(ref);
	}
	~GCStackRef(){
		gcPopGCRef();
	}
};
#define GC_STACKREF(object) GCStackRef ref##__FILE__##__LINE__(object);

#define GC_FINALIZER(ref) void gcFinalize(IGCRef* ref)

#define GC_UNMANAGED(operation) {gc_managed++; operation; gc_managed--;}