#pragma once

class IFlashDisplayObject;

/**
* Коллбек для процедуры IFlashDisplayObject::hitTest
*/
class IHitTestDelegate{
public:
	virtual ~IHitTestDelegate(){}
	/**
	* Вызывается для каждого объекта иерархии, в который попал hitTest
	*/
	virtual void receiveNext(IFlashDisplayObject* displayObject, float localX, float localY) = 0;
};