#pragma once
#include <stdexcept>

#define FLASH_ASSERT(c, e) if ( !(c) ) { throw e; }
#define FLASH_CHECK(c, e) if ( !(c) ) { throw e; }

#define FLASH_ERROR_DEF(a) class a: public std::runtime_error{ public : a(const std::string& what):std::runtime_error(what){} }

/**
* Исключение, возникающее при чтении некорректного swl
*/
FLASH_ERROR_DEF(invalid_swl_format);
/**
* Исключение при выходе за границы массива children
*/
FLASH_ERROR_DEF(child_index_out_of_bounds);
/**
* Исключение при ошибке разбора XML-атрибутов
*/
FLASH_ERROR_DEF(parse_error);
