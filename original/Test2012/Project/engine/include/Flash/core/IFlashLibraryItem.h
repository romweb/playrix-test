#pragma once

#include "Flash/core/FlashCommon.h"

//forward decl
class IFlashRasterSprite;
class IFlashMovieClip;
class IFlashText;
class IFlashSprite;

/**
* Интерфейс растрового спрайта в библиотеке
*/
class IFlashLibRasterSprite {
public:
	virtual ~IFlashLibRasterSprite() {}
	virtual IFlashRasterSprite* newInstance() = 0;
};

/**
* Интерфейс спрайта в библиотеке
*/
class IFlashLibSprite {
public:
	virtual ~IFlashLibSprite() {}
	virtual IFlashSprite* newInstance() = 0;
};

/**
* Интерфейс текстового поля в библиотеке
*/
class IFlashLibText {
public:
	virtual ~IFlashLibText() {}
	virtual IFlashText* newInstance() = 0;
};

/**
* Интерфейс клипа в библиотеке.
* В отличие от flash, понятие Sprite и MovieClip не разделяются. Можно считать, что
* любой MovieClip с одним кадром - это Sprite.
*/
class IFlashLibMovieClip {
public:
	virtual ~IFlashLibMovieClip() {}

	virtual IFlashMovieClip* newInstance() = 0;
	/**
	* Получить список всех меток, расположенных на timeline клипа
	*/
	virtual std::list<std::string> getLabels() = 0;
	/**
	* Получить номер кадра, на который ссылается определенная метка
	*/
	virtual int resolveLabelFrame(const std::string& label) = 0;

	/**
	* Поулчить общее число кадров клипа
	*/
	virtual int getFramesCount() = 0;
};

/**
* Интерфейс элемента библиотеки
* Т.к. инстанцирование произвольного элемента не имеет смысла, клиентский код
* должен получить интерфейс желаемого типа. Если символ не принадлежит к этому типу,
* будет возвращен 0.
*/
class IFlashLibraryItem {
public:
	virtual ~IFlashLibraryItem() {}
	virtual IFlashLibRasterSprite* RasterSprite() = 0;
	virtual IFlashLibMovieClip* MovieClip() = 0;
	virtual IFlashLibText* Text() = 0;
	virtual IFlashLibSprite* Sprite() = 0;
};
