#pragma once

#include "Flash/core/IFlashLibrary.h"
#include "Flash/core/FlashLibraryItem.h"
#include "Flash/core/FlashTextureInfo.h"
#include "../rapidxml/rapidxml.hpp"

/**
* Библиотека символов хранит и управляет flash-символами - шаблонами для создания
* экземпляров flash-объектов.
*/
class FlashLibrary : public IFlashLibrary{
public:
	FlashLibrary();
	
	FlashLibraryItem* getLibraryItem(const std::string& id);
	IFlashSprite* createEmptySprite();

	virtual int getUsageCount();

	virtual void retain(FlashLibraryItem*);
	virtual void release(FlashLibraryItem*);

	void unloadUnused();

	void getNames(std::vector<std::string>& target, bool privates);

	virtual void saveBinary(std::vector<unsigned char>& buffer);
	virtual void saveXML(rapidxml::xml_node<>* rootNode);
protected:
	int usageCount;

private:
	
	std::map<std::string, FlashLibraryItemPtr> libraryEntries;

	void putLibraryItem(const std::string& id, FlashLibraryItem* item);

	friend IFlashLibrary* parseLibrary(Data data, FlashTextureInfo textureInfo);
	friend IFlashLibrary* parseLibraryXML(rapidxml::xml_node<>* xml, FlashTextureInfo textureInfo);
};

typedef boost::intrusive_ptr<FlashLibrary> FlashLibraryPtr;
