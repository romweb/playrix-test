#pragma once

#include "Flash/core/IFlashLibrary.h"
#include "Flash/core/IFlashLibraryItem.h"
#include "Flash/core/IFlashMovieClip.h"
#include "Flash/core/IFlashRasterSprite.h"
#include "Flash/core/IFlashText.h"
#include "Flash/core/IFlashSprite.h"
#include "Flash/core/FlashTextureInfo.h"
#include "Flash/core/IHitTestDelegate.h"
#include "Flash/core/IFlashCustomDrawOperation.h"
#include "Flash/core/IFlashParticleEffect.h"
#include "Flash/core/DynamicLibraryChunkReader.h"

#include "Flash/core/FlashDisplayObject.h"
#include "Flash/core/FlashSprite.h"
#include "Flash/core/FlashClipContainer.h"
#include "../rapidxml/rapidxml.hpp"

/**
* Чтение формата swl, производимого air-приложением из swf.
* @param data указатель на начало блока данных. Данные должны оставаться доступными все
* время, пока клиентский код пользуется любыми символами полученной библиотеки.
* @param textureInfo содержит информацию о текстуре, которая будет использоваться для
* отображения объектов библиотеки
* @return интефрейс библиотеки символов
*/
IFlashLibrary* parseLibrary(Data data, FlashTextureInfo textureInfo);
IFlashLibrary* parseLibraryXML(rapidxml::xml_node<>* xml, FlashTextureInfo textureInfo);

/**
* Сохраняет флеш-библиотеку в бинарном формате в массив data
*/
void saveLibraryAsBinary(IFlashLibrary* library, std::vector<unsigned char>& data);
/**
* Сохраняет библиотеку в формате xml в переданный xml объект
*/
void saveLibraryAsXML(IFlashLibrary* library, rapidxml::xml_node<>* xml);

/**
* Чтение библиотеки в динамическом режиме.
* Такая библиотека не будет держаться в памяти вся целиком, вместо этого
* будут загружаться только нужные объекты.
*/
IFlashLibrary* parseLibrary(DynamicLibraryChunkReader* chunkReader, FlashTextureInfo textureInfo);
