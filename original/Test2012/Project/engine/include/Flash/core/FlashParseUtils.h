#pragma once

#include "Flash/core/FlashCommon.h"

/*
* Утилиты для разбора XML и бинарного формата
*/

class FlashBinaryInStream{
public:
	FlashBinaryInStream(Data data){
		this->data = data;
	}

	void readBytes(void* buffer, size_t size){
		unsigned char* s = (unsigned char*)buffer;
		while ( size-- ){
			*s = *data;
			s++;
			data++;
		}
	}

	template<class T>
	T read(){
		T value;
		readBytes(&value, sizeof(T));
		return value;
	}

	template<class T>
	void read(T& out){
		readBytes(&out, sizeof(T));
	}

	template<class T>
	T* get(){
		T* ptr = (T*)data;
		data += sizeof(T);
		return ptr;
	}

	void skip(size_t bytes){
		data += bytes;
	}

	template<size_t N>
	void align(){
		while ( ((size_t)data) % N ){
			data++;
		}
	}

	Data getPointer(){
		return data;
	}

	char* getCString(){
		return (char*)data;
	}

	void readString(char* s, int length){
		readBytes(s, length);
		s[length] = 0;
	}

	/*
	template<int N>
	void align(unsigned char ch = 0x00){
		while ( data->size() % N ){
			data->push_back(ch);
		}
	}
	 */
	
	Data data;
};

static int parseInt(const char* s){
	int value;
	char ch;
	if ( sscanf(s, "%d%c", &value, &ch) != 1 ){
		throw parse_error("expected integer value");
	}
	return value;
}

static int parseHexInt(const char* s){
	int value;
	char ch;
	if ( sscanf(s, "%x%c", &value, &ch) != 1 ){
		throw parse_error("expected integer value");
	}
	return value;
}

static bool parseBool(const char* s){
	if ( !strcmp(s, "true") )
		return true;
	if ( !strcmp(s, "false") )
		return false;
	throw parse_error("expected boolean value");
	return false;
}

static float parseFloat(const char* s){
	float value;
	char ch;
	if ( sscanf(s, "%f%c", &value, &ch) != 1 ){
		throw parse_error("expected float value");
	}
	return value;
}

static int countNodes(rapidxml::xml_node<>* node, const char* s){
	int n = 0;
	node = node->first_node(s);
	while ( node ){
		n++;
		node = node->next_sibling(s);
	}
	return n;
}

static unsigned int parseColor(const char* s){
	if ( s[0] == '#' ){
		return parseHexInt(s + 1);
	}
	if ( s[0] == '0' && s[1] == 'x' ){
		return parseHexInt(s + 2);
	}
	return parseInt(s);
}

template<class T>
static T withDefault(T val, T(*parser)(const char*), const char* s){
	if ( !s ){
		return val;
	}
	return parser(s);
}

static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};

typedef unsigned int uint32_t;

static char *base64_encode(const unsigned char *data,
                    size_t input_length,
                    size_t *output_length) {

    *output_length = 4 * ((input_length + 2) / 3);

    char *encoded_data = (char*)malloc(*output_length);
    if (encoded_data == NULL) return NULL;

    for (unsigned int i = 0, j = 0; i < input_length;) {

        uint32_t octet_a = i < input_length ? data[i++] : 0;
        uint32_t octet_b = i < input_length ? data[i++] : 0;
        uint32_t octet_c = i < input_length ? data[i++] : 0;

        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }

    for (int i = 0; i < mod_table[input_length % 3]; i++)
        encoded_data[*output_length - 1 - i] = '=';

    return encoded_data;
}
static void build_decoding_table();

static unsigned char *base64_decode(const char *data,
                             size_t input_length,
							 unsigned char* decoded_data,
                             size_t *output_length) {

    if (decoding_table == NULL) build_decoding_table();

    for (unsigned int i = 0, j = 0; i < input_length;) {
		
		switch ( data[i] ){
		case ' ':case '\n':case '\r':case '\t':continue;
		};

        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];

        uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);

        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}


static void build_decoding_table() {

    decoding_table = (char*)malloc(256);

    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}

