#pragma once

#include "RefCounter.h"
#include "Flash/core/IFlashLibraryItem.h"
#include "../rapidxml/rapidxml.hpp"

class FlashLibrary;
class FlashDisplayObject;

/**
* Шаблон для создания flash-объекта. Запрашивается у библиотеки символов.
* @see FlashLibrary
*/
class FlashLibraryItem : public RefCounter, public IFlashLibraryItem{
public:
	FlashLibraryItem(FlashLibrary* library);

	virtual FlashDisplayObject* createInstance() = 0;
	virtual IFlashLibRasterSprite* RasterSprite();
	virtual IFlashLibMovieClip* MovieClip();
	virtual IFlashLibText* Text();
	virtual IFlashLibSprite* Sprite();

	FlashLibrary* getContainingLibrary();

	std::pair<size_t, size_t> saveBinaryContainer(const std::string& id, std::vector<unsigned char>& buffer);

	virtual void saveBinary(const std::string& id, std::vector<unsigned char>& buffer) = 0;
	virtual void saveXML(const std::string& id, rapidxml::xml_node<>* rootNode) = 0;


public:
	virtual size_t beginWriteBinary(const std::string& id, std::vector<unsigned char>& buffer);
	virtual void endWriteBinary(std::vector<unsigned char>& buffer, size_t where);

private:

	FlashLibrary* library;
};

typedef boost::intrusive_ptr<FlashLibraryItem> FlashLibraryItemPtr;
