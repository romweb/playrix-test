#pragma once

#include "Flash/core/IFlashDisplayObject.h"

class IFlashText: public IFlashConcreteDisplayObject{
public:
	virtual ~IFlashText() {}

	virtual void setText(const std::string& text) = 0;
	virtual const std::string& getText() = 0;
	virtual void setWidth(float width) = 0;
	virtual void setHeight(float height) = 0;
	virtual float getWidth() = 0;
	virtual float getHeight() = 0;
};

