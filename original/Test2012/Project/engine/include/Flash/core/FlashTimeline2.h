#pragma once

#include "RefCounter.h"
#include "Flash/core/FlashCommon.h"
#include "../rapidxml/rapidxml.hpp"

class FlashMovieClip2;
class FlashLibrary;
class FlashSprite;

/**
* Используется для построения кадров в FlashMovieClip2.
*/
class FlashTimeline2 : public RefCounter {
public:
	FlashTimeline2(FlashLibrary* library);
	~FlashTimeline2();

	int getTotalFrames();
	bool hasLabel(const std::string& name);
	int getLabelFrame(const std::string& name);
	int getNearestKeyFrame(int targetFrame);
	bool isKeyFrame(int frame);
	std::list<std::string> getLabels();

	void link(FlashLibrary* library);

	void retain();
	void release();

	FlashLibrary* getContainingLibrary();

	static FlashTimeline2* fromBinary(FlashLibrary* library, Data data);
	static FlashTimeline2* fromXML(FlashLibrary* library, rapidxml::xml_node<>* xml);

	void saveXML(rapidxml::xml_node<>* node);
	void saveBinary(std::vector<unsigned char>& buffer);
private:
	//int refCount;

	void addLabel(int frame, const std::string& label);

	FlashLibrary* library;

	void check();

	bool reverseInfo;

	void decodeDeltaFrame(int frame, FlashMovieClip2* movieClip);
	void decodeReverseDeltaFrame(int frame, FlashMovieClip2* movieClip);

	void decodeFrame(Data frame, FlashMovieClip2* movieClip);

	void decodeKeyFrame(int frame, FlashMovieClip2* movieClip);

	void decodeFrameData(Data frame, FlashMovieClip2* movieClip);

	friend class FlashMovieClip2;
	friend class FlashLibSprite2;

	int totalFrames;
	Data binaryImage;
	size_t binarySize;

	Data* frameData;
	Data* reverseFrameData;
	Data* keyframeData;

	int childrenCount;
	Data* childrenLibrary;

	std::map<std::string, int> labels;

	std::vector<char*> allocations;
};

typedef boost::intrusive_ptr<FlashTimeline2> FlashTimeline2Ptr;
