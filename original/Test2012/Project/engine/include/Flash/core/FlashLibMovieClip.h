#pragma once

#include "Flash/core/FlashLibraryItem.h"
#include "Flash/core/FlashMovieClip2.h"

class FlashLibSprite2;
class FlashTimeline2;

typedef boost::intrusive_ptr<FlashLibSprite2> FlashLibSprite2Ptr;
typedef boost::intrusive_ptr<FlashTimeline2> FlashTimeline2Ptr;

/**
* Шаблон для FlashMovieClip2. Используется для создания экземпляров.
*/
class FlashLibMovieClip2: public FlashLibraryItem, public IFlashLibMovieClip{
public:
	FlashLibMovieClip2(FlashLibrary* library, FlashTimeline2* timeline);
	IFlashLibRasterSprite* RasterSprite();
	IFlashLibMovieClip* MovieClip();
	IFlashLibSprite* Sprite();
	IFlashMovieClip* newInstance();

	FlashTimeline2* getTimeline();

	FlashMovieClip2* createInstance();

	std::list<std::string> getLabels();
	int resolveLabelFrame(const std::string& label);
	int getFramesCount();

	void saveBinary(const std::string& id, std::vector<unsigned char>& buffer);
	void saveXML(const std::string& id, rapidxml::xml_node<>* rootNode);
private:
	FlashTimeline2Ptr timeline;
	FlashLibSprite2Ptr spriteCounterpart;
};

typedef boost::intrusive_ptr<FlashLibMovieClip2> FlashLibMovieClip2Ptr;
