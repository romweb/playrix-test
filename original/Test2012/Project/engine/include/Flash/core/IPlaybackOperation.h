#pragma once

class IFlashMovieClip;

/**
* Используется в качестве callback для событий анимации в FlashMovieClip.
*/
class IPlaybackOperation{
public:
	virtual ~IPlaybackOperation(){}

	///Вызывается при построении каждого следующего кадра
	virtual void onFrameConstructed(IFlashMovieClip* movieClip, int frame){
	}

	///Вызывается при окончании проигрывания loop'а, когда MovieClip готовится 
	///вновь перейти к первому кадру
	//Если внутри этой функции происходит управление клипом, то нужно вернуть false.
	//Если будет возвращено значение true, клип перейдет на первый кадр цикла.
	virtual bool onAnimationCompleted(IFlashMovieClip* movieClip){
		return true;
	}

	///Вызывается, когда MovieClip возвращается на первый кадр цикла
	virtual void onAnimationRestarted(IFlashMovieClip* movieClip){
	}

	///Вызывается, когда MovieClip удаляется
	virtual void onMovieDisposed(IFlashMovieClip* movieClip){
		delete this;
	}
};