/*
 *  PixelType.h
 *  Engine
 *
 *  Created by Slava on 2/18/11.
 *  Copyright 2011 Playrix Entertainment. All rights reserved.
 *
 */
#ifndef PIXEL_TYPE_H_INCLUDED
#define PIXEL_TYPE_H_INCLUDED

// Тип хранения цвета пикселя в видеопамяти
typedef enum
{
	PIXEL_TYPE_DEFAULT = 0,
	
	PIXEL_TYPE_8888,
		// RGBA по 8 бит на каждый канал
		// в XML: pixelType="8888"
	
	PIXEL_TYPE_888,
		// RGB по 8 бит на канал (без альфа-канала)
		// в XML: pixelType="888"
	
	PIXEL_TYPE_4444,
		// 4 бита на каждый канал (16-битный цвет)
		// в XML: pixelType="4444"
	
	PIXEL_TYPE_5551,
		// по 5 бит на R, G, B и 1 бит на A
		// в XML: pixelType="5551"
	
	PIXEL_TYPE_565,
		// 5 бит на R, 6 бит на G, 5 бит на B, каждый пиксел подразумевается полностью непрозрачным
		// в XML: pixelType="565"
	
	PIXEL_TYPE_GRAY,
		// Монохромное изображение 8 бит на пиксел
		// в XML: pixelType="gray"
	
	PIXEL_TYPE_ALPHA,
		// Только 8-битный альфа канал
		// в XML: pixelType="alpha"
	
	PIXEL_TYPE_GRAY_ALPHA,
		// 8 бит интенсивность и 8 бит альфа
		// в XML: pixelType="gray-alpha"
	
	PIXEL_TYPE_DXT1,
	PIXEL_TYPE_DXT3,
	PIXEL_TYPE_DXT5,
	PIXEL_TYPE_RGB_PVRTC_2BPP,
	PIXEL_TYPE_RGB_PVRTC_4BPP,
	PIXEL_TYPE_RGBA_PVRTC_2BPP,
	PIXEL_TYPE_RGBA_PVRTC_4BPP,
	PIXEL_TYPE_ETC1,
		// Для сжатых изображений
} EnginePixelType;


EnginePixelType StringToPixelType(const char* str);
std::string PixelTypeToString(EnginePixelType pt);
bool IsCompressedPixelType(EnginePixelType pt);

#endif
