// Platform specific staff

#ifndef _PLATFORM_H_
#define _PLATFORM_H_

#include "Platform/TargetPlatforms.h"

#ifdef ENGINE_TARGET_WIN32

#if _MSC_VER <= 1500 // vc2008
typedef __int8 int8_t;
typedef __int16 int16_t;
typedef __int32 int32_t;
typedef __int64 int64_t;

typedef unsigned __int8 uint8_t;
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;
typedef unsigned __int64 uint64_t;
#endif // _MSC_VER <= 1500

#else // !ENGINE_TARGET_WIN32

#	include <sys/types.h>
#	define MAX_PATH 512
#	define __forceinline

typedef unsigned int UINT;
typedef u_int8_t BYTE;
typedef u_int16_t WORD;
typedef u_int32_t DWORD;
typedef int64_t __int64;

typedef struct _SYSTEMTIME {
	WORD wYear;
	WORD wMonth;
	WORD wDayOfWeek;
	WORD wDay;
	WORD wHour;
	WORD wMinute;
	WORD wSecond;
	WORD wMilliseconds;
} SYSTEMTIME;

#define MAKELONG(a, b) ((long)(((WORD)(a & 0xffff)) | ((DWORD)((WORD)(b & 0xffff))) << 16))

#endif

#ifdef ENGINE_TARGET_WIN32
#	define WIN32_LEAN_AND_MEAN
#	define NOMINMAX
#	include <windows.h>
#	undef LoadString

#ifndef EDITOR_MODE
#	if defined(_DEBUG)
#		define _CRTDBG_MAP_ALLOC
#		include <stdlib.h>
#		include <crtdbg.h>
#	endif
#endif

#	include "Platform/win.h"
#endif // ENGINE_TARGET_WIN32

#ifdef ENGINE_TARGET_IPHONE
#include "Platform/iphone.h"
#endif

#ifdef ENGINE_TARGET_LINUX
#include "../android/jni/android/android.h"
#include <stdint.h>
#endif

#if !defined(ENGINE_TARGET_WIN32)
	int strcpy_s(char* Dst, size_t DstSize, const char* Src);
	int strcat_s(char* Dst, size_t DstSize, const char* Src);
	int memcpy_s(void* Dst, size_t DstSize, const void* Src, size_t MaxCount);
	int memmove_s(void* Dst, size_t DstSize, const void* Src, size_t MaxCount);
	int sscanf_s(const char* Src, const char* Format, ...);
	int sprintf_s(char* DstBuf, size_t DstSize, const char* Format, ...);
#endif

#endif // _PLATFORM_H_
