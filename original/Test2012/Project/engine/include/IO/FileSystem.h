#ifndef __FILESYSTEM_H__
#define __FILESYSTEM_H__

#ifdef _MSC_VER
#pragma once
#endif

#include "IO/Stream.h"
#include "RefCounter.h"

namespace IO {

#ifdef ENGINE_TARGET_LINUX
class FileSystem;

struct FileInfo {
    enum Type {TYPE_NORMAL, TYPE_2X, TYPE_HD, TYPE_HD2X};
    FileInfo() : system(0), type(TYPE_NORMAL) {}

    FileInfo(const std::string &p, FileSystem *fs, Type t)
        : path(p), system(fs), type(t) {}

    std::string path;
    FileSystem* system;
    Type type;
};
#endif

class FileSystem : public RefCounter {
public:
	FileSystem(const std::string& root);

	virtual bool FileExists(const std::string& filename) const = 0;

	virtual void FindFiles(const std::string& filespec, std::vector<std::string>& result) = 0;

	virtual InputStreamPtr OpenRead(const std::string& filename) = 0;

	virtual OutputStreamPtr OpenWrite(const std::string& filename) = 0;

	virtual StreamPtr OpenUpdate(const std::string& filename) = 0;

#ifdef ENGINE_TARGET_LINUX
    virtual void GenIndexMap(std::map<std::string, FileInfo> &map, const std::string& prefix, FileInfo::Type) = 0;
	virtual void RemoveFile(const std::string& filename) = 0;
#endif

protected:
	const std::string& GetRoot() const;

private:
	std::string _root;

private:
	DISALLOW_COPY_AND_ASSIGN(FileSystem);
};

typedef boost::intrusive_ptr<FileSystem> FileSystemPtr;


} // namespace IO

#endif // __FILESYSTEM_H__
