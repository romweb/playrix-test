#ifndef __PACKFILESYSTEM_H__
#define __PACKFILESYSTEM_H__

#ifdef _MSC_VER
#pragma once
#endif

#include "IO/FileSystem.h"
#include "IO/Stream.h"

namespace IO {

class PackFileSystem : public FileSystem {
public:
	PackFileSystem(const std::string& root);

	bool FileExists(const std::string& filename) const;

	void FindFiles(const std::string& filespec, std::vector<std::string>& result);

#ifdef ENGINE_TARGET_LINUX
    void GenIndexMap(std::map<std::string, FileInfo> &map, const std::string& prefix, FileInfo::Type);

	void RemoveFile(const std::string& filename);
#endif

	InputStreamPtr OpenRead(const std::string& filename);

	OutputStreamPtr OpenWrite(const std::string& filename);

	StreamPtr OpenUpdate(const std::string& filename);
};

} // namespace IO

#endif // __PACKFILESYSTEM_H__
