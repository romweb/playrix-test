#ifndef __BINARYWRITER_H__
#define __BINARYWRITER_H__

#if _MSC_VER > 1200
#pragma once
#endif // _MSC_VER

namespace IO {

class OutputStream;

class BinaryWriter {
public:
	explicit BinaryWriter(OutputStream* stream);

	OutputStream* GetBaseStream() const { return _stream; }

	void WriteInt8(int8_t value);
	void WriteInt16(int16_t value);
	void WriteInt32(int32_t value);
	void WriteInt64(int64_t value);
	void WriteUInt8(uint8_t value);
	void WriteUInt16(uint16_t value);
	void WriteUInt32(uint32_t value);
	void WriteUInt64(uint64_t value);
	void WriteBool(bool value);
	void WriteFloat(float value);
	void WriteDouble(double value);
	void WriteUtf8String(const std::string& value);
	void WriteUtf8String(const char* value);

private:
	OutputStream* _stream;
};

} // namespace IO

#endif // __BINARYWRITER_H__
