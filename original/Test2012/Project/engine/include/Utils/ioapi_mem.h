#ifndef IOAPI_MEM_H_
#define IOAPI_MEM_H_

#include <ioapi.h>

void fill_memory_filefunc OF((zlib_filefunc_def* pzlib_filefunc_def));
#endif /* end of include guard: IOAPI_MEM_H */
