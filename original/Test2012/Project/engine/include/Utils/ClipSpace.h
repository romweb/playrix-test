#pragma once
#include <vector>

/**
* ��������������� ����� ��� ���������� ��������� ������� ��������� �� ������
*/
namespace clipping{
	struct Vertex;
	struct ClipPlane{
		double a, b, c;

		ClipPlane(double x1, double y1, double x2, double y2){
			a = y1 - y2;
			b = x2 - x1;
			c = x1 * y2 - x2 * y1;

			double nn = 1.0f / sqrt(a * a + b * b);
			a *= nn;
			b *= nn;
			c *= nn;
		}

		double factor(double x, double y){
			return a * x + b * y + c;
		}

		Vertex intersection(ClipPlane other);
	};

	struct Vertex{
		double x, y;

		Vertex(double x, double y):x(x), y(y){
		}

		bool operator==(Vertex v) const{
			return x == v.x && y == v.y;
		}

		bool operator!=(Vertex v) const{
			return !(*this == v);
		}
	};

	class ClipSpace{
	public:
		void setViewport(double x, double y, double w, double h){
			this->x = x;
			this->y = y;
			this->w = w;
			this->h = h;
		}

		void push(ClipPlane plane){
			planes.push_back(plane);
		}

		void pop(){
			planes.pop_back();
		}

		/**
		* ���������� ������ ����� � ������� ������ �� ������� �������.
		* ������ ����� ��������� �������� �������������, �� �������� ����� ��������� ���������.
		*/
		std::vector<Vertex>& findPoly(){
			out.clear();

			out.push_back(Vertex(x, y));
			out.push_back(Vertex(x + w, y));
			out.push_back(Vertex(x + w, y + h));
			out.push_back(Vertex(x, y + h));

			for ( size_t i = 0; i < planes.size(); i++ ){
				ClipPlane& plane = planes[i];
				bool positiveFactor = false;
				bool negativeFactor = false;
				double eps = 1e-3f;
				for ( size_t j = 0; j < out.size(); j++ ){
					Vertex& v = out[j];
					double f = plane.factor(v.x, v.y);
					if ( f > eps ){
						positiveFactor = true;
					}
					if ( f < -eps ){
						negativeFactor = true;
					}
				}
				if ( !negativeFactor ){
					continue;
				}
				if ( !positiveFactor ){
					out.clear();
					break;
				}

				Vertex prev = out.back();
				double prevFactor = plane.factor(prev.x, prev.y);
				for ( size_t j = 0; j < out.size(); j++ ){
					Vertex& v = out[j];
					double f = plane.factor(v.x, v.y);
					if ( f < -eps ){
						//point discarded
						if ( prevFactor > eps ){
							Vertex iv = plane.intersection(ClipPlane(prev.x, prev.y, v.x, v.y));
							temp.push_back(iv);
						}
					}else{
						//point kept
						if ( prevFactor < -eps ){
							Vertex iv = plane.intersection(ClipPlane(prev.x, prev.y, v.x, v.y));
							temp.push_back(iv);
						}
						temp.push_back(v);
					}
					prevFactor = f;
					prev = v;
				}
				out = temp;
				temp.clear();
			}
            
            return out;
		}

	private:
		double x, y, w, h;
		std::vector<Vertex> out;
		std::vector<Vertex> temp;
		std::vector<ClipPlane> planes;
	};

	Vertex ClipPlane::intersection(ClipPlane other){
		double detA = a * other.b - b * other.a;
		double detB1 = -c * other.b + b * other.c;
		double detB2 = -a * other.c + c * other.a;

		return Vertex(detB1 / detA, detB2 / detA);
	}
}