#ifndef _IGAMEINFO_H_
#define _IGAMEINFO_H_

#include "DataStore.h"

class IGameInfo
{
protected:

	// глобальные переменные, сохраняются/загружаются из XML (global:)
	DataStore _globals;

	// глобальные свойства, создаются на время работы программы (temp:)
	DataStore _properties;

	// возможно сюда стоит добавить константы и локальные переменные (const и local:), но пока без них обойдемся
	DataStore _consts;

protected:

	void LoadGlobalData(Xml::TiXmlElement* elem);
	void SaveGlobalData(Xml::TiXmlElement* elem);
	void LoadConstData(Xml::TiXmlElement* elem);
	
	// доопределить в потомке для поддержки getString("local:..."), на вход уже пойдет просто имя переменной
	virtual std::string getLocalProperty(const std::string &name, const std::string &def = "") = 0;
	virtual void setLocalProperty(const std::string &name, const std::string &value) = 0;

public:

	IGameInfo();
	virtual ~IGameInfo() {}

	// выдают/задают значение строки по шаблону "тип_хранения:имя", например temp:ActivePlayerName
	std::string getProperty(const std::string &name, const std::string &def = "");
	void setProperty(const std::string &name, const std::string &value);
	template <class T>
	void setProperty(const std::string &name, const T &value) {
		std::string strValue = utils::lexical_cast(value);
		setProperty(name, strValue);
	}


	// Функции для работы с глобальными переменными
	int getGlobalInt(const std::string &name, int def = 0);
	void setGlobalInt(const std::string &name, int value);

	float getGlobalFloat(const std::string &name, float def = 0);
	void setGlobalFloat(const std::string &name, float value);
	
	double getGlobalDouble(const std::string &name, double def = 0);
	void setGlobalDouble(const std::string &name, double value);

	bool getGlobalBool(const std::string &name, bool def = false);
	void setGlobalBool(const std::string &name, bool value);

	std::string getGlobalString(const std::string &name, const std::string &def = "");
	void setGlobalString(const std::string &name, const std::string &value);

	IPoint getGlobalPoint(const std::string &name, const IPoint &def = IPoint());
	void setGlobalPoint(const std::string &name, const IPoint &value);

	IRect getGlobalRect(const std::string &name, const IRect &def = IRect());
	void setGlobalRect(const std::string &name, const IRect &value);

	int getGlobalArrInt(const std::string &name, int index, int def = 0);
	void setGlobalArrInt(const std::string &name, int index, int value);

	float getGlobalArrFloat(const std::string &name, int index, float def = 0);
	void setGlobalArrFloat(const std::string &name, int index, float value);
	
	double getGlobalArrDouble(const std::string &name, int index, double def = 0);
	void setGlobalArrDouble(const std::string &name, int index, double value);
	
	bool getGlobalArrBool(const std::string &name, int index, bool def = false);
	void setGlobalArrBool(const std::string &name, int index, bool value);
	
	std::string getGlobalArrString(const std::string &name, int index, const std::string &def = "");
	void setGlobalArrString(const std::string &name, int index, const std::string &value);
	
	IPoint getGlobalArrPoint(const std::string &name, int index, const IPoint &def = IPoint());
	void setGlobalArrPoint(const std::string &name, int index, const IPoint &value);
	
	IRect getGlobalArrRect(const std::string &name, int index, const IRect &def = IRect());
	void setGlobalArrRect(const std::string &name, int index, const IRect &value);
	
	bool globalNameDefined(const std::string &name);
	
	// Функции для работы с константами
	int getConstInt(const std::string &name, int def = 0);
	float getConstFloat(const std::string &name, float def = 0);
	double getConstDouble(const std::string &name, double def = 0);
	bool getConstBool(const std::string &name, bool def = false);
	std::string getConstString(const std::string &name, const std::string &def = "");
	IPoint getConstPoint(const std::string &name, const IPoint &def = IPoint());
	IRect getConstRect(const std::string &name, const IRect &def = IRect());
	int getConstArrInt(const std::string &name, int index, int def = 0);
	float getConstArrFloat(const std::string &name, int index, float def = 0);
	double getConstArrDouble(const std::string &name, int index, double def = 0);
	bool getConstArrBool(const std::string &name, int index, bool def = false);
	std::string getConstArrString(const std::string &name, int index, const std::string &def = "");
	IPoint getConstArrPoint(const std::string &name, int index, const IPoint &def = IPoint());
	IRect getConstArrRect(const std::string &name, int index, const IRect &def = IRect());

	// для работы с локальными переменными
	virtual int getLocalInt(const std::string &name, int def = 0) = 0;
	virtual void setLocalInt(const std::string &name, int value) = 0;

	virtual std::string getLocalString(const std::string &name, std::string def = "") = 0;
	virtual void setLocalString(const std::string &name, const std::string &value) = 0;

	virtual bool getLocalBool(const std::string &name, bool def = false) = 0;
	virtual void setLocalBool(const std::string &name, bool value) = 0;

	virtual float getLocalFloat(const std::string &name, float def = 0) = 0;
	virtual void setLocalFloat(const std::string &name, float value) = 0;

	virtual bool localNameDefined(const std::string &name) = 0;
	
};

extern IGameInfo *Engine_gameInfoPointer;

#endif
