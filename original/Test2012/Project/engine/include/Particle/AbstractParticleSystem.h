#ifndef ABSTRACT_PARTICLE_SYSTEM_H
#define ABSTRACT_PARTICLE_SYSTEM_H

#include "Render/VertexBuffer.h"
#include "Render/RenderDevice.h"
#include "Render/Texture.h"
#include "Core/ResourceManager.h"
#include "Core/Locale.h"
#include "Utils/Math.hpp"
#include "Utils/Str.h"
#include "Utils/random.hpp"
#include "File.h"
#include "Spline.h"
#include "BinaryData.h"
#include "ParticleSystemStrings.h"
#include "RefCounter.h"

#ifdef THREADED
#include "ThreadSupport.h"
#endif

#include <boost/unordered_set.hpp>

namespace Render {
	class SpriteBatch;
}

class AbstractParticleSystem : public RefCounter
{
	friend class EffectManager;
	friend class EffectTree;

protected:
	AbstractParticleSystem(int version);

public:
	virtual ~AbstractParticleSystem();
	
	virtual AbstractParticleSystem *Clone() = 0;
	
	virtual void Upload() = 0;
	virtual void Release() = 0;
	virtual bool Load(BinaryDataLoader *d) = 0;
	virtual void Save(BinaryDataSaver *d) = 0;
	virtual bool Load(rapidxml::xml_node<> *elem) = 0;
	virtual void Save(rapidxml::xml_node<> *elem) = 0;
	virtual void Reset() = 0;
	virtual void Update(float dt) = 0;
	virtual void Draw(Render::SpriteBatch* batch = NULL) = 0;
	virtual void DrawBlend(Render::SpriteBatch* batch = NULL) = 0;
	virtual bool IsAdditive() const = 0;
	virtual bool IsDead() = 0;
	virtual bool IsPermanent() = 0;
	virtual void Finish() = 0;
	virtual void SetPositionOffset(float x, float y) = 0;
	virtual void SetScale(float scale) = 0;
	virtual void SetAlphaFactor(float alpha) = 0;
	virtual void SetRGBFactor(float r, float g, float b) = 0;
	virtual void SetInitialForParamKey(const char *id, size_t key_index, float initial) = 0;
	virtual int FrameWidth() = 0;
	virtual int FrameHeight() = 0;
	virtual const std::string& GetTextureName() const = 0;
	virtual Render::Texture* GetTexture() const = 0;
	virtual size_t MemoryInUse() const = 0;

	int Version() const;
	void SetResourceGroup(const std::string &group) { _resourceGroup = group; }

	static const int MAX_PARTICLES;

	static const char* PARAM_X;
	static const char* PARAM_Y;
	static const char* PARAM_V;
	static const char* PARAM_SPIN;
	static const char* PARAM_ANGLE;
	static const char* PARAM_SIZE;
	static const char* PARAM_YSIZE;
	static const char* PARAM_FPS;
	static const char* PARAM_ALPHA;
	static const char* PARAM_RED;
	static const char* PARAM_GREEN;
	static const char* PARAM_BLUE;

	static std::string NormalizeTextureName(const std::string &name);
	static void SetTexturesPath(const std::string &path);
	static const std::string& GetTexturesPath();
	static void SetErrorMessage(const std::string &msg);
	static std::string ErrorMessage();
	static void CreateTexturePlug();
	static void DestroyTexturePlug();	
	static size_t TotalMemoryInUse();

protected:
	static const float PI;
	static const float RAD;
	static Render::Texture* _texture_plug;
	static std::string _error_message;
	static std::string _textures_path;

	int _version;
	std::string _resourceGroup;

	std::string BoolToString(bool value);

private:
	AbstractParticleSystem(const AbstractParticleSystem&);
	AbstractParticleSystem& operator=(const AbstractParticleSystem&);
	
	typedef boost::unordered_set<AbstractParticleSystem*> Instances;
	static Instances _instances;

#ifdef THREADED
	static MUTEX_TYPE _mutex;
#endif
	
	void InsertThisToList();
	void RemoveThisFromList();
};


typedef AbstractParticleSystem ParticleSystem;

typedef boost::intrusive_ptr<ParticleSystem> ParticleSystemPtr;

#endif