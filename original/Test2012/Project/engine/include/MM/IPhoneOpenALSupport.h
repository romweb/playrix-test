/*
 *  IPhoneOpenALSupport.h
 *  Engine
 *
 *  Created by Slava on 03.08.10.
 *  Copyright 2010 Playrix Entertainment. All rights reserved.
 *
 */

#ifndef IPHONE_OPEN_AL_SUPPORT_HEADER
#define IPHONE_OPEN_AL_SUPPORT_HEADER

#include "MM/AudioManager.h"

bool LoadUncompressed(const char* filename, unsigned char** data, ALsizei& size, ALenum& format, ALsizei& rate);

#endif