#ifndef __AUDIOSOURCEBUFFER_H__
#define __AUDIOSOURCEBUFFER_H__

#ifdef _MSC_VER
#pragma once
#endif

#include "AudioSource.h"

namespace MM {

class AudioSourceBufferConst : public AudioSource {
public:
	AudioSourceBufferConst(const unsigned char* buffer, size_t length, int channels, int rate, int bits);
	
	int GetChannels() const { return _channels; }
	
	int GetRate() const { return _rate; }
	
	int GetBits() const { return _bits; }
	
	size_t GetLengthInBytes() const { return _length; }
	
	size_t Read(void* buffer, size_t count);
	
	bool IsEof() const;
	
	void Rewind();

protected:
	const unsigned char* _buffer;
	size_t _length;
	size_t _position;
	int _channels;
	int _rate;
	int _bits;
};

class AudioSourceBufferOwned : public AudioSourceBufferConst {
public:
	AudioSourceBufferOwned(const unsigned char* buffer, size_t length, int channels, int rate, int bits)
		: AudioSourceBufferConst(buffer, length, channels, rate, bits)
	{
	}
	
	~AudioSourceBufferOwned() {
		delete[] _buffer;
	}
};

} // namespace MM

#endif // __AUDIOSOURCEBUFFER_H__
