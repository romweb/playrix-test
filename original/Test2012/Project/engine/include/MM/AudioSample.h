#ifndef __AUDIOSAMPLE_H__
#define __AUDIOSAMPLE_H__

#ifdef _MSC_VER
#pragma once
#endif

#include "MM/AudioSource.h"
#include "RefCounter.h"

#if defined(ENGINE_TARGET_IPHONE) || defined(ENGINE_TARGET_MACOS)
#	include <OpenAL/al.h>
#	include <OpenAL/alc.h>
#else
#	include <al.h>
#	include <alc.h>
#endif

template<typename T>
class ObservableValue {
public:
	ObservableValue(const T& v)
		: _value(v), _changed(true)
	{ }

	ObservableValue(const ObservableValue& rhs)
		: _value(rhs._value), _changed(true)
	{ }

	ObservableValue& operator=(const T& value) {
		_value = value;
		_changed = true;
		return *this;
	}

	ObservableValue& operator=(const ObservableValue& rhs) {
		if (this != &rhs) {
			_value = rhs._value;
			_changed = true;
		}
		return *this;
	}

	operator const T&() const { return _value; }

	const T& Data() const { return _value; }

	bool IsChanged() const { return _changed; }

	void ClearChanged() { _changed = false; }

private:
	T _value;

	bool _changed;
};

namespace MM {

class AudioManager;

///
/// �������� ��������, ������������� �������� �������� ������
///
class AudioSample : public RefCounter {
public:
	AudioSample(AudioManager* owner, AudioSource* audio, bool streamable, bool is_track);
	
	~AudioSample();
	
	void Play();
	
	void Pause();
	
	void Stop();
	
	bool IsPlaying() const;

	bool IsPaused() const;
	
	bool IsFinished() const;
	
	void SetVolume(float value);
	
	// ������ ���� (�������� ���������������): 0.5 .. 2.0
	void SetPitch(float value);
	
	// -1 - ����� �����, 0 - ��� ������, 1 - ������ �����
	void SetPan(float value);
	
	void SetLooping(bool value);
	
	bool IsLooping() const;
	
	void Update();
	
	bool IsPlayingSync() const { return _currentState == PLAYING; }
	
	bool IsPausedSync() const { return _currentState == PAUSED; }

protected:
	bool CreateSourceIfNeeded();

	void Destroy();

	bool PlayNonStreamable();

	void DoPlay();
	
	void DoPause();

	void DoStop();

	// ������ ������ ������ �� ������ � �������� � ������� ���������������.
	// ���� buffer <= 0, �� ����� ������ ����� �����.
	bool EnqueueStreamable(ALuint buffer = 0);

	// ���������� ���������������, ���� �������� ��� ����������
	bool GoOnStreamable();
	
	// ������� ����� �� _buffers � AL
	void DeleteBuffer(ALuint buffer);

protected:
	AudioManager* _owner;
	
	AudioSourcePtr _audio;
	
	ALuint _source;
	
	typedef std::deque<ALuint> Buffers;
	
	Buffers _buffers; // ������ ���� ��������� �������. ��� ��������� ��� ����������� AudioSample.
	
	bool _streamable;

	bool _is_track;

	ObservableValue<bool> _looping;
	
	ObservableValue<float> _volume;

	ObservableValue<float> _pitch;

	ObservableValue<float> _pan;

	enum { NONE, PLAYING, PAUSED, STOPPED } _currentState, _deferredState;
};

typedef boost::intrusive_ptr<AudioSample> AudioSamplePtr;

} // namespace MM

#endif // __AUDIOSAMPLE_H__
