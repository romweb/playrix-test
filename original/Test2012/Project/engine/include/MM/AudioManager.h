#ifndef __AUDIOMANAGER_H__
#define __AUDIOMANAGER_H__

#ifdef _MSC_VER
#pragma once
#endif

#include "MM/AudioSample.h"
#include "MM/AudioResource.h"

#ifdef THREADED
#	include "ThreadSupport.h"
#endif

#if defined(ENGINE_TARGET_IPHONE) || defined(ENGINE_TARGET_MACOS)
#	include <OpenAL/al.h>
#	include <OpenAL/alc.h>
#else
#	include <al.h>
#	include <alc.h>
#endif

#include <boost/function.hpp>

namespace MM {

class AudioManager {
public:
	AudioManager();	
	~AudioManager();
	
	static AudioManager& Self();
	
	void Initialize();
	void Shutdown();

	void StartUpdateThread();
	void StopUpdateThread();

	bool IsDevicePresent() const;
	
	/// ��������� �������� �����.
	/// ���������� ���������� ������������� ������ ��������������� ��� -1 ��� ������.
	long PlaySample(const std::string& id, bool looping = false, float volume = 1.0f, float pitch = 1.0f, bool asTrack = false);
	/// ��������� �������� ����. ���������� PlaySample.
	long PlayTrack(const std::string& id, bool looping = false, float volume = 1.0f, float pitch = 1.0f);
	/// ������ ���� � ������� �� ���������������.
	long QueueSample(const std::string& id, long sample, bool looping = false, float volume = 1.0f, float pitch = 1.0f, bool asTrack = false);
	/// ������ ���� � ������� �� ���������������. ���������� QueueSample.
	long QueueTrack(const std::string& id, long sample, bool looping = false, float volume = 1.0f, float pitch = 1.0f);
	/// ��������� ������������� ������ �� ����������
	bool IsValid(long id) const;
	/// ���������� ������������� ������ ��� �������� ������������ ����� (��� -1 � ������ ���������� ��������)
	long GetTrackId() const;	
	/// ��������� / ��������� ��� �������� ������ �����
	void PauseAll(bool pause, bool butTrack = false);
	/// ��������� / ��������� ��� �������� ������ ����� ���������
	void PauseAllSync(bool pause, bool butTrack = false);
	/// ��������� / ��������� ��� �������� ������ ������ (����� �����)
	void PauseAllButTrack(bool pause);
	/// ��������� / ��������� ��� �������� ������ ������ (����� �����) ���������
	void PauseAllButTrackSync(bool pause); 
	/// ���������/��������� �������� �������� �����
	void PauseSample(long id, bool pause);		
	/// ��������� �������� �� �����
	bool IsPaused(long id) const;
	/// ���������� ���������������
	void StopAll();
	/// ���������� ��������������� ��������� ������, ����� ����� ����� ���������� ���������� � ��� ��� ������� �������������
	void StopSample(long id);
	/// ���������� ��������������� �����, ����� ����� ����� ���������� ���������� � ��� ��� ������� �������������
	void StopTrack();
	/// ���������� ��������� (��� ����� ����������� ������� ��������� ����� 1.0f)
	/// �������� ��������� ����� �������� �� SoundVolume.
	void SetVolume(long id, float volume);
	/// ���������� ��������� (��� ����� ����������� ������� ��������� ����� 1.0f)
	/// �������� ��������� ����� �������� �� MusicVolume.
	void SetTrackVolume(float volume);
	/// �������� ��������� ������
	float GetVolume(long id) const;
	/// ���������� �������� (-1.0f - ������ ����� �����, 0 - ��� ������, 1.0f - ������ ������ �����)
	void SetPan(long id, float pan);
	/// ������������� ������� ��������� ������, ������� ���������� ��� �������� �����
	void SetCallback(long id, boost::function<void()> callback);
	/// �������� ��� ������ �� �������� �����. ���� ������ targetVolume �������� �� ����, �� ��� ������ ����� ��������� � �������� ���������
	void FadeAll(float time, float targetVolume = 0.0f);
	/// �������� ��� ������, ����� ����� �� �������� �����.
	void FadeOutAllButTrack(float time);
	/// �������� ����� �� �������� �����. ���� ������ targetVolume �������� �� ����, �� ����� ����� ������� � �������� ���������
	void FadeSample(long id, float time, float targetVolume = 0.0f);
	/// �������� ������� ���� �� �������� �����
	void FadeOutTrack(float time);
	/// �������� ������� ���� �� �������� �����
	void FadeInTrack(const std::string& id, float time, bool looping = false);
	/// ������� ������� ���� ��������� �� �������� �����
	void ChangeTrack(const std::string& id, float time);
	/// �������� ��������� ������. ���� ��������� ��������� � ������ ����.
	float GetMusicVolume() const;
	/// ���������� ��������� ������. ���� ��������� ��������� � ������ ����.
	void SetMusicVolume(float value);
	/// �������� ��������� ������. ���� ��������� ��������� � ������ �����.
	float GetSoundVolume() const;
	/// ���������� ��������� ������. ���� ��������� ��������� � ������ �����.
	void SetSoundVolume(float value);
	/// �������� ��������� ������ ���������. ���� ��������� ���������� � ������ ����� � ������������� ���������� volumeMode = "ambience"
	float GetAmbienceVolume() const;
	/// ���������� ��������� ������ ���������. ���� ��������� ���������� � ������ ����� � ������������� ���������� volumeMode = "ambience"
	void SetAmbienceVolume(float value);
	
	// ������ ��������
	// ����� � iOS � ������ �����-���������� (������, ���������, � �.�.)
	void InterruptNow();
	// ������ ��������� ����������
	// ����� � iOS � ����� �����-����������
	void ContinueNow();	
	// �������� ������� ��������
	// ����� � iOS, ����� �������� �����-���������� (������, ���������, � �.�.)
	void SuspendContext();	
	// ������������ ������� ��������
	// ����� � iOS, ����� ������������� �����-����������
	void ResumeContext();

	bool IsContextSuspended() const;
	
	/// �������� ���������
	void Update(float dt);

private:
	AudioManager(const AudioManager&);
	AudioManager& operator=(const AudioManager&);
	
	friend class AudioSample;

	/// ��������� ����� � ������ �� �������� ��-�� ������ �������
	void AddFailedBuffer(ALuint buffer);
	/// �������� ������� ������, ����������� � ������
	void DeleteFailedBuffers();

	/// ���������� ��������� �������� ��� �����
	ALuint GetTrackSource();
	/// ����������� �������� �����
	void FreeTrackSource(ALuint source);
	
private:
	ALCdevice* _device;
	ALCcontext* _context;
	
	typedef std::deque<ALuint> Buffers;
	Buffers _failedBuffers;

	typedef std::deque<ALuint> TrackSources;
	TrackSources _trackSources;
	
	float _musicVolume;
	float _soundVolume;
	float _ambienceVolume;
	
	long _trackId;
	
	std::string _nextTrackId;
	
	float _pauseBeforeNextTrack;
	float _timeFadeInNextTrack;
	
	bool _paused;
	bool _suspended;
	
	static long _nextId;
	
	struct SampleInfo {
		std::string resource;
		long id;
		AudioSamplePtr sample;
		VolumeMode::Type volumeMode;
		float volumeFactor;
		float currentVolume;
		float targetVolume;
		float fadeTime;
		float targetFade;
		float pan;
		float pitch;
		bool looping;
		bool pausable;
		bool deferred;
		bool track;
		long next;
		boost::function<void()> onStopped;
		
		SampleInfo()
			: id(-1), volumeMode(VolumeMode::Sound), volumeFactor(1.0f), currentVolume(1.0f), targetVolume(1.0f), fadeTime(0.0f), targetFade(0.0f)
			, pan(0.0f), pitch(1.0f), looping(false), pausable(true), deferred(false), track(false), next(0)
		{}
		
		SampleInfo(long id)
			: id(id), volumeMode(VolumeMode::Sound), volumeFactor(1.0f), currentVolume(1.0f), targetVolume(1.0f), fadeTime(0.0f), targetFade(0.0f)
			, pan(0.0f), pitch(1.0f), looping(false), pausable(true), deferred(false), track(false), next(0)
		{}
	};
	
	typedef std::map<long, SampleInfo> Samples;
	Samples _samples;
	
	typedef std::map<std::string, int> Mixes;
	Mixes _mixes;
	
#if defined(THREADED)

#if !defined(ENGINE_TARGET_WINDOWS_METRO)
	boost::scoped_ptr<boost::thread> _thread;
#endif // ENGINE_TARGET_WINDOWS_METRO

	mutable MUTEX_TYPE _mutex;

#endif // THREADED

private:
	void SetupSampleVolume(SampleInfo& sample);
	long DoPlaySample(long newSampleId, const std::string& id, bool looping, float volume, float pitch, bool asTrack);
};

extern AudioManager& manager;

} // namespace MM

#endif // __AUDIOMANAGER_H__
