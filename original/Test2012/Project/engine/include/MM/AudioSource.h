#ifndef __AUDIOSOURCE_H__
#define __AUDIOSOURCE_H__

#ifdef _MSC_VER
#pragma once
#endif

#include "RefCounter.h"

namespace MM {

///
/// �������� �������� ������
///
/// �� ���������� ������������ � ������, ������ ��������� ��������� ��������� ������
/// �� ������������� ������� �������� � PCM
///
class AudioSource : public RefCounter {
public:
	AudioSource() {}
	
	virtual int GetChannels() const = 0;
	
	virtual int GetRate() const = 0;
	
	virtual int GetBits() const = 0;
	
	virtual size_t GetLengthInBytes() const = 0;
	
	virtual size_t Read(void* buffer, size_t count) = 0;
	
	virtual bool IsEof() const = 0;
	
	virtual void Rewind() = 0;
	
	void ReadAll(std::vector<unsigned char>& buffer);
	
	void ReadSome(std::vector<unsigned char>& buffer, size_t size);

    static AudioSource* FromStream(IO::InputStream* stream);
	
	static AudioSource* FromBufferConst(const unsigned char* buffer, size_t length, int channels, int rate, int bits);
	
	static AudioSource* FromBufferOwned(const unsigned char* buffer, size_t length, int channels, int rate, int bits);
	
	static AudioSource* FromFileOgg(const std::string& filename);
	
	//static AudioSource* FromFileAiff(const std::string& filename));
	
	static AudioSource* FromFile(const std::string& filename);

private:
	AudioSource(const AudioSource&);
	AudioSource& operator=(const AudioSource&);
};

typedef boost::intrusive_ptr<AudioSource> AudioSourcePtr;

} // namespace MM

#endif // __AUDIOSOURCE_H__
