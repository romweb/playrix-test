#ifndef _PLATFORM_IPHONE_H_
#define _PLATFORM_IPHONE_H_

#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#ifdef __cplusplus
#include <string>
namespace utils
{

const char * GetIPhoneLocaleLang();
std::string GetDeviceID();
std::string GetDeviceDescription();
long getProcSize(); // size of application

#ifdef __OBJC__
UIViewController* getAppViewController();
#endif

}

#endif

// Некоторые коды клавиш
static const int VK_RETURN = 13;
static const int VK_BACK = 8;
static const int VK_ESCAPE = 27;
static const int VK_SPACE = 32;

// Для universal app данная функция возвращает, является ли железяка айпэдом или нет
// Для iphone приложений всегда false
// Для ipad приложений всегда true
bool DeviceIpad();

#endif
