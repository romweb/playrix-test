#ifndef _GUI_EDITBOX_H_
#define _GUI_EDITBOX_H_

#include "Widget.h"
#include "Render/RenderFunc.h"

namespace GUI
{

class EditBox
	: public Widget
{
public:
	EditBox(const std::string& name, rapidxml::xml_node<>* elem);

	void Update(float dt);

	virtual void Draw();

	virtual void AcceptMessage(const Message& message);

	virtual Message QueryState(const Message& message) const;

protected:
	std::string _text;
	
	size_t _ibeamPos;

    std::string _font;
	
    Color _fontColor;
    
	int _limit;

	float _timer;
	
	std::set<int> _denied;
};

}

#endif
