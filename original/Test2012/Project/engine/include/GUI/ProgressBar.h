/*
 *  ProgressBar.h
 *  Engine
 *
 *  Created by vasiliym on 13.05.10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef __PROGRESSBAR_H__
#define __PROGRESSBAR_H__
 
#pragma once

#include "GUI/Widget.h"
#include "Render/Texture.h"

namespace GUI {
	class ProgressBar: public Widget
	{
	public:
		ProgressBar(const std::string& name, rapidxml::xml_node<>* xmlElement);

		virtual void Draw();
		virtual void Update(float);
		virtual void AcceptMessage(const Message& message);

	private:
		int from, to;
		float pos, target, speed;
		Render::Texture* back, *front;
	};
}

#endif // __PROGRESSBAR_H__
