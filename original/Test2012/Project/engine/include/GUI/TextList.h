#ifndef __TEXTLIST_H__
#define __TEXTLIST_H__

#pragma once

#include "Widget.h"
#include "Core/ControllerManager.h"

namespace GUI {

class ActiveTextListItemController : public IController
{
public:
	int _activeAlpha;
	bool _alphaDir;
	float _alphaTime;

	ActiveTextListItemController();

	virtual void Update(float dt);
	virtual bool isFinish();
};

typedef boost::intrusive_ptr<ActiveTextListItemController> ActiveTextListItemControllerPtr;

class TextList : public Widget
{
public:
	TextList(const std::string& name, rapidxml::xml_node<>* xmlElement);
	
	void pushItem(const std::string& item);
	void eraseItem(const std::string& item);
	void clear();

	void SetActive(const std::string& item);
	void UpdateButtons(void);

	virtual void Draw();
	virtual void MouseDoubleClick(const IPoint& mouse_pos);
	virtual bool MouseDown(const IPoint& mouse_pos);
	virtual void AcceptMessage(const Message& message);
	virtual Message QueryState(const Message& message) const;

private:
	friend class ScrollTextController;

	typedef std::list<std::string> ListItems;

	ListItems _item_list;

	std::string _activeFont;
	std::string _normalFont;

	std::string _buttonUpName;
	std::string _buttonDownName;

	int _string_step;
	int _num_strings;
	int _start_string;

	int _offset;
	int _alpha;

	int _choosed_string;

	bool _isScrolled;

	class ScrollTextController : public IController
	{
	public:
		enum ScrollDirect
		{
			Up,
			Down
		};

		ScrollTextController(TextList* list, ScrollDirect dir, float speed = 0.2f);

		virtual void Update(float dt);
		virtual bool isFinish();

	private:
		TextList* _list;
		ScrollDirect _dir;
		float _speed;
	};

	static ActiveTextListItemControllerPtr activeTextListItemController;
};

} // namespace GUI

#endif // __TEXTLIST_H__
