#ifndef __SOLIDRECT_H__
#define __SOLIDRECT_H__

#pragma once

#include "GUI/Widget.h"
#include "Render/Texture.h"

namespace GUI {

	/// Прямоугольник залитый заданным цветом.

	/// Для создания экземпляра через Layer используется код подобный следующему:
	/// \code
	/// <SolidRect name="BlueRect">
	/// 	<rectangle x="150" y="200" width="200" height="200"/>
	/// 	<color r="0" g="0" b="255" a="255"/>
	/// </SolidRect>
	/// \endcode
	/// Где rectangle - координаты прямоугольника, color - цвет.

	class SolidRect : public GUI::Widget
	{
	public:
		SolidRect(const std::string& name);
		SolidRect(const std::string& name, rapidxml::xml_node<>* xmlElement);
		
		virtual void Draw();
	};
}

#endif // __SOLIDRECT_H__
