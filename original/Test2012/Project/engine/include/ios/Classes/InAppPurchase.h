//
//  InAppPurchase.h
//
//  Created by Slava on 4/25/11.
//  Copyright 2011 Playrix Entertainment. All rights reserved.
//

//Code ported (copied) from iphone version -- VLAD KHOREV

#ifdef __OBJC__

#import "MPGCenter.h"

extern MPGCenter* mpgCenter;

@interface InAppPurchase : NSObject {

}

+(void)prepareStoreWithView:(UIView*)view;

//+(void)setFullVersion;
//+(void)setLiteVersion;
//+(bool)isFullVersion;

@end

#endif


#ifdef __cplusplus

class InAppPurchaseInterface {
public:
	static void RestoreButtonActivity();

    static bool IsAnyNagOnScreen();
    
    static std::string GetAppPurchaseID();
    
	static void Buy();
	
	static void ShowActivityIndicator();
	
	static void HideActivityIndicator();
	
	static void Update();
	
	//static bool IsFullVersion();
	
	static bool IsProcessing();

};

#endif
