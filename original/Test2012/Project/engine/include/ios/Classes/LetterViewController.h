#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

#import "MPGCenter.h"

/*
 Do not change this code!
 Vlad Khorev
 */

//MailComposeViewController
@interface LetterViewController : MFMailComposeViewController <MFMailComposeViewControllerDelegate> {
	
@private
	
	UIViewController* parent;
	//Parent controller
	
	SEL SelectorOnLetterSent;
	//Selector of mpgCenter to be called when mail sent successfully
	
	SEL SelectorOnLetterCanceled;
	//Selector of mpgCenter to be called when mail failed to be sent
}

-(id)initWithParent:(UIViewController *)parentViewController;

-(void)setSelectorOnLetterSent:(SEL)selectorOnLetterSent;

-(void)setSelectorOnLetterCanceled:(SEL)selectorOnLetterCanceled;

-(void)show;

@end

