
//Same as in engine + some new functions -- VLAD KHOREV

namespace utils
{

	const char * GetLocaleLang();
	int GetPreferredLangID();
	const std::string& GetLocalePrefix();
	void CheckLocalResource(std::string& file);
		
}