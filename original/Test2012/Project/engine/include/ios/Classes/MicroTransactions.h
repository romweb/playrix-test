//
//  MicroTransactions.h
//  fishdom_ipad
//
//  Created by vasiliym on 21.02.11.
//  Copyright 2011 Playrix Ent. All rights reserved.
//
#ifndef _MICROTRANSACTIONS_H
#define _MICROTRANSACTIONS_H

#include <string>
#include <vector>

// Покупки работают так:
//
// При запуске программы нужно инициализировать магазин.
// Для этого в метод InitStore передаётся список всех идентификаторов покупок.
// Магазин асинхронно запрашивает объекты всех продуктов с сервера.
// Объекты продуктов нужны, чтобы затем совершать покупки.
//
// Если инициализация не закончена, то действия MakePurchase и RestorePurchases
// будут отложены и автоматически выполнены при успешном завершении инициализации.
//
// Если инициализация завершилась неуспешно (например, при запуске игры не было интернета),
// то при следующем вызове MakePurchase (или RestorePurchases) будет новая попытка инициализации магазина.
//
// Большинство операций - асинхронные.
//
// О некоторых событиях информация приходит через NSNotificationCenter.
// Имена уведомлений (с описанием передаваемых данных в userInfo):
// * MicroTransactionInvalidIDs : "ids" => NSArray*[NSString * productIds] -- при запросе продукта выяснилось, что эти Id невалидны.
// * MicroTransactionInitialized : nil -- инициализация магазина успешно завершена.
// * MicroTransactionInitFailed : "error" => NSError* -- инициализация магазина завершилась с ошибкой.
// * MicroTransactionRestoreFinished : nil -- все покупки восстановлены.
// * MicroTransactionRestoreCancelled : nil -- восстановление покупок отменено.
// * MicroTransactionRestoreFailed : "error" => NSError* -- восстановление покупок завершилась с ошибкой.
// * MicroTransactionPurchaseFinished : "productId" -- покупка совершена
// * MicroTransactionCancelled : "productId" -- транзакция отменена
// * MicroTransactionFailed : "productId" -- транзакция не удалась
// * MicroTransactionRemoved : "productId" -- транзакция удалена. Этим заканчивается жизненный цикл любой транзакции.
// События верификации.
// Если включена верификация, то следующие события должны быть обработаны, и в случае успеха должен быть вызван InApp::FinishPurchase(productId).
// В случае неуспешной транзакции нельзя вызывать InApp::FinishPurchase, т.к. это автоматически засчитает транзакцию как совершённую.
// Явно отмененить транзакцию вроде как нельзя, поэтому оставляем дело за таймаутом.
// * MicroTransactionVerificationStart: "productId"
// * MicroTransactionVerificationOk: "productId"
// * MicroTransactionVerificationFailed : "productId", (параметр "error" есть только при ошибке соединения).

#ifdef __cplusplus

namespace InApp {

/// Инициализация магазина.
/// Это асинхронная операция: системе требуется загрузить объекты покупок с сервера.
/// autoRestore - надо ли обращаться в iTunes за списком уже совершённых покупок (требует ввода пароля)
void InitStore(const std::vector<std::string>& identifiers, bool autoRestore = false);

#ifdef __OBJC__

/// Совершить покупку (асинхронно).
/// Требует ввода пароля.
/// Возвращает true если покупка была успешно поставлена в очередь.
bool MakePurchase(const std::string& _id, NSDictionary * info, int quantity = 1);

NSDictionary * FindPurchase(NSString * productId, BOOL remove = false);

#endif

/// Инициализировался ли магазин.
bool StoreIsInited();

/// Инициализация магазина в процессе?
bool IsInitInProcess();

void ReinitStore();

void Release();

/// Включить или выключить верификацию.
void SetVerificationRequired(bool b);

bool IsVerificationInProgress();

/// Возвращает false если пользователь запретил покупки, или если магазин ещё не инициализирован.
bool CanPurchase();

/// Восстановить все покупки (асинхронно).
/// Требует ввода пароля, поэтому обычно вызывается при нажатии кнопки "Already Paid".
/// Возвращает true, если процесс запущен.
bool RestorePurchases();

void FinishPurchase(const std::string& productId);

/// Возвращает ссылку на вектор с идентификаторами продуктов.
/// Это те же идентификаторы, которые были переданы в InitStore.
const std::vector<std::string>& GetProducts();

/// Информация по продуктам.
std::vector<std::string> GetActiveProducts(); ///< Возвращает id продуктов, успешно полученных с сервера.
bool HasProduct(const std::string& _id); ///< Возвращает, есть ли такой продукт.
bool HasActiveProduct(const std::string& _id); ///< Возвращает, получен ли с сервера этот продукт.
int ProductsCount(); ///< Кол-во продуктов в магазине.
std::string ProductName(int n); ///< n - порядковый номер продукта в магазине (n < ProductsCount())
std::string ProductDesc(int n);
std::string ProductPrice(int n);
std::string ProductId(int n);
std::string ProductName(const std::string& _id);
std::string ProductDesc(const std::string& _id);
std::string ProductPrice(const std::string& _id);
std::string getCurrencyCode(const std::string& productId);
    
/// С помощью следующих методов можно узнать, идёт ли процесс покупки.
bool HasActiveTransactions();
bool HasActiveTransactionsForProductId(const std::string& _id);
bool IsBusy(); // Учитывает deferredRestorePurchases, deferredPurchases и HasActiveTransactions.

} // End on namespace

#endif // __cplusplus

#endif // _MICROTRANSACTIONS_H
