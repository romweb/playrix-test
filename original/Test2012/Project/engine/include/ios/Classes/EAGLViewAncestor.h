//
//  EAGLView.h
//
//  Created by Slava on 12/3/10.
//  Copyright 2010 Playrix Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MediaPlayer/MediaPlayer.h>

#import "WebViewDelegate.h"


// This class wraps the CAEAGLLayer from CoreAnimation into a convenient UIView subclass.
// The view content is basically an EAGL surface you render your OpenGL scene into.
// Note that setting the view non-opaque will only work if the EAGL surface has an alpha channel.
@interface EAGLViewAncestor : UIView
{    
@private
    BOOL animating;
    
    BOOL displayLinkSupported;
    
    NSInteger animationFrameInterval;
    
    // Use of the CADisplayLink class is the preferred method for controlling your animation timing.
    // CADisplayLink will link to the main display and fire every vsync when added to a given run-loop.
    // The NSTimer class is used only as fallback when running on a pre 3.1 device where CADisplayLink
    // isn't available.
    id displayLink;

    NSTimer *animationTimer;
	
	MoreGamesDelegate *mgDelegate;
    
    MPMoviePlayerController* mplayer;        
}

@property (readonly, nonatomic, getter=isAnimating) BOOL animating;
@property (nonatomic) NSInteger animationFrameInterval;
@property (nonatomic, retain) NSString* settingsName;


- (void) resolutionWidth:(int)rw resolutionHeight:(int)rh matrixWidth:(int)mw matrixHeight:(int)mh;
- (void) startAnimation;
- (void) stopAnimation;
- (void) drawView:(id)sender;
- (void) showHtml:(NSString*)urlString;
- (void) closeHtml:(id)sender;

- (void) SetIsOnlineHtml:(bool)online;

- (void) playMovie;
- (void) stopMovie:(NSNotification*) notification;
- (void) pauseMovie;
- (void) movieStateChangeCallback:(NSNotification*) notification;

@end
