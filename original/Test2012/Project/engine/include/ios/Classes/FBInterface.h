#ifndef _FBINTERFACE_H
#define _FBINTERFACE_H

#include <string>
#include <vector>
#include <map>
#include <boost/function.hpp>

namespace FB
{

	struct Action {
		enum Type {
			LOGIN_COMPLETED = 0, // former lua:OnFBLoggedIn, успешный логин
			LOGIN_FAILED, // former lua:OnFBLoginCancelled, неуспешный логин
			LOGIN_FOR_PUBLISH_COMPLETED, // former lua:OnFBLoginFinished, успешный логин на запись, приходит после LOGIN_COMPLETED
			LOGIN_FOR_PUBLISH_FAILED, // неуспешный логин на запись, приходит после LOGIN_COMPLETED
			LOGOUT, // former lua:OnFBLogout, выход
			USERINFO_AVAILABLE, // former lua:OnFBUserInfo, скачали информацию о пользователе
			USERINFO_FAILED, // former lua:OnFBUserInfoFail, информацию о пользователе не удалось скачать
			FRIENDSINFO_AVAILABLE, // former lua:OnFBFriendsInfo, появилась новая информация о друзьях
			FRIENDSINFO_FAILED, // former lua:OnFBFriendsInfoFail, информацию о друзьях не удалось скачать
			DIALOG_POSTED, // сообщение с диалогом опубликовано
			DIALOG_CANCELED, // сообщение с диалогом не удалось опубликовать
			REQUEST_SENT, // запрос отправлен
			REQUEST_FAILED, // отправка запроса не удалась
			REQUEST_DIALOG_SHOWED, // показывается диалог отправки запроса
			REQUEST_DIALOG_CLOSED, // диалог отправки запроса закрыт
			APPREQUEST_AVAILABLE, // скачали информацию о доступных запросах
			APPREQUEST_FAILED, // информацию о доступных запросах не удалось скачать
			OS_VERSION_NOT_SUPPORTED // на данной версии iOS работать не будем
		};
	};

	typedef boost::function<void(Action::Type action)> CallbackType;

	void InitWithCallback(CallbackType callback);
	void LogIn(); ///< залогиниться
	void LogOut(); ///< вылогиниться
	bool IsLoggedIn(); ///< проверить залогинен ли
	bool CanPublish(); ///< есть ли права на запись
	bool AuthInProcess(); ///< Инициализация Facebook в процессе.

	bool LoadingUserInfo(); ///< Загрузка информации об игроке в процессе.

	std::string UserName(); ///< полное имя пользователя
	std::string UserId(); ///< идентификатор пользователя (числовой)
	std::string UserPicUrl(); ///< url аватарки залогиненного пользователя
	std::string UserFirstName(); ///< имя пользователя

	bool RequestFriendsInformation(); ///< запросить информацию о друзьях. Возвращает false, если ничего не делает.
	bool IsWaitingForFriends();
	bool RequestAppRequestsInformation(); ///< запросить информацию о "запросах приложения". Возвращает false, если ничего не делает
	
	struct Friend
	{
		std::string fbId;
		std::string name;
		std::string first_name;
		std::string pic_url;
	};
	
	struct AppRequest {
		std::string requestId;
		std::string senderId;
		std::string message;
		std::string data;
	};

	std::vector<std::string> FriendIds();
	Friend GetFriend(const std::string& fbId);

#ifdef __OBJC__
	bool HandleOpenUrl(NSURL *url);
	void HandleDidBecomeActive();
#endif

	typedef std::map<std::string, std::string> Parameters;
	void PostWithDialog(const Parameters &params); // deprecated
	void PostOnWall(const Parameters &params);
	void SendRequest(const Parameters &params);

	// example PostToOpenGraph("me/townshipgame:ACTION", "http://xml.playrix.com/township_ios/og1.php?object=OBJ&lang=LANG&...OTHER_PARAMS", "OBJ");
	void PostToOpenGraph(const std::string& actionWithPrefix, const std::string& objurl, const std::string& objname);
	
	std::vector<AppRequest> getAppRequests();
	void DeleteAppRequest(const std::string &requestid);

}

#endif // _FBINTERFACE_H
