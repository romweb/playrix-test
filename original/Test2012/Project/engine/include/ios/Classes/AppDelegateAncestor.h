#import <UIKit/UIKit.h>

@class EAGLViewAncestor;

@interface AppDelegateAncestor : NSObject {
    UIWindow *_window;
    EAGLViewAncestor *_glView;
	UINavigationController *_navigationController;
@private
	NSOperationQueue* queue;
}

// ������ ���� ����������� � ����������
//- (NSString*) getMorePlayrixGamesURL;

- (void) applicationDidFinishLaunching;
- (void) applicationWillResignActive;
- (void) applicationDidBecomeActive;

- (bool) tryAuthenticateLocalPlayer;
- (void) showAchievements;

- (void) ReinitView;
- (void) AskToSubscribeForExtension;

- (void) CheckMoreGames;
- (void) viewCheckMoreGames;
- (void) showMoreGames;
- (void) showSubscribe;
- (void) showShellSubscribe;
- (void) showISplashSubscribe;
- (void) showTellAFriend;

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet EAGLViewAncestor *glView;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;

@end

void CheckMoreGamesUpdate();
void SubscribeCallback();
void ShellSubscribe();
void ISplashSubscribe();
void TellAFriendCallback();
void ShowAchievements();
void FollowOnFacebook();
void FollowOnTwitter();
void OpenRateThisGameUrl();
void OpenUrl(const std::string& url);
void ShowAchievementAuthenticateCallback();
void ShowKeyboardCallback();
void ReinitMainMenu();
void ReinitView();
bool IsFullVersion();
bool MustShowDiscount();
bool MustShowShell();
void ShowMoreGamesCallback();
bool MPGMustShowSubscribe();
void UpdateMpgMessage();
bool MustShowISplashBanner();
void SetISplashAsShown();
void DoSetFullVersion();
bool MPGTrialWasExtended();
void MPGAllowTrialExtensionToAppear();
bool MPGTrialCanBeExtended();
void AskToSubscribeForExtensionCallback();
