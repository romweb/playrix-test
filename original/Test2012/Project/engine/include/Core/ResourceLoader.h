#pragma once

#include "RefCounter.h"
#include "AsyncWorkingQueue.h"

class Resource;

///
/// ����� �������� ��������
///
struct ResourceLoadMode {
	enum Type {
		Sync, ///< ����������
		Async, ///< �����������
		Post ///< ����������
	};
};

///
/// ��������� ��������.
///
/// ��������� ���������� ���������� � ����������� �������� ��������.
///
/// � ������-���������� ���������� ����������� ��� ������:
///	\DoLoadData ��� ����������� �������� ���������� �� ��������������� ������, ��� ���������� - �� ���������
///	\DoLoadObject ������ ���������� �� ��������� ������, �� ���� ������� ���������� � ������������ API
///	\DoUnload ������ ���������� �� ��������� ������
///
class ResourceLoader : public RefCounter
{
public:
	ResourceLoader();

	/// ���������� true �� ����� ��������
	bool IsLoading() const { return _is_loading; }

	/// ��������� ������
	void Load(Resource* resource, ResourceLoadMode::Type mode);

	/// ��������� ������
	void Unload(Resource* resource, ResourceLoadMode::Type mode);

	/// ��������� ����������� ��������
	void CancelLoading();

protected:
	/// ���������� (��� ����������� �������� �� ��������������� ������) ��� ������ ��������
	virtual void DoLoadData(Resource* resource) = 0;

	/// ���������� �� ��������� ������ ��� ���������� ��������
	virtual void DoLoadObject(Resource* resource) = 0;

	/// ���������� �� ��������� ������ ��� ��������
	virtual void DoUnload(Resource* resource) = 0;

private:
	/// ����������� �� ��������� ��� ��������������� ������ ��� ����������� ��������
	void StartLoad(Resource* resource);

	/// ����������� ������ �� ��������� ������
	void EndLoad(Resource* resource, bool error, const char* what);
	
	/// ����������� ������ �� ��������� ������
	void EndUnload(Resource* resource);

protected:
	AsyncWorkingQueue::ItemID _queueItemId; ///< ������������� �������� � ����������� �������

	ResourceLoadMode::Type _mode; ///< � ����� ������ ���� ������ ��������

	bool _is_loading; ///< ���������� �� �������� ����� ������ (��������, �����������)
};

typedef boost::intrusive_ptr<ResourceLoader> ResourceLoaderPtr;
