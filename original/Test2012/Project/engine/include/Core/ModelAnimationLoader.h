#ifndef __MODELANIMATIONLOADER_H__
#define __MODELANIMATIONLOADER_H__

#pragma once

#include "Core/ResourceLoader.h"

///
/// ��������� �������� ModelAnimation
///
class ModelAnimationLoader : public ResourceLoader
{
protected:
	void DoLoadData(Resource* resource);

	void DoLoadObject(Resource* resource);

	void DoUnload(Resource* resource);
};

#endif // __MODELANIMATIONLOADER_H__
