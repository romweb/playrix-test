#ifndef __SHADERPROGRAMLOADER_H__
#define __SHADERPROGRAMLOADER_H__

#pragma once

#include "Core/ResourceLoader.h"

///
/// ��������� ��������
///
class ShaderProgramLoader : public ResourceLoader
{
protected:
	void DoLoadData(Resource* resource);

	void DoLoadObject(Resource* resource);

	void DoUnload(Resource* resource);
};

#endif // __SHADERPROGRAMLOADER_H__
