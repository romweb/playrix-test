#ifndef _KEYCODES_H_
#define _KEYCODES_H_

static const BYTE VK_LEFT = 0x7B;
static const BYTE VK_RIGHT = 0x7C;
static const BYTE VK_UP = 0x7E;
static const BYTE VK_DOWN = 0x7D;
static const BYTE VK_TAB = 0x30;
static const BYTE VK_HOME = 0x73;
static const BYTE VK_SCROLL = 145;
static const BYTE VK_PAUSE = 19;
static const BYTE VK_RETURN = 13;
static const BYTE VK_SPACE = 49;
static const BYTE VK_ESCAPE = 0x35;
static const BYTE VK_F = 3;
static const BYTE VK_H = 4;
static const BYTE VK_M = 46;
static const BYTE VK_0 = 29;
static const BYTE VK_1 = 18;
static const BYTE VK_2 = 19;
static const BYTE VK_3 = 20;
static const BYTE VK_4 = 21;
static const BYTE VK_5 = 23;
static const BYTE VK_6 = 22;
static const BYTE VK_7 = 26;
static const BYTE VK_8 = 28;
static const BYTE VK_9 = 25;

static const BYTE VK_F1 = 0x7A;
static const BYTE VK_F2 = 0x78;

static const BYTE VK_CONTROL = VK_F1;
static const BYTE VK_SHIFT = VK_F2;

static const BYTE VK_PRIOR = 0x74;
static const BYTE VK_NEXT = 0x79;
static const BYTE VK_END = 0x77;
static const BYTE VK_NUMPAD1 = 83;
static const BYTE VK_NUMPAD2 = 84;
static const BYTE VK_NUMPAD3 = 85;
static const BYTE VK_NUMPAD4 = 86;
static const BYTE VK_NUMPAD5 = 87;
static const BYTE VK_NUMPAD6 = 88;
static const BYTE VK_NUMPAD7 = 89;
static const BYTE VK_NUMPAD8 = 91;
static const BYTE VK_NUMPAD9 = 92;
static const BYTE VK_ADD = 69;
static const BYTE VK_SUBTRACT = 78;
static const BYTE VK_DECIMAL = 65;
static const BYTE VK_DIVIDE = 75;
static const BYTE VK_MULTIPLY = 67;
static const BYTE VK_NUMLOCK = 71;
static const BYTE VK_NUMPAD0 = 82;

static const BYTE VK_F3 = 0x63;
static const BYTE VK_F4 = 0x76;
static const BYTE VK_F5 = 0x60;
static const BYTE VK_F6 = 0x61;
static const BYTE VK_F7 = 0x62;
static const BYTE VK_F8 = 0x64;
static const BYTE VK_BACK = 127;
static const BYTE VK_DELETE = 117;
///////////////////////////////
static const BYTE VK_F9 = 0x65;
static const BYTE VK_F10 = 0x6D;
static const BYTE VK_F11 = 0x67;
static const BYTE VK_F12 = 0x6F;
static const BYTE VK_SEPARATOR = 253;
static const BYTE VK_MENU = 245;
static const BYTE VK_CAPITAL = 247;
static const BYTE VK_LWIN = 248;
static const BYTE VK_RWIN = 249;
static const BYTE VK_APPS = 250;
static const BYTE VK_INSERT = 255;


#endif
