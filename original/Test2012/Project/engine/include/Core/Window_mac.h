#ifndef _WINDOW_MAC_H_
#define _WINDOW_MAC_H_

namespace Core
{
class Window
{
public:
	enum window_mode
	{
		undefined,
		fullscreen,
		windowed
	};

public:
	Window(void* cocoawin, window_mode mode);
	~Window();
	void Destroy();

	void SetMode(window_mode mode);
	window_mode getWindowMode();

	IRect GetClientSizes();
	bool IsCursorIn();

	void minimize();
	void restoreminimized();

private:
	void __SetMode(window_mode mode);

private:
	void* __m_cocoawin;
	void* __m_cocoacontentview;
	void* __m_cocoafullscreenWindow;
	void* __m_cocoacontentviewtracrectresponder;
	int __m_cocoacontentviewtracrectrespondertag;
	window_mode _mode;

	int __m_client_width;
	int __m_client_height;
	bool __m_cursor_in;
};
}
#endif
