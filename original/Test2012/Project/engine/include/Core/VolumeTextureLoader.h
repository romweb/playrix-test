#ifndef __VOLUMETEXTURELOADER_H__
#define __VOLUMETEXTURELOADER_H__

#pragma once

#include "Core/ResourceLoader.h"

///
/// ��������� 3D �������
///
class VolumeTextureLoader : public ResourceLoader
{
protected:
	void DoLoadData(Resource* resource);

	void DoLoadObject(Resource* resource);

	void DoUnload(Resource* resource);
};

#endif // __VOLUMETEXTURELOADER_H__
