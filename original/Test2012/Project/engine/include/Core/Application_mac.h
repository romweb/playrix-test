#ifndef _APPLICATION_MAC_H_
#define _APPLICATION_MAC_H_

#include "Render/Texture.h"
#include "Timer.h"
#include "LoadScreen.h"
#include "GUI/Cursor.h"

namespace Core
{
class Window;

class Application
{
	friend class Window;
	friend class DebugConsole;


private:
	Window* wnd;
	bool _loading;
	// Загружаемся ли;
	// этот флаг нужен во время загрузки, чтобы:
	// * игнорировать паузу;
	// * жестко выходить по требованию пользователя.

	float _dt;
	float _sleep_time;

	bool _canPause;

	void MainLoopContent(bool ignorePause, bool noCursor);

	void BreakMainLoop();

	//bool _isMouseOut; // находится ли курсор снаружи текущего вьюпорта

	int FpsCounter;
	float fpsTimer;

protected:
	int cmdShow;
	bool isFullscreen;
	bool showCustomCursor;
	int currentFps;
	long _resourceCounter;
	long _loadingCounter;
	void GetCursorProperties();
	bool mainLoop;
	std::string _keyMap[91];
	LoadScreen* loadScreen;
	void WriteComputerInfo();

public:
	Application(Window* appwin, LoadScreen* ls = 0);
	virtual ~Application();

	static std::string name;
	bool _saveGfxFileInfo;
	GUI::Cursor *cursor;

	bool enableCustomCursor;
	bool showFps;
	bool isPaused;
	bool splashesLoop;
	int maxFps;

	Timer timer;

	/// название окна программы (должно быть заполнено до вызова Init())
	std::string APPLICATION_NAME;
	/// ширина графики игры (должно быть заполнено до вызова Init())
	int GAME_CONTENT_WIDTH;
	/// высота графики игры (должно быть заполнено до вызова Init())
	int GAME_CONTENT_HEIGHT;

	bool FORCE_USE_NATIVE_RESOLUTION;

	Window* getMainWindow();

	/// Инициализация генератора случайных чисел таймером.
	void Randomize();

	virtual void ScriptMap();

	/// первичная инициализация (не может выполняться в конструкторе, ибо вызывает виртуальные методы).
	/// Можно загружать ресурсы (LoadResources) в ниточке если задать threaded
	/// Этот метод должен быть вызван до Start().
	void Init(bool threaded=false);

	/// основной цикл приложения
	void Start();
	virtual void Update(float dt);
	virtual void CloseWindow();
	/// деинициализация системы
	virtual void ShutDown();
	void LoadKeyMap();

	void DrawCursor();
	bool IsFullscreen();
	virtual bool ScreenSaverMode() { return false; };

	/// обновление экрана загрузки (если есть)
	virtual void UpdateLoadScreen();

	/// регистрация классов в фабрике, должен быть вызван из наследника при переопределении
	virtual void RegisterTypes();

	/// вызывается при переключении полноэкранного режима с помощью Alt-Enter
	/// для того, чтобы программа могла мониторить текущее состояние
	virtual void setFullscreenMode(bool);
	
	/// Показ сплэш экранов, если нужно.
	/// Вызывается из Init().
	virtual void Splashes();

	/// Загрузка ресурсов до основной части (НЕбывает отдельным потоком).
	/// Вызывается из Init().
	virtual void PreloadResources() {};

	/// Основная загрузка ресурсов (может быть отдельным потоком).
	/// Вызывается из Init().
	virtual void LoadResources();
	
	/// Отрисовка экрана игры
	virtual void PreDraw() {}
	virtual void Draw();
	virtual void PostDraw() {}

	/// Отрисовка эффекта паузы. Вызывается из Draw.
	virtual void DrawPause();

	/// Можно переопределить для отрисовки какой-либо информации в краях экрана,
	/// оставленных пустыми для сохранения Aspect Ratio.
	virtual void DrawBoxBars();

	/// Отрисовка FPS, можно (и нужно) переопределять
	virtual void DrawFps() {}

	/// Вернуть текущий fps
	int getCurrentFps();

	/// установить(true)/снять(false) паузу
	void SetPause(bool pause);

	/// задать новый LoadScreen или отключить передав NULL
	void SetLoadScreen(LoadScreen* ls);

	void ProcessFrame(bool ignorePause, bool noCursor);

	/// Завершить приложение
	void Stop();

	/// Сказать, что приложение загружено.
	/// Вызывается из движкового кода, поэтому в игре нет особой необходимости вызывать его.
	/// Может быть переопределено с целью отложить момент окончания загрузки.
	virtual void SetLoaded();
	void WaitLoading();
	bool IsLoading() { return _loading; }

	long GetResourceCounter() const { return _resourceCounter; }
	long GetLoadingCounter() const { return _loadingCounter; }
};

extern Application* appInstance;
}

#endif

