#ifndef __CORE_SYSTEMDIALOG_H__
#define __CORE_SYSTEMDIALOG_H__

#include "types.h"
#include <boost/function.hpp>

class SystemDialog
{
public:

	typedef boost::function<void()> CallbackType;

	struct ButtonType {
		enum Type {
			NORMAL = 0,
			CANCEL
		};
	};

	struct Button {
		std::string text;
		ButtonType::Type type;
		CallbackType callback;

		Button(const std::string &textId_, CallbackType callback_, ButtonType::Type type_);
	};

	typedef std::vector<Button> Buttons;
	
	SystemDialog();

	void SetCaption(const std::string &textId);

	void SetText(const std::string &textId);

	void AddButton(const std::string &textId, CallbackType callback, ButtonType::Type type = ButtonType::NORMAL);

	void Show();

private:

	bool _shown;

	std::string _caption;

	std::string _text;

	Buttons _buttons;

	friend void RunDialogCallback(SystemDialog *dialog, int number);

};

#endif //__CORE_SYSTEMDIALOG_H__