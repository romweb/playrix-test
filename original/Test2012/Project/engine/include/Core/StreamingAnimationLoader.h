#ifndef __STREAMINGANIMATIONLOADER_H__
#define __STREAMINGANIMATIONLOADER_H__

#pragma once

#include "Core/ResourceLoader.h"

///
/// ��������� ��������
///
class StreamingAnimationLoader : public ResourceLoader
{
protected:
	void DoLoadData(Resource* resource);

	void DoLoadObject(Resource* resource);

	void DoUnload(Resource* resource);
};

#endif // __STREAMINGANIMATIONLOADER_H__
