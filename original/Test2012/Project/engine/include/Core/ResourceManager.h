﻿#pragma once

#include "Resource.h"
#include "TypeIndex.h"
#include "AsyncWorkingQueue.h"
#include "Core/ResourceLoadManager.h"
#include "Core/ResourceFactory.h"
#include "Core/Resource.h"
#include "Core/ResourceGroup.h"
#include "Render/Text.h"
#include "Render/FreeTypeTexts.h"

#ifdef THREADED
#include "ThreadSupport.h"
#endif

namespace Core {

///
/// Менеджер ресурсов
///
class ResourceManager
{
public:
	ResourceManager();

	/// Добавляет новый ресурс
	Resource* Add(TypeIndex type, Resource* res);
	
	/// Удаляет ресурс указанного типа по его имени
	void Remove(TypeIndex type, const std::string& name);

	/// Удаляет все ресурсы указанного типа
	void RemoveAll(TypeIndex type);

	/// Удаляет все ресурсы
	void RemoveAll();

	/// Проверяет существование указанного типа ресурса
	bool Exists(TypeIndex type) const;

	/// Проверяет существование ресурса указанного типа по имени
	bool Exists(TypeIndex type, const std::string& name) const;

	/// Ищет все ресурсы с указанным именем
	void CollectResources(const std::string& name, std::vector<Resource*>& result) const;

	/// Ищет все ресурсы с указанным типом
	void CollectResources(TypeIndex type, std::vector<Resource*>& result) const;

	/// Ищет все группы, в которые включён указанный ресурс
	void CollectGroups(const Resource* res, std::vector<std::string>& result) const;

	/// Ищет ресурс указанного типа по имени
	/// \return NULL, если не найден
	Resource* Find(TypeIndex type, const std::string& name) const;

	/// Возвращает ресурс по указанному типу и имени.
	/// Если ресурс не найден, выбрасывается исключение.
	Resource* Get(TypeIndex type, const std::string& name) const;

	/// Возвращает группу ресурсов по имени.
	/// Если такой группы не существовало, тогда будет создана новая.
	ResourceGroup* GetOrCreateGroup(const std::string& name);

	/// Обновляет переменную в текстах
	void UpdateText(const std::string* varName, const std::string* varValue);

	/// Перезагружает ресурсы указанного типа.
	///
	/// В режимах загрузки ResourceLoadMode::Async и ResourceLoadMode::Post ответственность
	/// за вызовы AsyncWorkingQueue::ProcessPostponed() и AsyncWorkingQueue::ExecuteAllNow()
	/// лежит на вызывающей стороне.
	///
	void Reload(TypeIndex type, ResourceLoadMode::Type load_mode);

	/// Перезагружает ресурс указанного типа по имени
	void Reload(TypeIndex type, const std::string& name, ResourceLoadMode::Type load_mode);

	/// Возвращает размер памяти, занимаемый ресурсами указанного типа
	size_t GetMemoryInUse(TypeIndex type) const;

	/// Возвращает суммарный размер занимаемой ресурсами памяти
	size_t GetMemoryInUse() const;

	template<typename T> T* Add(Resource* res) {
		//* c++11 */ static_assert(std::is_base_of<Resource, T>::value, "T must be a base of res");
		Assert2(dynamic_cast<T*>(res) != NULL, "T must be a base of res");
		return static_cast<T*>(Add(TypeIndex(typeid(T)), res));
	}

	template<typename Base, typename T> T* Replace(Resource* res) {
		ResourcePtr guard = res;

		Base* base = Find<Base>(res->GetName());
		if (!base) {
			Halt(std::string("Resource '") + res->GetName() + "' of type '" + typeid(*res).name() + "' wasn't found for replacing");
			return NULL;
		}

		T* object = dynamic_cast<T*>(base);
		if (!object) {
			Halt(std::string("Resource '") + base->GetName() + "' of type '" + typeid(*base).name() + "' has an inappropriate type");
		}

		T* proto = dynamic_cast<T*>(res);
		if (!proto) {
			Halt(std::string("Resource '") + proto->GetName() + "' of type '" + typeid(*proto).name() + "' has an inappropriate type");
		}

		*object = *proto;
		return object;
	}

	template<typename T> void Remove(const std::string& name)          { Remove(TypeIndex(typeid(T)), name); }
	template<typename T> void RemoveAll()                              { RemoveAll(TypeIndex(typeid(T))); }
	template<typename T> bool Exists() const                           { return Exists(TypeIndex(typeid(T))); }
	template<typename T> bool Exists(const std::string& name) const    { return Exists(TypeIndex(typeid(T)), name); }
	template<typename T> void CollectResources(std::vector<Resource*>& result) { CollectResources(TypeIndex(typeid(T)), result); }
	template<typename T> T*   Find(const std::string& name) const      { return static_cast<T*>(Find(TypeIndex(typeid(T)), name)); }
	template<typename T> T*   Get(const std::string& name) const       { return static_cast<T*>(Get(TypeIndex(typeid(T)), name)); }
	template<typename T> void Reload(ResourceLoadMode::Type load_mode) { Reload(TypeIndex(typeid(T)), load_mode); }
	template<typename T> void Reload(const std::string& name,
		ResourceLoadMode::Type load_mode)                              { Reload(TypeIndex(typeid(T)), name, load_mode); }
	template<typename T> size_t GetMemoryInUse() const                 { return GetMemoryInUse(TypeIndex(typeid(T))); }

private:
	typedef boost::unordered_map<std::string, ResourcePtr> Resources;
	typedef boost::unordered_map<TypeIndex, Resources> ResourceTypes;

	ResourceTypes _resources; ///< Коллекция ресурсов

#ifdef THREADED
	mutable MUTEX_TYPE _mutex;
#endif
};

extern ResourceManager resourceManager;

} // namespace Core
