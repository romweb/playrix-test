#ifndef __SHEETLOADER_H__
#define __SHEETLOADER_H__

#pragma once

#include "Core/ResourceLoader.h"

///
/// ��������� �������
///
class SheetLoader : public ResourceLoader
{
protected:
	void DoLoadData(Resource* resource);

	void DoLoadObject(Resource* resource);

	void DoUnload(Resource* resource);
};

#endif // __SHEETLOADER_H__
