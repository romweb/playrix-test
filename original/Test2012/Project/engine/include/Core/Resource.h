#pragma once

#include "RefCounter.h"
#include "ResourceLoader.h"

class ResourceLoadManager;

///
/// ����������� ������
///
class Resource : virtual public RefCounter
{
public:
	Resource();

	/// ���������� ��� �������, ���������� ��� ����������� ����
	virtual const std::string& GetName() const = 0;

	/// ���������� ������ ������������ �������� ������
	virtual size_t GetMemoryInUse() const = 0;

	/// ���������� ��������� �������
	ResourceLoader* GetLoader() const { return _loader.get(); }

	/// ������������� ��������� �������
	void SetLoader(ResourceLoader* loader) { _loader = loader; }

	/// ���������, ����������� �� ������
	bool IsLoading() const;

	/// ���������, �������� �� ������
	bool IsLoaded() const { return _is_loaded; }
	
	/// ���������� ������� ������������� �������
	int GetUseCount() const { return _use_count; }

	/// ����������� ������� �������������
	void BeginUse(ResourceLoadMode::Type load_mode = ResourceLoadMode::Post);

	/// ��������� ������� ������������� �������
	void EndUse(ResourceLoadMode::Type load_mode = ResourceLoadMode::Post);

	/// ���������� ���������� ����������� ��������, ���� ����� �����������
	void EnsureLoaded();

	/// ������������� ����������� ������, ������� ������� ����������� ��������
	void Reload(ResourceLoadMode::Type load_mode);

protected:
	friend class ResourceGroup;
	friend class ResourceLoadManager;

	/// ��������� ��������� ������� �� ��������
	virtual void GroupLoad(ResourceLoadManager& ldm) { }

	/// ��������� ��������� ������� �� ��������
	virtual void GroupUnload(ResourceLoadManager& ldm) { }

	/// ��������� ������������� ������
	virtual void Load(ResourceLoadMode::Type load_mode);
	
	/// ��������� ����������� ������
	virtual void Unload(ResourceLoadMode::Type load_mode);

	/// ��������� ��� ��������� ������ � ����������� �� �������� �������� �������������
	virtual void SetUseCount(int use_count, ResourceLoadMode::Type load_mode);

protected:
	friend class ResourceLoader;

	int _use_count; ///< ������� ������������� �������

	bool _is_loaded; ///< ������ ��������?

	ResourceLoaderPtr _loader;
};

typedef boost::intrusive_ptr<Resource> ResourcePtr;
