#pragma once

#include "LoadScreen.h"
#include "Core/Timer.h"
#include "Utils/Vector3.h"

namespace Core {
	class LoadScreen;
};

bool isRetina();
bool isIPhone5();

namespace Core
{
/// Класс приложения
class Application
{
private:
	// Это нужно для того, чтобы иметь возможность запаузить тред.
	bool m_pause;
	boost::mutex m_pause_mutex;
	boost::condition_variable m_pause_changed;

	boost::asio::io_service _io_service;
	
protected:

	int currentFps;
	long _resourceCounter;
	long _loadingCounter;

	std::string _keyMap[91];
	LoadScreen* loadScreen;
	static std::string locale;
	math::Vector3 accel, accel_lo, accel_hi;
	float rotateOrientation, rotateOrientationPrev, rotateTime;
	bool invertMouse;
	bool _loading;
	std::string debugString;

public:

    static std::string name;
	
	bool showFps;
	bool isPaused;
	bool splashesLoop;
	int  maxFps;

	bool retinaEmulate;
		///< Будет ли iPhone приложение при запуске на iPad использовать ретина-графику.
		///< По умолчанию *отключено в релизе*. Устанавливать до вызова Init().

	static int cmdShow;

	Timer timer;

	/// путь в реестре для программы (должно быть заполнено до вызова Init())
	std::string SETTINGS_REG_KEY;
	/// название окна программы (должно быть заполнено до вызова Init())
	std::string APPLICATION_NAME;

	/// конструктор
	Application(LoadScreen* ls = 0);
	/// деструктор
	virtual ~Application();

	virtual void ScriptMap();

	/// первичная инициализация (не может выполняться в конструкторе, ибо вызывает виртуальные методы).
	/// Этот метод должен быть вызван до Start().
	// layer must be a CAEAGLLayer
	void Init(void *layer, int width=0, int height=0);

	//Reinit OpenGL with different projection matrix sizes
	void ReInit(void *layer, int matrixWidth, int matrixHeight);
	
	void SetLoadingPause(bool pause);
	void BlockLoadingWhilePaused();
	
	/// основной цикл приложения
	void Start();
	void MainLoop();
	void CloseWindow();
	/// деинициализация системы
	void ShutDown();
	void LoadKeyMap();
	/// Завершить приложение
	void Stop();
	
	void setAccelerationData(float x, float y, float z);
	void setAngle(float a);
	void setMousePos(float x, float y);
	void updateMousePos(float x, float y);
	void pinchBegan(float scale, float x, float y);
	void pinchChanged(float scale, float x, float y);
    
	void setDebugString(const std::string&);

	bool IsFullscreen();

	void RunInMainThread(boost::function<void()> func);

	/// обновление экрана загрузки (если есть)
	virtual void UpdateLoadScreen();
	/// Обновление экрана загрузки слоя (если есть)
	virtual void UpdateResourceUploadObserver();
	/// Инициализация экрана загрузки слоя (если есть)
	virtual void InitResourceUploadObserver();
	/// регистрация классов в фабрике, должен быть вызван из наследника при переопределении
	virtual void RegisterTypes();
	/// вызывается при переключении полноэкранного режима с помощью Alt-Enter
	/// для того, чтобы программа могла мониторить текущее состояние
	virtual void setFullscreenMode(bool);
	/// Показ сплэш экранов, если нужно.
	/// Вызывается из Init().
	virtual void Splashes();
	/// Загрузка ресурсов до основной части (НЕбывает отдельным потоком).
	/// Вызывается из Init().
	virtual void PreloadResources() {};
	/// Основная загрузка ресурсов (может быть отдельным потоком).
	/// Вызывается из Init().
	virtual void LoadResources();
	/// Отрисовка экрана игры
	virtual void Draw();
	virtual void BeginDraw();
	virtual void BaseDraw();
	virtual void DebugDraw();
	virtual void EndDraw();
	/// Отрисовка эффекта паузы. Вызывается из Draw.
	virtual void DrawPause();
	/// Вернуть текущий fps
	int getCurrentFps() {
		return currentFps;
	};
	virtual void Update(float dt);
	void SetLoaded();
	void WaitLoading();
	bool IsLoading();

	long GetResourceCounter() const { return _resourceCounter; }
	long GetLoadingCounter() const { return _loadingCounter; }

	// Находимся ли в лайт-версии
	// Эта функция помещена именно в Application, ибо мы всегда знаем, что Core::appInstance существует когда нужно
	virtual bool IsLiteVersion();

protected:
	void UpdateMessageQueue();
};

extern Application* appInstance;

}
