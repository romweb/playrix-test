#ifndef __APPLICATION_WIN_H__
#define __APPLICATION_WIN_H__

#pragma once

#include "Render/Texture.h"
#include "Timer.h"
#include "LoadScreen.h"
#include "GUI/Cursor.h"

namespace Core {

class Window;

/// Класс приложения
class Application
{
public:
	bool _saveGfxFileInfo;
	GUI::Cursor *cursor;
	
	bool enableCustomCursor;
	bool showFps;
	bool isPaused;
	bool splashesLoop;
	int maxFps;

	Timer timer;

	/// путь в реестре для программы (должно быть заполнено до вызова Init())
	std::string SETTINGS_REG_KEY;
	/// название окна программы (должно быть заполнено до вызова Init())
	std::string APPLICATION_NAME;
	/// класс окна программы (должно быть заполнено до вызова Init())
	std::string WINDOW_CLASS_NAME;
	/// ширина графики игры (должно быть заполнено до вызова Init())
	int GAME_CONTENT_WIDTH;
	/// высота графики игры (должно быть заполнено до вызова Init())
	int GAME_CONTENT_HEIGHT;

	/// если true, то игра будет запускаться в родном разрешении GAME_CONTENT_WIDTHxGAME_CONTENT_HEIGHT
	/// если false, то в случае несовпадения AspectRatio игры и экрана, будут черные полосы по краям
	/// либо сверху-снизу, либо с боков (должно быть заполнено до вызова Init())
	bool FORCE_USE_NATIVE_RESOLUTION;

	/// конструктор
	Application(HINSTANCE hInstance, int nCmdShow, bool fullscreenMode, LoadScreen* ls = 0);
	/// деструктор
	virtual ~Application();

	Window* getMainWindow();

	/// первичная инициализация (не может выполняться в конструкторе, ибо вызывает виртуальные методы).
	/// Можно загружать ресурсы (LoadResources) в ниточке если задать threaded
	/// Этот метод должен быть вызван до Start().
	void Init(bool threaded=false);
	
	/// основной цикл приложения
	void Start();
	void CloseWindow();
	/// деинициализация системы
	void ShutDown();
	void LoadKeyMap();

	void DrawCursor();
	bool IsFullscreen();

	void RunInMainThread(boost::function<void()> func);

	virtual void ScriptMap();

	/// обновление экрана загрузки (если есть)
	virtual void UpdateLoadScreen();

	/// регистрация классов в фабрике, должен быть вызван из наследника при переопределении
	virtual void RegisterTypes();

	/// вызывается при переключении полноэкранного режима с помощью Alt-Enter
	/// для того, чтобы программа могла мониторить текущее состояние
	virtual void setFullscreenMode(bool);

	/// Показ сплэш экранов, если нужно.
	/// Вызывается из Init().
	virtual void Splashes() {}

	/// Загрузка ресурсов до основной части (НЕбывает отдельным потоком).
	/// Вызывается из Init().
	virtual void PreloadResources() {};
	
	/// Основная загрузка ресурсов (может быть отдельным потоком).
	/// Вызывается из Init().
	virtual void LoadResources();

	virtual void Update(float dt);

	/// Отрисовка экрана игры
	virtual void PreDraw() {}
	virtual void Draw();
	virtual void PostDraw() {}

	/// Отрисовка эффекта паузы. Вызывается из Draw.
	virtual void DrawPause();

	/// Можно переопределить для отрисовки какой-либо информации в краях экрана,
	/// оставленных пустыми для сохранения Aspect Ratio.
	virtual void DrawBoxBars() {}

	/// Отрисовка FPS, можно (и нужно) переопределять
	virtual void DrawFps() {}
	
	/// Вернуть текущий fps
	int getCurrentFps();

	/// установить(true)/снять(false) паузу
	virtual void SetPause(bool pause);

	/// задать новый LoadScreen или отключить передав NULL
	void SetLoadScreen(LoadScreen* ls);

	/// Завершить приложение
	void Stop();

	/// Сказать, что приложение загружено.
	/// Вызывается из движкового кода, поэтому в игре нет особой необходимости вызывать его.
	/// Может быть переопределено с целью отложить момент окончания загрузки.
	virtual void SetLoaded();
	bool IsLoading() const { return _loading; }
	void WaitLoading();

	long GetResourceCounter() const { return _resourceCounter; }
	long GetLoadingCounter() const { return _loadingCounter; }
	
	// Для совместимости с айфоном
	virtual bool IsLiteVersion() {
		return false;
	}

	virtual void onDragFiles(const std::vector<std::string> &fileNames) {}

protected:
	Window* wnd;
	HINSTANCE _hInstance;
	int cmdShow;

	bool isFullscreen;
	bool showCustomCursor;
	int currentFps;
	long _resourceCounter;
	long _loadingCounter;

	void GetCursorProperties();

	HCURSOR arrowCursor;
	HCURSOR handCursor;
	HCURSOR forbiddenCursor;

	bool mainLoop;

	std::string _keyMap[91];
	LoadScreen* loadScreen;

	void WriteComputerInfo();

	void UpdateMessageQueue();

private:
	friend class Window;
	friend class DebugConsole;

	bool _loading;
		// Загружаемся ли;
		// этот флаг нужен во время загрузки, чтобы:
		// * игнорировать паузу;
		// * жестко выходить по требованию пользователя.

	float _dt;
	float _sleep_time;

	boost::asio::io_service _io_service;
};

extern Application* appInstance;

} // namespace Core

#endif // __APPLICATION_WIN_H__
