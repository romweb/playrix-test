/* RefCounter.h
 *
 * Вспомогательные классы для использования boost::intrusive_ptr.
 */

#ifndef __REFCOUNTER_H__
#define __REFCOUNTER_H__

#include "EngineAssert.h"

#include <boost/intrusive_ptr.hpp>

/** Класс реализует подсчёт ссылок для boost::intrusive_ptr.
 * Использовать в качестве базового класса:
 * class MyClass : public RefCounter //< Наследование!
 * {
 *    ...
 * };
 *
 * typedef boost::intrusive_ptr<MyClass> MyClassRef; //< Не обязательно, но удобно.
 * 
 * MyClassRef ref = p; // Делает intrusive_ptr из указателя на MyClass *с учётом всех ссылок*!
 * Это удобно, если нужно положить this в контейнер, хранящий MyClassRef.
 */
class RefCounter
{
protected:
	RefCounter() : counter_(0) {}
	explicit RefCounter(const RefCounter&) : counter_(0) {}
	RefCounter& operator=(const RefCounter&) { return *this; }
	
	virtual ~RefCounter() {
		Assert(counter_ == 0);
		counter_ = 0x7a7a7a7a;
	}

	virtual void Finalize() {
		delete this;
	}

private:
	void AddReference() {
		Assert(counter_ < 0x7fffffff);
		++counter_;
	}

	bool ReleaseReference() {
		Assert(counter_ > 0);
		--counter_;
		return (0 == counter_);
	}

	friend void intrusive_ptr_add_ref(RefCounter *p) {
		p->AddReference();
	}

	friend void intrusive_ptr_release(RefCounter *p) {
		if (p->ReleaseReference()) {
			p->Finalize();
		}
	}

private:
	mutable int counter_; ///< Счётчик ссылок.
};

#endif // __REFCOUNTER_H__
