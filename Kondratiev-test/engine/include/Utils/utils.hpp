#ifndef __UTILS_UTILS_H__
#define __UTILS_UTILS_H__

#pragma once

#include "types.h"
#include "File/FileMethods.h"
#include "Utils/IPoint.h"
#include "Utils/Color.h"
#include "RefCounter.h"

#ifndef ENGINE_TARGET_LINUX
#include "Utils/half.hpp"
#endif

#ifdef THREADED
#include "ThreadSupport.h"
#endif

#include <boost/intrusive_ptr.hpp>

namespace utils
{

#ifdef ENGINE_TARGET_WIN32

#ifdef THREADED
extern MUTEX_TYPE lexCastMutex;
#define LEX_CAST_THREAD_LOCK MUTEX_LOCK_TYPE lock(lexCastMutex);
#else
#define LEX_CAST_THREAD_LOCK
#endif

#endif

	FILE* fopen(const std::string& filename, const std::string& mode);
	FILE* fopen(const std::wstring& filename, const std::wstring& mode);

    // это служит для открытия сайтов в системном браузере а не то что вы подумали
    // для iOS тело определёно в Platform/iphone.mm
	void OpenPath(const std::string& sitePath);
	void GetLocalTime(SYSTEMTIME& stm);
	/// Мультиплатформенный sleep, время в миллисекундах (в тысячных долях)
	void Sleep(unsigned long msec);

	//
	// Вероятно, переводится как "Read `name=value` pair";
	// парсит строку формата `name=value`, записывает имя в name и значение в value.
	// Возвращает true, если строка в нужном формате и false в противном случае.
	//
	bool ReadNvp(const std::string& str, std::string& name, std::string& value);

	template <class Coll>
	inline void tokenize(const std::string& str, Coll &token, const std::string& separators = std::string(" /\t:,()"))
	{
		std::string input(str);
		std::string::size_type idx = 0;
		
		if ((idx = input.find_first_not_of(separators)) == std::string::npos) {
			return;
		}
		
		input = input.substr(idx);
		while ((idx = input.find_first_of(separators)) != std::string::npos) {
			token.push_back(input.substr(0, idx));
			input = input.substr(idx + 1);
			idx = input.find_first_not_of(separators);
			if (idx == std::string::npos) {
				break;
			}
			input = input.substr(idx);
		}
		
		if ((input.find_first_of(separators)) == std::string::npos) {
			token.push_back(input);
		}
	}

	inline bool equals(const char* a, const char* b)
	{
		if (a == NULL || b == NULL) {
			Assert(false);
			return false;
		}
		while (*a == *b) {
			if (*a == '\0') return true;
			++a; ++b;
		}
		return false;
	}

	inline bool iequals(const char* a, const char* b)
	{
		if (a == NULL || b == NULL) {
			Assert(false);
			return false;
		}
		std::locale loc;
		while (std::toupper(*a, loc) == std::toupper(*b, loc)) {
			if (*a == '\0') return true;
			++a; ++b;
		}
		return false;
	}

	template <class T>
	inline T lexical_cast(const char* str);

	template <class T>
	inline T lexical_cast(const std::string& str) { return lexical_cast<T>(str.c_str()); }

	template <>
	inline std::string lexical_cast(const std::string& value) {
		return value;
	}

#ifndef ENGINE_TARGET_LINUX
	template <>
	inline half_float::half lexical_cast(const char * str)
	{
		Assert(str != NULL);
		return half_float::half((float)std::atof(str));
	}
#endif

	template <>
	inline float lexical_cast(const char * str)
	{
		Assert(str != NULL);
		return (float)std::atof(str);
	}
	
	template <>
	inline double lexical_cast(const char * str)
	{
		Assert(str != NULL);
		return std::atof(str);
	}
	
	template <>
	inline short lexical_cast(const char * str)
	{
		Assert(str != NULL);
		return (short)std::atoi(str);
	}
	
	template <>
	inline int lexical_cast(const char * str)
	{
		Assert(str != NULL);
		return std::atoi(str);
	}
	
	template <>
	inline unsigned int lexical_cast(const char * str)
	{
		unsigned int n = 0;
		Assert(str != NULL);
		sscanf_s(str, "%u", &n);
		return n;
	}
	
	template <>
	inline long lexical_cast(const char * str)
	{
		Assert(str != NULL);
		return std::atol(str);
	}

	template <>
	inline unsigned char lexical_cast(const char * str)
	{
		Assert(str != NULL);
		return (unsigned char)std::atoi(str);
	}

	template <>
	inline bool lexical_cast(const char * str)
	{
		Assert(str != NULL);
		if (iequals(str, "true") || iequals(str, "yes") || equals(str, "1")) {
			return true;
		}
		return false;
	}

	template <>
	inline __int64 lexical_cast(const char * str)
	{
		__int64 n = 0;
		Assert(str != NULL);
		#ifdef ENGINE_TARGET_WIN32
			sscanf_s(str, "%I64d", &n);
		#else
			sscanf_s(str, "%lld", &n);
		#endif
		return n;
	}

	template <>
	inline std::string lexical_cast(const char * value) 
	{
		return value;
	}
	
	inline std::string lexical_cast(double value)
	{
		std::ostringstream ss;
		ss << std::setprecision(16) << value;
		return ss.str();
	}

	inline std::string lexical_cast(float value)
	{
		std::ostringstream ss;
		ss << std::setprecision(16) << value;
		return ss.str();
	}

	inline std::string lexical_cast(char value)
	{
		std::ostringstream ss;
		ss << (int)value;
		return ss.str();
	}

	inline std::string lexical_cast(unsigned char value)
	{
		std::ostringstream ss;
		ss << (int)value;
		return ss.str();
	}
	
	template <class T>
	inline std::string lexical_cast(const T& value)
	{
		std::ostringstream ss;
		ss << value;
		return ss.str();
	}

	bool is_char(int code);
	std::string to_char(int code);

	bool AllowAllAccess(const std::string& theFileName);
	std::string RemoveTrailingSlash(const std::string& theDirectory);

#ifdef ENGINE_TARGET_WIN32
	bool CheckForVista();
	bool CheckFor98Mill();
#endif
}

namespace Xml
{
	template <class T>
	inline bool TiXmlQueryAttribute(Xml::TiXmlElement* elem, const std::string& attrName, T& attrValue)
	{
		const char *value = elem->Attribute(attrName.c_str());
		if (value)
		{
			attrValue = utils::lexical_cast<T>(value);
			return true;
		}
		return false;
	}

	template <class T>
	inline bool TiXmlQueryAttribute(rapidxml::xml_node<>* elem, const std::string& attrName, T& attrValue)
	{
		if (elem->first_attribute(attrName.c_str()))
		{
			attrValue = utils::lexical_cast<T>(elem->first_attribute(attrName.c_str())->value());
			return true;
		}
		return false;
	}

	template <>
	inline bool TiXmlQueryAttribute(Xml::TiXmlElement* elem, const std::string& attrName, std::string& attrValue)
	{
		const char *value = elem->Attribute(attrName.c_str());
		if (value)
		{
			attrValue = std::string(value);
			return true;
		}
		return false;
	}

	template <>
	inline bool TiXmlQueryAttribute(rapidxml::xml_node<>* elem, const std::string& attrName, std::string& attrValue)
	{
		if (elem->first_attribute(attrName.c_str()))
		{
			attrValue = elem->first_attribute(attrName.c_str())->value();
			return true;
		}
		return false;
	}

	// Долгоживущий rapidxml::xml_document
	struct RapidXmlDocument : public RefCounter
	{
		std::string filename;
		rapidxml::file<char> file;
		rapidxml::xml_document<char> doc;

		RapidXmlDocument(const std::string &name)
			: filename(name)
			, file(filename.c_str())
		{
			doc.parse<rapidxml::parse_default>(file.data());
		}

		void Reload()
		{
			file = rapidxml::file<char>(filename.c_str());
			doc.parse<rapidxml::parse_default>(file.data());
		}

		rapidxml::xml_node<> *first_node()
		{
			return doc.first_node();
		}

		typedef boost::intrusive_ptr<RapidXmlDocument> HardPtr;
	};

}

#define TiXmlQueryNvpAttribute(elem, attr) Xml::TiXmlQueryAttribute(elem, #attr, attr)

#endif // __UTILS_UTILS_H__
