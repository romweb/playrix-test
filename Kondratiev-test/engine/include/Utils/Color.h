/*
 *  Color.h
 *  Engine
 *
 *  Created by Slava on 24.06.10.
 *  Copyright 2010 Playrix Entertainment. All rights reserved.
 *
 */

#ifndef _UTILS_COLOR_H_
#define _UTILS_COLOR_H_

#pragma once

#include "Platform/TargetPlatforms.h"
#include "Utils/utils.hpp"

#define MAKECOLOR4(a, r, g, b) Color(r, g, b, a)

namespace Xml {
	class TiXmlElement;
}

struct Color
{
	static const Color WHITE;	
	static const Color BLACK;	
	static const Color RED;	
	static const Color GREEN;	
	static const Color BLUE;	
	static const Color BLACK_TRANSPARENT;	
	static const Color WHITE_TRANSPARENT;
	
	union 
	{
		unsigned char pColor[4];
		struct 
		{
#if defined(ENGINE_TARGET_WIN32) && !defined(USING_GLES2)
			uint8_t blue;
			uint8_t green;
			uint8_t red;
			uint8_t alpha;
#else
			uint8_t red;
			uint8_t green;
			uint8_t blue;
			uint8_t alpha;
#endif
		};
	};
	
	Color();
	
	Color(uint8_t red, uint8_t green, uint8_t blue);
	
	Color(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha);
	
	Color(Xml::TiXmlElement* elem);

	Color(rapidxml::xml_node<>* elem);
	
	//
	// Читает цвет из строки.
	// Цвет задаётся в двух возможных форматах:
	// 1) "#RRGGBBAA", например #FFFFFFFF, #A10000;
	// 2) "RRR;GGG;BBB;AAA", например "128;255;60;50".
	//
	Color(const std::string& c);
	
	explicit Color(uint8_t alpha);

	Color& operator*=(const Color& c);
	
	bool operator==(const Color& c) const;
	
	bool operator!=(const Color& c) const { return !operator == (c); }
};

Color operator*(const Color& c1, const Color& c2);

#endif //_UTILS_COLOR_H_
