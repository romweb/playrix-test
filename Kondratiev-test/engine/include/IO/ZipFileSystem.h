#ifndef __ZIPFILESYSTEM_H__
#define __ZIPFILESYSTEM_H__

#ifdef _MSC_VER
#pragma once
#endif

#include "IO/FileSystem.h"
#include "IO/MemoryStream.h"

#include <unzip.h>

namespace IO {

class ZipFileSystem : public FileSystem {
public:
	ZipFileSystem(const std::string& filename, const std::string& innerPath);

	~ZipFileSystem();

	bool FileExists(const std::string& filename) const;

	void FindFiles(const std::string& filespec, std::vector<std::string>& result);

	InputStreamPtr OpenRead(const std::string& filename);

	OutputStreamPtr OpenWrite(const std::string& filename);

	StreamPtr OpenUpdate(const std::string& filename);

#ifdef ENGINE_TARGET_LINUX
    void GenIndexMap(std::map<std::string, FileInfo> &map, const std::string& prefix, FileInfo::Type);
	void RemoveFile(const std::string& filename);
#endif

private:
	unzFile _z;

	unzFile Open(const std::string& filename);

	void BuildIndex();

	struct ZipFile {
		ZipFile()
			: compressedSize(0)
			, uncompressedSize(0)
		{
		}

		ZipFile(const char* name, size_t compressed, size_t uncompressed, const unz64_file_pos& pos)
			: name(name)
			, compressedSize(compressed)
			, uncompressedSize(uncompressed)
			, pos(pos)
		{
		}

		std::string name;
		size_t compressedSize;
		size_t uncompressedSize;
		unz64_file_pos pos;

		bool operator<(const ZipFile& rhs) const {
			return unzStringFileNameCompare(name.c_str(), rhs.name.c_str(), 0) < 0;
		}

		friend bool operator<(const ZipFile& file, const std::string& filename) {
			return unzStringFileNameCompare(file.name.c_str(), filename.c_str(), 0) < 0;
		}

		friend bool operator<(const std::string& filename, const ZipFile& file) {
			return unzStringFileNameCompare(filename.c_str(), file.name.c_str(), 0) < 0;
		}
	};

	typedef std::vector<ZipFile> Files;

	Files _files;
};

} // namespace IO

#endif // __ZIPFILESYSTEM_H__
