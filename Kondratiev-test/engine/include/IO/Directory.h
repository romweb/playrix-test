#ifndef __DIRECTORY_H__
#define __DIRECTORY_H__

#ifdef _MSC_VER
#pragma once
#endif

namespace IO {

class Directory {
public:
	static bool Exists(const std::string& dirname);
	static void Create(const std::string& dirname);
	//static void Remove(const std::string& dirname);
};

} // namespace IO

#endif // __DIRECTORY_H__
