#ifndef __BINARYREADER_H__
#define __BINARYREADER_H__

#if _MSC_VER > 1200
#pragma once
#endif // _MSC_VER

namespace IO {

class InputStream;

class BinaryReader {
public:
	explicit BinaryReader(InputStream* stream);

	InputStream* GetBaseStream() const { return _stream; }

	int8_t ReadInt8();
	int16_t ReadInt16();
	int32_t ReadInt32();
	int64_t ReadInt64();
	uint8_t ReadUInt8();
	uint16_t ReadUInt16();
	uint32_t ReadUInt32();
	uint64_t ReadUInt64();
	bool ReadBool();
	float ReadFloat();
	double ReadDouble();
	std::string ReadUtf8String();

private:
	InputStream* _stream;
};

} // namespace IO

#endif // __BINARYREADER_H__
