#ifndef __VIRTUALFILESYSTEM_H__
#define __VIRTUALFILESYSTEM_H__

#ifdef _MSC_VER
#pragma once
#endif

#include "IO/FileSystem.h"
#include "RefCounter.h"

#ifdef THREADED
#include "ThreadSupport.h"
#endif

namespace IO {

class VirtualFileSystem : public RefCounter {
public:
	VirtualFileSystem();

	void SetWriteDirectory(const std::string& dir);

	void Mount(FileSystem* fs);

	void MountDirectory(const std::string& path);

	void MountZip(const std::string& filename, const std::string& innerPath);

	void MountPack();

	bool FileExists(const std::string& filename) const;

	void FindFiles(const std::string& filespec, std::vector<std::string>& result) const;

#ifdef ENGINE_TARGET_LINUX
    void FindDirs(const std::string& filespec, std::vector<std::string>& result) const;
    FileInfo::Type FileType(const std::string& filename) const;
    void GenIndexMap(const std::vector<std::pair<std::string, FileInfo::Type> >& searchPaths);
    void IndexFile(const std::string& filename);
	void RemoveFile(const std::string& filename);
#endif

	InputStreamPtr OpenRead(const std::string& filename);

	OutputStreamPtr OpenWrite(const std::string& filename);

	StreamPtr OpenUpdate(const std::string& filename);

private:
	typedef std::vector<FileSystemPtr> FileSystems;

#ifdef ENGINE_TARGET_LINUX
    std::map<std::string, FileInfo> _indexmap;
#endif

	FileSystems _systems;

#ifdef THREADED
	mutable MUTEX_TYPE _mutex;
#endif

private:
	DISALLOW_COPY_AND_ASSIGN(VirtualFileSystem);
};

} // namespace IO


namespace Core {
	extern IO::VirtualFileSystem fileSystem;
}

#endif // __VIRTUALFILESYSTEM_H__
