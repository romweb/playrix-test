#ifndef __AUDIOSOURCEFILEOGG_H__
#define __AUDIOSOURCEFILEOGG_H__

#ifdef _MSC_VER
#pragma once
#endif

#include "AudioSource.h"

#ifdef THREADED
#include "ThreadSupport.h"
#endif

#include <vorbis/vorbisfile.h>

namespace MM {

class AudioSourceFileOgg : public AudioSource {
public:
    AudioSourceFileOgg(IO::InputStream* stream);

	AudioSourceFileOgg(const std::string& filename);
	
	AudioSourceFileOgg(const unsigned char* buffer, size_t length);
	
	~AudioSourceFileOgg();
	
	int GetChannels() const;
	
	int GetRate() const;
	
	int GetBits() const;
	
	size_t GetLengthInBytes() const;
	
	size_t Read(void* buffer, size_t count);
	
	bool IsEof() const;
	
	void Rewind();

private:
	void InitCallbacks();

private:
	IO::InputStreamPtr _stream;

	mutable OggVorbis_File _ov;
	
	bool _eof;

#ifdef THREADED
	mutable MUTEX_TYPE _mutex;
#endif
};

} // namespace MM

#endif // __AUDIOSOURCEFILEOGG_H__
