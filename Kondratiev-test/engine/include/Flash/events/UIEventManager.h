#pragma once

#include "Flash/events/FlashEvent.h"
#include "Flash/events/EventManager.h"
#include "Flash/events/TouchEvents.h"

/**
* �����, ������������� ���������� � �������� � ������� 
*/
class UIEventManager{
public:
	UIEventManager();
	~UIEventManager();

	/**
	* ������������� ������, ������� �������� ������ ��������. ������ ������ ��������
	* ������� �������, ���� ���� ��� �� ������ �� � ���� ������.
	*/
	void setRoot(IFlashDisplayObject* displayObject);

	/**
	* ���������, ��� ������� � touchId=0 ��������������� ��� �������� �����
	*/
	void setMouseInputAtZero();

	int touchBegin(float x, float y);
	void touchMove(int touchId, float x, float y);
	void touchEnd(int touchId, float x, float y);
	void touchCancel(int touchId);

	void touchLeave(int touchId);
	void touchEnters(int touchId);

private:
	void touchRelatedObjectChanged(int touchId, IFlashDisplayObject* prev, IFlashDisplayObject* next);

	int getFreeTouchIndex();

	IFlashDisplayObject* root;

	struct Touch{
		int id;
		float initX, initY;
		float x, y;
		IFlashDisplayObject* relatedObject;
		bool captured;
		bool active;
		bool inStage;
		bool isMouse;
	};
	int activeTouches;
	int capturedTouches;
	std::vector<Touch> touches;

};