#pragma once

#include "Flash/events/FlashEvent.h"

//TODO:: ������ static
///������� ������ �������
static const std::string TouchBegin = "touchBegin";
///������� ��������� �������
static const std::string TouchEnd = "touchEnd";
///������� ����������� �������
static const std::string TouchMove = "touchMove";
///�������, ����������, ��� ����� ������� �������� ������� �������
static const std::string TouchLeave = "touchLeave";
///�������, ����������, ��� ����� ������� ����� � ������� �������
static const std::string TouchEnter = "touchEnter";
///������� ����������� ������� ���� ��� ������� ������
static const std::string TouchPassiveMove = "touchPassiveMove";
///�������, ����������, ��� ������� ���� ��������
static const std::string TouchCancel = "touchCancel";

///�������, ����������, ��� ������� �������� ������� ����� (���� ����������)
static const std::string TouchLeaveStage = "touchLeaveStage";
///�������, ����������, ��� ������� ��������� � ������� �����
static const std::string TouchEnterStage = "touchEnterStage";

///�������, ����������, ��� ����� ������� ����� � ������� �������.
///�������� � TouchEnter � ���, ��� TouchRollOver �� ����� ���� ������� � ����� � ���������.
///��������� ����������� � ������� MouseEvent.MOUSE_OVER / MouseEvent.ROLL_OVER � flash player.
///������ ����� ��� ����� ������ TouchRollOver.
static const std::string TouchRollOver = "rollover";
///�������, ����������, ��� ����� ������� �������� ������� �������.
static const std::string TouchRollOut = "rollout";

///������� ��������� �����
static const std::string GesturePinch = "gesturePinch";

class TouchEvent: public FlashEvent{
public:
	TouchEvent(const std::string& type, int touchIndex, float globalX, float globalY);

	/**
	* ID ������� - ��� ��������� multitouch �����������
	*/
	int getTouchIndex(){
		return touchIndex;
	}

	float getGlobalX(){
		return globalX;
	}

	float getGlobalY(){
		return globalY;
	}

	float getLocalX(){
		return localX;
	}

	float getLocalY(){
		return localY;
	}

	void visitNode(IFlashDisplayObject* node);
private:
	int touchIndex;
	float globalX, globalY;
	float localX, localY;
};

class PinchEvent: public FlashEvent{
public:
	PinchEvent(const std::string& type, float globalX, float globalY, float scale);

	float getGlobalX(){
		return globalX;
	}

	float getGlobalY(){
		return globalY;
	}

	float getLocalX(){
		return localX;
	}

	float getLocalY(){
		return localY;
	}

	float getScale(){
		return scale;
	}

	void visitNode(IFlashDisplayObject* node);

private:
	int touchIndex;
	float globalX, globalY;
	float localX, localY;
	float scale;
};