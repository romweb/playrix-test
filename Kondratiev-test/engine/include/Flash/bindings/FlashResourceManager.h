#pragma once

#include "Flash/bindings/FlashEngineBinding.h"
#include "Flash/bindings/ResourceCleanup.h"

namespace Render {
	class Texture;
}
 
namespace Flash {

	struct FlashLibraryRecord;
	typedef boost::intrusive_ptr<FlashLibraryRecord> FlashLibraryRecordPtr;

	class FlashResourceManager: public ICleanable {
	public:
		FlashResourceManager();

		/**
		* Устанавливает путь для поиска библиотек
		*/
		void setSourcePath(const std::string& path);

		/**
		* Очищает все ресурсы, на которые больше нет ссылок
		*/
		void clean();

		/**
		* Добавляет новый источник клипов.
		* К источнику можно затем обращаться через точку "source.clip" 
		* в методах getLibraryItem и createItem
		*/
		void addSource(const struct SwlRequest& request, const std::string& name);

		/**
		* Определяет, определен ли источник с именем name
		*/
		bool hasSource(const std::string& name);

		IFlashLibrary* getLibrary(const std::string& path);

		/**
		* Возвращает элемент библиотеки по указанному пути.
		* Данный метод вынуждает содержащий элемент файл загрузиться.
		*/
		IFlashLibraryItem* getLibraryItem(const std::string& path);
		
		/**
		* Возвращает созданный по элементу библиотеки объект
		*/
		IFlashDisplayObject* createItem(const std::string& path);

		/**
		* Определяет, есть ли в билиотеке элемент path.
		* Приводит к загрузке источника.
		*/
		bool hasItem(const std::string& path);

		/**
		* Возвращает текстуру, используемую определенным источником.
		* Источник при этом будет загружен.
		* Почему unsafe: если не создать ни один элемент библиотеки источника,
		* то он может быть очень скоро (буквально при следующем createItem для
		* другого источника) выгружен и текстура станет невалидной.
		* Метод safe, если запрос для источника создан как не владеющий текстурой.
		* В таком случае о разрушении текстуры должен заботиться пользовательский код
		* и это единственный способ что-то с ней сделать.
		*/
		Render::Texture* unsafeGetTextureOf(const std::string& source);

		/**
		* Проверить, готов ли определенный клип.
		* Если клип не готов, то запрос get/create будет блокирующим
		*/
		bool isSourceReady(const std::string& path);

		/**
		* Подсказать, что очень скоро нам нужен будет определенный клип.
		* Через эту функцию можно реализовать загрузку. Однако вообще говоря
		* менеджер вовсе не обязан загружать объект немедленно, особенно
		* если имеется недостаток памяти
		*/
		void hintItem(const std::string& path);

		/**
		* Установить предел памяти для менеджера.
		* Тем не менее, менеджер может потреблять больше. Однако до заданного уровня
		* будет очищаться кэш при каждом releaseMemory
		*/
		void setMemoryLimit(int bytes);

		/**
		* Удалить неиспользуемые ресурсы
		* @param agressive уточняет, надо ли сохранять MemoryLimit неиспользуемых ресурсов
		* для дальнейшего использования
		*/
		void releaseMemory(bool agressive = false);

		static FlashResourceManager* instance;

	private:

		void forceLibraryLoading(FlashLibraryRecord* libraryRecord);
		void unloadLibrary(FlashLibraryRecord* libraryRecord);

		int memoryLimit;
		int memoryConsumption;
		std::map<std::string, FlashLibraryRecordPtr> libraries;
		std::list<FlashLibraryRecord*> uploadedLibraries;

		bool hasSourcePath;
		std::string sourcePath;
	};

}
