#pragma once

#include "Flash/core/IFlashParticleEffect.h"

IFlashParticleEffect* createFlashParticleEffect(const std::string& name);
