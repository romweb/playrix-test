#pragma once

#include "Flash/fquery/fquery.h"
#include "Flash/core/FlashStage.h"

namespace fquery{

	namespace ui{
		class UIElement: public IGCRef{
		public:
			UIElement(IFlashDisplayObject* displayObject){
				this->displayObject = displayObject;
			}

			GC_BLACKEN_INLINE(UIElement){
				//�� ����� �������� displayObject - ��� ������ ������
			}
		protected:
			//weak ref
			IFlashDisplayObject* displayObject;
		};

		class Button: public UIElement{
		public:
			Button(IFlashDisplayObject* displayObject):UIElement(displayObject){
				fQuery(displayObject)
					.bind(TouchBegin, MEMBER_HANDLER(this, Button::onTouchBegin))
					.bind(TouchRollOver, MEMBER_HANDLER(this, Button::onTouchOver))
					.bind(TouchRollOut, MEMBER_HANDLER(this, Button::onTouchOut));
				isTouchOver = false;
				isTouching = false;

				active = fQuery(displayObject).children("active").maybeGet(0);
				pressed = fQuery(displayObject).children("pressed").maybeGet(0);
				IFlashDisplayObject* back = fQuery(displayObject).children("hitTest").maybeGet(0);
				if(back)
				{
					back->setAlpha(0.f);
				}
				updateState();
			}
		protected:
			void onTouchBegin(TouchEvent& event){
				isTouching = true;
				root = displayObject->getRoot();
				fQuery(root)
					.bind(TouchEnd, MEMBER_HANDLER(this, Button::onTouchEnd))
					.bind(TouchCancel, MEMBER_HANDLER(this, Button::onTouchCancel));
				updateState();
			}

			void onTouchOver(TouchEvent& event){
				isTouchOver = true;
				updateState();
			}

			void onTouchOut(TouchEvent& event){
				isTouchOver = false;
				updateState();
			}

			void onTouchEnd(TouchEvent& event){
				if ( isTouchOver ){
					FlashEvent event("click");
					EventManager::Get().dispatchTarget(displayObject, event);
				}
				isTouching = false;
				updateState();
				unbind();
			}

			void onTouchCancel(TouchEvent& event){
				isTouching = false;
				updateState();
				unbind();
			}

			void updateState(){
				if ( isTouching && isTouchOver ){
					if ( active ){
						active->setVisible(false);
					}
					if ( pressed ){
						pressed->setVisible(true);
					}
				}else{
					if ( active ){
						active->setVisible(true);
					}
					if ( pressed ){
						pressed->setVisible(false);
					}
				}
			}

			void unbind(){
				fQuery(root)
					.unbind(TouchEnd, MEMBER_HANDLER(this, Button::onTouchEnd))
					.unbind(TouchCancel, MEMBER_HANDLER(this, Button::onTouchCancel));
			}


			bool isTouchOver;
			bool isTouching;
			IFlashDisplayObject* root;

			IFlashDisplayObject* active;
			IFlashDisplayObject* pressed;
		};

		class Checkbox: public UIElement{
		public:
			Checkbox(IFlashDisplayObject* displayObject):UIElement(displayObject){
				fQuery(displayObject)
					.bind(TouchBegin, MEMBER_HANDLER(this, Checkbox::onTouchBegin))
					.bind("check", MEMBER_HANDLER(this, Checkbox::onCheck))
					.bind("uncheck", MEMBER_HANDLER(this, Checkbox::onUncheck))
					.bind("queryBoolValue", MEMBER_HANDLER(this, Checkbox::onQueryValue));
				
				check = fQuery(displayObject).children("check").maybeGet(0);
				state = false;
				IFlashDisplayObject* back = fQuery(displayObject).children("hitTest").maybeGet(0);
				if(back)
				{
					back->setAlpha(0.f);
				}
				updateState();
			}
		protected:
			void onTouchBegin(TouchEvent& /*event*/){
				state = !state;
				if ( state ){
					FlashEvent event("toggledOn");
					EventManager::Get().dispatchTarget(displayObject, event);
				}else{
					FlashEvent event("toggledOff");
					EventManager::Get().dispatchTarget(displayObject, event);
				}
				updateState();
			}

			void onQueryValue(QueryEvent<bool>& event){
				event.respond(state);
			}

			void onCheck(FlashEvent& event){
				state = true;
				updateState();
			}

			void onUncheck(FlashEvent& event){
				state = false;
				updateState();
			}

			void updateState(){
				if ( state ){
					if ( check ){
						check->setVisible(true);
					}
				}else{
					if ( check ){
						check->setVisible(false);
					}
				}
			}

			bool state;
			IFlashDisplayObject* root;
			IFlashDisplayObject* check;
		};

		class ToggleButton: public UIElement{
		public:
			ToggleButton(IFlashDisplayObject* displayObject):UIElement(displayObject){
				fQuery(displayObject)
					.bind(TouchBegin, MEMBER_HANDLER(this, ToggleButton::onTouchBegin))
					.bind("check", MEMBER_HANDLER(this, ToggleButton::onCheck))
					.bind("uncheck", MEMBER_HANDLER(this, ToggleButton::onUncheck))
					.bind("queryBoolValue", MEMBER_HANDLER(this, ToggleButton::onQueryValue));
				
				active = fQuery(displayObject).children("active").maybeGet(0);
				pressed = fQuery(displayObject).children("pressed").maybeGet(0);
				state = false;
				updateState();
			}
		protected:
			void onTouchBegin(TouchEvent& event){
				state = !state;
				if ( state ){
					FlashEvent event("toggledOn");
					EventManager::Get().dispatchTarget(displayObject, event);
				}else{
					FlashEvent event("toggledOff");
					EventManager::Get().dispatchTarget(displayObject, event);
				}
				updateState();
			}

			void onQueryValue(QueryEvent<bool>& event){
				event.respond(state);
			}

			void onCheck(FlashEvent& event){
				state = true;
				updateState();
			}

			void onUncheck(FlashEvent& event){
				state = false;
				updateState();
			}

			void updateState(){
				if ( state ){
					if ( active ){
						active->setVisible(false);
					}
					if ( pressed ){
						pressed->setVisible(true);
					}
				}else{
					if ( active ){
						active->setVisible(true);
					}
					if ( pressed ){
						pressed->setVisible(false);
					}
				}
			}

			bool state;
			// IFlashDisplayObject* root; // not in use
			IFlashDisplayObject* active;
			IFlashDisplayObject* pressed;
		};

		/// �����, ����������� ��������� �������� ������� � �����
		class Anchor : public UIElement {
		public:
			enum Type {
				None,
				Left = 1 << 0,
				Top = 1 << 1,
				Right = 1 << 2,
				Bottom = 1 << 3
			};

			Anchor(IFlashDisplayObject* displayObject, unsigned int anchor)
				: UIElement(displayObject)
				, anchor(anchor)
			{
				FlashStage* stage = dynamic_cast<FlashStage*>(displayObject->getRoot());
				if (!stage) {
					throw std::runtime_error("display object must be put into the stage");
				}

				stage->getOrigin(initStageX, initStageY);
				stage->getStageSize(initStageWidth, initStageHeight);

				displayObject->getBounds(left, top, right, bottom, NULL);

				fQuery(displayObject).bind(Resize, MEMBER_HANDLER(this, Anchor::onResize));
			}

		protected:
			void onResize(ResizeEvent& event) {
				FlashStage* stage = dynamic_cast<FlashStage*>(displayObject->getRoot());
				if (!stage) {
					throw std::runtime_error("display object must be put into the stage");
				}

				// �������� � ����� ����� ����� ������ � ������ ��� ���������������
				if (stage->getScaleMode() != ScaleModeNoScale) {
					return;
				}

				float origX, origY;
				stage->getOrigin(origX, origY);

				float stageWidth, stageHeight;
				stage->getStageSize(stageWidth, stageHeight);

				float dx0 = initStageX - origX;
				float dx1 = initStageX - origX + initStageWidth - stageWidth;
				float dy0 = initStageY - origY;
				float dy1 = initStageY - origY + initStageHeight - stageHeight;

				float newLeft = left, newTop = top, newRight = right, newBottom = bottom;

				if (anchor & Left || anchor & Right) {
					if (anchor & Left && anchor & Right) {
						newLeft -= dx0;
						newRight -= dx1;
					} else if (anchor & Left) {
						newLeft -= dx0;
						newRight -= dx0;
					} else if (anchor & Right) {
						newLeft -= dx1;
						newRight -= dx1;
					}
				}

				if (anchor & Top || anchor & Bottom) {
					if (anchor & Top && anchor & Bottom) {
						newTop -= dy0;
						newBottom -= dy1;
					} else if (anchor & Top) {
						newTop -= dy0;
						newBottom -= dy0;
					} else if (anchor & Bottom) {
						newTop -= dy1;
						newBottom -= dy1;
					}
				}

				if (anchor == None) {
					// �� ����, � ���� ������ ����� ��������������� ������� ����� ��������

					float centerX = (right + left) * 0.5f;
					float centerY = (top + bottom) * 0.5f;

					//newLeft = 
				}

				displayObject->setPosition(left, top);
				displayObject->setScale((newRight - newLeft) / (right - left), (newBottom - newTop) / (bottom - top));
			}

		protected:
			unsigned int anchor;
			float initStageX, initStageY, initStageWidth, initStageHeight;
			float left, top, right, bottom;
		};

		void makeButton(IFlashDisplayObject* displayObject);
		void makeCheckbox(IFlashDisplayObject* displayObject);
		void makeToggleButton(IFlashDisplayObject* displayObject);
		void makeAnchor(IFlashDisplayObject* displayObject, unsigned int anchor);

		FQuerySelector& button(FQuerySelector& selector);
		FQuerySelector& checkbox(FQuerySelector& selector);
		FQuerySelector& toggleButton(FQuerySelector& selector);
		FQuerySelector& anchor(FQuerySelector& selector, unsigned int anchor);
	}

}

#include "fquery_ui_press.h"
#include "fquery_ui_radio.h"
#include "fquery_ui_scroller.h"