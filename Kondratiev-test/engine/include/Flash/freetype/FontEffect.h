#include "Flash/fquery/fquery.h"
#include "Flash/fquery/fquery_ui.h"

#include "../picojson/picojson.h"

typedef unsigned char byte;

namespace freetype{    
	
    class FontEffect{
	public:
		virtual void getExtension(int& nx, int& ny, int& px, int& py) = 0;
		virtual void apply(byte* data, byte* target, int width, int height, float* aabb) = 0;
		virtual void getKerning(float& front, float& back){front = 0.0f; back = 0.0f;}
	};
    
    class FontEffectTemplate {
    public:
        virtual freetype::FontEffect* create( picojson::object &object ) = 0;
    };
    
}
