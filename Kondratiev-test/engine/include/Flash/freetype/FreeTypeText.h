#pragma once

#include "Flash/freetype/FreeType.h"
#include <Utils/IPoint.h>

namespace freetype{

	//Задает форматирование параграфа
	struct Paragraph{
	public:
		bool wordWrap;
        enum tagWordBreak{
			Normal,
			BreakAll
		} wordBreak;
		enum tagAlign{
			LeftAlign,
			RightAlign,
			CenterAlign,
			WidthAlign
		} align;
		float lineInterval; // 0..1

		Paragraph(){
			this->align = LeftAlign;
			this->wordWrap = true;
            this->wordBreak = BreakAll;
			this->lineInterval = 0.0f;
		}

		Paragraph(bool wordWrap, tagWordBreak wordBreak, tagAlign align, float lineInterval){
			this->align = align;
			this->wordWrap = wordWrap;
            this->wordBreak = wordBreak;
			this->lineInterval = lineInterval;
		}
	};

	//Задает слово - участок текста с постоянным шрифтом.
	class Word{
	public:
		Word(FontInstance* font, charcode* charcodes, int length, const byte* color, float letterSpacing = 0);
		virtual ~Word(){
			if ( glyphs )
				delete[] glyphs;
		}

		FontInstance* getFont();
		const byte* getColor();
		float getLetterSpacing();

		void getGlyphs(glyphindex** glyphs, int& count);

		template<typename Iter, typename Container, charcode(*Decoder)(Container container, Iter& position)>
		static Word* fromString(FontInstance* font, Container string, const byte* color, float letterSpacing = 0){
			Iter pointer = 0;
			std::vector<charcode> charcodes;
			charcode value;
			while ( value = Decoder(string, pointer) ){
				charcodes.push_back(value);
			}
			return new Word(font, charcodes.size() > 0 ? &charcodes[0] : 0, charcodes.size(), color, letterSpacing);
		}


	protected:
		virtual void update();
		FontInstance* fontInstance;
		byte color[4];
		glyphindex* glyphs;
		float letterSpacing;
		int length;
	};

	//Динамическое слово - обращается к переменной для получения своего значения
	template<class T, typename Format, typename Mutation>
	class DynamicWord: public Word{
	public:
		DynamicWord(FontInstance* font, const byte* color, const T* ref, float letterSpacing = 0)
			: Word(font, 0, 0, color, letterSpacing){
			this->ref = ref;
			chars = 0;

			allocated = 0;
			update();
		}

	protected:
		void update(){
			if ( mutation(ref) ){
				int charLength = length;
				format(ref, &chars, &charLength);
				if ( allocated < charLength ){
					delete[] glyphs;
					glyphs = new glyphindex[charLength];
					allocated = charLength;
				}
				length = charLength;
				freetype::resolveCharCodes(freetype::getReference(fontInstance), chars, charLength, glyphs);
			}
		}

		charcode* chars;
		int allocated;
		Mutation mutation;
		Format format;
		const T* ref;
	};

	//Встраиваемый объект (например, картинка)
	class InlinedObject{
	public:
		virtual ~InlinedObject(){}
		virtual void getSize(float& width, float& height) = 0;
		virtual void draw(float penX, float penY, freetype::byte* color) = 0;
	};

	
	struct TextRenderMetrics{
		float top;
		float bottom;
		float left;
		float right;
		float baseLine;
	};
	

	class Text{
	public:
		Text();
		~Text();

		int addParagraph(Paragraph* paragraph);
		int addWord(Word* word);
		int addObject(InlinedObject* object);

		void setLeft(float left);
		void setTop(float top);
		void setRight(float right);
		void setBottom(float bottom);
		
		void move(float x, float y);

		Paragraph* getLastParagraph();

		static Paragraph DefaultParagraph;
	
	private:
		float top, left, right, bottom;

		struct TextElement{
			union{
				Paragraph* paragraph;
				Word* word;
				InlinedObject* inlinedObject;
			};
			enum{
				PARAGRAPH,
				WORD,
				OBJECT
			}tag;
		};
		std::list<TextElement> elements;
		Paragraph* lastParagraph;

		friend class TextRenderer;
	};
	

	struct LineElement;

	class TextRenderer{
	public:
		TextRenderer(IRender*, IVertexStream*, FontTexture*); 
		void getMetrics(Text* text, TextRenderMetrics* metrix);
		void getLettersCoords(Text* text, std::vector<IPoint>& coords);
		void render(Text* text, freetype::byte* color);

	private:
		void getElements(Text* text, std::vector<LineElement>& line, float& x, float& y);

	private:
		IRender* renderer;
		FontTexture* fontTexture;
		IVertexStream* vertexStream;
	};
};