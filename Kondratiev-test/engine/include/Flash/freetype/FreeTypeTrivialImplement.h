#pragma once

#include "Flash/freetype/FreeType.h"

class NullTexture: public freetype::ITexture{
public:
	void upload(int x, int y, int width, int height, freetype::byte* data){
	}
};

class NullVertexStream: public freetype::IVertexStream{
public:
	void lock(freetype::Vertex** data, int verticiesRequired){
		internalData = *data = new freetype::Vertex[verticiesRequired];
	}

	void unlock(int verticiesWritten){
		delete internalData;
	}
private:
	freetype::Vertex* internalData;
};

class NullRender: public freetype::IRender{
public:
	void drawTriangleList(freetype::ITexture* texture, freetype::IVertexStream* stream, int triangleCount){
	}
};