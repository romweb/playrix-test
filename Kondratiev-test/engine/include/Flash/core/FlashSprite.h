#pragma once

#include "Flash/core/IFlashSprite.h"
#include "Flash/core/FlashDisplayObject.h"

/**
* FlashDisplayObject, который является контейнером для других объектов.
*/
class FlashSprite: public IFlashSprite, public FlashDisplayObject{
public:
	FlashSprite();
	~FlashSprite();

	IFlashDisplayObject* addChildAt(IFlashDisplayObject* displayObject, int index);
	IFlashDisplayObject* removeChildAt(int index);
	IFlashDisplayObject* getChildAt(int index);
	void swapChildren(int index1, int index2);
	int getChildrenCount();

	bool getBounds(float& left, float& top, float& right, float& bottom, IFlashDisplayObject* targetCoordinateSystem);

	void render(FlashRender& render);

	bool hitTest(float x, float y, IHitTestDelegate* hitTestDelegate);

	FlashDisplayObject* DisplayObject();

	IFlashSprite* Sprite();

	void advance(FlashUpdateListener* updateListener, float dt);

	void visitDFS(IFlashVisitor* visitor, bool preorder);
	void visitBFS(IFlashVisitor* visitor, bool preorder);

	GC_BLACKEN_DECL(FlashSprite)

private:
	std::vector<IFlashDisplayObject*> children;
	FlashSprite* spriteParent;
	int abstractCount;
};
