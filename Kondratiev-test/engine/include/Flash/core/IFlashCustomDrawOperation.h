#pragma once

#include "Flash/core/IFlashDisplayObject.h"
#include "Flash/core/FlashRender.h"
#include "Flash/core/FlashMarkSweep.h"

/**
* Определенная пользователем операция рисования
*/
GC_CLASS(IFlashCustomDrawOperation)
class IFlashCustomDrawOperation GC_CLASS_NOINHERITANCE(IFlashCustomDrawOperation){
public:
	IFlashCustomDrawOperation(){
		GC_SHOULD_TRACK(this);
	}

	virtual ~IFlashCustomDrawOperation(){}

	/**
	* Вызывается перед рисованием, если возвращает false - рисование отменяется.
	* Если операция оперирует низкоуровневым устройством вывода, то нужно вызвать render.invalidateConstants()
	*/
	virtual bool onPreRender(IFlashDisplayObject*, FlashRender& render) = 0;
	/**
	* Вызывается после рисования
	*/
	virtual void onPostRender(IFlashDisplayObject*, FlashRender& render) = 0;

	GC_CLASS_INTERFACE(IFlashCustomDrawOperation)
private:
	GC_CLASS_DATA(IFlashCustomDrawOperation)
};
