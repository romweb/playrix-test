#pragma once

#include "RefCounter.h"
#include "Flash/core/FlashCommon.h"

/**
* Служит для подгрузки бинарной билиотеки по частям
*/
class DynamicLibraryChunkReader : public RefCounter {
public:
	/**
	* Загружает из файла length байт начиная со смещения offset
	*/
	virtual Data readChunk(int offset, int length) = 0;
	
	/**
	* Выгружает ранее загруженный чанк
	*/
	virtual void releaseChunk(Data data) = 0;
	
	/**
	* Возвращает общий размер в байтах, занимаемый в памяти загруженными чанками
	*/
	virtual int getTotalLoadedSize() const = 0;
};

typedef boost::intrusive_ptr<DynamicLibraryChunkReader> DynamicLibraryChunkReaderPtr;
