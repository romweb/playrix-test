#pragma once

#include "Flash/core/FlashCommon.h"
#include "Flash/core/IFlashDisplayObject.h"

/**
* Используется для того, чтобы Update на объектах вызывался после обхода всей иерархии, а не во время.
* Таким образом решается проблема модификации дерева объектов из Update
*/
class FlashUpdateListener{
public:	
	/**
	* Указывает, что объект displayObject надо обновить на dt секунд
	*/
	void addUpdateRequest(IFlashDisplayObject* displayObject, float dt){
		if ( displayObject->hasUpdate() ){
			UpdateRequest request = {displayObject, dt};
			GC_ADD_REF(displayObject);
			sheduledUpdates.push_back(request);
		}
	}

	/**
	* Выполняет все запланированные обновления
	*/
	void doUpdates(){
		for ( int i = 0; i < (int)sheduledUpdates.size(); i++ ){
			sheduledUpdates[i].target->update(sheduledUpdates[i].dt);
		}
		clear();
	}
		
	/**
	* Очищает очередь обновления
	*/
	void clear(){
		for ( int i = 0; i < (int)sheduledUpdates.size(); i++ ){
			GC_REMOVE_REF(sheduledUpdates[i].target);
		}
		sheduledUpdates.clear();
	}

	struct UpdateRequest{
		IFlashDisplayObject* target;
		float dt;
	};

	std::vector<UpdateRequest> sheduledUpdates;
};