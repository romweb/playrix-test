#if !defined(TEXT_STYLE_H)
#define TEXT_STYLE_H

#pragma once

/**
* Стиль текста (шрифт + эффекты). Используется для выбора шрифта, которым будет рисоваться текст
*/
struct FlashTextStyle : public RefCounter{
	float fontSize;
	int fontColor;
	float letterSpacing;
	float lineInterval;
	float spaceWidth;
	int align;
	
	std::string fontName;

	std::vector<std::string> effects;

	FlashTextStyle()
		: fontSize(12)
		, fontColor(0xffffff)
		, letterSpacing(0)
		, lineInterval(0)
		, spaceWidth(1)
		, align(0){
	}
};

typedef boost::intrusive_ptr<FlashTextStyle> FlashTextStylePtr;

#endif
