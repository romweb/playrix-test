#pragma once

class IFlashDisplayObject;

/**
* Коллбек для функции обхода flash-иерархии 
* @see IFlashDisplayObject::visitBFS
* @see IFlashDisplayObject::visitDFS
*/
class IFlashVisitor{
public:
	virtual ~IFlashVisitor(){}
	virtual bool visit(IFlashDisplayObject* displayObject) = 0;
};