#pragma once

#include "Flash/core/FlashDisplayObject.h"
#include "Flash/core/IFlashText.h"

class FlashLibText;

class FlashText: public FlashDisplayObject, public IFlashText{
public:
	FlashText(FlashLibText* libText);
	virtual ~FlashText();

	void render(FlashRender& render);
	
	bool hitTest(float x, float y, IHitTestDelegate* hitTestDelegate);
	bool getBounds(float& left, float& top, float& right, float& bottom, IFlashDisplayObject* targetCoordinateSystem);
	
	void setWidth(float width);
	void setHeight(float height);
	float getWidth();
	float getHeight();

	void setText(const std::string& text);
	const std::string& getText();

	FlashText* Text();

	FlashText* DisplayObject();
private:
	FlashLibText* libText;
	float width, height;
	bool snapToPixels;
	std::string content;
};