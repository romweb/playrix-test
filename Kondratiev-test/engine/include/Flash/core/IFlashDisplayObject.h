#pragma once

#include "Flash/core/FlashCommon.h" 
#include "Flash/core/DisplayObjectRenderTarget.h"
#include "Flash/core/IFlashConcreteDisplayObject.h"
#include "Flash/core/IFlashVisitor.h"
#include "Flash/core/FlashRender.h"

class IFlashCustomDrawOperation;
class IFlashMovieClip;
class IHitTestDelegate;
class IFlashText;
class IFlashSprite;
class IFlashRasterSprite;
class FlashUpdateListener;
/**
* Интерфейс объекта отображения flash
*/
GC_CLASS(IFlashDisplayObject)
class IFlashDisplayObject GC_CLASS_NOINHERITANCE(IFlashDisplayObject){
public:
	IFlashDisplayObject(){
		GC_SHOULD_TRACK(this);
	}

	virtual ~IFlashDisplayObject() {
	}

	/**
	* Устанавливает пользовательскую операцию рисования
	*/
	virtual void setCustomDrawOperation(IFlashCustomDrawOperation*) = 0;

	/**
	* Рисование объекта в заданный массив данных
	* За преобразование координат и применение цвета несет ответственность вызывающий.
	*/
	virtual void render(FlashRender& render) = 0;

	/**
	* Получение родительского объекта
	*/
	virtual IFlashDisplayObject* getParent() = 0;

	/**
	* Получение корневого объекта - самого дальнего предка, не имеющего родителя
	*/
	virtual IFlashDisplayObject* getRoot() = 0;

	/**
	* Эффективная реализация получения общего предка
	* O(n + m), где n, m - высота деревьев this и another.
	*/
	IFlashDisplayObject* getCommonAncestor(IFlashDisplayObject* another);

	/**
	* Устанавливает родительский объект
	* 'Safe conditions': вызывается из метода объекта parent, который уже 
	* поместил текущий объект в свой список потомков.
	*/
	virtual void unsafeSetParent(IFlashDisplayObject* parent) = 0;

	/**
	* Устанавливает, является ли объект прозрачным для hitTest процедуры
	*/
	virtual void setHitTestTransparent(bool value) = 0;
	/**
	* Устанавливает, посылает ли объект попадание по нему при hitTest.
	* Свойство можно использовать, например, чтобы не получать информацию о
	* попадании во внутренний спрайт составного клипа. При этом сам объект
	* может быть непрозрачным, в таком случае о получении попадания сообщит его
	* родительский элемент
	*/
	virtual void setHitTestDispatcher(bool value) = 0;

	virtual bool getHitTestTransparent() = 0;
	virtual bool getHitTestDispatcher() = 0;

	/**
	* Устанавливает позицию относительно родительского объекта 
	*/
	virtual void setPosition(float x, float y) = 0;
	/**
	* Получает позицию относительно родительского объекта
	*/
	virtual void getPosition(float& x, float& y) = 0;
	/**
	* Устанавливает угол поворота в координатной системе родительского объекта
	*/
	virtual void setRotation(float radians) = 0;
	/**
	* Возвращает поворот в координатной системе родительского объекта
	*/
	virtual float getRotation() = 0;

	/**
	* Устанавливает искажение в координатной системе родительского объекта
	*/
	virtual void setShear(float k) = 0;
	/**
	* Возвращает искажение в координатной системе родительского объекта
	*/
	virtual float getShear() = 0;

	/**
	* Устанавливает масштаб в координатной системе родительского объекта
	*/
	virtual void setScale(float scaleX, float scaleY) = 0;
	/**
	* Получает масштаб в координатной системе родительского объекта
	*/
	virtual void getScale(float& x, float& y) = 0;
	/**
	* Устанавливает матрицу трансформации объекта
	* Должно быть предоставлено 6 значений:
	* a c tx
	* b d ty
	*/
	virtual void setMatrix(const float *matrix) = 0;
	/**
	* Возвращает матрицу трансформации объекта
	* Будет записано 6 значений:
	* a c tx
	* b d ty
	*/
	virtual void getMatrix(float *matrix) = 0;

	/**
	* Устанавливает видимость объекта
	*/
	virtual void setVisible(bool value) = 0;
	/**
	* Возвращает флаг видимости объекта
	*/
	virtual bool getVisible() = 0;

	/**
	* Устанавливает значение alpha для объекта
	*/
	virtual void setAlpha(float value) = 0;
	/**
	* Возвращает значение alpha для клипа
	*/
	virtual float getAlpha() = 0;

	/**
	* Возвращает значение множителя цвета объекта в формате 0xRRGGBB
	*/
	virtual unsigned int getColor() = 0;

	/**
	* Устанавливает значение множителя цвета объекта
	*/
	virtual void setColor(unsigned int color) = 0;

	virtual void getColorVector(float color[4]) = 0;
	virtual void setColorVector(const float color[4]) = 0;

	/**
	* Производит преобразование точки в локальной системе координат
	* в систему координат родителя
	*/
	virtual void localToParent(float& x, float& y) = 0;
	/**
	* Производит преобразование точки в системе координат родителя в
	* локальную систему координат
	*/
	virtual void parentToLocal(float& x, float& y) = 0;

	/**
	* Запускает процедуру проверки попадания на объекте.
	* @param x, y задают точку в системе координат данного объекта
	* @see IHitTestDelegate
	*/
	virtual bool hitTest(float x, float y, IHitTestDelegate* hitTestDelegate) = 0;

	/**
	* Возвращает имя объекта (instance name в adobe flash)
	*/
	virtual const std::string& getName() = 0;

	/**
	* Устанавливает имя объекта
	*/
	virtual void setName(const std::string& name) = 0;

	/**
	* Выполняет преобразование точки в локальной системе координат в глобальную
	*/
	virtual void localToGlobal(float &x, float &y) = 0;
	/**
	* Выполняет преобразование точки в глобальной системе координат в локальную
	*/
	virtual void globalToLocal(float &x, float &y) = 0;

	/**
	* Выполняет преобразование локальных координат в координаты целевого объекта
	*/
	virtual void localToTarget(float &x, float& y, IFlashDisplayObject* target) = 0;

	/**
	* Выполняет корректное приведение типа к MovieClip
	*/
	virtual IFlashMovieClip* MovieClip() = 0;
	/**
	* Выполняет корректное приведение типа к Text
	*/
	virtual IFlashText* Text() = 0;

	/**
	* Выполняет корректное приведение типа к RasterSprite
	*/
	virtual IFlashRasterSprite* RasterSprite() = 0;

	/**
	* Выполняет корректное приведение типа к Sprite
	*/
	virtual IFlashSprite* Sprite() = 0;

	/**
	* Рекурсивное обновление объектов в иерархии
	*/
	virtual void advance(FlashUpdateListener* updateListener, float dt) = 0;

	/**
	* Функция, обновляющая состояние объекта. В потомках ее следует переопределять вместе с hasUpdate
	*/
	virtual void update(float dt){}
	/**
	* Имеется ли функция update. Должна возвращать константу.
	*/
	virtual bool hasUpdate(){ return false; }

	/**
	* Устанавливает скорость обновления DisplayObject в долях от нормальной
	*/
	virtual void setUpdateRate(float rate) = 0;
	virtual float getUpdateRate() = 0;

	/**
	* Получает координаты ограничивающего прямоугольника в указанной системе координат
	* @return true если объект имеет ограничивающий прямоугольник и false - если объект пуст
	*/
	virtual bool getBounds(float& left, float& top, float& right, float& bottom, IFlashDisplayObject* targetCoordinateSystem) = 0;

	/**
	* Применяет трансформацию another объекта так, будто бы он является родителем this
	*/
	virtual void applyTransform(IFlashDisplayObject* another);

	/**
	* Изменяет число потомков в иерархии, желающих получать событие update.
	*/
	virtual void changeUpdateListeners(int value) = 0;

	virtual IFlashDisplayObject* queryChild(const std::string& name);

	/**
	* Рекурсивных обход иерархии в глубину.
	* @param preorder определяет, будет ли текущяя вершина посещена до (true) либо после (false) дочерних
	*/
	virtual void visitDFS(IFlashVisitor* visitor, bool preorder) = 0;
	/**
	* Рекурсивных обход иерархии в ширину.
	* @param preorder определяет, будет ли текущяя вершина посещена до (true) либо после (false) дочерних
	*/
	virtual void visitBFS(IFlashVisitor* visitor, bool preorder) = 0;

	GC_CLASS_INTERFACE(IFlashDisplayObject)
	
private:
	const IFlashDisplayObject& operator=(const IFlashDisplayObject&);
	IFlashDisplayObject(const IFlashDisplayObject&);

	GC_CLASS_DATA(IFlashDisplayObject)
};