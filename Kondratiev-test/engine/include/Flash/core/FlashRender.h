#pragma once

#include "Flash/core/FlashCommon.h"
#include "Flash/core/DisplayObjectRenderTarget.h"

typedef unsigned int FlashEnum;
typedef unsigned int FlashConstantId;
typedef unsigned int FlashShader;
typedef unsigned int FlashTexture;

static const FlashConstantId FlashTextureCh0 = 1;
static const FlashConstantId FlashBlendMode = 2;
static const FlashConstantId FlashShaderCh0 = 3;
static const FlashConstantId FlashMVPMatrix = 4;
static const FlashConstantId FlashClipRect = 5;

static const FlashEnum FlashBlendNormal = 0;
static const FlashEnum FlashBlendAdd = 1;
static const FlashEnum FlashBlendMultiply = 2;

static const int FLASH_MAX_CONSTANTS = 64;

static const int VERTEX_BUFFER_SIZE = 8096;

struct FlashVertexCoords{
	float x, y;
};
typedef unsigned int FlashColor;
struct FlashTexCoords{
	float x, y;
};

struct FlashVertex{
	FlashVertexCoords coords;
	FlashColor color;
	FlashTexCoords texCoords;
};

struct FlashVertexBuffer{
	FlashVertexCoords* coordsBuffer;
	unsigned int coordsOffset;
	
	FlashColor* colorBuffer;
	unsigned int colorOffset;
	
	FlashTexCoords* texCoordsBuffer;
	unsigned int texCoordsOffset;
	
	unsigned int renderedQuads;
};

class FlashRender;

/**
* Буфер вершин устройства, предназначенный для рисования четырехугольников (последовательные 4
* вершины образуют четырехугольник.
*/
class IDeviceQuadBuffer{
public:
	virtual ~IDeviceQuadBuffer(){}
	/**
	* Загружает в буфер данные из data в количестве quads четырехугольников
	*/
	virtual void upload(char* data, int quads) = 0;
	/**
	* Отрисовывает заданное число четырехугольников
	*/
	virtual void draw(int quads) = 0;
};

/**
* Абстракция для устройства рисования
* Важно использовать эту абстракцию, а не конкретный RenderDevice. С ее помощью можно реализовать
* кэширование отрисовки, если в качестве реализации интерфейса записывать команды, а затем воспроизводить их.
*/
class IDeviceRender{
public:
	virtual ~IDeviceRender(){}

	virtual IDeviceQuadBuffer* getQuadBuffer(unsigned int quads) = 0;
	virtual void setShader(FlashRender* render, FlashConstantId id, FlashShader shader) = 0;
	virtual void setConstantEnum(FlashRender* render, FlashConstantId id, FlashEnum x) = 0;
	virtual void setConstantFloat(FlashRender* render, FlashConstantId id, float x) = 0;
	virtual void setConstantVector(FlashRender* render, FlashConstantId id, float* v) = 0;
	virtual void setConstantMatrix3x2(FlashRender* render, FlashConstantId id, float* m) = 0;
	virtual void setConstantMatrix4x4(FlashRender* render, FlashConstantId id, float* m) = 0;
	virtual void setTexture(FlashRender* render, FlashConstantId id, FlashTexture texture) = 0;

	virtual void beginClipPlane(float x1, float y1, float x2, float y2) = 0;
	virtual void endClipPlane() = 0;

	/**
	* Функция должна заполнить структуру FlashVertexBuffer, используя vboBase в качестве начала массива данных.
	* Например, если формат данных для вершины - x,y, color, u1,v1, u2,v2, то FlashVertexBuffer должен
	* быть заполнен таким образом:
	* coordsBuffer = vboBase //координаты первой вершины
	* coordsOffset = sizeof(x,y, color, u1,v1, u2,v2) //расстояние между координатами соседних вершин
	*
	* colorBuffer = vboBase + offsetof(color) //цвет первой вершины
	* colorOffset = sizeof(x,y, color, u1,v1, u2,v2)
	*
	* texCoordsBuffer vboBase + offsetof(u1) //uv первой вершины
	* texCoordsOffset = sizeof(x,y, color, u1,v1, u2,v2)
	*/
	virtual void getVertexBufferDescriptor(FlashRender* render, FlashVertexBuffer* vertexBuffer, void* vboBase) = 0;
};

#define CHECK_CONSTANT_TAG(id, val) FLASH_CHECK(constants[id].tag == val, std::logic_error("read constant with different type"))
#define SET_CONSTANT_TAG(id, val) constants[id].tag = val;
#define FLASH_CHECK_CONSTANT_ID FLASH_CHECK(id < FLASH_MAX_CONSTANTS, std::domain_error("constant id out of range"));

/**
* Содержит стек матриц и цветов.
*/
class FlashRenderSupport{
public:
	FlashRenderSupport(){
		matrix[0] = 1.0f;
		matrix[4] = 1.0f;

		matrix[1] = matrix[2] = matrix[3] = matrix[5] = 0.0f;

		color[0] = color[1] = color[2] = color[3] = 1.0f;
	}

	/**
	* Сохраняет текущую матрицу в массив localStorage (предполагается выделить его на стеке)
	*/
	void saveMatrix(float localStorage[6]) const{
		for ( int i = 0; i < 6; i++ ){
			localStorage[i] = matrix[i];
		}
	}

	/**
	* Загружает матрицу из массива m
	*/
	void restoreMatrix(const float m[6]){
		for ( int i = 0; i < 6; i++ ){
			matrix[i] = m[i];
		}
	}

	/**
	* Умножает текущую матрицу на m
	*/
	void multMatrix(const float m[6]){
		float tmp[6];
		for ( int i = 0; i < 6; i++ ){
			tmp[i] = matrix[i];
		}
		::multMatrix3x3(matrix, tmp, m);
	}

	/**
	* Преобразует точку (x,y) при помощи текущей матрицы
	*/
	void transform(float& x, float& y){
		float tx = x;
		float ty = y;
		x = matrix[0] * tx + matrix[1] * ty + matrix[2];
		y = matrix[3] * tx + matrix[4] * ty + matrix[5];
	}

	/**
	* Сохраняет текущую матрицу в localStorage, а затем умножает ее на m
	*/
	void saveMultMatrix(const float m[6], float localStorage[6]){
		saveMatrix(localStorage);
		multMatrix(m);
	}

	/**
	* Сохраняет текущий цвет в localStorage. Предполагается, что этот массив будет выделен на стеке.
	*/
	void saveColor(float localStorage[4]) const{
		for ( int i = 0; i < 4; i++ ){
			localStorage[i] = color[i];
		}
	}

	/**
	* Загружает текущий цвет из v
	*/
	void restoreColor(const float v[4]){
		for ( int i = 0; i < 4; i++ ){
			color[i] = v[i];
		}
	}

	/**
	* Умножает текущий цвет на v
	*/
	void multColor(const float v[4]){
		for ( int i = 0; i < 4; i++ ){
			color[i] *= v[i];
		}
	}

	/**
	* Сохраняет текущий цвет в localStorage, а затем умножает его на v
	*/
	void saveMultColor(const float v[4], float localStorage[4]){
		saveColor(localStorage);
		multColor(v);
	}

	bool isTransformDirect(){
		return matrix[0] * matrix[4] - matrix[1] * matrix[3] > 0;
	}

	float matrix[6];
	float color[4];
};

/**
* Используется рендером для вывода текстов
*/
class IFlashTextRenderingEngine{
public:
	virtual ~IFlashTextRenderingEngine(){}
	virtual void renderText(FlashRender* render, TextObjectOutputParams& params) = 0;
};

/**
* Используется для рисования FlashDisplayObject'ов
*
* Вместо конкретных функций вроде setBlendMode... использует именованные константы.
* Например, blendMode задается при помощи setConstantEnum(FlashBlendMode, ...)
* Сам по себе FlashRender ничего не знает о этих константах и лишь передает в IDeviceRender,
* который уже должен правильно интерпретировать их.
* 
* Такое решение принято в связи с тем, что при использовании шейдеров невозможно угадать, какие
* именно параметры нужно будет передавать.
*/
class FlashRender: public FlashRenderSupport{
public:
	struct FlashConstant{
		union {
			FlashEnum f_enum;
			FlashTexture f_texture;
			FlashShader f_shader;
			float f_float;
			float f_vector[4];
			float f_matrix3x2[6];
			float f_matrix4x4[16];
		};
		enum Tag{
			TagNotSet,
			TagEnum,
			TagTexture,
			TagFloat,
			TagShader,
			TagVector,
			TagMatrix3x2,
			TagMatrix4x4
		};
		Tag tag;
		bool valid;
	};

	FlashRender(){
		device = 0;
		textRenderingEngine = 0;

		::memset(constants, 0, sizeof(constants));
		vertexBufferData = new char[VERTEX_BUFFER_SIZE * sizeof(FlashVertex) * 4];
		bufferSize = VERTEX_BUFFER_SIZE * 4;
		::memset(vertexBufferData, 0, VERTEX_BUFFER_SIZE * sizeof(FlashVertex) * 4);

		constantsValid = false;
	}

	~FlashRender(){
		vertexBufferData = 0;
	}

	/**
	* Устанавливает "физическое" устройство вывода. Подробнее
	* @see IDeviceRender
	*/
	void setDeviceRender(IDeviceRender* device){
		this->device = device;
		device->getVertexBufferDescriptor(this, &targetBuffer, vertexBufferData);
		targetBuffer.renderedQuads = 0;
	}

	/**
	* Устанавливает устройство для вывода текстов
	*/
	void setTextRenderingEngine(IFlashTextRenderingEngine* textRenderingEngine){
		this->textRenderingEngine = textRenderingEngine;
	}

	bool needSaveConstant(FlashConstantId id){
		FLASH_CHECK_CONSTANT_ID;
		return constants[id].tag != FlashConstant::TagNotSet;
	}

	FlashConstant saveConstant(FlashConstantId id){
		FLASH_CHECK_CONSTANT_ID;
		return constants[id];
	}

	void restoreConstant(FlashConstantId id, FlashConstant constant){
		validateConstants();
		FLASH_CHECK_CONSTANT_ID;
		constants[id] = constant;
		constants[id].valid = false;
	}

	void unsetConstant(FlashConstantId id){
		FLASH_CHECK_CONSTANT_ID;
		constants[id].tag = FlashConstant::TagNotSet;
	}

	void invalidateConstant(FlashConstantId id){
		FLASH_CHECK_CONSTANT_ID;
		constants[id].valid = false;
		constantsValid = false;
	}

	FlashShader setConstantShader(FlashConstantId id, FlashShader x){
		validateConstants();
		FLASH_CHECK_CONSTANT_ID;
		if ( constants[id].f_shader != x || constants[id].tag != FlashConstant::TagShader ){
			SET_CONSTANT_TAG(id, FlashConstant::TagShader);

			FlashShader oldValue = constants[id].f_shader;
			constants[id].f_shader = x;
			constants[id].valid = true;
			device->setShader(this, id, x);
			
			return oldValue;
		}
		return x;
	}

	FlashShader getConstantShader(FlashConstantId id){
		FLASH_CHECK_CONSTANT_ID;
		CHECK_CONSTANT_TAG(id, FlashConstant::TagShader);

		return constants[id].f_shader;
	}	
	
	FlashEnum setConstantEnum(FlashConstantId id, FlashEnum x){
		validateConstants();
		FLASH_CHECK_CONSTANT_ID;
		if ( constants[id].f_enum != x || constants[id].tag != FlashConstant::TagEnum ){
			SET_CONSTANT_TAG(id, FlashConstant::TagEnum);

			FlashEnum oldValue = constants[id].f_enum;
			constants[id].f_enum = x;
			constants[id].valid = true;
			device->setConstantEnum(this, id, x);
			
			return oldValue;
		}
		return x;
	}

	FlashEnum getConstantEnum(FlashConstantId id){
		FLASH_CHECK_CONSTANT_ID;
		CHECK_CONSTANT_TAG(id, FlashConstant::TagEnum);

		return constants[id].f_enum;
	}	
	
	float setConstantFloat(FlashConstantId id, float x){
		validateConstants();
		FLASH_CHECK_CONSTANT_ID;
		if ( constants[id].f_float != x || constants[id].tag != FlashConstant::TagFloat ){
			SET_CONSTANT_TAG(id, FlashConstant::TagFloat);

			float oldValue = constants[id].f_float;
			constants[id].f_float = x;
			constants[id].valid = true;
			device->setConstantFloat(this, id, x);
			
			return oldValue;
		}
		return x;
	}

	float getConstantFloat(FlashConstantId id){
		FLASH_CHECK_CONSTANT_ID;
		CHECK_CONSTANT_TAG(id, FlashConstant::TagFloat);

		return constants[id].f_float;
	}
	
	void setConstantVector(FlashConstantId id, float* v){
		validateConstants();
		FLASH_CHECK_CONSTANT_ID;

		bool anyChanged = false;
		for ( int i = 0; i < 4; i++ ){
			if ( v[i] != constants[id].f_vector[i] ){
				anyChanged = true;
				break;
			}
		}

		if ( anyChanged || constants[id].tag != FlashConstant::TagVector ){
			SET_CONSTANT_TAG(id, FlashConstant::TagVector);

			for ( int i = 0; i < 4; i++ ){
				std::swap(v[i], constants[id].f_vector[i]);
			}
		}
	}

	void getConstantVector(FlashConstantId id, float* v){
		FLASH_CHECK_CONSTANT_ID;
		CHECK_CONSTANT_TAG(id, FlashConstant::TagVector);

		for ( int i = 0; i < 4; i++ ){
			v[i] = constants[id].f_vector[i];
		}
	}
	
	void setConstantMatrix3x2(FlashConstantId id, float* v){
		validateConstants();
		FLASH_CHECK_CONSTANT_ID;

		bool anyChanged = false;
		for ( int i = 0; i < 6; i++ ){
			if ( v[i] != constants[id].f_matrix3x2[i] ){
				anyChanged = true;
				break;
			}
		}

		if ( anyChanged || constants[id].tag != FlashConstant::TagMatrix3x2 ){
			SET_CONSTANT_TAG(id, FlashConstant::TagMatrix3x2);
			constants[id].valid = true;
			device->setConstantMatrix3x2(this, id, v);			
			for ( int i = 0; i < 6; i++ ){
				std::swap(v[i], constants[id].f_matrix3x2[i]);
			}
		}
	}

	void getConstantMatrix3x2(FlashConstantId id, float* v){
		FLASH_CHECK_CONSTANT_ID;
		CHECK_CONSTANT_TAG(id, FlashConstant::TagMatrix3x2);

		for ( int i = 0; i < 6; i++ ){
			v[i] = constants[id].f_matrix3x2[i];
		}
	}
		
	void setConstantMatrix4x4(FlashConstantId id, float* v){
		validateConstants();
		FLASH_CHECK_CONSTANT_ID;
		bool anyChanged = false;
		for ( int i = 0; i < 16; i++ ){
			if ( v[i] != constants[id].f_matrix4x4[i] ){
				anyChanged = true;
				break;
			}
		}

		if ( anyChanged || constants[id].tag != FlashConstant::TagMatrix4x4 ){
			SET_CONSTANT_TAG(id, FlashConstant::TagMatrix4x4);
			constants[id].valid = true;
			device->setConstantMatrix4x4(this, id, v);
			for ( int i = 0; i < 16; i++ ){
				std::swap(v[i], constants[id].f_matrix4x4[i]);
			}
		}
	}

	void getConstantMatrix4x4(FlashConstantId id, float* v){
		FLASH_CHECK_CONSTANT_ID;
		CHECK_CONSTANT_TAG(id, FlashConstant::TagMatrix4x4);

		for ( int i = 0; i < 16; i++ ){
			v[i] = constants[id].f_matrix4x4[i];
		}
	}
	
	FlashTexture setTexture(FlashConstantId id, FlashTexture texture){
		validateConstants();
		FLASH_CHECK_CONSTANT_ID;
		if ( constants[id].f_texture != texture || constants[id].tag != FlashConstant::TagTexture ){
			SET_CONSTANT_TAG(id, FlashConstant::TagTexture);

			FlashTexture oldValue = constants[id].f_texture;
			constants[id].valid = true;
			constants[id].f_texture = texture;
			device->setTexture(this, id, texture);
			return oldValue;
		}
		return texture;
	}

	FlashTexture getTexture(FlashConstantId id){
		FLASH_CHECK_CONSTANT_ID;
		CHECK_CONSTANT_TAG(id, FlashConstant::TagTexture);

		return constants[id].f_texture;
	}
	
	/**
	* Запрашивает буфер под vertexCount вершин
	*/
	FlashVertexBuffer* getVertexBufferPointer(unsigned int vertexCount){
		validateConstants();
		if ( bufferSize - targetBuffer.renderedQuads * 4 < vertexCount ){
			flush();
		}
		return &targetBuffer;
	}

	/**
	* Функцию следует вызывать, если предполагается низкоуровневая работа с устройством в обход FlashRender
	*/
	void invalidateConstants(){
		flush();
		constantsValid = false;
		for ( int i = 0; i < FLASH_MAX_CONSTANTS; i++ ){
			constants[i].valid = false;
		}
	}

	/**
	* Очищает внутренний буфер и выводит все накопленные четырехугольники на устройство.
	*/
	void flush(){
		if ( targetBuffer.renderedQuads > 0 ){
			int renderedQuads = targetBuffer.renderedQuads;
			targetBuffer.renderedQuads = 0;
			validateConstants();

			IDeviceQuadBuffer* quadBuffer = device->getQuadBuffer(renderedQuads);
			quadBuffer->upload(vertexBufferData, renderedQuads);
			quadBuffer->draw(renderedQuads);
			device->getVertexBufferDescriptor(this, &targetBuffer, vertexBufferData);
			
		}
	}

	void renderText(TextObjectOutputParams& params){
		if ( textRenderingEngine ){
			textRenderingEngine->renderText(this, params);
		}
	}

	void beginClipPlane(float x1, float y1, float x2, float y2){
		flush();

		transform(x1, y1);
		transform(x2, y2);
		
		if ( isTransformDirect() ){
			device->beginClipPlane(x1, y1, x2, y2);
		}else{
			device->beginClipPlane(x2, y2, x1, y1);
		}
	}

	void endClipPlane(){
		flush();
		device->endClipPlane();
	}

private:
	void validateConstant(FlashConstantId id){
		switch ( constants[id].tag ){
		case FlashConstant::TagEnum: device->setConstantEnum(this, id, constants[id].f_enum); break;
		case FlashConstant::TagFloat: device->setConstantFloat(this, id, constants[id].f_float); break;
		case FlashConstant::TagShader: device->setShader(this, id, constants[id].f_shader); break;
		case FlashConstant::TagTexture: device->setTexture(this, id, constants[id].f_texture); break;
		case FlashConstant::TagVector: device->setConstantVector(this, id, constants[id].f_vector); break;
		case FlashConstant::TagMatrix3x2: device->setConstantMatrix3x2(this, id, constants[id].f_matrix3x2); break;
		case FlashConstant::TagMatrix4x4: device->setConstantMatrix4x4(this, id, constants[id].f_matrix4x4); break;
		}
		constants[id].valid = true;
	}

	void validateConstants(){
		if ( !constantsValid ){
			for ( int i = 0; i < FLASH_MAX_CONSTANTS; i++ ){
				if ( !constants[i].valid ){
					validateConstant(i);
				}
			}
			constantsValid = true;
		}
	}

	bool constantsValid;

	FlashConstant constants[FLASH_MAX_CONSTANTS];

	IDeviceRender* device;
	IFlashTextRenderingEngine* textRenderingEngine;

	char* vertexBufferData;
	int bufferSize;
	FlashVertexBuffer targetBuffer;
};