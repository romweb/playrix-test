#pragma once

#include "Flash/core/IFlashLibraryItem.h"
#include "Flash/core/FlashCommon.h"
#include "Flash/core/FlashLibraryItem.h"
#include "Flash/core/FlashRasterSprite.h"
#include "Flash/core/FlashTextureInfo.h"
#include "Flash/core/FlashText.h"
#include "../rapidxml/rapidxml.hpp"

/**
* Шаблон для создания FlashText
* @see FlashText
*/
class FlashLibText: public FlashLibraryItem, public IFlashLibText{
public:
	FlashLibText(FlashLibrary* library, FlashTextStyle* textStyle, const char* defaultText);
	FlashLibText(FlashLibrary* library, FlashTextStyle* textStyle, float width, float height, std::string defaultText);

	IFlashLibText* Text();

	FlashText* createInstance();
	IFlashText* newInstance();

	static FlashLibText* fromBinary(FlashLibrary* library, Data data);
	static FlashLibText* fromXML(FlashLibrary* library, rapidxml::xml_node<>* xml);

	void saveBinary(const std::string& id, std::vector<unsigned char>& buffer);
	void saveXML(const std::string& id, rapidxml::xml_node<>* rootNode);
private:
	FlashTextStylePtr textStyle;
	const char* defaultText;
	std::string defaultTextString;
	std::vector<std::string> effects;
	float width, height;

	friend class FlashText;
};

typedef boost::intrusive_ptr<FlashLibText> FlashLibTextPtr;
