#pragma once

#include "Flash/core/FlashLibrary.h"
#include "Flash/core/DynamicLibraryChunkReader.h"

/**
* Динамическая библиотека символов - билиотека, которая позволяет догружать и выгружать символы 
* при необходимости
*/
class FlashDynamicLibrary:public FlashLibrary{
public:
	FlashDynamicLibrary();
	~FlashDynamicLibrary();
	
	FlashLibraryItem* getLibraryItem(const std::string& id);
	IFlashSprite* createEmptySprite();

	int getUsageCount();

	void unloadUnused();

	void retain(FlashLibraryItem*);
	void release(FlashLibraryItem*);
	void getNames(std::vector<std::string>& target, bool privates);
private:
	int usageCount;

	DynamicLibraryChunkReaderPtr chunkReader;

	bool hasUnusedChunks;

	struct DynamicItemInfo : public RefCounter {
		std::string itemId;
		FlashLibraryItemPtr libraryItem;
		FlashTextureInfo textureInfo;
		Data data;
		int refs;
		int offset, length;
	};
	typedef boost::intrusive_ptr<DynamicItemInfo> DynamicItemInfoPtr;
	std::map<std::string, DynamicItemInfoPtr> libraryEntries;
	std::map<FlashLibraryItem*, std::string> itemDictionary;

	void putLibraryItem(const std::string& id, DynamicItemInfo* item);
	FlashLibraryItem* loadLibraryItem(DynamicItemInfo* item);

	friend FlashLibraryItem* parseLibraryItem(FlashLibrary* library, const std::string& id, FlashTextureInfo textureInfo, Data data);
	friend IFlashLibrary* parseLibrary(DynamicLibraryChunkReader* chunkReader, FlashTextureInfo textureInfo);
};

typedef boost::intrusive_ptr<FlashDynamicLibrary> FlashDynamicLibraryPtr;
