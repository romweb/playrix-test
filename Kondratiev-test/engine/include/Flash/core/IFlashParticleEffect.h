#pragma once

#include "Flash/core/FlashCommon.h"
#include "Flash/core/IFlashDisplayObject.h"

class IFlashParticleEffect: public IFlashConcreteDisplayObject{
public:
	virtual ~IFlashParticleEffect(){}

	virtual bool isEnd() = 0; // Закончился ли эффект
	virtual void reset() = 0; // Перезапустить эффект
	virtual void finish() = 0; // Закончить эффект
	virtual void pause() = 0; // Поставить эффект на паузу
	virtual void resume() = 0; // Продолжить выполнение эффекта
};