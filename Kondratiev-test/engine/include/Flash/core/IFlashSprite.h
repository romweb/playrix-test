#pragma once

#include "Flash/core/FlashCommon.h"
#include "Flash/core/IFlashDisplayObject.h"

class IFlashSprite : public IFlashConcreteDisplayObject{
public:
	virtual IFlashDisplayObject* getChildByName(const std::string& name){
		int n = getChildrenCount();
		for ( int i = 0; i < n; i++ ){
			if ( getChildAt(i)->getName() == name ){
				return getChildAt(i);
			}
		}
		return 0;
	}

	virtual IFlashDisplayObject* addChild(IFlashDisplayObject* displayObject){
		return addChildAt(displayObject, getChildrenCount());
	}

	virtual IFlashDisplayObject* removeChild(IFlashDisplayObject* displayObject){
		return removeChildAt(getChildIndex(displayObject));
	}

	virtual IFlashDisplayObject* addChildAt(IFlashDisplayObject* displayObject, int index) = 0;
	virtual IFlashDisplayObject* removeChildAt(int index) = 0;
	virtual IFlashDisplayObject* getChildAt(int index) = 0;
	virtual void swapChildren(int index1, int index2) = 0;
	virtual int getChildrenCount() = 0;

	virtual int getChildIndex(IFlashDisplayObject* displayObject){
		int n = getChildrenCount();
		for ( int i = 0; i < n; i++ ){
			if ( getChildAt(i) == displayObject ){
				return i;
			}
		}
		throw child_index_out_of_bounds("display object is not a child");
	}
};

IFlashSprite* createFlashSprite();
