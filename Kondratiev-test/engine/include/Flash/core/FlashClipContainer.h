#pragma once

#include "Flash/core/FlashSprite.h"

/**
* Контейнер, имеющий определенные размеры, для которого можно задать плоскости отсечения
*/
class FlashClipContainer: public FlashSprite{
public:	
	static const int ClipNone = 0;
	static const int ClipTop = 1;
	static const int ClipLeft = 2;
	static const int ClipBottom = 4;
	static const int ClipRight = 8;
	static const int ClipHorizontal = ClipTop | ClipBottom;
	static const int ClipVertical = ClipLeft | ClipRight;
	static const int ClipAll = ClipHorizontal | ClipVertical;

	FlashClipContainer(float x, float y, float width, float height, int clipMask = ClipNone);

	virtual void render(FlashRender& render);
	virtual bool hitTest(float x, float y, IHitTestDelegate* hitTestDelegate);
	virtual bool getBounds(float& left, float& top, float& right, float& bottom, IFlashDisplayObject* targetCoordinateSystem);

	void setClipRect(float x, float y, float width, float height);
	void getClipRect(float &x, float &y, float &width, float &height);

	void setClipping(int mask);
	int getClipping();

	
private:
	float x, y, width, height;
	int clipMask;
};