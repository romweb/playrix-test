#pragma once

struct TextObjectOutputParams;
struct FlashTextStyle;


/**
* Структура, определяющая цель вывода для объекта списка отображения.
* @deprecated УДАЛИТЬ
*/
struct DisplayObjectOutputTarget{
	/**
	* First quad:
	* (coordArray,coordArray+1) <- (x1, y1)
	* (coordArray + coordsSpacing,coordArray + coordsSpacing + 1) <- (x2, y1)
	* (coordArray + 2coordsSpacing,coordArray + 2coordsSpacing + 1) <- (x2, y2)
	* (coordArray + 3coordsSpacing,coordArray + 3coordsSpacing + 1) <- (x1, y2)
	* Next quad:
	* (coordArray + coordsOffset,coordArray + coordsOffset +1) <- (x1, y1)
	*/
	
	float* coordArray;
	int coordsSpacing, coordsOffset;

	unsigned int* colorArray;
	int colorSpacing, colorOffset;

	float* uvArray;
	int uvSpacing, uvOffset;

	unsigned char colorTransform[4];

	//Число выведенных квадов
	int renderedQuads;

	//Если в процессе рендера поменялась текстура, вызовем эту процедуру
	//Она может внести изменения в OutputTarget
	FlashTextureId currentTexture;
	void(*onTextureChanged)(DisplayObjectOutputTarget*, FlashTextureId textureId);

	//Если хочется вывести текст - надо воспользоваться этой функцией
	void(*onRenderText)(DisplayObjectOutputTarget*, TextObjectOutputParams* );
};


struct TextObjectOutputParams{
	float matrix[9];
	float width, height;
	bool snapToPixels;
	const char* text;
	int textLength;
	FlashTextStyle* textStyle;
	unsigned char color[4];
};

#include "FlashTextStyle.h"