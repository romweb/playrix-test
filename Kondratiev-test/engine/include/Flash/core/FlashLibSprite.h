#pragma once

#include "Flash/core/FlashLibraryItem.h"
#include "Flash/core/FlashSprite.h"
#include "Flash/core/FlashMovieClip2.h"
#include "../rapidxml/rapidxml.hpp"

class FlashLibMovieClip2;
class FlashTimeline2;

typedef boost::intrusive_ptr<FlashLibMovieClip2> FlashLibMovieClip2Ptr;
typedef boost::intrusive_ptr<FlashTimeline2> FlashTimeline2Ptr;

/**
* Шаблон для создания FlashSprite
* @see FlashSprite
*/
class FlashLibSprite: public FlashLibraryItem, public IFlashLibSprite{
public:
	FlashLibSprite(FlashLibrary* library);
	IFlashLibSprite* Sprite();
	IFlashSprite* newInstance();
	FlashSprite* createInstance();

	static FlashLibSprite* fromXML(rapidxml::xml_node<>* xml, FlashLibrary* library);

	void saveBinary(const std::string& id, std::vector<unsigned char>& buffer);
	void saveXML(const std::string& id, rapidxml::xml_node<>* rootNode);
private:
	struct ChildInfo{
		std::string id;
		std::string name;
		float matrix[6];
		unsigned char color[3];
		bool visible;
		float alpha;
	};
	std::vector<ChildInfo> childrenInfo;
	FlashLibrary* library;
};

typedef boost::intrusive_ptr<FlashLibSprite> FlashLibSpritePtr;

class FlashLibSprite2: public FlashLibraryItem, public IFlashLibSprite{
public:
	FlashLibSprite2(FlashLibrary* library, FlashLibMovieClip2* movieClipCounterpart, FlashTimeline2* timeline);
	IFlashLibMovieClip* MovieClip();
	IFlashLibSprite* Sprite();
	IFlashSprite* newInstance();
	FlashMovieClip2* createInstance();

	void saveBinary(const std::string& id, std::vector<unsigned char>& buffer);
	void saveXML(const std::string& id, rapidxml::xml_node<>* rootNode);
private:
	FlashTimeline2Ptr timeline;
	FlashLibMovieClip2Ptr movieClipCounterpart;
};

typedef boost::intrusive_ptr<FlashLibSprite2> FlashLibSprite2Ptr;
