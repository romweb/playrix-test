#pragma once

class IFlashDisplayObject;

/**
* Что-то, что может быть представленно как DisplayObject.
* Интерфейс нужен, чтобы избежать виртуального наследования в иерархии:
* class IFlashDisplayObject;
* class IFlashMovieClip: public IFlashConcreteDisplayObject;
* class FlashDisplayObject: public IFlashDisplayObject;
* class FlashMovieClip: public FlashDisplayObject, public IFlashMovieClip;
*/
class IFlashConcreteDisplayObject{
public:
	virtual ~IFlashConcreteDisplayObject() {}
	virtual IFlashDisplayObject* DisplayObject() = 0;
};