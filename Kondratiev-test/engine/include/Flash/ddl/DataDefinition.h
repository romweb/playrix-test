#pragma once

#include "../rapidxml/rapidxml.hpp"

using namespace rapidxml;
 
void _DataParseError(const std::string& reason);
std::string _DataParseString(rapidxml::xml_attribute<>* xml);
std::string _DataParseLongString(rapidxml::xml_node<>* xml);
int _DataParseInt(rapidxml::xml_attribute<>* xml);
float _DataParseFloat(rapidxml::xml_attribute<>* xml);
bool _DataParseBoolean(rapidxml::xml_attribute<>* xml);
