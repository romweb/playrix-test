#pragma once

#include "../rapidxml/rapidxml.hpp"

namespace DataScheme{

	template<class T>
	T* LoadWithScheme(const std::string& filename){
		std::vector<uint8_t> contents;
		File::LoadFile(filename, contents);
		contents.push_back(0);

		rapidxml::xml_document<> doc;   
		doc.parse<0>((char*)&contents.front());    

		T* t = T::parse(doc.first_node());
		return t;
	}

	template<class T>
	T* MatchScheme(rapidxml::xml_node<>* node){
		return T::parse(node);
	}

}