#ifndef __PARTICLESYSTEM_H__
#define __PARTICLESYSTEM_H__

#pragma once

#include "Particle/AbstractParticleSystem.h"
#include "Particle/ParticleSystemVer2.h"
#include "RefCounter.h"

#include <boost/intrusive_ptr.hpp>

class ParticleEffect : public RefCounter
{
public:
	float posX, posY;
	float localTime;
	bool needTimeCount;
	bool ended;
	std::string name, group;

	bool _paused;

	typedef std::vector<ParticleSystemPtr> ParticleSystems;

	ParticleSystems _systems;

	bool _isExpanded; // используется только в редакторе эффектов

public:
	ParticleEffect();
	ParticleEffect(const std::string& gr);
	ParticleEffect(const ParticleEffect&);
	ParticleEffect(ParticleEffect *);
	ParticleEffect& operator=(const ParticleEffect&);
	
	ParticleEffect* Clone() { return new ParticleEffect(*this); }
	
	void SetScale(float scale);

	bool isEnd() const { return ended; }
	void Kill(void) { ended = true; }
	void Finish();
	void Reset();
	
	bool Load(BinaryDataLoader *d);
	bool Load(rapidxml::xml_node<> *elem);
	bool Load(rapidxml::xml_node<> *elem, int version);	// загрузка эффекта нужной версии
														// добавлено из-за загрузки эффектов НЕ через EffectPresets
	void Save(rapidxml::xml_node<> *elem);
	void Save(BinaryDataSaver *d);
	void Update(float dt);
	void Draw(Render::SpriteBatch* batch = NULL);
	void DrawBlend(Render::SpriteBatch* batch = NULL);
	void SetCurrentGroup(const std::string &group);

	void Upload();
	void Release();

	// Устанавливает системам частиц флаг, с которым отключается туман на время
	// рисования частитц в режиме ADD. Флаг пока есть только для ParticleSystemVer2
	void DisableFog(bool disable);

	void Pause();
	void Continue();
	bool IsPermanent();
	void SetAlphaFactor(float alpha);
	void SetRGBFactor(float r, float g, float b);
	void SetPos(const FPoint& pos);
	void SetPos(float x, float y) { SetPos(FPoint(x, y)); }
};

typedef boost::intrusive_ptr<ParticleEffect> ParticleEffectPtr;

class EffectPresets
{
public:
	void UploadEffect(const std::string& name);
	void ReleaseEffect(const std::string& name);

	bool LoadEffects(const std::string& filename, const std::string& group = "Common");

	bool SaveToXml(const std::string& filename);
	bool SaveToBin(const std::string& filename);
	
	/// Выгрузка эффектов заданной группы
	void UnloadEffectsGroup(const std::string& group);

	ParticleEffect* getParticleEffect(const std::string& name) const;
	ParticleEffect* CreateParticleEffect(const std::string& name) const;
	std::vector<ParticleEffectPtr>& getEffects();

	int LastVersion() const;
	ParticleEffect* Find(const std::string &name) const;
	void ReloadBinaryEffects(const std::string& filename, const std::string& group = "Common");

private:
	typedef std::vector<ParticleEffectPtr> Effects;

	Effects _effects;

	bool LoadFromXml(const std::string& filename, const std::string& group);
	bool LoadFromBin(const std::string& filename, const std::string& group);
};

extern EffectPresets effectPresets;

#endif // __PARTICLESYSTEM_H__
