//
//  MPGCenter.h
//
//  Created by Влад on 9/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


#import "MPGCenterAncestor.h"


@interface MPGCenter : MPGCenterAncestor 
{
}

- (BOOL) IsFullVersion;
//Returns YES if your game is a full version
	
- (BOOL) IsLiteVersion;
//Returns YES if your game is a lite (i.e. free) version
	
- (void) SetFullVersion;
//Switch your game to full version
	
- (void) SetLiteVersion;
//Returns YES if your game is a lite (i.e. free) version


- (void) OnISplashLetterSent;

- (void) OnISplashLetterCanceled;

- (void) OnSubscribeLetterSent;

- (void) OnShellSubscribeLetterSent;

- (void) OnExtensionLetterSent;




@end

extern MPGCenter* mpgCenter;