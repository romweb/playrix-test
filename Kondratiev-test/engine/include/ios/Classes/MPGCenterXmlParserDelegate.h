//
//  MPGCenterXmlParserDelegate.h
//
//  Created by Влад on 9/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


@interface MPGCenterXmlParserDelegate : NSObject<NSXMLParserDelegate>
	
- (id)init;
- (void)dealloc;

//for NSXMLParserDelegate protocol:
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict;
- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError;

//Properties to read data
@property (nonatomic, retain) NSString* XmlFullAttrib;
@property (nonatomic, retain) NSString* XmlInAppAttrib;
@property (nonatomic, retain) NSString* XmlUpdateDateAttrib;
@property (nonatomic, retain) NSString* XmlNewAttrib;
@property (nonatomic, retain) NSString* XmlUrlAttrib;
@property (nonatomic, retain) NSString* XmlFirstAttrib;
@property (nonatomic, retain) NSString* XmlMessageAttrib;
@property (nonatomic, retain) NSString* XmlDiscountAttrib;

	
@end
