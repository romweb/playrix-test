//
//  MicroTransactionsNotifications.h
//  royal_envoy_ipad
//
//  Created by dimaya on 02.03.12.
//  Copyright (c) 2012 Playrix Ent. All rights reserved.
//

#ifndef royal_envoy_ipad_MicroTransactionsNotifications_h
#define royal_envoy_ipad_MicroTransactionsNotifications_h

#define kTransactionObserverFullVersionUnlockedNotification "kTransactionObserverFullVersionUnlockedNotification"
#define kTransactionObserverBonusContentUnlockedNotification "kTransactionObserverBonusContentUnlockedNotification"
#define kTransactionObserverFullPlusBonusUnlockedNotification "kTransactionObserverFullPlusBonusUnlockedNotification"
#define kTransactionObserverRestorationProcessCompleteNotification "kTransactionObserverRestorationProcessCompleteNotification"
#define kTransactionObserverRestorationProcessFailedNotification "kTransactionObserverRestorationProcessFailedNotification"

#endif
