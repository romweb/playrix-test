//
//  MPGCenter.h
//
//  Created by Влад on 9/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

/* This is a basic class to process MPG. 
 It works like a "Black Box" - give it MPG file and ask for actions to do
 
 DO NOT CHANGE THIS CODE!
 If you need to setup a different logic - just derive your own class
 
 Vlad Khorev
 */

#import "WebViewDelegate.h"

@interface MPGCenterAncestor : NSObject

- (id)init;

- (void)dealloc;

- (void) ParseMoreGamesFile:(NSData*)data;
//Call this method if you want MPG Center to parse XML file

- (void) ResetFirstLaunch;
//Call this methods when MPG Parsing is finished. Comes with method below

- (BOOL) IsItFirstLaunch;
//Return true if MPG Parsing is finished. Comes with method above

- (BOOL) MustShowShell;
//Returns YES if your game must show shell

- (void) SetMoreGamesAsVisited;
//Call method when player successfully visited MPG page

- (BOOL) MustShowISplashBanner;
//Returns YES when your game must show ISplash Banner

- (void) SetISplashAsShown;
//Call this method after you shown ISplash banner

- (void) SetISplashAsSubscribed;
//Call this method when player successfully subscribes to ISplash. INews will dissapear too

- (void) SetSubscribePromptAsRejected;
//Call this method each time player press "Cancel" on subscribe letter

- (void) SetINewsAsSubscribed;
//Call this method when player successfully subscribes to INews. If ISplash is avaiable, it will dissapear too

- (BOOL) ISplashMustAskToSubscribe;
//Returns YES when your game must show ISplash Subscribe prompt at start

- (BOOL) INewsMustAskToSubscribe;
//Returns YES when your game must show INews banner at main menu

- (NSString*) MessageToShow;
//Returns message to show for More Games Informer or nil if Informer must be hidden

- (NSString*) MoreGamesUrl;
//Url to go on "MPG" button click. Returns nil if mpg not loaded

- (BOOL) MustShowNewMark;
//Returns YES if you must show "NEW" mark on MPG Button

- (void) ShowLetterWithParent:(UIViewController *) parentViewController;
//Show letter prepared to send on parent controller. You must specify address, body, subject and selectors before you call this method


- (int) GetDiscountValue;
//Returns current discount value (30, 50, 75) or 0 if discount value is invalid

- (BOOL) TrialCanBeExtended;
- (void) ExtendTrial;
- (BOOL) TrialWasExtended;
- (void) AllowTrialExtensionToAppear;



@property (assign) BOOL MustShowDiscount;
//Returns YES when you need to show SALE mark. If you don't need this mark, swith value to NO direcly

@property (nonatomic, retain) NSString* LetterAddress;
@property (nonatomic, retain) NSString* LetterSubject;
@property (nonatomic, retain) NSString* LetterBody;
@property (nonatomic, assign) SEL SelectorOnLetterSent;
@property (nonatomic, assign) SEL SelectorOnLetterCanceled;


//=========================================================================
//========= Those functions you must implement in inheritor by yourself:
//=========================================================================

- (BOOL) IsFullVersion;
//Returns YES if your game is a full version

- (BOOL) IsLiteVersion;
//Returns YES if your game is a lite (i.e. free) version

- (void) SetFullVersion;
//Switch your game to full version

- (void) SetLiteVersion;
//Returns YES if your game is a lite (i.e. free) version

- (void) OnLetterCanNotBeSent;
//What to do if MFMailComposeViewController fails to send mail


//===========================================================================
//========= Protected methods and fields - only inheritor can access them 
//===========================================================================

//protected

- (BOOL) PlayerNeverCheckedMpgBefore;
- (BOOL) PlayerVisitedLastMpg;
- (BOOL) PlayerVisitedMpgAtLeastOnce;
- (BOOL) MpgIsLoadedAtLeastOnce;

- (BOOL) CheckFullToInapp;
- (BOOL) CheckFullToDiscount;
- (BOOL) CheckInappToFull;
- (BOOL) CheckInappToDiscount;
- (BOOL) CheckToISplash; //Both for inapp and full version

- (void) SwitchFullToISplashVersion; //Lite version must already be switched to full

- (void) SetPlayerCheckedMpgWithTimeStamp:(NSString*)timeStamp;

- (void) ResetFirstLaunch;

- (void) StartMpgLine;
//To start all checks

- (void) StartMpgCheckLine;





@property (nonatomic, retain) NSString* XmlFullAttrib;
@property (nonatomic, retain) NSString* XmlInAppAttrib;
@property (nonatomic, retain) NSString* XmlUpdateDateAttrib;
@property (nonatomic, retain) NSString* XmlNewAttrib;
@property (nonatomic, retain) NSString* XmlUrlAttrib;
@property (nonatomic, retain) NSString* XmlFirstAttrib;
@property (nonatomic, retain) NSString* XmlMessageAttrib;
@property (nonatomic, retain) NSString* XmlDiscountAttrib;


@end
