#ifndef _CLASSES_RATINGNOTIFICATION_H_
#define _CLASSES_RATINGNOTIFICATION_H_

#import <UIKit/UIKit.h>

@interface RatingNotification : NSObject<UIAlertViewDelegate> 
{
	float _timer;
	
	bool _enabled;
	
	bool _disabledByUser;
}

+ (id)instance;

- (id)init;

- (void)update:(float)dt;

- (void)show;

- (void)enable;

- (void)disableByUser;

- (void)remindLater;

- (void)openITunes;

@end

void UpdateRatingNotification(float dt);

void ShowRatingNotification();

#endif