//
//  TellAFriendViewController.h
//
//  Created by Slava on 03.11.10.
//  Copyright 2010 Playrix Entertainment. All rights reserved.
//

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>



// Контроллер формы для письма другу
// и по совместительству делегат самого себя
@interface TellAFriendViewController : MFMailComposeViewController <MFMailComposeViewControllerDelegate> {
	
@private
	
	UIViewController* parent;
		// родительский контроллер - для модального появления и удаления
}

-(id)initWithParent:(UIViewController *)parentViewController;

-(void)show;

@end
