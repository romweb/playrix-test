#ifndef KEYBOARD_RESPONDER_H_INCLUDED
#define KEYBOARD_RESPONDER_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

	// Показать клавиатуру
	void ShowKeyboard();
	
	void HideKeyboard();
#ifdef __cplusplus
}
#endif



#ifdef __OBJC__

#import <UIKit/UIKit.h>

@interface KeyboardResponder : UIView<UIKeyInput> {
}

-(id)init;

+(id)sharedInstance;

-(void)keyboardDidShow;

-(void)keyboardDidHide;

@end

#endif

#endif // KEYBOARD_RESPONDER_H_INCLUDED