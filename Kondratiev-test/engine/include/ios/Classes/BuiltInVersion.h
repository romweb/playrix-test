#ifndef __BUILTINVERSION_H__
#define __BUILTINVERSION_H__

#include "Core/Application.h"

//
// В коде игры необходимо реализовать эту функцию. Скорей всего, она будет выглядеть так:
//
// bool IsBuiltInVersionLite() {
// #ifdef LITE_VERSION
//     return true;
// #else
//     return false;
// #endif
// }
//
bool IsBuiltInVersionLite();

inline bool IsBuiltInVersionFull() {
	return !IsBuiltInVersionLite();
}

//
// В коде игры необходимо реализовать эту функцию. Скорей всего, она будет выглядеть так:
//
// std::string GetBuiltInPurchaseID() {
// #ifdef LITE_VERSION
//     return "<purchaseID-free>";
// #else
//     return "<purchaseID-full>";
// #endif
// }
//
std::string GetBuiltInPurchaseID();

//
// В коде игры необходимо реализовать эту функцию.
//
std::string GetITunesUrlLite();

//
// В коде игры необходимо реализовать эту функцию.
//
std::string GetITunesUrlFull();

//
// В коде игры необходимо реализовать эту функцию.
//
std::string GetMpgUrlWithLanguagePlaceholder();

Core::Application* CreateApplication();

#endif // __BUILTINVERSION_H__
