#import <UIKit/UIKit.h>

@interface MoreGamesDelegate : NSObject<UIWebViewDelegate> {

}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error;
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;
@property (nonatomic, retain) id glView;
@property (nonatomic, retain) id activityIndicator;

@end