//
//  Application.h
//
//  Created by Slava on 12/3/10.
//  Copyright 2010 Playrix Entertainment. All rights reserved.
//

//See also AppDelegate.h/mm for additional script code

#import <QuartzCore/QuartzCore.h>

//
// Objective-C обертка вокруг всех движковых функций
//
@interface Engine : NSObject {
}

// Создать объект приложения (унаследованный от движкового)
+ (void)createApplicationWithLayer:(CAEAGLLayer*)layer;

+ (void)recreateApplicationWithLayer:(CAEAGLLayer*)layer setMatrixWidth:(int)matrixWidth setMatrixHeight:(int)matrixHeight;

// Обновить и отрисовать объекты приложения
+ (void)updateAndDraw;

// Инициализировать пути к base и текстурам партиклов
+ (void)initializePaths:(const char*)currentPath;

+ (void)setMousePos:(CGPoint)position;

+ (CGPoint)getMousePos;

+ (void)mouseDown;

+ (void)mouseDoubleClick;

+ (void)mouseMove:(CGPoint)position;

+ (void)mouseUp;

+ (void)pinchBegan:(float)scale :(CGPoint)position :(UIView*)view;

+ (void)pinchChanged:(float)scale :(CGPoint)position :(UIView*)view;

+ (void)pinchEnded: (UIView*)view;

+ (NSString*)getText:(const char*)idResource;

// Поставить / снять с паузы всю игру
+ (void) setPause:(BOOL)pause;

// Обработать нажатие на клавиатуре
+ (void) keyPress:(int) keyCode;

+ (void) keyPressReturn;

+ (void) keyPressBack;

@end
