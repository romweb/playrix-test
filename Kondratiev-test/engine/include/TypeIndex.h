#pragma once

#include <typeinfo>

#include <boost/functional/hash.hpp>

///
/// ����������� type_info ��� ����������� �������������� �� �����������,
/// ��� �� ������ ��������, ��� � ���������������
///
class TypeIndex
{
public:
	TypeIndex(const std::type_info& info)
		: _pinfo(&info)
	{
	}

	const char *name() const
	{
		return _pinfo->name();
	}

	bool operator==(const TypeIndex& rhs) const
	{
		return *_pinfo == *rhs._pinfo;
	}

	bool operator!=(const TypeIndex& rhs) const
	{
		return !(*this == rhs);
	}

	bool operator<(const TypeIndex& rhs) const
	{
		return _pinfo->before(*rhs._pinfo);
	}

	bool operator>=(const TypeIndex& rhs) const
	{
		return !(*this < rhs);
	}

	bool operator>(const TypeIndex& rhs) const
	{
		return rhs < *this;
	}

	bool operator<=(const TypeIndex& rhs) const
	{
		return !(rhs < *this);
	}

	friend size_t hash_value(const TypeIndex& v)
	{
		// ��� �� ���������; ��� ������ � ����� ������ �������, �.�. ������� type_info �����������
		return boost::hash_value(v._pinfo);

		// ��� �� ������, ���� ��� �� ��������� ������-�� ����� �� ���������
		//const char* p = v._pinfo->name();
		//return boost::hash_range(p, p + std::strlen(p));
	}

private:
	const std::type_info* _pinfo;
};
