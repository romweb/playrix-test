#ifndef _FILE_FILEMETHODS_H_
#define _FILE_FILEMETHODS_H_

#pragma once

#include "IO/Stream.h"
#include "IO/VirtualFileSystem.h"

#include <string>

struct FileNameOp {
	enum Type {
		Resolve,
		DontResolve
	};
};

struct ErrorMode {
	enum Type {
		ReturnCode,
		ThrowException
	};
};

namespace File {

	/// �������� ������������� �����
	bool Exists(const std::string& filename);
	bool Exists(const std::wstring& filename);

	/// �������� ������������� ����� � �������� �������
	bool ExistsInFs(const std::string& filename);
	bool ExistsInFs(const std::wstring& filename);

	/// ���������� �������� ���������� �������. ���������� false ��� ������.
	bool cd(const std::string& dir);
	bool cd(const std::wstring& dir);

	/// ���������� ����, ���� ���� ����������, �� �� �� ����������������, � ������������ false;
	bool cp(const std::string& src, const std::string& dst);
	bool cp(const std::wstring& src, const std::wstring& dst);

	/// ����������� �����, ���� ���� ����������, �� �� �� ����������������, � ������������ false;
	bool mv(const std::string& src, const std::string& dst);
	bool mv(const std::wstring& src, const std::wstring& dst);

	/// �������� �����.
	bool rm(const std::string& file);
	bool rm(const std::wstring& file);

	/// �������� ����������.
	void rmdirr(const std::string& path);
	void rmdirr(const std::wstring& path);

	/// ������� ����������
	void mkdir(const std::string& theDir);
	void mkdir(const std::wstring& theDir);

	/// ������� ��� ����� �� ������� ����
	std::string fileName(const std::string &path);
	std::wstring fileName(const std::wstring &path);

	/// ������� ��� ������ �����
	std::string outputDirectory();
	std::wstring outputDirectoryW();

	/// ���������� ������� ����������
	std::string pwd();
	std::wstring pwdW();

	/// ������� "���������"
	std::string GetDocumentsPath();
	std::wstring GetDocumentsPathW();
	
#if defined(ENGINE_TARGET_ANDROID)
	//��������� ���� � ���������� ����������.
	void setDocumentsPath(std::string documentsPath);
#endif

	/// ������� "����� ������"
	std::string GetCommonAppDataPath();
	std::wstring GetCommonAppDataPathW();

	/// ������� "������ ������"
	std::string GetUserAppDataPath();
	std::wstring GetUserAppDataPathW();

	std::string ResolveFile(const std::string& filename, bool canBeOverriden = true);
	IO::InputStreamPtr OpenRead(const std::string& filename, FileNameOp::Type op = FileNameOp::Resolve, ErrorMode::Type err = ErrorMode::ThrowException);
	bool LoadFile(const std::string& filename, std::vector<uint8_t>& contents);
	
	bool HasOverrideLocation();
	std::string GetOverrideLocation();
	std::wstring GetOverrideLocationW();
	
#if !defined(ENGINE_TARGET_WIN32)
	void CopyDir(std::string src, std::string dest);
	void RemoveDir(std::string path);
#endif
}

#endif //_FILE_FILEMETHODS_H_
