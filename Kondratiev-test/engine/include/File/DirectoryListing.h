#ifndef _FILE_DIRECTORYLISTING_H_
#define _FILE_DIRECTORYLISTING_H_

#pragma once

#include "FileAttributesCondition.h"

typedef std::vector<std::string> StringVector;

namespace File {
/// Статический класс, умеющий выдавать названия файлов в каталоге.
/// Обёртка ::FindFirstFile - ::FindNextFile - ::FindClose.
class DirectoryListing
{
public:

	/// Получение файлов с именем, удовлетворяющим маске mask,
	/// и атрибутами, удовлетворяющими attrCond.
	static StringVector Get(const std::string& mask, FileAttributesCondition attrCond = FileAttributesCondition());
	/// Получение файлов из пакета с именем, удовлетворяющим маске mask,
	/// и атрибутами, удовлетворяющими attrCond.
	static StringVector GetFromPack(const std::string& mask, FileAttributesCondition attrCond = FileAttributesCondition());
	/// Получение файлов из файловой системы с именем, удовлетворяющим маске mask,
	/// и атрибутами, удовлетворяющими attrCond.
	static StringVector GetFromFs(const std::string& mask, FileAttributesCondition attrCond = FileAttributesCondition());
};
}
#endif //_FILE_DIRECTORYLISTING_H_
