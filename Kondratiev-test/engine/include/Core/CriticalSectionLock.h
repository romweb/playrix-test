#pragma once

#include "Core/CriticalSection.h"

//
// �����, ����������� �������� � �������������� (CriticalSection)
// � ����� C++, � ������ ������� ���������� � ������ ��������
// ������� � ������� ���������� � ������ �������� ��� ������
// �� ����� (� ��������� ��� ��������� ��� ������������� ����������).
//
class CriticalSectionLock
{
private:

	CriticalSection &_cs;
		// ���������� ������ �������������

public:

	//
	// ��������� ���������� � ����������� ������� �������������.
	//
	CriticalSectionLock(CriticalSection &cs);

	//
	// ������ ����������.
	//
	~CriticalSectionLock();

};