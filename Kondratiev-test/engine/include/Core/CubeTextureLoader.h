#ifndef __CUBETEXTURELOADER_H__
#define __CUBETEXTURELOADER_H__

#pragma once

#include "Core/ResourceLoader.h"

///
/// ��������� ���������� �������
///
class CubeTextureLoader : public ResourceLoader
{
protected:
	void DoLoadData(Resource* resource);

	void DoLoadObject(Resource* resource);

	void DoUnload(Resource* resource);
};

#endif // __CUBETEXTURELOADER_H__
