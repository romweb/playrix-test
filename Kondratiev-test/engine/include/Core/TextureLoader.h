#ifndef __TEXTURELOADER_H__
#define __TEXTURELOADER_H__

#pragma once

#include "Core/ResourceLoader.h"

///
/// ��������� �������
///
class TextureLoader : public ResourceLoader
{
protected:
	void DoLoadData(Resource* resource);

	void DoLoadObject(Resource* resource);

	void DoUnload(Resource* resource);
};

#endif // __TEXTURELOADER_H__
