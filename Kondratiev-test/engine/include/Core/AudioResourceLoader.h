#ifndef __AUDIORESOURCELOADER_H__
#define __AUDIORESOURCELOADER_H__

#pragma once

#include "Core/ResourceLoader.h"

///
/// ��������� �����
///
class AudioResourceLoader : public ResourceLoader
{
protected:
	void DoLoadData(Resource* resource);

	void DoLoadObject(Resource* resource);

	void DoUnload(Resource* resource);
};

#endif // __AUDIORESOURCELOADER_H__
