#pragma once
#include "Core/Timer.h"
#include <string>
#include <boost/function.hpp>

namespace Xml {
	class TiXmlElement;
}

namespace Core
{

class LoadScreen;

/// ����� ����������
class Application
{
protected:
	std::string _keyMap[91];
	bool _loading;
	long _loadingCounter;
	long _resourceCounter;
	int currentFps;
	std::string debugString;

	LoadScreen *loadScreen;

	boost::asio::io_service _io_service;

public:
	bool isPaused;
	bool splashesLoop;
	int  maxFps;
	bool showFps;
	Timer timer;
	
	Application(LoadScreen *ls = NULL);
	virtual ~Application();

	void RunInMainThread(boost::function<void()> func);

	virtual void ScriptMap();

	/// ��������� ������������� (�� ����� ����������� � ������������, ��� �������� ����������� ������).
	/// ���� ����� ������ ���� ������ �� Start().
	void Init();
	
	/// �������� ���� ����������
	void Start();
	void MainLoop();
	void CloseWindow() {};

	/// ��������������� �������
	void ShutDown();
	void LoadKeyMap();

	/// ��������� ����������
	void Stop();

	void setMousePos(int x, int y);
	void updateMousePos(int x, int y);

	/// ���������� ������ �������� (���� ����)
	virtual void UpdateLoadScreen();

	/// ����������� ������� � �������, ������ ���� ������ �� ���������� ��� ���������������
	virtual void RegisterTypes();

	/// �������� �������� �� �������� ����� (�������� ��������� �������).
	/// ���������� �� Init().
	virtual void PreloadResources() {};

	/// �������� �������� �������� (����� ���� ��������� �������).
	/// ���������� �� Init().
	virtual void LoadResources();

	/// ��������� ������ ����
	virtual void Draw();
	virtual void BeginDraw();
	virtual void BaseDraw();
	virtual void DebugDraw();
	virtual void EndDraw();

	/// ��������� ������� �����. ���������� �� Draw.
	virtual void DrawPause();

	virtual void Update(float dt);

	int getCurrentFps() {
		return currentFps;
	};
	
	void SetLoaded();
	void WaitLoading();
	bool IsLoading();

	long GetLoadingCounter() const { return _loadingCounter; }
	long GetResourceCounter() const { return _resourceCounter; }

	void setDebugString(const std::string &str) {
		debugString = str;
	}

protected:
	void UpdateMessageQueue();
};

extern Application* appInstance;
}
