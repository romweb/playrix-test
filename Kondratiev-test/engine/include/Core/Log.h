#ifndef _LOG_H_
#define _LOG_H_

#include "BuildSettings.h"
#include "IO/Stream.h"
#include "RefCounter.h"

#include <vector>

#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/intrusive_ptr.hpp>

#ifdef THREADED
#include "ThreadSupport.h"
#endif

namespace Log {

///
/// Перечисляет уровни логгирования
///
struct Severity {
	enum Type {
		Debug,
		Info,
		Warn,
		Error,
		Fatal
	};
};

class LogSink : public RefCounter {
public:
	virtual void WriteMessage(Severity::Type severity, const std::string& message) = 0;
};

typedef boost::intrusive_ptr<LogSink> LogSinkPtr;

class Log
{
public:
	Log();
	
	void AddSink(LogSink* sink);
	
	void RemoveAllSinks();
	
	void SetMinSeverityLevel(Severity::Type severity) { _severity = severity; }
	
	Severity::Type GetMinSeverityLevel() const { return _severity; }
	
	void WriteMessage(Severity::Type severity, const std::string& message);
	
	void WriteDebug(const std::string& msg);
	
	void WriteInfo(const std::string& msg);
	
	void WriteWarn(const std::string& msg);
	
	void WriteError(const std::string& msg);
	
	void WriteFatal(const std::string& msg);
	
private:
	typedef std::vector<LogSinkPtr> Sinks;
	
	Sinks _sinks;
	
	Severity::Type _severity;
	
#ifdef THREADED
	MUTEX_TYPE _writeMutex;
#endif
};

extern Log log;

void Debug(const std::string& msg);

void Info(const std::string& msg);

void Warn(const std::string& msg);

void Error(const std::string& msg);

void Fatal(const std::string& msg);

///////////////////////////////////////////////////////////////////////////////
// Log sinks

/// Logging to html formatted file
class HtmlFileLogSink : public LogSink {
public:
	HtmlFileLogSink(const std::string& filename, bool recreate);
		
	~HtmlFileLogSink();
	
	void WriteMessage(Severity::Type severity, const std::string& message);

private:
	void WriteFileString(const std::string& str);

private:
	IO::OutputStreamPtr _stream;

	boost::asio::io_service _work_io_service;
	boost::scoped_ptr<boost::asio::io_service::work> _work;
	boost::scoped_ptr<boost::thread> _work_thread;

private:
	static const char* GetSeverityFontAttribs(Severity::Type severity);	
	static std::string EncodeHtml(const std::string& str);
};

/// Logging to debug output
class DebugOutputLogSink : public LogSink {
public:
	void WriteMessage(Severity::Type severity, const std::string& message);

private:
	static const char* GetSeverityName(Severity::Type severity);
};

} // namespace Log

#endif

