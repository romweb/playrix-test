#ifndef __PLAYRIXENGINE_H__
#define __PLAYRIXENGINE_H__

/// Этот файл включает все заголовки для PlayrixEngine

#include "types.h"

#include "BuildSettings.h"
#include "EngineAssert.h"
#include "Utils.h"
#include "File.h"
#include "Render.h"
#include "Core.h"
#include "IO.h"
#include "GUI.h"
#include "MM.h"
#include "Flash.h"
#include "SceneGraph.h"
#include "Animation3D.h"
#include "DataStore.h"
#include "Distortion.h"
#include "Factory.h"
#include "ImageLoader.h"
#include "Layer.h"
#include "MessageFunc.h"
#include "mgFrustumConsts.h"
#include "MiniSlider.h"
#include "ParticleSystem.h"
#include "SmoothTextureChanger.h"
#include "Spline.h"
#include "IGameInfo.h"
#include "GameFeatures.h"
#include "Marketing.h"
#include "Particle/EffectsContainer.h"

#endif // __PLAYRIXENGINE_H__
