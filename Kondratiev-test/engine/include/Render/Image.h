/*
 *  Image.h
 *  Engine
 *
 *  Created by Slava on 2/17/11.
 *  Copyright 2011 Playrix Entertainment. All rights reserved.
 *
 */

#ifndef IMAGE_H_INCLUDED
#define IMAGE_H_INCLUDED

#include <map>

#include "PixelType.h"
#include "platform.h"
#include <boost/scoped_array.hpp>
#include <boost/shared_array.hpp>

namespace Render {

class GfxFileInfo
{
public:
	GfxFileInfo();

	bool HasImageType(uint8_t imageType) const {
		return imageTypes.find(imageType) != imageTypes.end();
	}
	
public:
	std::set<uint8_t> imageTypes;
	uint16_t width;
	uint16_t height;
	bool alphaChannel;
};
	
/// Изображение, загруженная в память текстура.
class Image
{
public:
	Image();
	explicit Image(const std::string& filename);

	void LoadFromFile(const std::string& filename);
	void ApplyPixelType(EnginePixelType new_type, bool useDithering);
	void FlipVertically();
	
	void GetPropertiesFromFile(const std::string& filename);
	
	template <class Loader>
	void Load(Loader& loader, EnginePixelType pixelType) {
		loader.Load(pixelType);
	}
	
	template <class ImageAlgorithm>
	void Apply(ImageAlgorithm& algoritm) {
		algoritm.apply(this);
	}
	
	void Purge();

	static void LoadGfxFileInfo();
	static GfxFileInfo* FindGfxFileInfo(const std::string& name, int imageType);
	static void AddGfxFileInfo(const std::string& path, const GfxFileInfo& info);
	
public:
	uint16_t width, height;
	unsigned int compressedSize;

	typedef boost::shared_array<unsigned char> ByteArray;
	
	ByteArray data;

	uint8_t mipLevels;
	uint8_t components;

	uint8_t pixelType; ///< Формат пиксельных данных. // EnginePixelType
	bool alphaChannel;

private:
	typedef std::map<std::string, GfxFileInfo> GfxFileMap;

	static GfxFileMap _gfxFileInfo;
	
	friend class ImageLoader;
};

} // namespace Render

#endif
