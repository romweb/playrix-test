#ifndef _RENDER_SHADERPROGRAMGL_H_
#define _RENDER_SHADERPROGRAMGL_H_

#if defined(ENGINE_TARGET_WIN32)
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#elif defined (ENGINE_TARGET_LINUX)
#include <GLES/gl.h>
#include <GLES/glext.h>
#elif defined(ENGINE_TARGET_IPHONE)
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>
#elif defined(ENGINE_TARGET_MACOS)
#include <OpenGL/OpenGL.h>
#endif
#include "Render/ShaderProgram.h"
#include <map>

namespace Render {

/// Класс для управления вершинными и пиксельными шейдерами.
class ShaderProgramGL : public ShaderProgram
{
 
protected:
	GLuint _program;

	typedef std::map<std::string, GLuint> RMap;
	RMap uniforms;
	RMap uniformsTypes;
	RMap attributes;

public:
	/// Стандартные юниформы.
	int u_modelview;
	int u_sampler;
	int u_sampler1;
	int u_sampler2;
	int u_color;
	
	/// Стандартные атрибуты.
	int a_vertex;
	int a_color;
	int a_texcoord; ///< Текстурные координаты для нулевого текстурного канала
	int a_texcoord1;
	int a_texcoord2; ///< Текстурные координаты для каналов 1 и 2 (для мультитекстурных шейдеров).
	int a_normal; // нормали
	int a_weights; // веса вершин
	int a_indices; // индексы в массиве матриц

	bool setup; ///< Этот флаг используется, чтобы отложить настройки шейдера, которые должны быть сделаны в основном контексте.

private:
	ShaderProgramGL(const ShaderProgramGL& shader); // no copy
	ShaderProgramGL& operator=(const ShaderProgramGL& shader);

public:
	ShaderProgramGL(rapidxml::xml_node<>* elem = NULL)
		: ShaderProgram()
		, _program(0)
		, u_modelview(-1)
		, u_sampler(-1)
		, u_sampler1(-1)
		, u_sampler2(-1)
		, u_color(-1)
		, a_vertex(-1)
		, a_color(-1)
		, a_texcoord(-1)
		, a_texcoord1(-1)
		, a_texcoord2(-1)
		, a_normal(-1)
		, a_weights(-1)
		, a_indices(-1)
		, setup(false)
	{}
	
	virtual ~ShaderProgramGL()
	{
		UnloadShader();
	}

	/// Создание пары шейдеров из файлов. Возможно задание пустой строки вместо одного из шейдеров.
	virtual bool LoadFile(const std::string& filenameVS, const std::string& filenamePS);

	/// Создание пары шейдеров из строк с программой. Возможно задание пустой строки вместо одного из шейдеров
	virtual bool LoadSource(const std::string& sourceVS, const std::string& sourcePS);
	
	void UnloadShader();
	
	/// Наследники могут переопределять эти методы, чтобы сохранять позиции параметров.
	virtual void locUniform(const char * uniformName, GLuint loc_id);
	virtual void locAttribute(const char * attribName, GLuint loc_id);
	
	/// Загрузка шейдеров в устройство
	virtual void Upload() {}
	
	/// Выгрузка шейдеров из устройства
	virtual void Release() {}

	/// Связывание шейдеров с устройством
	virtual void Bind();
	
	/// Отвязывание шейдеров от устройства
	virtual void Unbind();

	/// Эти методы используются в DX-шейдерах.
	/// В GLES2 вместо них используйте методы SetUniform().
    /// Пока эти данные равнозначны данным в юниформах для вершинных и пиксельных шейдеров
	virtual void setVSParam(const std::string& name, const float* fv, int size);
	virtual bool hasVSParam(const std::string& name) { return getUniformLocation(name) != -1; }
	virtual void setPSParam(const std::string& name, const float* fv, int size);
	virtual bool hasPSParam(const std::string& name) { return getUniformLocation(name) != -1; }

	/// Получить нужный текстурный канал для текстуры
	virtual int getPSTextureChannel(const std::string &name) { Assert(false); return -1; }

	size_t getProgram() { return _program; }

public:
	void Setup(); ///< Настроить шейдер. Вызывается автоматически при первом использовании шейдера.
		///< При использовании шейдера в другом треде понадобится вызвать эту функцию вручную ещё раз.

	/// Возвращает -1, если юниформа или аттрибута нет.
	/// Остальные функции ничего не делают, если uniform = -1.
	int getUniformLocation(const std::string& uniformName);
	int getAttributeLocation(const std::string& attribName);
    
    /// Возвращает тип юниформа (-1 если нет такого юниформа)
    GLuint getUniformType(const std::string& uniformName);

	void SetUniform(int uniform, int value);
	void SetUniform(int uniform, const math::Matrix4& value);
	void SetUniform(int uniform, const float* fv, int size);
    void SetUniform(GLuint type, int uniform, const float* fv, int size);

	void EnableVertexAttribArray(int attrib);
	void DisableVertexAttribArray(int attrib);
	void EnableVertexAttribArraysAll();
	
	void VertexAttribPointer(int attrib, int size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* pointer);
};

} // namespace Render

#endif // _RENDER_SHADERPROGRAMGL_H_
