#ifndef __VERTEXBUFFER_H__
#define __VERTEXBUFFER_H__

#pragma once

class VertexBuffer;
class VertexBufferIndexed;

#include "Render/RenderDevice.h"
#include "EngineAssert.h"

#if defined(ENGINE_TARGET_WIN32) && !defined(USING_GLES2)
#	define HARDWAREBUFFER IDirect3DVertexBuffer9*
#	define HARDWAREIBUFFER IDirect3DIndexBuffer9* 
#	define INDEX uint16_t
#else
#	define HARDWAREBUFFER GLuint
#	define HARDWAREIBUFFER GLuint
#	define INDEX GLushort
#endif

class VertexBuffer 
{
private:
	bool reloadWithAlpha;

public:

	HARDWAREBUFFER _deviceBuffer;
	bool isStatic;

	unsigned int numVertices;

	std::vector<Render::QuadVert> _buffer;

	struct VertexAttribute
	{
		bool bNormal;
		bool bVertexColor;
		bool bTexCoords;
		int numOfMappingChannels;
	};

	VertexAttribute vertexAttribs;

public:

	VertexBuffer();
	VertexBuffer(const VertexBuffer& vb);
	virtual ~VertexBuffer();

	virtual void Init(int numOfVertices);
	virtual void InitQuadBuffer(int numOfQuads);

	//void LoadData(const std::string& filename);

	virtual void SetQuad(int quadIndex, float centerX, float centerY, float width, float height, float angle, Color color, float uStart, float uEnd, float vStart, float vEnd);
	
	void SetQuad(int quadIndex, float x, float y, float width, float height, Color color);
	void SetQuad(int quadIndex, float x, float y, float width, float height, const FRect& uv, Color color);
	void SetQuad(int quadIndex, float x, float y, float width, float height, float angle, Color color);
	void SetQuad(int quadIndex, const FRect& rect, const FRect& uv, Color color = Color::WHITE);
	void SetQuad(int quadIndex, const IRect& rect, const FRect& uv, Color color = Color::WHITE);
	
	virtual void SetQuad(int quadIndex, const math::Vector3& v0, const math::Vector3& v1, const math::Vector3& v2, const math::Vector3& v3,
		const Color& c1, const Color& c2, const Color& c3, const Color& c4, float uStart = 0.0f, float uEnd = 1.0f, float vStart = 0.0f, float vEnd = 1.0f);
	
	void SetQuad(int quadIndex, const math::Vector3& v0, const math::Vector3& v1, const math::Vector3& v2, const math::Vector3& v3,
		const Color& color, float uStart = 0.0f, float uEnd = 1.0f, float vStart = 0.0f, float vEnd = 1.0f);
	
	void SetRawData(Render::QuadVert *buf);
	void SetRawData(int vertIndex, Render::QuadVert *buf, int vertCount);

	virtual void Upload(int verticesCount = -1);
			// VerticesCount - количество вершин для подгрузки (-1 - будут подгружены все вершины в буфере)
	virtual void Draw(int verticesCount = -1);
			// VerticesCount - количество вершин для отрисовки (-1 - будут отрисованы все вершины в буфере)
	void DrawAlphaBounded();
	
};

class VertexBufferIndexed : public VertexBuffer
{
public:
	int numIndices;
	HARDWAREIBUFFER _deviceIndex;
	std::vector<INDEX> _ibuffer;
	bool _ibufferDirty;				// Нужно ли заново зааплоадить индексный буффер при отрисовке
									// Устанавливается в true в случае изменения любого индекса
	bool _quadBuffer;				// Является ли буффер вместилищем квадов
	int _quadsCount;				// Количество квадов при инициализации

public:
	VertexBufferIndexed();
	VertexBufferIndexed(const VertexBufferIndexed &v);
	virtual ~VertexBufferIndexed();

	virtual void Init(int numOfVertices, int numOfIndices = 0);
	virtual void Reinit(int numOfVertices, int numOfIndices = 0); 
	virtual void InitQuadBuffer(int numOfQuads);

	/// x, y - центр вращения
	virtual void SetQuad(int quadIndex, float x, float y, float width, float height, float sx, float sy, float angle, Color color,
		float uStart, float uEnd, float vStart, float vEnd);
	
	/// x, y - центр вращения
	void SetQuad(int quadIndex, float x, float y, float width, float height, float angle, Color color);
	void SetQuad(int quadIndex, float x, float y, float width, float height, float angle, Color color,
		float uStart, float uEnd, float vStart, float vEnd);
	
	void SetQuad(int quadIndex, float x, float y, float width, float height, Color color);

	virtual void SetQuad(int quadIndex, const math::Vector3& v0, const math::Vector3& v1, const math::Vector3& v2, const math::Vector3& v3,
		const Color& c1, const Color& c2, const Color& c3, const Color& c4, float uStart = 0.0f, float uEnd = 1.0f, float vStart = 0.0f, float vEnd = 1.0f);
	
	void SetQuad(int quadIndex, const math::Vector3& v0, const math::Vector3& v1, const math::Vector3& v2, const math::Vector3& v3,
		const Color& color, float uStart = 0.0f, float uEnd = 1.0f, float vStart = 0.0f, float vEnd = 1.0f);
	
	virtual void Upload(int verticesCount = -1);
	virtual void Draw(int verticesCount = -1);
	
	void setVertex(int qIndex, const Render::QuadVert& quad);
	void setIndex(int qIndex, int index);
	void setBuffer(const std::vector<Render::QuadVert> &buffer);
	void setIndexBuffer(const std::vector<INDEX> &ibuffer);
	void UploadVertex();
	void UploadIndex(); 
};

#endif // __VERTEXBUFFER_H__
