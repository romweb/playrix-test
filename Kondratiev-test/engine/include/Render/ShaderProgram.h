#ifndef _RENDER_SHADERPROGRAM_H_
#define _RENDER_SHADERPROGRAM_H_

#include "Utils/Vector4.h"
#include "Utils/Matrix4.h"
#include "Core/Resource.h"

#include <boost/intrusive_ptr.hpp>

namespace Render {

/// Класс для управления вершинными и пиксельными шейдерами.
class ShaderProgram : public Resource {
public:
	static ShaderProgram * CreateFromXml(rapidxml::xml_node<>* elem);

	const std::string& GetName() const { return _name; }

	void SetName(const std::string& name) { _name = name; }
		
	/// Создание пары шейдеров из файлов. Возможно задание пустой строки вместо одного из шейдеров
	virtual bool LoadFile(const std::string& filenameVS, const std::string& filenamePS) = 0;

	/// Создание пары шейдеров из строк с программой. Возможно задание пустой строки вместо одного из шейдеров
	virtual bool LoadSource(const std::string& sourceVS, const std::string& sourcePS) = 0;
	
	/// Загрузка шейдеров в устройство
	virtual void Upload() = 0;
	
	/// Выгрузка шейдеров из устройства
	virtual void Release() = 0;

	/// Связывание шейдеров с устройством
	virtual void Bind() = 0;
	
	/// Отвязывание шейдеров от устройства
	virtual void Unbind() = 0;
	
	/// Установка констант для вершинного шейдера
	virtual void setVSParam(const std::string& name, const float* fv, int size) = 0;

	/// Имеется ли данный параметр у вершинного шейдера
	virtual bool hasVSParam(const std::string& name) = 0;
	
	/// Установка констант для пиксельного шейдера
	virtual void setPSParam(const std::string& name, const float* fv, int size) = 0;

	/// Имеется ли данный параметр у пиксельного шейдера
	virtual bool hasPSParam(const std::string& name) = 0;

	/// Получить нужный текстурный канал для текстуры
	virtual int  getPSTextureChannel(const std::string &name) = 0;
	
	void setVSParam(const std::string& name, const math::Vector4& v);
	
	void setVSParam(const std::string& name, const math::Matrix4& m);
	
	void setPSParam(const std::string& name, float f);
	
	void setPSParam(const std::string& name, const math::Vector4& v);
	
	void setPSParam(const std::string& name, const math::Matrix4& m);
	
	virtual size_t getProgram() const { return 0; }

#ifdef ENGINE_TARGET_LINUX
    void setFilenameVS(const std::string& name) { filenameVS_ = name; }
    void setFilenamePS(const std::string& name) { filenamePS_ = name; }
    const std::string &filenameVS() const { return filenameVS_; }
    const std::string &filenamePS() const { return filenamePS_; }
private:
    std::string filenameVS_;
    std::string filenamePS_;
#endif

protected:
	std::string _name;
};

typedef boost::intrusive_ptr<ShaderProgram> ShaderProgramPtr;

inline void ShaderProgram::setVSParam(const std::string& name, const math::Vector4& v) {
	setVSParam(name, v.v, 4);
}

inline void ShaderProgram::setVSParam(const std::string& name, const math::Matrix4& m) {
	setVSParam(name, m.v, 16);
}

inline void ShaderProgram::setPSParam(const std::string& name, float f) {
	setPSParam(name, math::Vector4(f, 0, 0, 0).v, 4);
}

inline void ShaderProgram::setPSParam(const std::string& name, const math::Vector4& v) {
	setPSParam(name, v.v, 4);
}

inline void ShaderProgram::setPSParam(const std::string& name, const math::Matrix4& m) {
	setPSParam(name, m.v, 16);
}

} // namespace Render

#endif // _RENDER_SHADERPROGRAM_H_
