#ifndef __SPRITEBATCH_H__
#define __SPRITEBATCH_H__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER

#include "RenderDevice.h"
#include "Utils/Color.h"
#include "Utils/IRect.h"
#include "RefCounter.h"

#include <boost/intrusive_ptr.hpp>
#include <boost/noncopyable.hpp>

namespace math {
	class Vector3;
};

namespace Render {

///
/// ����������� ������ ���������� ��������
///
struct SpriteSortMode {
	enum Type {
		// ����������� ������� �� ���������� ������, ��� ���������� ������������ ������
		Deferred,
		// ����� ������������ ������� �� ������
		Immediate,
		// ����������� ������� �� ���������� ������, ����� ��������� �� ���������
		Texture,
		// ����������� ������� �� ���������� ������, ����� ��������� �� ������ ����������
		Blend,
		// ����������� ������� �� ���������� ������, ����� ��������� �� �������� � ��������
		BackToFront,
		// ����������� ������� �� ���������� ������, ����� ��������� �� �������� � ��������
		FrontToBack
	};
};

///
/// ����������� ������ ����������������� ��������
///
struct SpriteTransformMode {
	enum Type {
		// ��� �����������������
		None,
		// ��������� ������� ������������� �� ����������
		Auto
	};
};

///
/// ����� ��������
///
class SpriteBatch : public RefCounter, private boost::noncopyable {
public:
	SpriteBatch();
	~SpriteBatch();
	
	void Begin(SpriteSortMode::Type sortMode = SpriteSortMode::Deferred, SpriteTransformMode::Type transformMode = SpriteTransformMode::None);
		/// �������� ����� ��������
	
	void Draw(Texture* texture, BlendMode blend, const IRect& sourceRect, const math::Vector3& position);
	void Draw(Texture* texture, BlendMode blend, const FPoint& position);
		/// ������ ������, ����������� ��� ������������� ���������� � ������� TranslateUV

	void Draw(Texture* texture, BlendMode blend, const FRect& screenRect, const FRect& textureUV);
	void Draw(Texture* texture, BlendMode blend, const math::Vector3& vec1, const math::Vector3& vec2, const math::Vector3& vec3, const math::Vector3& vec4, Color color, const FRect& uv);
	void Draw(Texture* texture, BlendMode blend, const math::Vector3& vec1, const math::Vector3& vec2, const math::Vector3& vec3, const math::Vector3& vec4, Color color[4], const FRect& uv);
		/// ������ ������, �� ����������� ���������� � ������� TranslateUV

	void Flush();
		/// ������������� ������������ �������������� �������
	
	void End();
		/// ����������� ����� ��������

	static void ResetBatches();
		/// ����������� ���������� ����������� �������

private:
	class Impl;
	std::auto_ptr<Impl> _impl;
	
	SpriteTransformMode::Type _transformMode;
};

typedef boost::intrusive_ptr<SpriteBatch> SpriteBatchPtr;

} // namespace Render

#endif // __SPRITEBATCH_H__
