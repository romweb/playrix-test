#ifndef _REDNER_RENDERFUNC_H_
#define _REDNER_RENDERFUNC_H_

/// Тут собраны платформо независымые функции для разнообразной отрисовки и прочего

#include "Render/RenderDevice.h"
#include "Render/BitmapFont.h"
#include "RefCounter.h"

#include <stack>

namespace Render {

	// для BitmapFontImpl
	void AddFontAliasBitmapFont(const std::string &newName, const std::string &oldName);

	struct BitmapCharImpl
	{
		int aWidth;
			// см. BitmapFont::CharInfo::aWidth

		int bWidth;
			// см. BitmapFont::CharInfo::bWidth

		int cWidth;
			// см. BitmapFont::CharInfo::cWidth
        
        int charHeight;
        
        int charYOffset;

		float minU;
			// текстурная координата левой стороны квадрата буквы

		float maxU;
			// текстурная координата правой стороны квадрата буквы

		float minV;
			// текстурная координата нижней стороны квадрата буквы

		float maxV;
			// текстурная координата верхней стороны квадрата буквы
	};

	//
	// Растровый шрифт для использования в RenderDevice
	//
	struct BitmapFontImpl : public RefCounter
	{
	private:
		Texture *tex;
			// текстура с изображением букв

		int ascent;
			// расстояние от базовой линии до верхней границы строки
		
		int descent;
			// расстояние от базовой линии до нижней границы строки
		
		int fontHeight;
			// высота шрифта в пикселях

		int charTrack;
			// дополнительное расстояние между символами

		int yOffset;
			// отступы сверху и снизу для декорирования, не должны учитываться

		int spaceWidth;
			// spaceWidth == fontHeight / 4 - ширина пробела

		std::string fontName;
			// имя шрифта для загрузки текстуры

		// Коэффициент, на который умножается размер пробела при выводе его в качестве разделителя групп в больших числах
		float spaceCoeff;
		
		float scale;

	public:
		BitmapFontImpl();

		~BitmapFontImpl();

		BitmapFontImpl(const BitmapFontImpl& src);

		BitmapFontImpl& operator=(const BitmapFontImpl& src);

		//
		// Получение символа по его коду. Если символа не было - создаётся.
		//
		BitmapCharImpl& GetChar(int code) const;

		//
		// Занесён ли в таблицу символ code.
		//
		bool IsCharSet(int code) const;

		//
		// Зарезервировать место под символы
		//
		void ReserveChars(size_t size);
		
		float Ascent() const { return ascent * scale; }
		
		float Descent() const { return descent * scale; }

		float Height() const { return fontHeight * scale; }
		
		float BaseLine() const { return ascent * scale; }
		
		float CharTrack() const { return charTrack * scale; }
		
		void SetCharTrack(int track) { charTrack = track; }
		
		float YOffset() const { return yOffset * scale; }
		
		float SpaceWidth() const { return spaceWidth * scale; }
		
		float SpaceCoeff() const { return spaceCoeff; }
		
		float Scale() const { return scale; }
		
		float GetKerning(int first, int second) const;
		
		void UseKerning(bool doUse) { useKerning = doUse; }
		
		bool IsUsingKerning() const { return useKerning; }
		
		const std::string& FontName() const { return fontName; }
		
		Texture* GetTexture() const { return tex; }

	private:

		friend class BitmapFont;
		
		friend void AddFontAliasBitmapFont(const std::string &newName, const std::string &oldName);

		bool isAlias;

		typedef unsigned short CharIndex, *CharIndexPtr;

		mutable std::vector<CharIndexPtr> charOffsets;
			// массив указателей на блоки длиной 256 элементов
			// старший байт кода символа - это индекс в charOffsets
			// младший - индекс в соответсвующем блоке
			// значение в блоке - индекс символа в массиве chars

		mutable std::vector<BitmapCharImpl> chars;
			// Занесённые символы в порядке занесения,
			// индекс - не код символа.
		
		typedef std::vector<BitmapFont::KerningInfo> Kernings;
		
		Kernings kernings;
		
		bool useKerning;
	};

	typedef boost::intrusive_ptr<BitmapFontImpl> BitmapFontImplPtr;

	// ссылка на шрифт, можно хранить вместо имени шрифта
	// дюже экономит память
	class FontRef {
	private:
		uint8_t fontNum; // индекс в массиве fontsIndex, 255 - неинициализированное состояние
		uint8_t GetFontIndex(BitmapFontImpl* font);
	public:
		FontRef();
		FontRef(const std::string& fontname);
		FontRef& operator=(const std::string& fontname);
		const FontRef& operator=(const FontRef& fref);
		bool operator==(const FontRef& fref) const;
		inline bool operator!=(const FontRef& fref) const { return !(*this == fref); }
		Render::BitmapFontImpl* Font();
		const Render::BitmapFontImpl* FontConst() const;
		const std::string& operator()() const;
	};
    
    
///////////////////////////////////////////////////////
/// Функции общие для FeeType и BitMap шрифтов
///////////////////////////////////////////////////////
    
	void BindFont(const std::string& font_name);
	bool isFontLoaded(const std::string &font_name);
    
    bool IsCharSet( const std::string& font_name, int code);
    
	void PrintString(int x, int y, const std::string& str, float scale = 1.0f, TextAlign align = LeftAlign, TextAlign valign = TopAlign, bool formatNumbers = true);
	void PrintString(float x, float y, const std::string& str, float scale = 1.0f, TextAlign align = LeftAlign, TextAlign valign = TopAlign, bool formatNumbers = true);
	void PrintString(const IPoint& pos, const std::string& str, float scale = 1.0f, TextAlign align = LeftAlign, TextAlign valign = TopAlign, bool formatNumbers = true);
	void PrintString(const FPoint& pos, const std::string& str, float scale = 1.0f, TextAlign align = LeftAlign, TextAlign valign = TopAlign, bool formatNumbers = true);
	void PrintStringInternal(float x, float y, const std::string& str, float scale = 1.0f, TextAlign align = LeftAlign, TextAlign valign = TopAlign);
    
    
    
	float getStringWidth(const std::string& str, const std::string& fontName);
	float getStringWidth(const std::string& str);
    
	float getFontHeight(const std::string& fontName);
	float getFontHeight();
    
	float getFontDescender(const std::string& fontName);
	float getFontDescender();

///////////////////////////////////////////////////////

    //Является ли символ пробелом
	bool CharIsASpaceBitmapFont(int code);

	void FillStringBufferBitmapFont (const std::string& str, Color* c = 0);
	QuadVert& getLetterBitmapFont(int i);

	int getFontHeightBitmapFont (const std::string& fontName);
	inline int getFontHeightBitmapFont (const FontRef& f) { return (int)f.FontConst()->Height(); }
	int getFontHeightBitmapFont ();

	float getFontSpaceCoeffBitmapFont (const std::string &fontName);
	inline float getFontSpaceCoeffBitmapFont (const FontRef& f) { return f.FontConst()->SpaceCoeff(); };

	void setFontTrackBitmapFont(const std::string& fontName, int trackValue);
	inline void setFontTrackBitmapFont(FontRef& f, int trackValue) { f.Font()->SetCharTrack(trackValue); };
	void setFontTrackBitmapFont(int trackValue);
	void setSpaceWidthCorrectBitmapFont(int value);
	int getSpaceWidthCorrectBitmapFont();
	// Возвращает ширину пробела-разделителя групп разрядов в числах
	float getThousandsSpaceSeparatorWidthBitmapFont(const BitmapFontImpl *fnt);

    float getFontDescenderBitmapFont();
    
	int getStringBufferCountBitmapFont();

	int getStringWidthBitmapFont(const std::string& str, const std::string& fontName);
	int getStringWidthBitmapFont(const std::string& str, const FontRef& f);
	int getStringWidthBitmapFont(const std::string& str);

	void UploadBitmapFont(BitmapFont *font);
	bool isFontLoadedBitmapFont(const std::string &font_name);

	void BindBitmapFont(BitmapFont *font);
	void BindFontBitmapFont(const std::string& font_name);
	void BindFontBitmapFont(FontRef& f);
	void BindAlphaBitmapFont(BitmapFont* font);
	void BindFontAlphaBitmapFont(const std::string& font_name);
	void BindFontAlphaBitmapFont(FontRef& f);
	BitmapFontImpl* GetBitmapFont(const std::string& font_name);
	BitmapFontImpl* GetBitmapFont();
	// взять шрифт по умолчанию, используется когда не найден нужный шрифт
	BitmapFontImpl* GetDefaultBitmapFont();

	float getSpaceSizeBitmapFont(const std::string& font);
	float getSpaceSizeBitmapFont(const FontRef& f);
	float getSpaceSizeBitmapFont(const BitmapFontImpl* fnt);

	/// Печатать строку в буфер, можно принудительно задать цвет букв
	void PrintStringToBufferBitmapFont(const std::string& str, VertexBuffer* vbuffer, Color* c = 0);
	
	void PrintStringBitmapFont(int x, int y, const std::string& str, float scale = 1.0f, TextAlign align = LeftAlign, TextAlign valign = TopAlign, bool formatNumbers = true);
	void PrintStringBitmapFont(float x, float y, const std::string& str, float scale = 1.0f, TextAlign align = LeftAlign, TextAlign valign = TopAlign, bool formatNumbers = true);
	void PrintStringBitmapFont(const IPoint& pos, const std::string& str, float scale = 1.0f, TextAlign align = LeftAlign, TextAlign valign = TopAlign, bool formatNumbers = true);
	void PrintStringBitmapFont(const FPoint& pos, const std::string& str, float scale = 1.0f, TextAlign align = LeftAlign, TextAlign valign = TopAlign, bool formatNumbers = true);
	// Собственно, рисует строку
	void PrintStringInternalBitmapFont(float x, float y, const std::string& str, float scale = 1.0f, TextAlign align = LeftAlign, TextAlign valign = TopAlign);
	// Рисует строку с бликом шириной count символов, local_time in [0.0f, 1.0f], 
	void PrintBlickStringBitmapFont(int count, float local_time, const Color &color, const FPoint &pos, const std::string& str, int fakeLen = 0, float scale = 1.0f, TextAlign align = LeftAlign, TextAlign valign = TopAlign, bool formatNumbers = true);
	// Рисует строку, сменяющую цвет
	void PrintStringChangingColorBitmapFont(const FPoint &pos, const std::string &str, float local_time, const Color &color, float scale = 1.0f, TextAlign align = LeftAlign, TextAlign valign = TopAlign, bool formatNumbers = true);
	// Печатать одну (или больше) букв. Отрисовка происходит без использования вертексного буфера.
	void PrintLetterBitmapFont(float x, float y, const std::string& str, float scale = 1.0f, TextAlign align = LeftAlign, TextAlign valign = TopAlign);

	/// Текущая альфа домножается на multiplier,
	/// старая запоминается в стеке,
	/// восстановление с помощью EndAlphaMul
	///
	/// Множитель ожидается из отрезка [0;1]
	void BeginAlphaMul(float multiplier);
	/// Восстановление старой альфы после BeginAlphaMul
	void EndAlphaMul();

	/// Установка текущего цвета, как произведения заданного и предыдущего.
	/// Используется для управления цветом/альфой для вложенных объектов, значения их цвета/альфы не будут превышать заданных здесь.
	/// Обязательно нужно в конце блока использовать ResetColor, который восстановит исходное значение цвета.
	void SetColor(const Color& color);

	/// Восстанавливает предыдущее сохранённое значение цвета. Используется после SetColor.
	void ResetColor();
	/// Используется для контроля за глубиной стека
	int GetColorStackSize();

	// Псевдоним SetColor с более подходящим названием
	inline void BeginColor(const Color& color) {
		SetColor(color);
	}

	// Псевдоним ResetColor с более подходящим названием
	inline void EndColor() {
		ResetColor();
	}

	void DrawRect(int x, int y, int width, int height, float uStart = 0.f, float uEnd = 1.f, float vStart = 0.f, float vEnd = 1.f);
	void DrawRect(const IRect& rect, float uStart, float uEnd, float vStart, float vEnd);
	void DrawRect(const IRect& rect, const FRect& frect);
	void DrawRect(const IRect& rect, const FRect& frect, RectOrient orient);
	void DrawRect(int x, int y, int width, int height, float factorX, float factorY);
	void DrawRect(const IRect& rect);
	void DrawRect(const FRect& rect, const FRect &uv);
	void DrawRect(const IRect& rect, float factorX, float factorY);
	void DrawRect(const IRect& rect, RectOrient orient);
	void DrawRect(const IRect& rect, float uStart, float uEnd, float vStart, float vEnd, RectOrient orient);

	void DrawRotatedQuad(float x, float y, float width, float height, float angle);
	void DrawQuad(const FRect& xy, const FRect& uv);
	void DrawQuad(const FRect& xy, const FRect& uv, RectOrient orient);
	void DrawQuad(float x, float y, float width, float height);
	void DrawQuad(float x, float y, float width, float height, FRect uv);
	void DrawQuad(float x, float y, float width, float height, FRect uv, FRect uv2);
	void DrawQuad(float x, float y, float width, float height, FRect uv, FRect uv2, FRect uv3);
	void DrawQuad(float x, float y, float width, float height, 
				  float u1, float v1, float u2, float v2, float u3, float v3, float u4, float v4,
				  float u21, float v21, float u22, float v22, float u23, float v23, float u24, float v24);
	void DrawQuad(float x, float y, float width, float height, RectOrient orient);
	void DrawQuad(float x, float y, float width, float height, float tex_delta);
	void DrawQuad(float x, float y, float width, float height, float factorX, float factorY);
	void DrawQuad(const math::Vector3& v1, const math::Vector3& v2, const math::Vector3& v3, const math::Vector3& v4
		, float uStart=0.f, float uEnd=1.f, float vStart=0.f, float vEnd=1.f);
	void DrawQuad(const math::Vector3& v1, const math::Vector3& v2, const math::Vector3& v3, const math::Vector3& v4
		, const Color& cv1, const Color& cv2, const Color& cv3, const Color& cv4
		, float uStart=0.f, float uEnd=1.f, float vStart=0.f, float vEnd=1.f);

	// функция, необходимая для DrawBlic
	int getAlphaForBlic(int x, int y, float alpha, float local_time);

	// рисуем блик слева направо (тексуру по которой пускаем блик, кол-во столбцов в сетке текстуры, время от 0 до 1, яркость блика)
	void DrawBlic(Texture *tex, int count, float local_time, float alpha);

	// рисуем диагональный блик слева направо
	void DrawDiagonalBlic(Texture *tex, int countX, int countY, float fi, float time, int alpha, Color c = Color(255, 255, 255), bool draw_base = true);
	void DrawDiagonalBlic(Target *target, int countX, int countY, float fi, float time, int alpha, Color c = Color(255, 255, 255), bool draw_base = true);
	// аналог предыдущей функции, без использования текстуры 
	// (хорошо подходит для блика на полностью непрозрачных текстурах);
	// time всегда приводится к [0,1];
	// width определяет ширину бликовой полосы (0 -- отсутствует, 1 -- максимальная);
	// brightness >= 0 влияет на яркость полосы
	void DrawDiagonalGlare(const FRect& rect, int gridRows, int gridCols, float time, 
		float width = 1.0f, float brightness = 1.0f, int alpha = 255, Color c = Color(255, 255, 255));

	// появление текстуры слева-направо
	// blurring - количество пикселей размытия (для нечеткой границы)
	void DrawFromLeftToRight(Texture *tex, FPoint pos, float part, float blurring);
	
	// появление текстуры справа-налево
	// blurring - количество пикселей размытия (для нечеткой границы)
	void DrawFromRightToLeft(Texture *tex, FPoint pos, float part, float blurring);
	
	// появление текстуры снизу-вверх
	// blurring - количество пикселей размытия (для нечеткой границы)
	void DrawFromBottomToTop(Texture *tex, FPoint pos, float part, float blurring);
	
	// появление текстуры сверху-вниз
	// blurring - количество пикселей размытия (для нечеткой границы)
	void DrawFromTopToBottom(Texture *tex, FPoint pos, float part, float blurring);

	void DrawBlic(float w, float h, float wr, float hr, int count, float local_time, float alpha);
	
	/// Рисует отрезок [p1, p2)
	/// Важно: последний пиксел при этом не рисуется
	void DrawLine(FPoint p1, FPoint p2);

	// Рисует внутреннюю рамку прямоугольника толщиной 1 пиксель.
	// То есть если поверх нарисовать DrawRect, то рамка закроется
	// При этом MatrixScale не сможет повлиять на толщину.
	void DrawFrame(IRect r);

	// Проверить, входит ли прямоугольник в единичный квадрат (при текущей текстуре с адресацией CLAMP)
	bool CheckUV(const FRect& r);

	/// Стек цвета. Все Draw... методы (кроме Direct... методов) пересчитывают цвет/альфу точек, относительно заданных ранее. То есть, если был задан цвет ARGB=0.5,0.3,1,1, то рисование точки с цветом 1,0.5,0.5,1 приведёт к реальному значению 0.5,0.15,0.5,1. При восстановлении из стека предыдущего значения, пересчёт прекращается. Смотри SetColor и ResetColor.
	extern std::stack<Color> _colorStack;
	
	/// Push/PopMatrix helper
	class PushMatrix {
	public:
		PushMatrix(MatrixMode mode = MODELVIEW);
		~PushMatrix();
	private:
		MatrixMode _oldmode;
		MatrixMode _newmode;
	};
	
	/// Set/ResetColor helper
	class PushColor {
	public:
		PushColor(const Color& color);
		~PushColor();
	};
	
	/// Begin/EndAlphaMul helper
	class PushAlphaMul {
	public:
		PushAlphaMul(float alpha);
		~PushAlphaMul();
	};


	class PushBlendMode {
	public:
		PushBlendMode(Render::BlendMode blendMode);
		~PushBlendMode();
	private:
		Render::BlendMode _savedBlendMode;
	};

	class PushTexturing {
	public:
		PushTexturing(bool value);
		~PushTexturing();
	private:
		bool _savedValue;
	};
    
} // namespace Render

#endif //_REDNER_RENDERFUNC_H_
