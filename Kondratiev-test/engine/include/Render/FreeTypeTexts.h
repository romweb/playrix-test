#ifndef __RENDER_FREETYPE__
#define __RENDER_FREETYPE__

#include "Render/RenderDevice.h"
#include "Render/Text.h"
#include "Flash/core/FlashTextStyle.h"

#include <boost/intrusive_ptr.hpp>

namespace Render { namespace FreeType {

	struct FreeTypeFont
	{
		std::string font;
		std::vector<std::string> effects;
		FlashTextStyle style;
	};
    
    void ShowDebugInfo(bool enable );   

	void BindFont(const std::string& font_name);
	bool isFontLoaded(const std::string &font_name);
	bool IsCharSet( const std::string& font_name, int code);

	FreeTypeFont getFont(const std::string &fontName);
	void addFont(const std::string &fontName, const FreeTypeFont &freeTypeFont);

	void PrintString(int x, int y, const std::string& str, float scale = 1.0f, TextAlign align = LeftAlign, TextAlign valign = TopAlign, bool formatNumbers = true);
	void PrintString(float x, float y, const std::string& str, float scale = 1.0f, TextAlign align = LeftAlign, TextAlign valign = TopAlign, bool formatNumbers = true);
	void PrintString(const IPoint& pos, const std::string& str, float scale = 1.0f, TextAlign align = LeftAlign, TextAlign valign = TopAlign, bool formatNumbers = true);
	void PrintString(const FPoint& pos, const std::string& str, float scale = 1.0f, TextAlign align = LeftAlign, TextAlign valign = TopAlign, bool formatNumbers = true);
	void PrintStringInternal(float x, float y, const std::string& str, float scale = 1.0f, TextAlign align = LeftAlign, TextAlign valign = TopAlign);

	float getStringWidth(const std::string& str, const std::string& fontName);
	float getStringWidth(const std::string& str);

	float getFontHeight(const std::string& fontName);
	float getFontHeight();

	float getFontDescender(const std::string& fontName);
	float getFontDescender();
    
    float getLineSpacing(const std::string& fontName);
    float getLineSpacing();

	void LoadFont(rapidxml::xml_node<> *elem);
    void LoadStyledTextFromFont(rapidxml::xml_node<> *elem, std::string &font_style_id, std::string &font_style_description, std::string &font_style_atributes );

	class Text : public Render::Text
	{
	public:
		Text(const std::string& name);
		Text(const std::string& name, const std::string& text, const std::string& attributes);
		Text(const std::string& name, const std::string& text, const std::string& font, float width, TextAlign align, TextAlign valign);
		Text(const std::string& name, const std::string& text, FlashTextStyle *font, float width, TextAlign align, TextAlign valign);

		const std::string& GetName() const { return _name; }

		size_t GetMemoryInUse() const;
		
		float getWidth() const;
		float getHeight() const;
		float getBaseLine() const;

		TextAlign getHAlign() const;
		TextAlign getVAlign() const;
		float getScale() const;
		const std::string& getFontName() const;

		void getLettersCoords(std::vector<IPoint>& coords);
		
		IPoint GetSize() const;

		void Draw(const FPoint& pos = FPoint());
		void Draw(const FPoint& pos, float scale);

		void SetVariable(const std::string& name, const std::string& value);
		void UpdateVariables();

		const std::string& GetSource() const { return _text; }
		void SetSource(const std::string& source) { _text = source; }

		std::string ToString(const std::string& lineSeparatorChar = "\n");
        
        Render::TextPtr Clone() const;

		void SetPlain(bool value);
		void SetSnapToPixels(bool value);
	
	private:
		void ParseAttributes(const std::string& format);
	
	private:
		std::string _name;
		std::string _text;
		FlashTextStyle *_style;
		float _width;
		TextAlign _align;
		TextAlign _valign;
		float _scale;
		Color _color;
		bool _is_color_set;
		bool _is_plain;
		std::string _plainTextRef;
		bool _snapToPixels;
	};

	typedef boost::intrusive_ptr<Text> TextPtr;

}} // namespace FreeType namespace Render

#endif