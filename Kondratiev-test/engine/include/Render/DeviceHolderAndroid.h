/*
 *  DeviceHolderAndroid.h
 *  Engine
 *
 *  Copyright 2011 Playrix Entertainment. All rights reserved.
 *
 */

namespace Render {



class RenderDeviceInterface;
	
//
// �����, ������������� �� �����������
// ������ OpenGL ES, �������� ������� �������
// � �������� ���.
// (Android only)
//
// ��������� ������ ������ ���� ���������� ����������� ����������,
// ����������� �� Render::device.
//
class DeviceHolderAndroid {

public:
	
	DeviceHolderAndroid();
	
	~DeviceHolderAndroid();
	
	// ���������� ������ �� �������������� ������
	// (���� ����� ���������������� Render::device)
	RenderDeviceInterface& GetDevice();

private:
	
	RenderDeviceInterface* _device;
		// ���������� ������, �������� ����� �������� � ��������� �����
	
};

extern DeviceHolderAndroid deviceHolder;


} // namespace Render
