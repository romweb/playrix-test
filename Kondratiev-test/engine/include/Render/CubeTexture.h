#ifndef __CUBETEXTURE_H__
#define __CUBETEXTURE_H__

#pragma once

#include "Utils/FPoint.h"
#include "Utils/Math.hpp"
#include "Render/Texture.h"
#include "Core/Resource.h"

namespace Core
{
	class ResourceManager;
}

#if defined(ENGINE_TARGET_WIN32_DX9) && !defined(USING_GLES2)
struct IDirect3DCubeTexture9;
#	define DEVCUBETEXTURE IDirect3DCubeTexture9*
#endif

#if defined(USING_GLES2)
#	include <GLES2/gl2.h>
#	include <GLES2/gl2ext.h>
#	define DEVCUBETEXTURE GLuint
#endif

#ifdef ENGINE_TARGET_IPHONE
#	include <OpenGLES/ES1/gl.h>
#	define DEVCUBETEXTURE GLuint
#endif

#if defined(ENGINE_TARGET_LINUX)
#	include <GLES/gl.h>
#	include <GLES/glext.h>
#	define DEVCUBETEXTURE GLuint
#endif

#ifdef ENGINE_TARGET_MACOS
#	include <OpenGL/OpenGL.h>
#	define DEVCUBETEXTURE GLuint
#endif

namespace Render
{
	class RenderDeviceImpl;
	class PartialTexture;

	// ����� ���������� ��������
	class CubeTexture : public Resource
	{
		friend class Render::RenderDeviceImpl;
		friend class Render::RenderDeviceGLES2;

	public:
		std::string textureID;
		std::string path;

	protected:
		/// �������� ������ ���������
		bool binded;
		/// ���� �� �������� ���������
		bool wasUploaded;
		DEVCUBETEXTURE _tex;
		Texture::FilteringType _filter;
		Texture::AddressType _address;

	public:
		CubeTexture();
		virtual ~CubeTexture();
		
		static CubeTexture * CreateFromXml(rapidxml::xml_node<>* elem);
		
		const std::string& GetName() const { return textureID; }
		size_t GetMemoryInUse() const;

		/// �������� �� ���������, ��� ������������� ���������. ������������� ����������� ������������ �������� ��� ����������������� RenderDeviceImpl. ��. Texture::setDynamic
		bool Empty() const;
		void setFilteringType(Texture::FilteringType filter);
		void setAddressType(Texture::AddressType address);
		Texture::AddressType getAddressType() const { return _address; }
		/// ��������� ��������
		virtual void Upload();
		/// ��������� �������� � �������� �����
		virtual void Bind(int channel = 0, int stageOp = 0);
		/// ��������� ��������
		virtual void Release();
	};

	typedef boost::intrusive_ptr<CubeTexture> CubeTexturePtr;
}

#endif // __CUBETEXTURE_H__
