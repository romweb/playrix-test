#ifndef _CORE_CURSOR_H_
#define _CORE_CURSOR_H_

#include "Render/Texture.h"

namespace GUI {

class Cursor
{
public:
	static IPoint HOT_POINT_REMOVAL;

	Cursor();
	virtual ~Cursor();

	virtual void Load();
	virtual void setNormal();
	virtual void setActive();
	virtual void setPressed();
	virtual void setForbidden();
	virtual void Draw(const IPoint& pos);
	virtual bool isActive() const;
	virtual bool isPressed() const;
	virtual bool isForbidden() const;
	virtual void Update(float dt);

	static void setCursor(Cursor* cursor);
	static Cursor* getCursor();

	// Показать системный курсор мыши.
	static void ShowSystem();
	// Скрыть системный курсор мыши. Обычно чтобы его самим нарисовать.
	static void HideSystem();

	void Hide();
	void Show();
	bool isHidden() const;

private:
	Render::TexturePtr _normal;
	Render::TexturePtr _active;
	Render::TexturePtr _pressed;
	Render::TexturePtr _forbidden;
	Render::Texture* _current;
	bool _isActive;
	bool _isPressed;
	bool _hidden;

	static Cursor* _cursor;
};

} // namespace GUI

#endif
