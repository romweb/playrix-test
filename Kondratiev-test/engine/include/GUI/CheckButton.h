#ifndef __CHECKBUTTON_H__
#define __CHECKBUTTON_H__

#pragma once

#include "GUI/Widget.h"
#include "Render/Texture.h"
#include "Render/Text.h"

namespace GUI
{

class CheckButton : public Widget
{
public:
	CheckButton(const std::string& name, const std::string& str, IPoint pos, const std::string& font, float fontScale, IPoint textPos, bool state);
	CheckButton(const std::string& name, rapidxml::xml_node<>* xmlElement);

	virtual void Draw();
	virtual bool MouseDown(const IPoint& mouse_pos);

	virtual void AcceptMessage(const Message& message);
	virtual Message QueryState(const Message& message) const;

private:
	IPoint _textPosition;

    Render::Texture* _check;
	Render::Texture* _on_check;

	bool _state;

	Render::TextPtr _text;
};

}

#endif // __CHECKBUTTON_H__
