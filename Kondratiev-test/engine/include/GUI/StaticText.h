#ifndef __STATICTEXT_H__
#define __STATICTEXT_H__

#pragma once

#include "Widget.h"
#include "Render/Text.h"
#include "Render/RenderDevice.h"

namespace GUI
{
	/// Статический текст.

	/// Для создания экземпляра через Layer нужно использовать код типа:
	/// \code
	/// <Text name="Text2">
	/// 	<position x="400" y="300"/>
	/// 	<text id="TextID_in_Language_xml"/>
	/// </Text>
	/// \endcode

	class StaticText : public Widget
	{
    protected:
        Render::TextPtr _text;

	public:
		StaticText(const std::string name);
		StaticText(const std::string name, rapidxml::xml_node<>* xmlElement);

		virtual void Draw();
		virtual bool MouseDown(const IPoint& mouse_pos);
		virtual void AcceptMessage(const Message& message);

		/// Возвращает ссылку на текст.
        Render::Text* GetText();
	};

}

#endif // __STATICTEXT_H__
