#ifndef __PANEL_H__
#define __PANEL_H__

#pragma once

#include "Widget.h"
#include "Render/Texture.h"

namespace GUI
{

class Panel : public Widget
{
public:
	Panel(const std::string &name, rapidxml::xml_node<>* xmlElement);

	virtual void Draw();
	virtual bool MouseDown(const IPoint &mouse_pos);
	virtual void MouseMove(const IPoint &mouse_pos);
	virtual void AcceptMessage(const Message& message);

private:
	Render::Texture* _texture;
    float _texWidth;
	bool noInput;
	bool _flippedHor; // развернута ли текстура по горизонтали
    bool _mirror; // развернута ли текстура по горизонтали (с сохранением позиции)
};

}

#endif // __PANEL_H__
