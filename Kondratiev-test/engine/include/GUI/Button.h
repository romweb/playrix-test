#ifndef _BUTTON_H_
#define _BUTTON_H_

#include "types.h"
#include "GUI/Widget.h"
#include "Render/Texture.h"
#include "Render/Text.h"
#include "SmoothTextureChanger.h"

namespace GUI
{

class Button : public Widget
{
public:
	Button(const std::string& name, rapidxml::xml_node<>* xmlElement);
	virtual ~Button();

	virtual void Draw();
	virtual bool MouseDown(const IPoint& mouse_pos);
	virtual void MouseUp(const IPoint& mouse_pos);
	virtual void AcceptMessage(const Message& message);
	virtual void MouseMove(const IPoint& mouse_pos);
	virtual void Update(float dt);
	virtual bool HitTest(const IPoint& pos);

private:
	void InitWithXml(rapidxml::xml_node<>* xmlElement);

	enum ButtonState { ButtonNormal, ButtonActive, ButtonPress, ButtonDeactive };

	/// возможно ли включение заданного состояния
	bool StatePossible(ButtonState st);
	/// меняет состояние если можно, возвращает false если нельзя
	bool SetState(ButtonState st);
	
	void Activate();
	void Deactivate();
	void Fire();
	void ChangeFace();

private:
	std::string sampleOver, samplePress;

	float _scaleFont;
	IPoint _textPosition;
	Render::TextPtr textPressed;
	
	enum {
		INPUT, OUTPUT, BOTH
	} signal;

	RectOrient _orient;

	Render::Texture* _boardNormal;
	Render::Texture* _boardPress;
	Render::Texture* _boardActive;
	Render::Texture* _boardDeactive;

	SmoothTextureChanger *_smooth;

	Render::TextPtr _text;
	
	bool _clientRectHack;
		// используем ли хак с clientrect
	std::string fireMessage;
	
	ButtonState _state;
};

}

#endif // _BUTTON_H_
