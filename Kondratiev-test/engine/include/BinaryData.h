#ifndef _BINARYDATA_H_
#define _BINARYDATA_H_

typedef std::vector<uint8_t> ByteVector;

class BinaryDataLoader
{
public:

	BinaryDataLoader();

	bool Load(const std::string& filename);

	bool LoadBool();

	uint8_t LoadByte();

	int LoadInt();

	float LoadFloat();

	std::string LoadString();

private:

	size_t ptr;

	ByteVector data;

};

class BinaryDataSaver
{
public:

	BinaryDataSaver();

	bool Save(const std::string& filename);

	void SaveBool(bool value);

	void SaveByte(uint8_t value);

	void SaveInt(int value);

	void SaveFloat(float value);

	void SaveString(const std::string& value);

private:

	ByteVector data;

};

#endif
