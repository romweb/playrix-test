#ifndef __SKINNEDMESH_H__
#define __SKINNEDMESH_H__

#ifdef _MSC_VER
#pragma once
#endif // _MSC_VER

#include "SceneGraph/Mesh.h"
#include "SceneGraph/BoneContent.h"
#include "Utils/Vector3.h"

#include <boost/intrusive_ptr.hpp>

namespace SceneGraph {

///
/// Скелетный меш.
///
class SkinnedMesh : public Mesh {
public:
	typedef boost::intrusive_ptr<SkinnedMesh> HardPtr;

	typedef std::vector<BoneContent::HardPtr> BonesContent;
	typedef std::vector<BoneContent::Weights> VertexWeights;

	///
	/// Формат вершины.
	///
	struct Vertex {
		math::Vector3 position;
		union {
			float beta[4];
			struct {
				float weights[3];
				unsigned char indices[4];
			};
		};
		math::Vector3 normal;
		float tu, tv;
		
		static const unsigned long FVF;
	};
	
	Mesh::HardPtr Clone();
		/// Клонирует экземпляр объекта
	
	size_t GetMemoryInUse() const;
	
	void AddBoneContent(BoneContent::HardPtr content);
		/// Добавляет веса вершин для кости
	
	BonesContent& GetBonesContent();
		/// Возвращает коллекцию весов вершин меша
	
	void LoadData();
		/// Загружает данные меша в аппаратные буферы
	
	void UnloadData();
		/// Выгружает данные и освобождает аппаратные буферы
	
	void Draw() const;
		/// Рисует себя
	
	static SkinnedMesh::HardPtr Create();
		/// Создаёт объект меша, но не заполняет его данными

protected:
	SkinnedMesh(Render::MeshVertexFormat FVF);
	
	SkinnedMesh(const SkinnedMesh& rhs);
	
	SkinnedMesh& operator = (const SkinnedMesh& rhs);
	
	void UpdateBoneMatrices() const;
		/// Обновляет матрицы смещения костей

protected:
	BonesContent _bonesContent;
		/// Кости, к которым привязан данный меш
	
	VertexWeights _vertexWeights;
		/// Веса вершин, сформированные для блендинга костей
	
	typedef std::vector<math::Matrix4> Matrices;

	mutable Matrices _matrices;
	mutable Matrices _matricesOld;
		/// Массив матриц костей
	
	mutable int _differenceOfMatrices;
		/// Количество несовпадающих матриц после обновления костей
};

} // namespace SceneGraph

#endif // __SKINNEDMESH_H__
