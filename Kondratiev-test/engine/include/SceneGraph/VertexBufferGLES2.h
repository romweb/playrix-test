/*
 *  VertexBufferGL.h
 *  Engine_Mac
 *
 *  Created by andreyk on 11/19/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __VERTEXBUFFERGLES2_H__
#define __VERTEXBUFFERGLES2_H__
#ifdef _MSC_VER
#pragma once
#endif // _MSC_VER

#include "SceneGraph/DeviceBuffer.h"
#include "Render/RenderTypes.h"

#if defined(ENGINE_TARGET_WIN32) || defined(ENGINE_TARGET_LINUX)
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#elif defined(ENGINE_TARGET_IPHONE)
#include <OpenGLES/ES2/gl.h>
#elif defined(ENGINE_TARGET_MACOS)
#include <OpenGL/OpenGL.h>
#endif

#include <boost/intrusive_ptr.hpp>

namespace SceneGraph {
	
	///
	/// Буфер вершин.
	///
	class VertexBufferGLES2 : public DeviceBuffer {
	public:
		typedef boost::intrusive_ptr<VertexBufferGLES2> HardPtr;
		
		VertexBufferGLES2(Render::MeshVertexFormat FVF, std::size_t count, std::size_t stride);
		
		virtual ~VertexBufferGLES2();
		
		void* GetDeviceObject() const;
		/// Возвращает указатель на исходный буфер
        
        GLuint GetDeviceBuffer() const;
        /// Возвращает идентификатор буфера в случае использования VBO
		
        Render::MeshVertexFormat GetFVF() const;
		/// Возвращает формат вершин, хранящихся в буфере
		
		void* Lock();
		/// Блокирует весь буфер для доступа к его данным
		
		void Unlock();
		/// Разблокирует буфер.
		/// Должен быть вызван, если предшествующий вызов Lock был выполнен успешно.
		
	protected:
		Render::MeshVertexFormat _FVF;
		/// Формат вершины
        
        unsigned char* _lockBuffer;
        /// Буфер для заполнения данными
        
        std::size_t _lockBufferSize;
        /// Размер буфера
        
        GLuint _bufferID;
        /// Идентификатор буфера при использовании VBO

	};
	
} // namespace SceneGraph

#endif // __VERTEXBUFFERGLES2_H__
