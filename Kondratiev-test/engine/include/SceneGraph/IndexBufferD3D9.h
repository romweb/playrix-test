#ifndef __INDEXBUFFERD3D9_H__
#define __INDEXBUFFERD3D9_H__

#ifdef _MSC_VER
#pragma once
#endif // _MSC_VER

#include "DeviceBuffer.h"

#include <d3d9.h>

#include <boost/intrusive_ptr.hpp>

namespace SceneGraph {

///
/// ����� ��������.
///
class IndexBufferD3D9 : public DeviceBuffer {
public:
	typedef boost::intrusive_ptr<IndexBufferD3D9> HardPtr;
	
	IndexBufferD3D9(LPDIRECT3DINDEXBUFFER9 indices, std::size_t count, std::size_t stride);
	
	virtual ~IndexBufferD3D9();
	
	void* GetDeviceObject() const;
		/// ���������� ��������� �� ������ ����������� ������
	
	void* Lock();
		/// ��������� ���� ����� ��� ������� � ��� ������
	
	void Unlock();
		/// ������������ �����.
		/// ������ ���� ������, ���� �������������� ����� Lock ��� �������� �������.

protected:
	LPDIRECT3DINDEXBUFFER9 _indices;
		/// ����� ��������
};

} // namespace SceneGraph

#endif // __INDEXBUFFERD3D9_H__
