#ifndef __MESHDATA_H__
#define __MESHDATA_H__

#ifdef _MSC_VER
#pragma once
#endif // _MSC_VER

#include "Mesh.h"
#include "AxisAlignedBoundingBox.h"
#include "RefCounter.h"

namespace SceneGraph {

///
/// Данные меша.
///
class MeshData : public RefCounter {
public:
	typedef boost::intrusive_ptr<MeshData> HardPtr;

	typedef std::vector<Mesh::Vertex> Vertices;
	typedef std::vector<uint16_t> Indices;
	
	MeshData();
	
	MeshData(std::size_t verticesHint, std::size_t indicesHint);

	size_t GetMemoryInUse() const;

	bool IsEmpty() const { return _vertices.empty(); }

	void Clear();
		/// Удаляет данные
	
	void AddVertex(const Mesh::Vertex& vertex);
		/// Добавляет вершину в коллекцию
	
	void AddIndex(uint16_t index);
		/// Добавляет индекс в коллекцию
	
	const Vertices& GetVertexData() const { return _vertices; }
		/// Возвращает коллекцию вершин
	
	const Indices& GetIndexData() const { return _indices; }
		/// Возвращает коллекцию индексов
	
	const AxisAlignedBoundingBox& GetBoundingBox() const { return _aabbox; }
		/// Возвращает ограничивающий параллелепипед

private:
	Vertices _vertices;
		/// Данные буфера вершин
	
	Indices _indices;
		/// Данные буфера индексов
	
	AxisAlignedBoundingBox _aabbox;
		/// Ограничивающий параллелепипед для набора точек
};

} // namespace SceneGraph

#endif // __MESHDATA_H__
