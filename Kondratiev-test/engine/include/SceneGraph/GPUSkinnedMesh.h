#ifndef __GPUSKINNEDMESH_H__
#define __GPUSKINNEDMESH_H__

#ifdef _MSC_VER
#pragma once
#endif // _MSC_VER

#include "SkinnedMesh.h"
#include "Render/ShaderProgram.h"

#include <boost/intrusive_ptr.hpp>

namespace SceneGraph {

///
/// Специальная структура матрицы кости для аппаратного скиннинга
/// За счёт удаления одного столбца можно вычислять больше костей в шейдере
/// Но данная возможность существует только для hlsl (в GLSL 1.0 матрицы только 4 на 4)
///
class Matrix4x3
{
public:
	union {
		struct {
			float _11, _12, _13, _14;
			float _21, _22, _23, _24;
			float _31, _32, _33, _34;
		};
		float m[3][4];
		float v[12];
	};

	// Оператор присваивания транспонирует матрицу 4x4 и берёт только верхние 3 ряда матрицы
	Matrix4x3 & operator = (const math::Matrix4 &source)
	{
		_11 = source._11; _12 = source._21; _13 = source._31; _14 = source._41;
		_21 = source._12; _22 = source._22; _23 = source._32; _24 = source._42;
		_31 = source._13; _32 = source._23; _33 = source._33; _34 = source._43;
		return (*this);
	}
};

///
/// Скелетный меш с аппаратным скиннингом на GPU.
///
class GPUSkinnedMesh : public SkinnedMesh {
public:
	typedef boost::intrusive_ptr<GPUSkinnedMesh> HardPtr;
	
	struct Vertex {
		math::Vector3 position;
		union {
			float beta[5];
			struct {
				float weights[4];
				unsigned char indices[4];
			};
		};
		math::Vector3 normal;
		float tu, tv;
		
		static const unsigned long FVF;
	};
	
	Mesh::HardPtr Clone();
		/// Клонирует экземпляр объекта
	
	size_t GetMemoryInUse() const;
	
	void LoadData();
		/// Загружает данные меша в аппаратные буферы
	
	void UnloadData();
		/// Выгружает данные и освобождает аппаратные буферы
	
	void Draw() const;
		/// Рисует себя
	
	virtual bool Hit(const math::Vector3 &base, const math::Vector3 &dir) const;
		/// проверка попадания в модель с учетом текущей анимации модели

	static GPUSkinnedMesh::HardPtr Create();
		/// Создаёт объект меша, но не заполняет его данными
	
#if defined(ENGINE_TARGET_IPHONE) || defined(ENGINE_TARGET_LINUX)
	static void Use4x3BoneMatrix();
		/// использовать для скелетной анимации матрицы 4x3, вызывать в самом начале работы, нужен специальный шейдер
#endif
	
	virtual ~GPUSkinnedMesh();
	
protected:
	GPUSkinnedMesh(Render::MeshVertexFormat FVF);
	
	GPUSkinnedMesh(const GPUSkinnedMesh& rhs);
	
	GPUSkinnedMesh& operator = (const GPUSkinnedMesh& rhs);
	
private:
	Render::ShaderProgram *_program;
		/// Шейдер для аппаратного скиннинга
	void *_bones;
		/// Массив матриц костей
	unsigned int _bonesCount;
		/// Количество костей

#if defined(ENGINE_TARGET_IPHONE) || defined(ENGINE_TARGET_LINUX)
	static bool _use4x3Matrix;
	/// использовать ли для скелетной анимации матрицы 4x3
#endif
};

} // namespace SceneGraph

#endif // __GPUSKINNEDMESH_H__
