/*
 *  IndexBufferGL.h
 *  Engine_Mac
 *
 *  Created by andreyk on 11/19/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __INDEXBUFFERGLES2_H__
#define __INDEXBUFFERGLES2_H__

#ifdef _MSC_VER
#pragma once
#endif // _MSC_VER

#include "DeviceBuffer.h"

#if defined(ENGINE_TARGET_WIN32) || defined(ENGINE_TARGET_LINUX)
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#elif defined(ENGINE_TARGET_IPHONE)
#include <OpenGLES/ES2/gl.h>
#elif defined(ENGINE_TARGET_MACOS)
#include <OpenGL/OpenGL.h>
#endif
#include <boost/intrusive_ptr.hpp>

namespace SceneGraph {
	
	///
	/// Буфер индексов.
    /// Всегда используется VBO, т.к. этот буфер очень редко меняется программно, если вообще меняется
	///
	class IndexBufferGLES2 : public DeviceBuffer {
	public:
		typedef boost::intrusive_ptr<IndexBufferGLES2> HardPtr;
		
		IndexBufferGLES2(std::size_t count, std::size_t stride);

		virtual ~IndexBufferGLES2();
		
		void* GetDeviceObject() const;
		/// Возвращает указатель на объект аппаратного буфера
        
        GLuint GetDeviceBuffer() const;
        /// Возвращает идентификатор буфера в GL
		
		void* Lock();
		/// Блокирует весь буфер для доступа к его данным
		
		void Unlock();
		/// Разблокирует буфер.
		/// Должен быть вызван, если предшествующий вызов Lock был выполнен успешно.
		
	protected:
		unsigned char* _lockBuffer;
        /// Буфер для заполнения данными
        
        std::size_t _lockBufferSize;
        /// Размер буфера
        
        GLuint _bufferID;
        /// Идентификатор буфера в GL
	};
	
} // namespace SceneGraph

#endif // __INDEXBUFFERGLES2_H__
