#ifndef _MARKETING_RATINGCONTROL_H_
#define _MARKETING_RATINGCONTROL_H_

#include <boost/function.hpp>

namespace Marketing
{

class RatingControl
{
public:

	struct Action {
		enum Type {
			SHOWN = 0, // показали окошко
			ACCEPTED, // перешли в магазин
			REJECTED, // нажали Never
			POSTPONED // нажали Later
		};
	};

	typedef boost::function<void(Action::Type result)> CallbackType;

	static void Init(float firstShowTime = 1800.0f, float otherShowTime = 900.0f, const std::string url = "", CallbackType callback = CallbackType());

	static void Run();

	static void Update(float dt);

};

}

#endif // _MARKETING_RATINGCONTROL_H_