#ifndef __EFFECTSCONTAINER_H__
#define __EFFECTSCONTAINER_H__

#pragma once

#include "ParticleSystem.h"
#include "Render/SpriteBatch.h"

class EffectsContainer
{
public:
	ParticleEffect* AddEffect(const std::string& effectName);
	
	/// возвращает true если указатель числится среди живых
	bool IsEffectAlive(const ParticleEffect* eff) const;

	void Draw();
	void DrawBlend();

	void Update(float dt);

	void Pause();
	void Continue();
	void Finish();
	void KillAllEffects();
    
    bool IsFinished() const;

	void SetAlphaFactor(float alphaFactor);
	void SetRGBFactor(float redFactor, float greenFactor, float blueFactor);

private:
	typedef std::list<ParticleEffectPtr> ParticleEffects;
	
	ParticleEffects _effects;

	Render::SpriteBatchPtr _batch;
};

#endif // __EFFECTSCONTAINER_H__
