#ifndef PARTICLE_SYSTEM_HELP2_H
#define PARTICLE_SYSTEM_HELP2_H

#include "Render/Texture.h"
#include "Core/ResourceManager.h"
#include "Utils/Math.hpp"
#include "Utils/random.hpp"
#include "Spline.h"
#include "BinaryData.h"

class EmitterMask
{
public:
	EmitterMask();

	void SetMask(const unsigned char* data, int width, int height);
	FPoint RandomPoint(float scale = 1);

	void SetAlphaMin(unsigned char min) { alpha_min = min; }
	void SetAlphaMax(unsigned char max) { alpha_max = max; }
	void SetScaleX(float scale) { scale_x = scale < 0 ? 0 : scale; }
	void SetScaleY(float scale) { scale_y = scale < 0 ? 0 : scale; }
	unsigned char AlphaMin() { return alpha_min; }
	unsigned char AlphaMax() { return alpha_max; }
	float ScaleX() { return scale_x; }
	float ScaleY() { return scale_y; }
private:
	struct ProbablePoints
	{
		float p;
		struct Point { short x, y; };
		std::vector<Point> points;
		void Add(short x, short y)
		{
			Point point;
			point.x = x;
			point.y = y;
			points.push_back(point);
		}
	};

	std::vector<ProbablePoints> mask;
	float total_p;
	EngineFloat scale_x, scale_y;
	unsigned char alpha_min, alpha_max;
	unsigned char num_gradations;

	unsigned char Gradation(unsigned char p);
	ProbablePoints& GetProbablePoints(unsigned char alpha);
};

class TimeParam
{
public:
	struct Variation
	{
		float lower;
		float upper;
		Variation(): lower(0), upper(0) { }
		bool operator==(const Variation& rhs) const {
			return
				math::IsEqualFloat(lower, rhs.lower) &&
				math::IsEqualFloat(upper, rhs.upper);
		}
	};
	
	struct SplineKey
	{
		EngineFloat time;
		bool fixed_grad;
		Variation value, lgrad, rgrad;
		SplineKey() : time(0), fixed_grad(false) { }
		bool operator==(const SplineKey& rhs) const {
			return
				math::IsEqualFloat(time, rhs.time) &&
				fixed_grad == rhs.fixed_grad &&
				value == rhs.value &&
				lgrad == rhs.lgrad &&
				rgrad == rhs.rgrad;
		}
	};
	
	TimeParam();
	
	bool operator==(const TimeParam& rhs) const;

	float Reset(GradientSpline *spline);
	void Load(BinaryDataLoader *d);
	void Save(BinaryDataSaver *d, const char* name);
	void Load(rapidxml::xml_node<> *elem);
	void Save(rapidxml::xml_node<> *elem, const char* name);
	void SetValue(float value, float variation = 0);
	void SetScale(float scale) { _scale = scale; }

	float Removal() { return _removal; }
	
	rapidxml::xml_node<>* ToXml(rapidxml::xml_node<>* parent, const char* name) {
		rapidxml::xml_node<> *elem = parent->document()->allocate_node(rapidxml::node_element, TAG_PARAM);
		Save(elem, name);
		return elem;
	}
	
	size_t MemoryInUse() const {
		return _keys.capacity() * sizeof(SplineKey);
	}

	bool _similar;
#ifdef EDITOR_MODE
	float _value;
#endif
	float _scale;
	float _removal;
	std::vector<SplineKey> _keys;

public:
	static const char* TAG_PARAM;
	static const char* TAG_KEY;
	static const char* ATTR_NAME;
	static const char* ATTR_SIMILAR;
	static const char* ATTR_REMOVAL;
	static const char* ATTR_TIME;
	static const char* ATTR_FIXED_GRAD;
	static const char* ATTR_VALUE_LOWER;
	static const char* ATTR_VALUE_UPPER;
	static const char* ATTR_LGRAD_LOWER;
	static const char* ATTR_LGRAD_UPPER;
	static const char* ATTR_RGRAD_LOWER;
	static const char* ATTR_RGRAD_UPPER;

	float Lerp(const Variation &v, const float t) { return math::lerp<float>(v.lower, v.upper, t); }
};

#endif