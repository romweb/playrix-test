#ifndef PARTICLE_SYSTEM_VER2_H
#define PARTICLE_SYSTEM_VER2_H

#include "Particle/AbstractParticleSystem.h"
#include "Particle/ParticleSystemHelp2.h"

class ParticleSystemVer2: public AbstractParticleSystem
{
	friend class EffectManager;
	friend class EffectTree;

	ParticleSystemVer2(const ParticleSystemVer2&);
	ParticleSystemVer2& operator=(const ParticleSystemVer2&);
public:
	ParticleSystemVer2();
	virtual ~ParticleSystemVer2();
	
	virtual AbstractParticleSystem *Clone();

	virtual bool Load(BinaryDataLoader *d);
	virtual bool Load(rapidxml::xml_node<> *elem);
	virtual void Save(BinaryDataSaver *d);
	virtual void Save(rapidxml::xml_node<> *elem);
	virtual void Reset();
	virtual void Update(float dt);
	virtual void Draw(Render::SpriteBatch* batch = NULL);
	virtual void DrawBlend(Render::SpriteBatch* batch = NULL);
	virtual bool IsAdditive() const { return _addBlend; }
	virtual bool IsDead();
	virtual bool IsPermanent();
	virtual void Finish();
	virtual void SetScale(float scale);
	virtual void SetPositionOffset(float x, float y);
	virtual void SetAlphaFactor(float alpha);
	virtual void SetRGBFactor(float r, float g, float b);
	virtual void SetInitialForParamKey(const char *id, size_t key_index, float initial);
	virtual int FrameWidth() { return _frameWidth; }
	virtual int FrameHeight() { return _frameHeight; }
	virtual void DisableFog(bool disable);
	virtual const std::string& GetTextureName() const { return _texName; }
	virtual Render::Texture* GetTexture() const { return _tex; }

	struct Particle
	{
		float localTime;
		
		EngineFloat lifeTime;
		
		EngineFloat spinStart;

		EngineFloat emitX;
		EngineFloat emitY;

		float xStart;
		float yStart;

		float xValue;
		float yValue;

		float x, y, size, angle, v, spin, ySize;
		EngineFloat red, green, blue, alpha, fps;

		short _currentFrame, _firstFrame, _lastFrame;
		EngineFloat _speed;
		EngineFloat _progress;

		bool isVisible;

		GradientSpline xSpl, ySpl, sizeSpl, angleSpl, vSpl, spinSpl, redSpl, greenSpl, blueSpl, alphaSpl, fpsSpl, ySizeSpl;
		
		ParticleSystemVer2* _owner;

		void UpdateFrames(float dt, float normTime);
		void ResetFrames();
		void Update(float dt);
		void UpdateVelocity(float dt);
		void Reset();
		
		size_t MemoryInUse() const {
			return sizeof(*this) +
				xSpl.MemoryInUse() +
				ySpl.MemoryInUse() +
				sizeSpl.MemoryInUse() +
				angleSpl.MemoryInUse() +
				vSpl.MemoryInUse() +
				spinSpl.MemoryInUse() +
				redSpl.MemoryInUse() +
				greenSpl.MemoryInUse() +
				blueSpl.MemoryInUse() +
				alphaSpl.MemoryInUse() +
				fpsSpl.MemoryInUse() +
				ySizeSpl.MemoryInUse();
		}
	};

	enum EmitterType
	{
		POINT = 0,
		LINE,
		AREA,
		CIRCLE,
		MASK
	};
	
	EngineFloat emitterAngle, emitterRange, emitterOrientation;
	EngineFloat emitterSizeX, emitterSizeY, emitterFactor;
	EngineFloat _lifeInitial;
	
	size_t MemoryInUse() const {
		size_t memoryInUse =
			sizeof(*this) +
			x.MemoryInUse() +
			y.MemoryInUse() +
			size.MemoryInUse() +
			angle.MemoryInUse() +
			v.MemoryInUse() +
			spin.MemoryInUse() +
			colorRed.MemoryInUse() +
			colorGreen.MemoryInUse() +
			colorBlue.MemoryInUse() +
			colorAlpha.MemoryInUse() +
			fps.MemoryInUse() +
			ySize.MemoryInUse() +
			(_particles.capacity() - _particles.size()) * sizeof(Particle);
		
		for (std::vector<Particle>::const_iterator it = _particles.begin(); it != _particles.end(); ++it) {
			memoryInUse += (*it).MemoryInUse();
		}
		
		return memoryInUse;
	}

private:
	bool _isVisible;
	bool _isAnimation;
	short _numOfParticles;
	
	std::vector<Particle> _particles;
	
	float _scale;
	float _localTime;
	EngineFloat _lifeVariation;
	EngineFloat _startTime;

#ifdef EDITOR_MODE
	std::string name;
#endif
	std::string _texName;
	Render::Texture* _tex;

	ParticleSystemVer2* _flameLink;
	
	short _frameWidth, _frameHeight;

	bool _isScaledNonproportional;
	bool _isEqualCreateTime;

	bool _needParticleReborn;
	bool _needDeadCount;
	size_t _deadParticleCounter;
	bool _allParticlesIsDead;
	bool _addBlend;

	bool showEmitter;
	
	uint8_t emitType; // EmitterType
	
	std::string emitterMaskName;
	EmitterMask emitterMask;
	Render::Texture *emitterTex;

	// ��� ����������� � ParticleSystemComposite
	EngineFloat bornTime;
	EngineFloat _deadCountTime;
	bool _needStartDeadCounter;
	bool _isFreeze;
	bool _isDead;

	bool _disableFog;	//����, � ������� ����������� ����� �� ����� ��������� ������� � ������ ADD

	friend struct Particle;
	TimeParam x, y, size, angle, v, spin, colorRed, colorGreen, colorBlue, colorAlpha, fps, ySize;
	
	//static std::list<TimeParam> timeParamPool;
	//static TimeParam* GetOrCreateTimeParam(const TimeParam& param);
	//static TimeParam* GetOrLoadTimeParam(BinaryDataLoader* loader);
	//static TimeParam* GetOrLoadTimeParam(Xml::TiXmlElement* elem);

	EngineFloat redFactor, greenFactor, blueFactor, alphaFactor;

	float posX, posY;

	bool linkParticles;
	bool orientParticles;
	bool isVelocity;
	bool fixedAngleStep;

	FPoint _hotPoint;

private:
	rapidxml::xml_node<>* GetParamElement(rapidxml::xml_node<> *elem, const std::string& name);

	void LinkFlameEmitter(ParticleSystemVer2* ps);

	void SetParticleNumber(int numOfParticle);

	void RebornParticle(Particle& part, int index);

	void SetPositionScale(float xScale, float yScale);

	void Upload();
	void Release();
	void LoadTexture(const std::string& filename);
	void LoadTextureMask(const std::string& filename);

	void InternalDraw(Render::SpriteBatch* batch, float alpha);

	TimeParam * GetParam(const char *id);
};


#endif