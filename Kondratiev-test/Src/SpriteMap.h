#pragma once
using namespace std;

// ������ ���� ��������

template<typename T>
class SpriteMap
{
public:
	SpriteMap()
	{
	};

	virtual ~SpriteMap()
	{
	};

	typedef typename map<int, T> _SpriteMap;
	typedef typename map<int, T>::iterator _SpriteIter;

	// ��� �������� �
	_SpriteMap map; 

public:

	// ����� ���������� ������
	void Reset()
	{
		map.clear();
	}

	// �������� ��������� ���������� � ������� (�������) ������ �������� ���� (������ ��������)
	bool InBounds(FPoint position, _SpriteIter& resIter)
	{
		bool res = FALSE;

		for (_SpriteIter theIterator = map.begin(); theIterator != map.end(); theIterator++)
		{
			if (theIterator->second.InSpriteBound(position))
			{
				resIter = theIterator;
				res = TRUE;
				break; // TODO: ����� ���� � ������ ��������� ����� 
			}
		}

		return res;
	}

	// ���������� ������� � ���
	void Add(int id, T data)
	{
		data.id = id;
		map.insert(_SpriteMap::value_type(id, data));
	}

	// ��������� ������� � �� id
	T* Get(int id)
	{
		_SpriteBase* res = NULL;

		_SpriteIter theIterator; 
		theIterator = map.find(id);
		if (theIterator != map.end())
			res = &theIterator->second;

		return res;
	}

private:

};