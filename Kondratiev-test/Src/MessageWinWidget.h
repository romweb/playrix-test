#pragma once

class MessageWinWidget : public GUI::Widget
{
public:
	MessageWinWidget(const std::string& name, rapidxml::xml_node<>* elem);

	void Draw();
	bool MouseDown(const IPoint& mouse_pos);

private:
	void Init();
};
