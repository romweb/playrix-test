#pragma once

#include "TextureAtlas.h"

// ���� ����������� �������
enum DestroyTypes
{
	unknownDestroyType = 0,
	destroyOutBound = 1 // ������ ������������ ��� ������ �� �������� �������
};

// ���� ���� �������� �������
enum TargetTypes
{
	unknownTargetType = 0,
	randomInBoundTarget = 1, // ���� ������� ��������
	horizontalTarget = 2, // ���� - �������������� �������, ��������� 
	verticalTopTarget = 3, // ���� - ����
	verticalBottomTarget = 4 // ���� - ���
};

// ���� �������� �������
enum SpeedTypes
{
	unknownSpeedType = 0,
	randomSpeed = 1, // ��������� ��������
	constantSpeed = 2 // ���������� ��������
};

// ������� ����� ��� ��������. ��������� �� ������ � �����������

class SpriteBase
{
public:
	SpriteBase();
	virtual ~SpriteBase();

	// ������������� �������. ���������� ��������� �������, ����������� �� ������
	virtual void Init(int id, SpriteParams* param);

public:

	// ��������� ������, ����������� �� ������
	SpriteParams* param;
	// ������� �������
	FPoint position;
	// ���� �������� �������
	FPoint target;
	// ���������� ������������� �������
	int id;
	// ���������� �������� ����������� �������
	float speedFactor;
	// ��� �������
	int tag;
	// ���������� ���������� ���������
	float distanceFactor;

public:

	// ��������� �������� ������
	void EnableBoundControl(bool enable = true);
	// ������� ���������� �������� ������
	void SetBoundControl(float leftBottomX, float rightTopX, float leftBottomY, float rightTopY);
	// �������� ��������� ����� ������� � �������
	bool InBound(FPoint position);
	
	// ��������� �������� ����������� ������� � � ���� (����� ���� ���������� ��� ���������)
	void SetSpeedFactor(float speed, SpeedTypes type = SpeedTypes::constantSpeed, float speed2 = -1.0);
	
	// ��������� ���� ����������� �������. ����� ���� ��������� ���� ����������� � ����� ������.
	// distance - ���������, �� ���������� ������� ���� ��������� �����������  
	bool SetTarget(float distance, TargetTypes type = TargetTypes::randomInBoundTarget);
	
	// ��������� ���� ����������� �������
	void SetDestroyType(DestroyTypes type = unknownDestroyType);
	
	// �������� ����� ����������� �������
	bool NeedDestroy();

	// ������������� ������� �������
	void CorrectPosition(float factor);

	// �������� ��������� ����� � ������� ������� 
	bool InSpriteBound(FPoint pos);

	// ����������� �������, ����� ���� �������������� ������-���������� ��� ������������� ����������� ���-�� �� ��������
	virtual void Transform(float dt);

	// ����������� �������, ������� � ������-���������� ��� ������������� ���-�� ������� ��� �������� �������
	virtual void DestroyFunc(){};

private:

	// ���� �������� ������
	bool boundControlEnabled;
	// �������
	FRect uvBound;

	// ���� ������������� �������� �������
	bool needDestroy;
	// ����, ����� true ���� ������ �� ���� �� ��������� �� �����
	bool noRenderer;

	SpeedTypes speedType;
	TargetTypes targetType;
	DestroyTypes destroyType;
};