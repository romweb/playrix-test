#pragma once

/// ������ ��� ������ ����

class BackgroundWidget : public GUI::Widget
{
public:
	BackgroundWidget(const std::string& name, rapidxml::xml_node<>* elem);
	void Draw();

private:
	void Init();

private:
	Render::Texture *textureBackground;
};
