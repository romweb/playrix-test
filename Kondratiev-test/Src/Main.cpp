#include "stdafx.h"
#include "TestApp.h"
using namespace std;

#include "GlobalDatas.h"
GlobalDatas _g; // ����� � ����������� �������

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	_g.Init(); // ������������� ������ � ����������� �������

	Core::fileSystem.SetWriteDirectory(WRITEDIR);
	Log::log.AddSink(new Log::HtmlFileLogSink(LOG_FILENAME, true));

	try
	{
		// ������ � ��������� ���� � �����������
		// TODO: ����� ������� ����� ��� ������ ���������� �� XML

		IO::FileStream stream(SETTINGS_FILE);
		IO::TextReader reader(&stream);

		string str, name, value;

		str = reader.ReadLine();
		if (utils::ReadNvp(str, name, value) &&
			name == SETTINGPARAM_ASSETSPATH &&
			!value.empty())
		{
			_g.paramAssetPath = value;
			Core::fileSystem.MountDirectory(value);
		}

		value.clear();
		str = reader.ReadLine();
		if (utils::ReadNvp(str, name, value) &&
			name == SETTINGPARAM_COUNTTARGET &&
			!value.empty())
		{
			_g.paramCountTarget = Int::Parse(value);
		}
				
		value.clear();
		str = reader.ReadLine();
		if (utils::ReadNvp(str, name, value) &&
			name == SETTINGPARAM_SPEED &&
			!value.empty())
		{
			_g.paramSpeed = Int::Parse(value);
		}

		value.clear();
		str = reader.ReadLine();
		if (utils::ReadNvp(str, name, value) &&
			name == SETTINGPARAM_TIME &&
			!value.empty())
		{
			_g.paramTime = Int::Parse(value);
		}

		if (_g.paramAssetPath.empty())
			throw runtime_error(string("Failed loading paramAssetPath")); // TODO: ������ �.�. ������� � ��������� ���� ��� �����������?
		if (_g.paramCountTarget == -1)
			throw runtime_error(string("Failed loading paramCountTarget"));
		if (_g.paramSpeed == -1)
			throw runtime_error(string("Failed loading paramSpeed"));
		if (_g.paramTime == -1)
			throw runtime_error(string("Failed loading paramTime"));
	}
	catch(exception& e)
	{
		string message;
		message = "Exception: ";
		message += e.what();
		Log::log.WriteFatal(message);
		Log::log.WriteFatal("Can't load settings.");
	}

	bool fullscreen = false;
	TestApp app(hInstance, nCmdShow, fullscreen);

	app.SETTINGS_REG_KEY = MYSETTINGS_REG_KEY;
	app.APPLICATION_NAME = MYAPPLICATION_NAME;
	app.WINDOW_CLASS_NAME = MYWINDOW_CLASS_NAME;

	_g.app = (Core::Application*)&app; // ���������� ������ �� ���� ���������� (��������� ��� ������� � �������, � ���������) - ����� ���� ������ �����?!

	app.Init(true);

	app.Start();
	app.ShutDown();
	
	return 0;
}

