#pragma once
using namespace std;

#include "TextureAtlas.h"
#include "SpriteBubble.h"
#include "SpriteShip.h"
#include "SpriteShot.h"
#include "SpriteMap.h"
#include "SpriteMoney.h"

#define WRITEDIR "./write_directory"
#define LOG_FILENAME "kondratiev-test-log.htm"
#define MYSETTINGS_REG_KEY "Software\\Kondratiev\\Test"
#define MYWINDOW_CLASS_NAME "KONDRATIEV_PLAYRIX_TEST_CLASS"
#define MYAPPLICATION_NAME "KondratievPlayrixTest"
#define SETTINGS_FILE "input.txt"

#define SETTINGPARAM_ASSETSPATH "AssetsPath"
#define SETTINGPARAM_COUNTTARGET "CountTarget"
#define SETTINGPARAM_SPEED "Speed"
#define SETTINGPARAM_TIME "Time"

#define SPRITENAME_SHIP "ship"
#define SPRITENAME_REDBUBBLE "red-bubble"
#define SPRITENAME_YELLOWBUBBLE "yellow-bubble"
#define SPRITENAME_BLUEBUBBLE "blue-bubble"
#define SPRITENAME_PINKBUBBLE "pink-bubble"
#define SPRITENAME_SHOT "shot"
#define SPRITENAME_MONEY "money"
#define TEXTURE_BACKGROUND "Background"

#define SOUND_BACKGROUND "Sound"
#define SOUND_EXPLODE "Explode"

// ������� ����
enum GameStatus
{
	playing = 0,
	stoppedWin = 1,
	stoppedLost = 2
};

// ����, ������������ � ��������-��������
enum GameTags
{
	unknownTag = 0,
	destroyObjectTag = 1,
	playerTag = 2
};

// ������� ������ �����
enum BubbleIndex
{
	red = 0,
	yellow = 1,
	blue = 2,
	pink = 3
};
const BubbleIndex BubbleIndexEnd = BubbleIndex::pink;

typedef SpriteMap<SpriteBubble> BubblesMap;
typedef SpriteMap<SpriteShot> ShotsMap;
typedef SpriteMap<SpriteMoney> MoneysMap;

// ����� � ����������� �������. �������� � ������ �������.
class GlobalDatas
{
public:
	GlobalDatas()
	{
	}

	virtual ~GlobalDatas()
	{
	}

	// ����� ���������� ������
	void Restart()
	{
		bubbleCounter = moneyIndex = shotCounter = 0;
		score = 0;

		bubbles.Reset();
		shots.Reset();
		moneys.Reset();

		effects.KillAllEffects();
	}

	// �������������� ������������� ���������� ������
	void Init()
	{
		paramCountTarget = paramSpeed = paramTime = -1;
		screenWidth = screenHeight = 0;
		app = NULL;
		
		// TODO: ������� ��� ��������� � ����������� ����
		shipSpeedMin = 0.2f;
		shipSpeedMax = 0.5f;
		bubbleSpeedMin = 0.1f;
		bubbleSpeedMax = 1.0f;
		shotOffsetX = 65.0f;
		shotEffDictanceX = 7.0f;
		shotEffDictanceY = -30.0f;
		moneySpeed = 0.5f;
		bubbleDistance = 20.0f;

		Restart();

		statusGame = playing;
	}

public:
	
	// �������� ����������������� ����� - ���� � ������
	string paramAssetPath;
	// �������� ����������������� ����� - ���������� ����� (�����)
	int paramCountTarget;
	// �������� ����������������� ����� - ������� ��������
	int paramSpeed;
	// �������� ����������������� ����� - ����� ���������� �� ����, � ���
	int paramTime;

public:

	// �������� ������� �����
	float moneySpeed;
	// ����������� �������� �������
	float shipSpeedMin;
	// ������������ �������� �������
	float shipSpeedMax;
	// ����������� �������� �����
	float bubbleSpeedMin;
	// ������������ �������� �����
    float bubbleSpeedMax;
	// ���������� ��������� ��� ����� - ��������� ����� ��� �� ��������������.
	float bubbleDistance;
	// �������� ������� �� X ������������ ������ ����� ����� ������� ������� 
	float shotOffsetX;
	// �������� ������� �� X ������������ ������ ����� ����� ������� ��������
	float shotEffDictanceX;
	// �������� ������� �� Y ������������ ������ ����� ����� ������� ��������
	float shotEffDictanceY;

public:

	// ������ �������
	SpriteShip ship;
	// ��� �����
	BubblesMap bubbles;
	// ��� ���������
	ShotsMap shots;
	// ��� �����
	MoneysMap moneys;
	// ��������� ��������
	EffectsContainer effects;

	// ����� �������
	TextureAtlas atlas;

	// ���������� ����� �� ������
	int bubbleCounter;

	// ���������� ������ ��������� ��� ���������� � ���. ����� ��������� �� �����
	int shotCounter;
	// ���������� ������ ����� ��� ���������� � ���
	int moneyIndex;

	// ���� ����
	int score;

	// ����� ������ ����
	TimeType timeStart;

	// ������ ����
	GameStatus statusGame;

	// ������ ������
	int screenWidth;
	// ������ ������
	int screenHeight;

	// ������ �� ����� ����������
	Core::Application* app;
};
