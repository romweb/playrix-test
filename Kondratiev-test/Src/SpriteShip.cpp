#include "stdafx.h"
#include "SpriteShip.h"

// ������ �������

SpriteShip::SpriteShip():
	SpriteBase()
{
}

SpriteShip::~SpriteShip()
{
}

// ������������� �������
void SpriteShip::Init(int id, SpriteParams* param)
{
	SpriteBase::Init(id, param);

	// TODO: ����� �������� ��� �������� ��� �������� ������������, ��������, �� ���������� ������ ��� � ����������
	
	SetBoundControl(-150.0f, (float)_g.screenWidth, 0.0f, (float)_g.screenHeight); // ������������� ������� - �����-������ ������� ������ ���������
	SetSpeedFactor(_g.shipSpeedMin, SpeedTypes::randomSpeed, _g.shipSpeedMax); // ��������� ��������
	SetTarget(150.0f, horizontalTarget); // ����  - �����-������ ����� ������ ���������

	// ��������� ������� - �����-��� ������
	position.x = (_g.screenWidth - 150.0f) / 2.0f; 
	position.y = 30.0f;

	tag  = (int)GameTags::playerTag;
}
