#include "stdafx.h"
#include "SpriteMoney.h"

// ������ �����

SpriteMoney::SpriteMoney():
	SpriteBase()
{
}

SpriteMoney::~SpriteMoney()
{
}

// ������������� �������. ����� �������������� pos ��� ������������� �� ������ �����������.
void SpriteMoney::Init(int id, SpriteParams* param, FPoint pos)
{
	SpriteBase::Init(id, param);

	position = pos; // ����� ������� �������

	SetBoundControl(0.0f, (float)_g.screenWidth, 0.0f, (float)_g.screenHeight); // ������� - ��� ������
	SetSpeedFactor(_g.moneySpeed, SpeedTypes::constantSpeed); // ���������� ��������
	SetTarget(0, verticalBottomTarget); // ���� - ����
	SetDestroyType(destroyOutBound); // ����������� ����� ���������� ������
	
	tag  = (int)GameTags::destroyObjectTag;
}