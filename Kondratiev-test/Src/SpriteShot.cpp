#include "stdafx.h"
#include "SpriteShot.h"

// ������ ��������

SpriteShot::SpriteShot():
	SpriteBase(), eff(NULL)
{
}

SpriteShot::~SpriteShot()
{
}

// ������������� �������. ������������ ������� ������� shipPositionX.
void SpriteShot::Init(int id, SpriteParams* param, float shipPositionX)
{
	SpriteBase::Init(id, param);

	position = FPoint(shipPositionX + _g.shotOffsetX, 165.0f); // ���������� ��������� � �������

	SetBoundControl(30.0f, (float)_g.screenWidth - 30.0f, 0, (float)_g.screenHeight - 30.0f); // ����� ������� - ����
	SetTarget(0, verticalTopTarget); // ����� ���� - ����
	SetDestroyType(destroyOutBound); // ����� ��������� ����� ���������� ����
	SetSpeedFactor(_g.paramSpeed, SpeedTypes::constantSpeed); // ���������� ��������

	tag  = (int)GameTags::destroyObjectTag;
}

// ����������� �������-�������� � �������
void SpriteShot::Transform(float dt)
{
	SpriteBase::Transform(dt); // ���������� ��� ������
	if (eff)
	{
		// ���������� �� �������� ������
		eff->posX = position.x + _g.shotEffDictanceX;
		eff->posY = position.y + _g.shotEffDictanceY;
		eff->Reset();
		eff->Update(dt);
	}
}
