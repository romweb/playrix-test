#include "stdafx.h"
#include "GameObjectsWidget.h"

GameObjectsWidget::GameObjectsWidget(const std::string& name, rapidxml::xml_node<>* elem)
	: Widget(name), shipPositionX(0), shotSprite(NULL)
{
	Init();
}

// ������������� ���� ��� ������� ����������
void GameObjectsWidget::Init()
{
	try
	{
		if (_g.atlas.Open("Sprites")) // ��������� �������� � ������� ��������
		{
			// �������������� �������
			_g.atlas.AddSprite(SPRITENAME_SHIP, 0.02, 0.28, 0.0, 0.68, 148, 148);
			_g.atlas.AddSprite(SPRITENAME_YELLOWBUBBLE, 0.02, 0.12, 0.7, 0.98, 55, 55);
			_g.atlas.AddSprite(SPRITENAME_BLUEBUBBLE, 0.12, 0.22, 0.7, 0.98, 55, 55);
			_g.atlas.AddSprite(SPRITENAME_PINKBUBBLE, 0.24, 0.34, 0.7, 0.98, 55, 55);
			_g.atlas.AddSprite(SPRITENAME_REDBUBBLE, 0.36, 0.46, 0.7, 0.98, 35, 35);
			_g.atlas.AddSprite(SPRITENAME_SHOT, 0.28, 0.32, 0.52, 0.68, 18, 28);
			_g.atlas.AddSprite(SPRITENAME_MONEY, 0.46, 0.52, 0.8, 0.98, 33, 35);
		}
		else
			throw runtime_error(string("Can't sprites initialize"));

		Reset();
	}
	catch(exception& e)
	{
		Log::log.WriteFatal(string(e.what()));
	}
}

// ������������� ���������� ����. �������� ������� ��������.
void GameObjectsWidget::Reset()
{
	SpriteParams* shipParams = _g.atlas.GetSpriteParams(SPRITENAME_SHIP);
	if (shipParams)
		_g.ship.Init(0, shipParams);
	else
		throw runtime_error(string("Can't ship initialize"));

	try
	{
		_g.bubbleCounter = _g.paramCountTarget;

		// ������ ���� (���-�� �� ������� � ���������������� ����� input.txt)
		// ��������� ��������� ������� ��������
		for (int i = 0; i < _g.paramCountTarget; i++)
		{
			SpriteParams* param = NULL;
			BubbleIndex color = (BubbleIndex)math::random((int)BubbleIndex::red, (int)BubbleIndexEnd);
			switch (color)
			{
			case BubbleIndex::red:
				param = _g.atlas.GetSpriteParams(SPRITENAME_REDBUBBLE);
				break;
			case BubbleIndex::blue:
				param = _g.atlas.GetSpriteParams(SPRITENAME_BLUEBUBBLE);
				break;
			case BubbleIndex::yellow:
				param = _g.atlas.GetSpriteParams(SPRITENAME_YELLOWBUBBLE);
				break;
			case BubbleIndex::pink:
				param = _g.atlas.GetSpriteParams(SPRITENAME_PINKBUBBLE);
				break;
			}

			if (param != NULL)
			{
				SpriteBubble bubble;
				bubble.Init(i, param);
				_g.bubbles.Add(i, bubble); // ��������� ��� � ���
			}
			else
				throw runtime_error(string("Can't bubble initialize"));
		}

		// �������� ��������� �������-�������
		shotSprite = _g.atlas.GetSpriteParams(SPRITENAME_SHOT);
	}
	catch(exception& e)
	{
		Log::log.WriteFatal(string(e.what()));
	}
}

// ���������� ����
void GameObjectsWidget::RestartGame()
{
	_g.Restart(); // ������������� ���������� �������� ������ (��� �� �������� ����)
	Reset(); // ������ ������� �������
	_g.timeStart = _g.app->timer.getTime(); // ����� ������ ������� �������
	_g.statusGame = playing; // ������������� ������� �����
}

void GameObjectsWidget::Draw() 
{
	if (_g.statusGame == playing) // ���� ������� ������� �����, �� ������ ������� �������
	{
		// ������ ������� - ������� ������� � Update
		_g.atlas.DrawSprite(SPRITENAME_SHIP, _g.ship.position.x , _g.ship.position.y);

		// ������ �������� ���� � ������, ���� ��� ����
		for (BubblesMap::_SpriteIter theIterator = _g.bubbles.map.begin(); theIterator != _g.bubbles.map.end(); theIterator++)
			_g.atlas.DrawSprite(theIterator->second.param->name, theIterator->second.position.x, theIterator->second.position.y);

		// ������ �������� ���� � ��������, ���� ��� ����
		for (MoneysMap::_SpriteIter theIterator = _g.moneys.map.begin(); theIterator != _g.moneys.map.end(); theIterator++)
			_g.atlas.DrawSprite(theIterator->second.param->name, theIterator->second.position.x, theIterator->second.position.y);

		// ������ �������� ���� � ����������, ���� ��� ����
		for (ShotsMap::_SpriteIter theIterator = _g.shots.map.begin(); theIterator != _g.shots.map.end(); theIterator++)
		{
			// �������� ��������� ������� ��������. ��������� ������������ ������� � ����� ������� ������ ����.
			// �� ������� �� ����� �������� � ���������� ��� ������� ���� ����� ������� �� ��������.
			theIterator->second.CorrectPosition(_g.ship.position.x + _g.shotOffsetX);

			_g.atlas.DrawSprite(theIterator->second.param->name, theIterator->second.position.x, theIterator->second.position.y, true);
		}

		// ������ �������
		_g.effects.Draw();

		// TODO: ����� ������� ���������� ���������� �������� 
	}
	else // ���� �� ������� ������� �����, �� ������� ��������� 
	{
		string message;
		message = (_g.statusGame == stoppedWin) ? "YOU WIN! :)" : "YOU LOST! :("; // TODO: ��������� ������ � ������� ��� �����������

		Render::BindFont("arialbig");
		Render::PrintString(_g.screenWidth / 2, _g.screenHeight / 2 + 100, message, 1.0f, CenterAlign);
		Render::BindFont("arial");
		Render::PrintString(_g.screenWidth / 2, _g.screenHeight / 2 + 50, "Press CTRL key and click mouse for restart game", 1.0f, CenterAlign);
	}
}

void GameObjectsWidget::Update(float dt)
{
	if (_g.statusGame == playing) // ���� ������� ������� �����, �� ������ ������� �������
	{
		// ���������� �������...
		_g.ship.Transform(dt);
		shipPositionX = _g.ship.position.x; // ...� ���������� ��� ������� ��� ����������� ��������

		// ���������� ���� � ������������ ��������� ��� �� ��� ���������, ������� ���� ���������� � ����
		// ���� ������������� �������� ������������ � SpriteBase
		for (BubblesMap::_SpriteIter theIterator = _g.bubbles.map.begin(); theIterator != _g.bubbles.map.end();)
		{
			if (theIterator->second.NeedDestroy())
				_g.bubbles.map.erase(theIterator++); // �����������
			else
			{
				theIterator->second.Transform(dt); // ����������
				theIterator++;
			}
		}

		// ���������� �������-������� � ��������� ���� �� �� �������
		for (ShotsMap::_SpriteIter theIterator = _g.shots.map.begin(); theIterator != _g.shots.map.end();)
		{
			if (theIterator->second.NeedDestroy())
				_g.shots.map.erase(theIterator++); // ��������
			else
			{
				// ��������� ��������� ��������� �������� � ���������� ��������-�����
				// ���� ���� ��������� - ��������� ������� �������� ���� (����, �������, ��������)
				// TODO: ������� InBounds ��������� ������ ���� ���������, � ����� ���� ���������
				BubblesMap::_SpriteIter bubbleIterator;
				if (_g.bubbles.InBounds(theIterator->second.position, bubbleIterator)) 
				{
					// ���� ������������ �������� ���� bubbleIterator 
					_g.shots.map.erase(theIterator++); // �������� ��������
					bubbleIterator->second.DestroyFunc();  // �������� ����
				}
				else
				{
					theIterator->second.Transform(dt); // �����������
					theIterator++;
				}
			}
		}

		// ���������� � ������� ������
		for (MoneysMap::_SpriteIter theIterator = _g.moneys.map.begin(); theIterator != _g.moneys.map.end();)
		{
			if (theIterator->second.NeedDestroy())
				_g.moneys.map.erase(theIterator++);
			else
			{
				theIterator->second.Transform(dt);
				theIterator++;
			}
		}

		_g.effects.Update(dt);
	}
}

bool GameObjectsWidget::MouseDown(const IPoint &mouse_pos)
{
	if (Core::mainInput.GetMouseLeftButton() || Core::mainInput.GetMouseRightButton())
	{
		if (_g.statusGame == playing) // ����� ���� �������
		{
			// ������ ������ ���� - ��������

			try
			{
				if (shotSprite)
				{
					// ������������� ������ ��������
					SpriteShot shot;
					shot.Init(_g.shotCounter, shotSprite, shipPositionX);
					shot.eff = _g.effects.AddEffect("train"); // ��������� ������ (����� ��������� �� ��������� � ������������ ����������)
					if (shot.eff)
					{
						shot.eff->posX = shot.position.x + _g.shotEffDictanceX;
						shot.eff->posY = shot.position.y + _g.shotEffDictanceY;
						shot.eff->Reset();
					}
					_g.shots.Add(_g.shotCounter, shot); // ��������� � ���

					MM::manager.PlaySample("Shot"); // ����������� ����
				
					_g.shotCounter ++; // ����������� ������� ��������� (��������� �� ����� � StatsWidget)
				}
				else
					throw runtime_error(string("Can't shot initialize"));
			}
			catch(exception& e)
			{
				Log::log.WriteFatal(string(e.what()));
			}
		}
		else if (Core::mainInput.IsControlKeyDown()) // ���� �� ������� ����� � ����� CTRL � ������ ����...
		{
			RestartGame(); // ...������������� ����
		}
	}

	return false;
}