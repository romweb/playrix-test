#pragma once

// ������ ��� ������ ������� �������� - ��������� ��� ������� ������

class GameObjectsWidget : public GUI::Widget
{
public:
	GameObjectsWidget(const std::string& name, rapidxml::xml_node<>* elem);

	void Draw();
	void Update(float dt);
	bool MouseDown(const IPoint& mouse_pos);

	// ������������� ���������� ����. �������� ������� ��������.
	void Reset();

	// ���������� ����
	void RestartGame();

private:
	// ������������� ���� ��� ������� ����������
	void Init();

private:

	// ������� ������� ������� �� X
	float shipPositionX;
	// ��������� ������� �������� �� ������
	SpriteParams* shotSprite;
};
