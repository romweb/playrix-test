#include "stdafx.h"
#include "SpriteBubble.h"
#include "SpriteMoney.h"

// ������ �����

SpriteBubble::SpriteBubble():
	SpriteBase()
{
}

SpriteBubble::~SpriteBubble()
{
}

// ������������� ������� �����
void SpriteBubble::Init(int id, SpriteParams* param)
{
	SpriteBase::Init(id, param);

	// TODO: ����� �������� ��� �������� ��� �������� ������������, ��������, �� ���������� ������ ��� � ����������

	SetBoundControl(30.0f, (float)_g.screenWidth - 30.0f, 160.0f, (float)_g.screenHeight - 30.0f); // ������� - ���� ������ ������
	SetSpeedFactor(_g.bubbleSpeedMin, SpeedTypes::randomSpeed, _g.bubbleSpeedMax); // ��������� ��������
	SetTarget(_g.bubbleDistance); // ��������� ���� ����������� 
	position = FPoint(math::random(30.0f, (float)_g.screenWidth - 30.0f), math::random(160.0f, (float)_g.screenHeight - 30.0f)); // ��������� ��������� ������� � �������� ������

	tag  = (int)GameTags::destroyObjectTag;

}

// ������� ��������� ��� ����������� ����
void SpriteBubble::DestroyFunc()
{
	try
	{
		// �������� ������ ������
		SpriteParams* p = _g.atlas.GetSpriteParams(SPRITENAME_MONEY);
		if (p)
		{
			// �������������� ������
			ParticleEffect* eff = _g.effects.AddEffect("bubble2");
			if (eff)
			{
				eff->posX = position.x + 25;
				eff->posY = position.y + 25;
				eff->Reset();
			}

			// ����� ��������� ������� � ��������� � ���
			SpriteMoney money;
			money.Init(_g.moneyIndex, p, position);
			_g.moneys.Add(_g.moneyIndex, money);
			
			_g.moneyIndex++; // ����������� ���������� ������ ����� - ��������� ��� ���������� � ���
			
			_g.bubbleCounter--; // ��������� ������ ����� - ��������� �� �����

			// ����������� ���� ������
			MM::manager.PlaySample(SOUND_EXPLODE);

			// ����������� ���� ���� - ��� �������� ����� ������
			if (param->name == SPRITENAME_REDBUBBLE)
				_g.score += 200;
			else
				_g.score += 100;
		}
		else
			throw runtime_error(string("Can't money initialize"));
	}
	catch(exception& e)
	{
		Log::log.WriteFatal(string(e.what()));
	}

	SpriteBase::DestroyFunc();
}
