#include "stdafx.h"
#include "StatsWidget.h"

// ������ ������ ����������

StatsWidget::StatsWidget(const std::string& name, rapidxml::xml_node<>* elem)
	: Widget(name)
{
	Init();
}

// �������������
void StatsWidget::Init()
{
	if (_g.app)
		_g.timeStart = _g.app->timer.getTime(); // ��������� �������� �������
}

void StatsWidget::Draw() 
{
	// ������� ����� ��������� �������
	int timerSec = _g.paramTime - (int)_g.app->timer.Interval(_g.timeStart, _g.app->timer.getTime());
	if (timerSec < 0)
		timerSec = 0;

	if (timerSec == 0 || _g.bubbleCounter == 0) // ���� ��������
	{
		// ������� �� ����� ����
		_g.statusGame = (_g.bubbleCounter == 0) ? stoppedWin : stoppedLost; // ���� ������� ���� ���
	}
	else
	{
		// ���� ���� �� �������� - ������� ���������� �� �����

		Render::BindFont("arial");
		Render::PrintString(10, 100, "SCORE: " + utils::lexical_cast(_g.score) +
									 "\nSHOT COUNT: " + utils::lexical_cast(_g.shotCounter) +
									 "\nBUBBLE COUNT: " + utils::lexical_cast(_g.bubbleCounter) +
									 "\nTIME: " + utils::lexical_cast(timerSec) + " sec" +
									 "\nSTATUS: " + ((timerSec != 0) ? " playing" : " stoped"),
 									 1.f, LeftAlign);
	}
}