#pragma once
using namespace std;

// ��������� ������
class SpriteParams
{
public:

	SpriteParams():
		width(0), height(0)
	{
	};

	string name; // ���
	FRect uvSize; // UV ���������� ������ � ������ 
	float width; // ������ ������� � ��������
	float height; // ������ ������� � ��������
};

// ����� ��������

class TextureAtlas
{
public:

	typedef map<string, SpriteParams> SpritesMap;

public:
	TextureAtlas();
	virtual ~TextureAtlas();

	// ������� ������ ������ �������� �� �����
	bool Open(string name);

	// ������� ���������� ������� - ��� � ������� � UV �����������
	void AddSprite(string spriteName, float leftBottomX, float rightTopX, float leftBottomY, float rightTopY, float width, float height);
	
	// ����� ������� spriteName �� ����� �� ����������� 
	bool DrawSprite(string spriteName, float leftBottomX, float leftBottomY, bool blend = false);

	// ��������� ���������� ������ spriteName
	SpriteParams* GetSpriteParams(string spriteName);

private:

	SpritesMap sprites; // ��� ��������
	Render::Texture* texture; // �������� ������ ��������
};