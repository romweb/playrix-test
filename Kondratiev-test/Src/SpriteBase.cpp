#include "stdafx.h"
#include "SpriteBase.h"

// ������� ����� ��� ��������. ��������� �� ������ � �����������

SpriteBase::SpriteBase():
	boundControlEnabled(false), tag(0), speedType(SpeedTypes::unknownSpeedType), targetType(TargetTypes::unknownTargetType),
	distanceFactor(0), noRenderer(true), needDestroy(false)
{
}

// �������������
void SpriteBase::Init(int id, SpriteParams* param)
{
	SpriteBase::id = id;
	SpriteBase::param = param;
}

SpriteBase::~SpriteBase()
{
}

// ������������� ������� �������
void SpriteBase::CorrectPosition(float factor)
{
	if (noRenderer)
	{
		position.x = factor;
		noRenderer = false;
	}
}

// ����������� ������� � ����������� �� ��� ����. ����� ����������� ���� ���������� ��������� ����.
void SpriteBase::Transform(float dt)
{
	if (!NeedDestroy()) // ���� ����� ��������� ���������� - �� ������ ����� �� ��� �����������
	{
		if (targetType != unknownTargetType)
		{
			float d = position.GetDistanceTo(target); // �������� ��������� ����� ������������ ���� � �������� ������������ �������
			if (position.GetDistanceTo(target) <= distanceFactor || (boundControlEnabled && !InBound(position))) // ���� ���������� ���� ��� ����� �� ������� ������...
				SetTarget(distanceFactor, targetType); // ...������������� ����� ����
		}

		// ������������ ���������� � ����������� �� ���� (����� ���� ������ ����������� �� ����������� ��� ���������)
		FPoint newPos;
		if (targetType != verticalTopTarget && targetType != verticalBottomTarget)
			newPos.x = math::lerp(position.x, target.x, dt * speedFactor);
		else
			newPos.x = position.x;

		if (targetType != horizontalTarget)
			newPos.y = math::lerp(position.y, target.y, dt * speedFactor);
		else
			newPos.y = position.y;

		position = newPos; // ����� ���������� ������
	}
}

// ��������� ���� ����������� �������
void SpriteBase::SetDestroyType(DestroyTypes type/* = unknownDestroyType*/)
{
	destroyType = type;
}

// �������� ����� ����������� �������
bool SpriteBase::NeedDestroy()
{
	return needDestroy;
}

// ��������� �������� ����������� ������� � � ���� (����� ���� ���������� ��� ���������)
void SpriteBase::SetSpeedFactor(float speed, SpeedTypes type/* = SpeedTypes::constantSpeed*/, float speed2/* = -1f*/)
{
	speedType = type; // ���������� ��� �������� �������

	// ����� �������� � ����������� �� ����
	switch (type)
	{
	case SpeedTypes::constantSpeed:
		speedFactor = speed;
		break;
	case SpeedTypes::randomSpeed:
		try
		{
			if (speed2 != -1.0)
				speedFactor = math::random(speed, speed2);
			else
				throw runtime_error(string("SetSpeedFactor incorrect initialize"));
		}
		catch(exception& e)
		{
			Log::log.WriteFatal(string(e.what()));
		}
		break;
	}
}

// ��������� ���� ����������� �������. ����� ���� ��������� ���� ����������� � ����� ������.
// distance - ���������, �� ���������� ������� ���� ��������� �����������  
bool SpriteBase::SetTarget(float distance, TargetTypes type/* = TargetTypes::randomInBoundTarget*/)
{
	bool res = false;

	distanceFactor = distance; // ���������� ���������  

	try
	{
		if (boundControlEnabled) // ����� ����� �������
		{
			switch (type)
			{
			case TargetTypes::verticalBottomTarget:
				if (targetType == unknownTargetType)
					// ������� - �� ������ ������ ������ (� ��������, ��� ������������ ������)
					target.y = uvBound.yStart - 200.0f;
				res = true;
				break;
			case TargetTypes::verticalTopTarget:
				if (targetType == unknownTargetType)
					// ������� - �� ������� ������ ������ (� ��������, ��� ������������ �������)
					target.y = uvBound.yEnd + 200.0f;
				res = true;
				break;
			case TargetTypes::horizontalTarget:
				// ������� - ����� ��� ������ ������� ������, ��������� (��� �������)
				if (target.x == uvBound.xStart || targetType == unknownTargetType)
					target.x = uvBound.xEnd;
				else 
					target.x = uvBound.xStart;
				res = true;
				break;
			case TargetTypes::randomInBoundTarget:
				// ��������� ������� (��� �����)
				target.x = math::random(uvBound.xStart, uvBound.xEnd);
				target.y = math::random(uvBound.yStart, uvBound.yEnd);
				res = true;
				break;
			}
		}
		else
			throw runtime_error(string("SetTarget incorrect initialize"));
	}
	catch(exception& e)
	{
		Log::log.WriteFatal(string(e.what()));
	}

	targetType = type; // ���������� ��� ���������� ����

	return res;
}

// ��������� �������� ������
void SpriteBase::EnableBoundControl(bool enable/* = true*/)
{
	boundControlEnabled = enable;
}

// ������� ���������� �������� ������
void SpriteBase::SetBoundControl(float leftBottomX, float rightTopX, float leftBottomY, float rightTopY)
{
	EnableBoundControl(true);
	uvBound = FRect(leftBottomX, rightTopX, leftBottomY, rightTopY);
}

// �������� ��������� ����� ������� � �������
bool SpriteBase::InBound(FPoint position)
{
	bool res = uvBound.Contains(position);
	
	if (!res && destroyType == destroyOutBound)
		needDestroy = true; // ���� ��� ������ � ��� destroyOutBound - ���������� ������!

	return res;
}

// �������� ��������� ����� � ������� ������� 
bool SpriteBase::InSpriteBound(FPoint pos)
{
	FRect bound = FRect(position.x, position.x + param->width, position.y, position.y + param->height);

	bool res = bound.Contains(pos);
	
	if (res)
		needDestroy = true;

	return res;
}