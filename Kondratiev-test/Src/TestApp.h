#pragma once

#include "BackgroundWidget.h"
#include "GameObjectsWidget.h"
#include "StatsWidget.h"

class TestApp : public Core::Application
{
public:
	TestApp(HINSTANCE hInstance, int nCmdShow, bool fullscreen)
		: Application(hInstance, nCmdShow, fullscreen)
	{
		GAME_CONTENT_WIDTH = 1024;
		GAME_CONTENT_HEIGHT = 768;
		enableCustomCursor = true;
	}
	
	void RegisterTypes()
	{
		Application::RegisterTypes();
		REGISTER_WIDGET_XML(BackgroundWidget, "BackgroundWidget");
		REGISTER_WIDGET_XML(GameObjectsWidget, "GameObjectsWidget");
		REGISTER_WIDGET_XML(StatsWidget, "StatsWidget");
	}
	
	void LoadResources()
	{
		Core::LuaExecuteStartupScript("start.lua");
	}
};