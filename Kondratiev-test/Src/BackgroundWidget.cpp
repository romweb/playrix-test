#include "stdafx.h"
#include "BackgroundWidget.h"

BackgroundWidget::BackgroundWidget(const std::string& name, rapidxml::xml_node<>* elem)
	: Widget(name),
	textureBackground(0)
{
	Init();
}

void BackgroundWidget::Init()
{
	// ���������� ��������� ������
	_g.screenWidth = Render::device.Width();
	_g.screenHeight = Render::device.Height();

	// ��������� ���
	textureBackground = Core::resourceManager.Get<Render::Texture>(TEXTURE_BACKGROUND);

	// � ����������� ������� ������
	MM::manager.PlayTrack(SOUND_BACKGROUND, true);
}

void BackgroundWidget::Draw() 
{
	if (textureBackground) // ������ ���
		textureBackground->Draw();
}