#include "stdafx.h"
#include "TextureAtlas.h"

// ����� ��������

TextureAtlas::TextureAtlas():
	texture(NULL)
{
}

TextureAtlas::~TextureAtlas()
{
}

// ������� ������ ������ �������� �� �����
bool TextureAtlas::Open(string name)
{
	texture = Core::resourceManager.Get<Render::Texture>("Sprites");
	return (texture != NULL);
}

// ������� ���������� ������� - ��� � ������� � UV �����������
void TextureAtlas::AddSprite(string spriteName, float leftBottomX, float rightTopX, float leftBottomY, float rightTopY, float width, float height)
{
	if (texture != NULL)
	{
		SpriteParams param;
		param.name = spriteName;
		param.uvSize = FRect(leftBottomX, rightTopX, leftBottomY, rightTopY);
		param.width = width;
		param.height = height;
		sprites.insert(SpritesMap::value_type(param.name, param));
	}
}

// ����� ������� spriteName �� ����� �� ����������� 
bool TextureAtlas::DrawSprite(string spriteName, float leftBottomX, float leftBottomY, bool blend/* = false*/)
{
	SpriteParams* param = GetSpriteParams(spriteName);
	if (texture != NULL && param != NULL)
	{
		texture->Draw(
			FRect(leftBottomX, leftBottomX + param->width, leftBottomY, leftBottomY + param->height),
			param->uvSize);

		if (blend)
		{
			Render::device.SetBlendMode(Render::ADD);
			texture->Draw(
				FRect(leftBottomX, leftBottomX + param->width, leftBottomY, leftBottomY + param->height),
				param->uvSize);
			Render::device.SetBlendMode(Render::ALPHA);
		}
	}

	return (param != NULL);
}

// ��������� ���������� ������ spriteName
SpriteParams* TextureAtlas::GetSpriteParams(string spriteName)
{
	SpriteParams* res = NULL;

	SpritesMap::iterator theIterator; 
	theIterator = sprites.find(spriteName);
	if (theIterator != sprites.end())
		res = &theIterator->second;

	return res;
}