#pragma once
#include "SpriteBase.h"

// ������ ��������

class SpriteShot: public SpriteBase
{
public:
	SpriteShot();
	virtual ~SpriteShot();

	// ������������� �������. ������������ ������� ������� shipPositionX.
	virtual void Init(int id, SpriteParams* param, float shipPositionX);
	// ����������� �������-�������� � �������
	virtual void Transform(float dt);

public:
	// ������ ������ ��������
	ParticleEffect* eff;
};